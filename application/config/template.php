<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Site Theme
|--------------------------------------------------------------------------
|
| Specify the name of the default site template view subfolder  
| application/modules/templates/views via using $config['jb_ttheme'] .
|  
| In the "regular" controller, call a view as follows:
|
|       $data['module'] = 'templates/NAME OF MODULE CONTAINING "REGULAR" HMVC CONTROLLER';
|       $data['view_file'] = 'NAME OF VIEW FILE IN HMVC MODULE';
|       echo Modules::run('templates', $data);
|
| To specify a different template to the one specified in this config file
| add the following before the last line of the above:
| 
|       $data['template'] = 'NAME OF THEME TO BE USED';
|
| Themes may have multiple views. Specify the default template file via $config['jb_default_template']
|
| Call a view using a template file other than the default by specifying 
| $data['template'] as follows:
|
|       $data['module'] = 'templates/NAME OF MODULE CONTAINING "REGULAR" HMVC CONTROLLER';
|       $data['view_file'] = 'NAME OF VIEW FILE IN HMVC MODULE';
|       $data['template_file'] = 'NAME OF ALTERNATIVE TEMPLATE FILE';
|       echo Modules::run('templates', $data);
|
| 
*/
$config['main_template']	= 'default';


/* End of file template.php */
/* Location: ./application/config/template.php */