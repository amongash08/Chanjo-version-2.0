<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth_Controller extends MX_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('users/mdl_user', 'user');
        $this->load->model('users/mdl_user_ext', 'user_ext');
        $this->load->model('users/mdl_location', 'location');
        $this->load->model('region/mdl_region', 'region');
        $this->load->model('county/mdl_county', 'county');
        $this->load->model('county/mdl_county_ext', 'county_ext');
        $this->load->model('subcounty/mdl_subcounty', 'subcounty');
        $this->load->model('subcounty/mdl_subcounty_ext', 'subcounty_ext');
        $this->load->model('facility/mdl_facility', 'facility');
        $this->load->model('vaccine/mdl_vaccine', 'vaccines');
        $this->load->model('vaccine/mdl_vvm', 'vvm');
    }

    public function user_info($id = false)
    {
        if ($this->ion_auth->user()->row()) {
            $id || $id = $this->ion_auth->user()->row()->id;
        }

        $query = $this->user_ext->get_user_info($id);
        $typecast_query = (array)$query;
        if ($query) {
            if ($query->level_id == 4) {
                $location_nation_id = $this->location_by_level(1);
                $location_region_id = $this->location_by_level(2);
                $location_county_id = $this->location_by_level(3);
                $location_subcounty_id = $this->location_by_level(4);
                foreach ($typecast_query as $key => $value) {
                    $typecast_query['location_nation_id'] = $location_nation_id->id;
                    $typecast_query['location_region_id'] = $location_region_id->id;
                    $typecast_query['location_county_id'] = $location_county_id->id;
                    $typecast_query['location_subcounty_id'] = $location_subcounty_id->id;
                }

            } else if ($query->level_id == 3) {
                $location_nation_id = $this->location_by_level(1);
                $location_region_id = $this->location_by_level(2);
                $location_county_id = $this->location_by_level(3);
                foreach ($typecast_query as $key => $value) {
                    $typecast_query['location_nation_id'] = $location_nation_id->id;
                    $typecast_query['location_region_id'] = $location_region_id->id;
                    $typecast_query['location_county_id'] = $location_county_id->id;
                    $typecast_query['location_county_id'] = null;
                    $typecast_query['location_subcounty_id'] = null;
                }

            } else if ($query->level_id == 2) {
                $location_nation_id = $this->location_by_level(1);
                $location_region_id = $this->location_by_level(2);
                foreach ($typecast_query as $key => $value) {
                    $typecast_query['location_nation_id'] = $location_nation_id->id;
                    $typecast_query['location_region_id'] = $location_region_id->id;
                    $typecast_query['location_county_id'] = null;
                    $typecast_query['location_subcounty_id'] = null;
                }

            } elseif ($query->level_id == 1) {
                $location_nation_id = $this->location_by_level(1);
                foreach ($typecast_query as $key => $value) {
                    $typecast_query['location_nation_id'] = $location_nation_id->id;
                    $typecast_query['location_region_id'] = null;
                    $typecast_query['location_county_id'] = null;
                    $typecast_query['location_subcounty_id'] = null;
                }

            }
            return (object)$typecast_query;

        }
        return;
    }

    public function location_by_level($id)
    {
        $user_id = $this->ion_auth->user()->row()->id;
        $query = $this->user_ext->get_user_info($user_id);
        $data = [
            'national' => (int)$query->nation_id,
            'region' => (int)$query->region_id,
            'county' => (int)$query->county_id,
            'subcounty' => (int)$query->subcounty_id,
            'facility' => 0,
        ];

        if ($id == 1) {
            foreach ($data as $key => $value) {
                if ($key !== 'national') {
                    $data[$key] = 0;

                }
            }
            $query = $this->user_ext->get_location($data);
        } elseif ($id == 2) {
            foreach ($data as $key => $value) {
                if ($key !== 'national' && $key !== 'region') {
                    $data[$key] = 0;

                }
            }
            $query = $this->user_ext->get_location($data);
        } elseif ($id == 3) {
            foreach ($data as $key => $value) {
                if ($key == 'subcounty') {
                    $data[$key] = 0;

                }
            }
            $query = $this->user_ext->get_location($data);
        } elseif ($id == 4) {
            $query = $this->user_ext->get_location($data);
        }

        if ($query) {
            return $query;
        }
        return;
    }

    public function location_name($id)
    {
        $query = $this->user_ext->get_location_name($id);

        if ($query) {
            $filter = count(array_filter((array)$query));
            $location = false;
            if ($filter == 1) {
                $location = $query->nation;
            } elseif ($filter == 2) {
                $location = $query->region;
            } elseif ($filter == 3) {
                $location = $query->county;
            } elseif ($filter == 4) {
                $location = $query->subcounty;
            } elseif ($filter == 5) {
                $location = $query->facility;
            }
            return $location;
        }
        return;
    }

    public function location_id($data = array())
    {
        $query = $this->user_ext->get_location($data);
        if ($query) {
            return $query->id;
        }
        return;
    }


    public function get_county_by_region($region_id = false)
    {
        if (isset($_POST) && !empty($_POST)) {
            if ($this->input->post('id') && is_numeric($this->input->post('id'))) {
                $id = $this->input->post('id');
                $data = $this->county->get_many_by('region_id', $id);

                if (!is_null($data)) {
                    echo json_encode($data);
                }
            }
        } else {
            $id = $region_id;
            $data = $this->county->get_many_by('region_id', $id);

            if (!is_null($data)) {
                return $data;
            }

        }
    }

    public function get_county_by_region_dropdown($region_id = false)
    {
        if ($region_id && is_numeric($region_id)) {
            $id = $region_id;
            $data = $this->county_ext->get_by_field_dropdown('region_id', $id);

            if (!is_null($data)) {
                return $data;
            }
        }
    }

    public function get_subcounty_by_county($county_id = false)
    {
        if (isset($_POST) && !empty($_POST)) {
            if ($this->input->post('id') && is_numeric($this->input->post('id'))) {
                $id = $this->input->post('id');
                $data = $this->subcounty->get_many_by('county_id', $id);

                if (!is_null($data)) {
                    echo json_encode($data);
                }
            }
        } else {
            $id = $county_id;
            $data = $this->subcounty->get_many_by('county_id', $id);

            if (!is_null($data)) {
                return $data;
            }
        }
    }

    public function get_subcounty_by_county_dropdown($county_id = false)
    {
        if ($county_id && is_numeric($county_id)) {
            $id = $county_id;
            $data = $this->subcounty_ext->get_by_field_dropdown('county_id', $id);

            if (!is_null($data)) {
                return $data;
            }
        }
    }

    public function vaccine_dropdown()
    {
        $vaccine_array = $this->vaccines->dropdown('vaccine_name');
        foreach ($vaccine_array as $key => $value){
            if($key == '7' || $key == '11'){
                unset($vaccine_array[$key]);
            }
        }
        asort($vaccine_array);
        return $vaccine_array;
    }


    public function recalculate($vaccine_id)
    {
        $user_info = $this->user_info();
        $location_id = $user_info->location_id;
        $user_id = $user_info->id;

        $con = [
            'host' => $this->db->hostname,
            'port' => 3306,
            'database' => $this->db->database,
            'user' => $this->db->username,
            'password' => $this->db->password,
        ];
        $con = json_encode($con);

        system("python " . APPPATH . "../scripts/recalculate.py '$con' '$user_id' '$location_id' '$vaccine_id' 2>&1");

    }

    public function stock_data($vaccine_id, $location_id)
    {

        $con = [
            'host' => $this->db->hostname,
            'port' => 3306,
            'database' => $this->db->database,
            'user' => $this->db->username,
            'password' => $this->db->password,
        ];
        $con = json_encode($con);

        system("python " . APPPATH . "../scripts/stock_data.py '$con' '$location_id' '$vaccine_id' 2>&1");
    }

    public function update_location_name()
    {
        $con = [
            'host' => $this->db->hostname,
            'port' => 3306,
            'database' => $this->db->database,
            'user' => $this->db->username,
            'password' => $this->db->password,
        ];
        $con = json_encode($con);

        system("python " . APPPATH . "../scripts/generate_location_name.py '$con'");
    }

    public function add_location_fields()
    {
        $con = [
            'host' => $this->db->hostname,
            'port' => 3306,
            'database' => $this->db->database,
            'user' => $this->db->username,
            'password' => $this->db->password,
        ];
        $con = json_encode($con);

        system("python " . APPPATH . "../scripts/add_location_fields.py '$con' 2>&1");
    }

}