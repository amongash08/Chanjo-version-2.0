<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Communication extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();
        if (!$this->ion_auth->logged_in()) {
            //redirect them to the login page
            redirect('users');
        }
    }

    public function index()
    {

        $data['title'] = 'Messages';
        $data['page_title'] = 'Messages';
        $data['subtitle'] = 'Communication';
        $data['module'] = 'communication';
        $data['view_file'] = 'messages';

        echo Modules::run('templates', $data);

    }

    public function send_mail()
    {
        date_default_timezone_set('Africa/Nairobi');
        $this->email->from('info@epikenya.org', 'Chanjo eLMIS');
        $this->email->to('amongash08@gmail.com');
        $this->email->subject('Test Email');
        $this->email->message('Test');
        if (!$this->email->send()) {
            echo $this->email->print_debugger();
        } else {
            echo "Success";
        }
    }

    public function send_monthly_report()
    {
        date_default_timezone_set('Africa/Nairobi');
        $this->email->from('info@epikenya.org', 'Chanjo eLMIS');
        $this->email->to('amongash08@gmail.com');
        $this->email->subject('CHANJO elMIS: Monthly report for period ' . date('M Y'));
        $data = [];
        $body = $this->load->view('communication/monthly_report', $data, TRUE);
        $this->email->message($body);
        if (!$this->email->send()) {
            echo $this->email->print_debugger();
        } else {
            echo "Success";
        }
    }

}