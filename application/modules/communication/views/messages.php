<!-- Tagsinput -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/tagsinput/dist/bootstrap-tagsinput.css">
<script src="<?php echo base_url(); ?>assets/plugins/tagsinput/dist/bootstrap-tagsinput.min.js"></script>
<div class="row">
    <div class="col-sm-12">
        <!--        <div class="box box-solid">-->
        <!--            <div class="box-body">-->
        <!--                <div class="col-sm-2">-->
        <!--                    <button class="btn btn-block btn-primary btn-flat" type="button">Insert Contacts</button>-->
        <!--                </div>-->
        <!--                <div class="col-sm-2">-->
        <!--                    <button class="btn btn-block btn-primary btn-flat" type="button">Send Sms</button>-->
        <!--                </div>-->
        <!--                <div class="col-sm-2">-->
        <!--                    <button class="btn btn-block btn-primary btn-flat" type="button">Send Custom Sms</button>-->
        <!--                </div>-->
        <!--                <div class="col-sm-2">-->
        <!--                    <button class="btn btn-block btn-primary btn-flat" type="button">Send Email</button>-->
        <!--                </div>-->
        <!--            </div>-->
        <!--        </div>-->
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-navy"><i class="fa fa-envelope-o"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Messages Sent</span>
                    <span class="info-box-number">0</span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>

        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-info"><i class="fa fa-users"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Total Contacts</span>
                    <span class="info-box-number"></span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>

        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-green"><i class="fa fa-share"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Received Messages</span>
                    <span class="info-box-number"></span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>

        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-red"><i class="fa fa-reply"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Failed Messages</span>
                    <span class="info-box-number"></span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <div class="box box-solid">
            <div class="box-body">
                <div class="col-md-8 well well-sm">
                    <form>
                        <div>
                            <div class="col-md-6">
                                <div class="form-group-sm">
                                    <label for="contact">
                                        Contacts</label>
                                    <div><input data-role="tagsinput" class="form-control">
                                    </div>
                                </div>
                                <br>
                                <div class="btn-group-vertical">
                                    <button type="button" class="btn btn-primary" style="width: 200px"
                                            data-toggle="modal" data-target="#subcounty-modal">Import Subcounty Contacts
                                    </button>
                                    <button type="button" class="btn btn-primary" style="width: 200px"
                                            data-toggle="modal" data-target="#county-modal">Import County Contacts
                                    </button>
                                    <button type="button" class="btn btn-primary" style="width: 200px"
                                            data-toggle="modal" data-target="#region-modal">Import Region Contacts
                                    </button>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group-sm">
                                    <label for="subject">
                                        Subject</label>
                                    <div>
                                        <input type="text" class="form-control" id="subject" placeholder="Enter Subject"
                                        /></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group-sm">
                                    <label for="name">
                                        Message</label>
                                    <textarea name="message" id="message" class="form-control" rows="9" cols="25"

                                              placeholder="Message"></textarea>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <button type="button" class="btn btn-primary pull-right margin" id="send">
                                    Send Message
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-md-4">
                    <form>
                        <legend><span class="glyphicon glyphicon-piggy-bank">&nbsp;</span>Your Balance:</legend>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="submit-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-label="Close" data-dismiss="modal" class="close" type="button">
                    <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Confirm Form Submission</h4>
            </div>
            <div class="modal-body">
                <p>Are you sure you want to send this message?</p><br>
                <pre id="display"></pre>
            </div>
            <div class="modal-footer">
                <button data-dismiss="modal" class="btn btn-default pull-left" type="button">Close</button>
                <button type="button" name="submit" id="submit" class="btn btn-sm btn-danger">
                    <i class="fa fa-paper-plane"></i>&nbsp;Submit
                </button>

            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<div class="modal fade" id="subcounty-modal">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-label="Close" data-dismiss="modal" class="close" type="button">
                    <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Import Subcounty Contacts</h4>
            </div>
            <div class="modal-body">
                <img style='margin:20% 40% 20% ;' src="<?php echo site_url('assets/images/ring.gif'); ?>">
            </div>
            <div class="modal-footer">
                <button data-dismiss="modal" class="btn btn-default pull-left" type="button">Close</button>
                <button type="button" name="" id="" class="btn btn-sm btn-danger">
                    <i class="fa fa-paper-plane"></i>&nbsp;Done
                </button>

            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<div class="modal fade" id="county-modal">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-label="Close" data-dismiss="modal" class="close" type="button">
                    <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Import County Contacts</h4>
            </div>
            <div class="modal-body">
                <img style='margin:20% 40% 20% ;' src="<?php echo site_url('assets/images/ring.gif'); ?>">
            </div>
            <div class="modal-footer">
                <button data-dismiss="modal" class="btn btn-default pull-left" type="button">Close</button>
                <button type="button" name="" id="" class="btn btn-sm btn-danger">
                    <i class="fa fa-paper-plane"></i>&nbsp;Done
                </button>

            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->


<div class="modal fade" id="region-modal">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-label="Close" data-dismiss="modal" class="close" type="button">
                    <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Import Region Contacts</h4>
            </div>
            <div class="modal-body">
                <img style='margin:20% 40% 20% ;' src="<?php echo site_url('assets/images/ring.gif'); ?>">
            </div>
            <div class="modal-footer">
                <button data-dismiss="modal" class="btn btn-default pull-left" type="button">Close</button>
                <button type="button" name="" id="" class="btn btn-sm btn-danger">
                    <i class="fa fa-paper-plane"></i>&nbsp;Done
                </button>

            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<script>
    $(document).ready(function () {
        $('#send').on('click', function () {
            subject = $('#subject').val();
            message = $('#message').val();
            $('#submit-modal').modal('show');
            $('#display').text(subject + ': ' + message);
        });

    });
</script>