<?php defined('BASEPATH') OR exit('No direct script access allowed');

class County extends MX_Controller
{
    function __construct()
    {
        parent::__construct();
        if (!$this->ion_auth->logged_in()) {
            //redirect them to the login page
            redirect('users');
        }

        $this->load->model('mdl_county');
        $this->load->model('mdl_county_ext');

    }

    public function index()
    {
        $data['title'] = 'View Counties';
        $data['page_title'] = 'County';
        $data['subtitle'] = 'View Counties';
        $data['module'] = 'county';
        if (!$this->ion_auth->is_admin()) {
            $data['view_file'] = '404';
        } else {
            $data['view_file'] = 'county/index';

        }

        echo Modules::run('templates', $data);
    }

    function edit($id = null)
    {
        $data['title'] = "Edit County";
        $data['subtitle'] = 'Edit County';
        $data['module'] = 'county';
        $data['view_file'] = 'county/edit';
        $data['id'] = (is_numeric($id)) ? $id : null;

        if (!$this->ion_auth->logged_in() || (!$this->ion_auth->is_admin())) {
            redirect('users', 'refresh');
        }

        $selected_county = $this->mdl_county->get($id);

        //validate form input
        $this->form_validation->set_rules('women_population', 'Women Population', 'required');
        $this->form_validation->set_rules('under_one_population', 'Under One Population', 'required');
        $this->form_validation->set_rules('total_population', 'Total Population', 'required');

        if (isset($_POST) && !empty($_POST)) {
            // do we have a valid request?
            if ($id !== $this->input->post('id')) {
                $this->session->set_flashdata('error_message', 'An error has occurred.');
                redirect('county', 'refresh');
            }

            if ($this->form_validation->run() === TRUE) {

                $data = array(
                    'women_population' => $this->input->post('women_population'),
                    'under_one_population' => $this->input->post('under_one_population'),
                    'total_population' => $this->input->post('total_population'),
                );


                //check to see if we are updating the user
                if ($this->mdl_county->update($selected_county->id, $data, TRUE)) {

                    //redirect them back to the admin page if admin, or to the base url if non admin
                    $this->session->set_flashdata('success_message', 'County updated successfully.');
                    if ($this->ion_auth->is_admin()) {
                        redirect('county', 'refresh');
                    } else {
                        redirect('/', 'refresh');
                    }

                } else {
                    //redirect them back to the admin page if admin, or to the base url if non admin
                    $this->session->set_flashdata('error_message', 'An error has occurred.');
                    if ($this->ion_auth->is_admin()) {
                        redirect('county', 'refresh');
                    } else {
                        redirect('/', 'refresh');
                    }

                }

            }
        }

        //pass the county to the view
        $data['selected_county'] = $selected_county;

        if (!is_null($selected_county)) {
            $data['county_name'] = array(
                'name' => 'county_name',
                'id' => 'county_name',
                'type' => 'text',
                'class' => 'form-control',
                'disabled' => '',
                'value' => $this->form_validation->set_value('county_name', $selected_county->county_name),
            );
            $data['women_population'] = array(
                'name' => 'women_population',
                'id' => 'women_population',
                'type' => 'text',
                'class' => 'form-control',
                'value' => $this->form_validation->set_value('women_population', $selected_county->women_population),
            );
            $data['under_one_population'] = array(
                'name' => 'under_one_population',
                'id' => 'under_one_population',
                'type' => 'text',
                'class' => 'form-control',
                'value' => $this->form_validation->set_value('under_one_population', $selected_county->under_one_population),
            );
            $data['total_population'] = array(
                'name' => 'total_population',
                'id' => 'total_population',
                'type' => 'text',
                'class' => 'form-control',
                'value' => $this->form_validation->set_value('total_population', $selected_county->total_population),
            );
        }

        echo Modules::run('templates', $data);
    }

    public function county_data()
    {
        //retrieve county data array
        $counties = $this->mdl_county->get_all();
        $output['data'] = $counties;
        echo json_encode($output);
    }

    public function county_dropdown()
    {
        $counties = $this->mdl_county->county_location();
        asort($counties);
        return $counties;
    }

    /** Get county by region */
    public function get_by_region($region_id = false)
    {
        if (isset($_POST) && !empty($_POST)) {
            if ($this->input->post('id') && is_numeric($this->input->post('id'))) {
                $id = $this->input->post('id');
                $data = $this->mdl_county->get_many_by('region_id', $id);

                if (!is_null($data)) {
                    echo json_encode($data);
                }
            }
        } else {
            $id = $region_id;
            $data = $this->mdl_county->get_many_by('region_id', $id);

            if (!is_null($data)) {
                return $data;
            }

        }
    }

    public function get_by_region_dropdown($region_id = false)
    {
        if ($region_id && is_numeric($region_id)) {
            $id = $region_id;
            $data = $this->mdl_county_ext->get_by_field_dropdown('region_id', $id);

            if (!is_null($data)) {
                return $data;
            }
        }
    }

    public function getcountiesjson($region_id)
    {
        echo json_encode($this->mdl_county->get_by_region($region_id), true);
    }
}