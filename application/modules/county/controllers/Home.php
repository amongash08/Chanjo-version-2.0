<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();
        if (!$this->ion_auth->logged_in()) {
            //redirect them to the login page
            redirect('users');
        }
        $this->load->module('users');
        $this->load->module('vaccine');
        $this->load->module('subcounty');
    }

    public function index()
    {

        $data['title'] = 'Home';
        $data['page_title'] = 'Home';
        $data['subtitle'] = 'Dashboard';
        $data['module'] = 'county';
        $data['view_file'] = 'home/dashboard';
        $data['vaccines'] = $this->vaccine->vaccine_dropdown();
        $user_info = $this->users->user_info();

        $subcounties = $this->subcounty->get_by_county_dropdown($user_info->county_id);

        $data['subcounties'] = $subcounties;
        echo Modules::run('templates', $data);

    }
}
