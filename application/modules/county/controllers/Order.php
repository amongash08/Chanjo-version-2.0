<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Order extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->module('users');
        $this->load->module('subcounty');
        $this->load->model('order/mdl_order');
        $this->load->model('user/mdl_user_ext', 'user_ext');
    }

    public function get_orders()
    {
        $data = null;
        $user_info = $this->users->user_info();
        $location_data = array(
            'national' => 1,
            'region' => (int)$user_info->region_id,
            'county' => 0,
            'subcounty' => 0,
            'facility' => 0,
        );
        $location = $this->mdl_user_ext->get_location($location_data);
        $other_orders = $this->mdl_order->get_where(array('to_from' => $location->id));
        $user_id = null;
        $format_other_orders = null;

        if ($other_orders) {
            foreach ($other_orders as $key => $value) {
                $user_id = $value->user_id;
            }
            $info = $this->mdl_user_ext->get_user_info($user_id);
            if ($info) {
                if ($info->level_id == '2') {
                    $from = $info->region;
                } elseif ($info->level_id == '3') {
                    $from = $info->county;
                } elseif ($info->level_id == '4') {
                    $from = $info->subcounty;
                }
                foreach ($other_orders as $key => $value) {
                    $data = [
                        'id' => $value->id,
                        'location_id' => $value->location_id,
                        'user_id' => $value->user_id,
                        'pickup_date' => $value->pickup_date,
                        'status' => $value->status,
                        'timestamp' => $value->timestamp,
                        'from' => $from,
                    ];
                    $format_other_orders[] = (object)$data;
                }

            }
        }

        $data = [
            'other_orders' => $format_other_orders,
            'user_info' => $user_info
        ];

        $this->load->view('county/order/order', $data);

    }



}