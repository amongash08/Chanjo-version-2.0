<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Reports extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        if (!$this->ion_auth->logged_in()) {
            //redirect them to the login page
            redirect('users');
        }
        $this->load->module('users');
        $this->load->module('vaccine');
        $this->load->module('subcounty');
        $this->load->model('users/mdl_user_ext');
        $this->load->model('reports/mdl_reports');

    }

    public function supply_chain()
    {
        $data['title'] = 'Reports';
        $data['module'] = 'county';
        $data['view_file'] = 'reports/supply_chain';
        $data['subtitle'] = 'Supply Chain';
        $user_info = $this->users->user_info();

        $subcounties = $this->subcounty->get_by_county_dropdown($user_info->county_id);
        foreach ($subcounties as $key => $value) {
            $location_data = array(
                'national' => (int)'1',
                'region' => (int)$user_info->region_id,
                'county' => (int)$user_info->county_id,
                'subcounty' => (int)$key,
                'facility' => 0,
            );
            $location = $this->mdl_user_ext->get_location($location_data);
            if ($location) {
                unset($subcounties[$key]);
                $key = $location->id;
                $subcounties[$key] = $value;
            }
        }
        $data['subcounties'] = $subcounties;
        $data['vaccines'] = $this->vaccine->vaccine_dropdown();

        echo Modules::run('templates', $data);

    }

    public function cold_chain()
    {
        $data['module'] = 'national';
        $data['view_file'] = 'reports/cold_chain';
        $data['title'] = 'Reports';
        $data['subtitle'] = 'Cold Chain';
        echo Modules::run('templates', $data);

    }

    public function program_management()
    {
        $data['module'] = 'national';
        $data['view_file'] = 'reports/program_management';
        $data['title'] = 'Reports';
        $data['subtitle'] = 'Program management';
        echo Modules::run('templates', $data);

    }

    public function general_admin()
    {

        $data['module'] = 'national';
        $data['view_file'] = 'reports/general_admin';
        $data['title'] = 'Reports';
        $data['subtitle'] = 'General administration';

        echo Modules::run('templates', $data);

    }

    public function table_stock_level()
    {
        $user_info = $this->users->user_info();
        if ($user_info->county_id) {
            $data['result'] = $this->mdl_reports->get_subcounty_stock_level_by_county($user_info->county_id);
            $view = 'reports/balances_pivot_table';
            $this->load->view($view, $data);
        } else {
            echo '<div style="margin:5%;font-size:2em;font-weight:400;"> &#128559;Please contact your administrator. </div>';
            exit;
        }

    }

    public function table_last_updated()
    {
        $user_info = $this->users->user_info();
        if ($user_info->county_id) {
            $data['result'] = $this->mdl_reports->get_subcounty_last_updated_by_county($user_info->county_id);
            $view = 'reports/last_updated_pivot_table';
            $this->load->view($view, $data);
        } else {
            echo '<div style="margin:5%;font-size:2em;font-weight:400;"> &#128559;Please contact your administrator. </div>';
            exit;
        }

    }

    public function plot_coverage()
    {
        $user_info = $this->users->user_info();

        if ($user_info->county_id && $this->input->post()) {
            $location = $this->input->post('location');
            $period = $this->input->post('period');
            $where = [
                'subcounty_id' => $location,
                'date_format(period, "%Y-%m")=' => $period
            ];

            $result = $this->mdl_reports->get_subcounty_coverage_by_county($where);
            $result = json_decode(json_encode($result), true);
            if ($result) {
                foreach ($result as $key => $value) {

                    $subcounty = $value['subcounty_name'];
                    $series_data[] = [
                        'name' => $subcounty,
                        'data' => [
                            (float)number_format((float)$value['bcg'], 1, '.', ''),
                            (float)number_format((float)$value['dpt1'], 2, '.', ''),
                            (float)number_format((float)$value['dpt2'], 2, '.', ''),
                            (float)number_format((float)$value['dpt3'], 2, '.', ''),
                            (float)number_format((float)$value['measles1'], 2, '.', ''),
                            (float)number_format((float)$value['measles2'], 2, '.', ''),
                            (float)number_format((float)$value['measles3'], 2, '.', ''),
                            (float)number_format((float)$value['opv'], 2, '.', ''),
                            (float)number_format((float)$value['opv1'], 2, '.', ''),
                            (float)number_format((float)$value['opv2'], 2, '.', ''),
                            (float)number_format((float)$value['opv3'], 2, '.', ''),
                            (float)number_format((float)$value['pcv1'], 2, '.', ''),
                            (float)number_format((float)$value['pcv2'], 2, '.', ''),
                            (float)number_format((float)$value['pcv3'], 2, '.', ''),
                            (float)number_format((float)$value['rota1'], 2, '.', ''),
                            (float)number_format((float)$value['rota2'], 2, '.', ''),
                        ]
                    ];

                }

                $category_data = [
                    'BCG',
                    'DPT1',
                    'DPT2',
                    'DPT3',
                    'MR1',
                    'MR2',
                    'MR3',
                    'OPV',
                    'OPV1',
                    'OPV2',
                    'OPV3',
                    'PCV1',
                    'PCV2',
                    'PCV3',
                    'ROTA1',
                    'ROTA2'
                ];
                $data['graph_type'] = 'line';
                $data['graph_title'] = $subcounty . ' Coverage';
                $data['series_data'] = json_encode($series_data);
                $data['category_data'] = json_encode($category_data);
                $this->load->view('reports/coverage', $data);
            } else {
                echo '<div style="margin:5%;font-size:2em;font-weight:400;"> &#128559;No data for the selected filters! </div>';
                exit;
            }

        } else {
            echo '<div style="margin:5%;font-size:2em;font-weight:400;"> &#128559;No data for the selected filters! </div>';
            exit;
        }
    }

    public function plot_coverage_over_time()
    {
        $user_info = $this->users->user_info();

        if ($user_info->county_id && $this->input->post()) {
            $select = $this->input->post('vaccine');

            $where = [
                'county_id' => $user_info->county_id,
            ];

            $result = $this->mdl_reports->get_coverage($select, $where);
            $result = json_decode(json_encode($result), true);

            foreach ($result as $key => $value) {
                $subcounty = $value['subcounty_name'];
                $location_data[] = [
                    'name' => $subcounty,
                    'data' => [
                        number_format((float)$value[$select], 2, '.', ''),
                    ]
                ];

                $period = strtotime($value['period']);
                $category_data[] = date('Y-M', ($period));

            }
            $subcounties = $this->subcounty->get_by_county_dropdown($user_info->county_id);

            foreach ($subcounties as $subcounty_id => $subcounty_name) {
                foreach ($location_data as $key => $value) {
                    if ((string)$value['name'] === (string)$subcounty_name) {
                        $series_data[$subcounty_name][] = (float)$value['data'];
                    }
                }
            }

            foreach ($series_data as $key => $value) {
                $new_series_data[] = [
                    'name' => $key,
                    'data' => $value
                ];
            }
            $category_data = array_unique($category_data);

            foreach ($category_data as $key => $value) {
                $new_category_data[] = [$value];
            }
            $data['graph_type'] = 'line';
            $data['graph_title'] = strtoupper($select) . ' Coverage';
            $data['yaxis_title'] = strtoupper($select) . ' Coverage';
            $data['series_data'] = json_encode($new_series_data);
            $data['category_data'] = json_encode($new_category_data);
            $this->load->view('reports/coverage_over_time', $data);
        } else {
            echo '<div style="margin:5%;font-size:2em;font-weight:400;"> &#128559;No data for the selected filters! </div>';
            exit;
        }
    }

    public function plot_dpt_dropout()
    {
        $user_info = $this->users->user_info();

        if ($user_info->county_id) {

            $where = [
                'county_id' => $user_info->county_id,
            ];

            $result = $this->mdl_reports->get_subcounty_coverage_by_county($where);
            $result = json_decode(json_encode($result), true);

            foreach ($result as $key => $value) {
                $subcounty = $value['subcounty_name'];
                if ((float)$value['dpt1'] > 0 && (float)$value['dpt3'] > 0) {
                    $dropout = (($value['dpt1'] - $value['dpt3']) / $value['dpt1']) * 100;
                } else {
                    $dropout = 0;
                }

                $location_data[] = [
                    'name' => $subcounty,
                    'dropout' => $dropout
                ];
                $period = strtotime($value['period']);
                $category_data[] = date('Y-M', ($period));

            }
            $subcounties = $this->subcounty->get_by_county_dropdown($user_info->county_id);

            foreach ($subcounties as $subcounty_id => $subcounty_name) {
                foreach ($location_data as $key => $value) {
                    if ((string)$value['name'] === (string)$subcounty_name) {
                        $series_data[$subcounty_name][] = (float)number_format((float)$value['dropout'], 2, '.', '');

                    }
                }
            }

            foreach ($series_data as $key => $value) {
                $new_series_data[] = [
                    'name' => $key,
                    'data' => $value
                ];
            }
            $category_data = array_unique($category_data);

            foreach ($category_data as $key => $value) {
                $new_category_data[] = [$value];
            }
            $data['graph_type'] = 'line';
            $data['graph_title'] = 'DPT Dropout Rate(%)';
            $data['series_data'] = json_encode($new_series_data);
            $data['category_data'] = json_encode($new_category_data);
            $this->load->view('reports/dropout', $data);
        } else {
            echo '<div style="margin:5%;font-size:2em;font-weight:400;"> &#128559;You are not authorized to view this content! </div>';
            exit;
        }
    }

}