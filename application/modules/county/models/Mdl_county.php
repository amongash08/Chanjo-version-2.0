<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mdl_county extends MY_Model
{

    public $_table = 'counties';

    public function __construct()
    {
        parent::__construct();
    }

    public function get_table()
    {
        $table = 'counties';

        return $table;
    }

    public function get_all()
    {
        $table = $this->get_table();
        $this->db->order_by('county_name', 'asc');
        $query = $this->db->get($table);

        return $query->result();
    }

    public function get_by_region($region_id)
    {
        $this->db->select('id,county_name,region_id');
        $this->db->from('counties');
        $this->db->where('region_id', $region_id);
        $query = $this->db->get();
        return $query->result();
    }

    public function get_population($station)
    {
        $this->db->select('under_one_population as population');
        $this->db->from('counties');
        $this->db->where('id', $station);
        $this->db->limit('1');
        $query = $this->db->get();
        return $query->result();
    }

    public function county_location()
    {
        $query = $this->db->select('
                    locations.id,
                    region_name as region,
                    county_name as county,
                    subcounty_name as subcounty
                    ')
            ->from('locations')
            ->join('regions', 'regions.id = JSON_UNQUOTE(JSON_EXTRACT(locations.json_location, "$.region"))', 'left')
            ->join('counties', 'counties.id = JSON_UNQUOTE(JSON_EXTRACT(locations.json_location, "$.county"))', 'left')
            ->join('subcounties', 'subcounties.id = JSON_UNQUOTE(JSON_EXTRACT(locations.json_location, "$.subcounty"))', 'left')
            ->get();
        $location = array();
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $filter = count(array_filter((array)$row));
                if ($filter == 3) {
                    $location[$row->id] = $row->county;
                }
            }
            return $location;
        }
        return false;
    }


}