<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mdl_county_ext extends CI_Model
{

    public $_table = 'counties';

    public function __construct()
    {
        parent::__construct();
    }

    public function get_by_field_dropdown($field, $id)
    {
        $query = $this->db->select('id, county_name')
            ->where($field, $id)
            ->order_by('county_name', 'asc')
            ->get($this->_table);
        $options = array();
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $options[$row->id] = $row->county_name;
            }
            return $options;
        }
        return false;
    }


}