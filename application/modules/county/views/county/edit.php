<div class="box">
    <div class="box-header with-border">
        <h3>County Details</h3>
    </div>

    <div class="box-body col-sm-offset-2">
        <?php echo form_open('county/edit/'.$id, array('class' => 'form-horizontal')); ?>

        <div class="form-group">
            <label class="col-sm-2 control-label" for="county_name">County Name</label>
            <div class="col-sm-5">
                <?php echo form_input($county_name); ?>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label" for="women_population">Women Population</label>
            <div class="col-sm-5">
                <?php echo form_input($women_population); ?>
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-2 control-label" for="women_population">Under One Population</label>
            <div class="col-sm-5">
                <?php echo form_input($under_one_population); ?>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label" for="total_population">Total Population</label>
            <div class="col-sm-5">
                <?php echo form_input($total_population); ?>
            </div>
        </div>


        <?php echo form_hidden('id', $selected_county->id); ?>
    </div>

    <!-- /.box-body -->
    <div class="box-footer">
        <!-- buttons -->
        <?php echo form_submit('submit', 'Submit', array('class' => 'btn bg-navy margin')); ?>
    </div>
    <!-- /.box-footer -->
    <?php echo form_close(); ?>
</div>
<!-- /.box -->