<link href="<?php echo base_url(); ?>assets/plugins/jquery-month-picker/MonthPicker.css" rel="stylesheet"
      type="text/css"/>
<script src="<?php echo base_url(); ?>assets/plugins/jquery-month-picker/MonthPicker.js"></script>
<div class="row">
    <section class="col-md-12">

        <div class="box box-info">
            <div class="box-header with-border">
                <i class="fa fa-clock-o"></i>

                <h3 class="box-title">
                    Coverage
                </h3>
                <div class="form-group">
                    <div class="col-sm-4">
                        <select class="form-control" id="vaccine">
                            <option data-name='NULL'>Select Antigen</option>
                            <option data-value='bcg'>BCG</option>
                            <option data-value='dpt1'>DPT1</option>
                            <option data-value='dpt2'>DPT2</option>
                            <option data-value='dpt3'>DPT3</option>
                            <option data-value='measles1'>MEASLES1</option>
                            <option data-value='measles2'>MEASLES2</option>
                            <option data-value='opv'>OPV</option>
                            <option data-value='opv1'>OPV1</option>
                            <option data-value='opv2'>OPV2</option>
                            <option data-value='opv3'>OPV3</option>
                            <option data-value='pcv1'>PCV1</option>
                            <option data-value='pcv2'>PCV2</option>
                            <option data-value='pcv3'>PCV3</option>
                            <option data-value='rota1'>ROTA1</option>
                            <option data-value='rota2'>ROTA2</option>

                        </select>
                    </div>

                    <div class="col-sm-2">
                        <span class="input-group-btn">
                      <button type="button" class="btn bg-purple btn-flat" id="stock_coverage1">Submit</button>
                    </span>
                    </div>
                </div>


            </div>
            <div class="box-body">
                <div class="chart">
                    <div class="col-md-12" id="coverage1"></div>
                </div>
            </div>


        </div>
        <!-- /.box -->
    </section>
    <section class="col-md-12">

        <div class="box box-info">
            <div class="box-header with-border">
                <i class="fa fa-clock-o"></i>

                <h3 class="box-title">
                    Coverage
                </h3>


            </div>
            <div class="box-body">

                <div class="row">
                    <div class="form-group">
                        <div class="col-sm-4">
                            <select class="form-control" id="location">
                                <option data-name='NULL'>Select Subcounty</option>

                                <?php
                                foreach ($subcounties as $key => $value) {
                                    echo "<option value='$key' data-value='$key'>$value</option>";
                                }
                                ?>

                            </select>
                        </div>
                        <div class="col-sm-4">
                            <input id="period" class="form-control" placeholder="Select Month">
                        </div>
                        <div class="col-sm-2">
                        <span class="input-group-btn">
                      <button type="button" class="btn bg-purple btn-flat" id="stock_coverage">Submit</button>
                    </span>
                        </div>
                    </div>


                    <div class="chart">
                        <div class="col-md-12" id="coverage"></div>
                    </div>


                </div>


            </div>
            <!-- /.box -->
    </section>

    <section class="col-md-12">

        <div class="box box-info">
            <div class="box-header with-border">
                <i class="fa fa-plus-square-o"></i>

                <h3 class="box-title">
                    Stock Levels of Subcounty Stores
                </h3>
                <div class="box-tools pull-right">
                    <button data-widget="collapse" class="btn btn-box-tool" type="button"><i class="fa fa-minus"></i>
                    </button>
                    <button data-widget="remove" class="btn btn-box-tool" type="button"><i class="fa fa-times"></i>
                    </button>
                </div>
            </div>

            <div class="chart">
                <div id="stock_levels"></div>
            </div>


        </div>
        <!--/.box-->
    </section>

    <section class="col-md-12">

        <div class="box box-info">
            <div class="box-header with-border">
                <i class="fa fa-plus-square-o"></i>

                <h3 class="box-title">
                    Stock Last Updated
                </h3>
                <div class="box-tools pull-right">
                    <button data-widget="collapse" class="btn btn-box-tool" type="button"><i class="fa fa-minus"></i>
                    </button>
                    <button data-widget="remove" class="btn btn-box-tool" type="button"><i class="fa fa-times"></i>
                    </button>
                </div>
            </div>

            <div class="chart">
                <div id="last_updated"></div>
            </div>


        </div>
        <!--/.box-->
    </section>

    <section class="col-md-12">

        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Latest Orders</h3>

                <div class="box-tools pull-right">
                    <button data-widget="collapse" class="btn btn-box-tool" type="button"><i class="fa fa-minus"></i>
                    </button>
                    <button data-widget="remove" class="btn btn-box-tool" type="button"><i class="fa fa-times"></i>
                    </button>
                </div>
            </div>
            <div id="orders"></div>

        </div>
        <!-- /.box -->

    </section>

</div>


<script>

    var url = "<?php echo base_url(); ?>";


    ajax_fill_data('county/order/get_orders', "#orders");
    ajax_fill_data('county/reports/plot_coverage_over_time', "#coverage1");
    ajax_fill_data('county/reports/plot_coverage', "#coverage");
    ajax_fill_data('county/reports/table_stock_level', "#stock_levels");
    ajax_fill_data('county/reports/table_last_updated', "#last_updated");

    $('#period').MonthPicker({StartYear: 2016, ShowIcon: false, MonthFormat: 'yy-mm'});

    $('#stock_coverage1').click(function () {
        var vaccine = $('option:selected', '#vaccine').attr('data-value');
        data = {'vaccine': vaccine};
        ajax_fill_data('county/reports/plot_coverage_over_time', "#coverage1", data);
    });

    $('#stock_coverage').click(function () {
        var location = $('option:selected', '#location').attr('data-value');
        var period = $('#period').val();
        data = {'location': location, 'period': period};
        ajax_fill_data('county/reports/plot_coverage', "#coverage", data);
    });

    function ajax_fill_data(function_url, div, data) {
        var function_url = url + function_url;
        var loading_icon = url + "assets/images/ring.gif";
        $.ajax({
            type: "POST",
            url: function_url,
            data: data,
            beforeSend: function () {
                $(div).html("<img style='margin:10% 50% 10% ;' src=" + loading_icon + ">");
            },
            success: function (msg) {
                $(div).html(msg);
            }
        });
    }

</script>
