<style>
    .small-box .icon {font-size: 50px;}
    .small-box:hover .icon {font-size: 54px;}
    .select2-hidden-accessible {
        border: 1px solid #ccc;}
    .small-box h3 {font-size: 22px;}
</style>
<script type="text/javascript" src="<?php echo base_url() ?>assets/plugins/bootstrap-multiselect/dist/js/bootstrap-multiselect.js"></script>
<link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/bootstrap-multiselect/dist/css/bootstrap-multiselect.css" type="text/css"/>
<div class="row">

    <section class="col-lg-12 connectedSortable ui-sortable">


        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#activity" data-toggle="tab" aria-expanded="true">MY STORE</a></li>
                <li class=""><a href="#timeline" data-toggle="tab" aria-expanded="false">SUBCOUNTY STORES</a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="activity">

                    <div class="row">
                        <div class="col-lg-2 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-aqua">
                                <div class="inner">
                                    <h3><?php echo count($myfridges); ?></h3>

                                    <p>Fridges</p>
                                </div>
                                <div class="icon">
                                    <i class="ion ion-thermometer"></i>
                                </div>
                            </div>
                        </div>
                        <!-- ./col -->
                        <div class="col-lg-2 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-green">
                                <div class="inner">
                                    <h3><?php echo $mystore ->vaccine_carriers; ?><sup style="font-size: 20px"></sup></h3>

                                    <p>Vaccine Carriers</p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-archive" aria-hidden="true"></i>
                                </div>
                            </div>
                        </div>
                        <!-- ./col -->
                        <div class="col-lg-2 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-yellow">
                                <div class="inner">
                                    <h3><?php echo $mystore ->live_birth_population; ?></h3>

                                    <p>Live Births</p>
                                </div>
                                <div class="icon">
                                    <i class="ion ion-ios-people"></i>
                                </div>
                            </div>
                        </div>
                        <!-- ./col -->
                        <div class="col-lg-2 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-red">
                                <div class="inner">
                                    <h3><?php echo $mystore ->catchment_population; ?></h3>

                                    <p>Catchment Pop</p>
                                </div>
                                <div class="icon">
                                    <i class="ion ion-person-stalker"></i>
                                </div>
                            </div>
                        </div>
                        <!-- ./col -->
                        <div class="col-lg-2 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-purple">
                                <div class="inner">
                                    <h3><?php echo $mystore ->cold_boxes; ?></h3>

                                    <p>Cold Boxes</p>
                                </div>
                                <div class="icon">
                                    <i class="ion ion-cube"></i>
                                </div>
                            </div>
                        </div>
                        <!-- ./col -->

                        <div class="col-lg-2 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-teal">
                                <div class="inner">
                                    <h3><?php echo $mystore ->ice_packs; ?></h3>

                                    <p>Ice Packs</p>
                                </div>
                                <div class="icon">
                                    <i class="ion ion-ios-snowy"></i>
                                </div>
                            </div>
                        </div>
                        <!-- ./col -->
                    </div>

                    <div class="box box-solid ">
                        <div class="box-header with-border">

                            <i class="fa fa-thermometer"></i>

                            <h3 class="box-title">
                                My Fridges
                            </h3>

                            <button type="button" id="myfridgesubmit" name="myfridgesubmit" class="btn btn-sm bg-olive btn-flat margin pull-right" data-toggle="modal" data-target="#modal-addfridges">
                                <i class="fa fa-thermometer-full"></i>  Add Fridge
                            </button>


                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="table-responsive">
                                <table class="table no-margin">
                                    <thead>
                                    <tr>
                                        <th>Fridge ID</th>
                                        <th>Model</th>
                                        <th>Temp. Monitor No.</th>
                                        <th>Power Source</th>
                                        <th>Year of Installation</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    if(count($myfridges)==0){

                                        echo '<div style="margin:5%;font-size:3em;font-weight:400;"> No Fridges at this time.</div>';
                                    }else{


                                        foreach ($myfridges as $key=> $value) { ?>



                                            <tr>

                                                <td class="">
                                                    <?php echo $value->equipment_id; ?>
                                                </td>
                                                <td class="">
                                                    <?php echo $value->model; ?>
                                                </td>
                                                <td class="">
                                                    <span class="label label-danger"><?php echo $value->temperature_monitor_no; ?></span>
                                                </td>

                                                <td class="">
                                                    <?php echo $value->power_source; ?>
                                                </td>
                                                <td class="">
                                                    <?php echo $value->installation_year; ?>

                                                </td>

                                                <td class="">
                                                    <?php echo $value->fridge_status; ?>
                                                </td>
                                                <td><button type="button" id="" class="btn btn-sm btn-flat btn-primary">Edit</button></td>

                                            </tr>
                                        <?php } }?>


                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer clearfix">

                            <!--                            <a href="javascript:void(0)" class="btn btn-sm btn-default btn-flat pull-right">View</a>-->
                        </div>
                        <!-- /.box-footer -->
                    </div>

                </div>
                <!-- /.tab-pane -->
                <div class="tab-pane" id="timeline">
                    <!-- The timeline -->

                    <div class="box box-info">
                        <div class="box-header with-border">
                            <i class="fa fa-podcast"></i>

                            <h3 class="box-title">
                                Sub-County Stores
                            </h3>

                            <button type="button" class="btn btn-sm bg-olive btn-flat margin pull-right" data-toggle="modal" data-target="#">
                                <i class="fa fa-print"></i>  Print
                            </button>

                            <button type="button" class="btn btn-sm bg-olive btn-flat margin pull-right" data-toggle="modal" data-target="#modal-addstore">
                                <i class="fa fa-thermometer-full"></i>  Add Store
                            </button>


                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="table-responsive">
                                <table class="table no-margin">
                                    <thead>
                                    <tr>
                                        <th>Region</th>
                                        <th>Catchment Pop</th>
                                        <th>Live Births</th>
                                        <th>Cold Boxes</th>
                                        <th>Vaccine Carriers</th>
                                        <th>Ice Packs</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    if(count($stores)==0){

                                        echo '<div style="margin:5%;font-size:3em;font-weight:400;"> No Stores Found at this time.</div>';
                                    }else{


                                        foreach ($stores as $key=> $value) { ?>



                                            <tr>

                                                <td class="" >
                                                    <?php echo $value->name; ?>
                                                </td>
                                                <td class="">
                                                    <?php echo $value->catchment_population; ?>
                                                </td>
                                                <td class="">
                                                    <?php echo $value->live_birth_population; ?>
                                                </td>
                                                <td class="">
                                                    <?php echo $value->cold_boxes; ?>
                                                </td>
                                                <td class="">
                                                    <?php echo $value->vaccine_carriers; ?>
                                                </td>
                                                <td class="">
                                                    <?php echo $value->ice_packs; ?>
                                                </td>


                                                <td class="stores" id="<?php echo $value->id; ?>"><button type="button"  class="btn btn-sm btn-flat btn-primary " >View Fridges</button></td>

                                            </tr>
                                        <?php } }?>


                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.box-body -->

                    </div>

                </div>
                <!-- /.tab-pane -->



            </div>
            <!-- /.tab-content -->
        </div>

    </section>


</div>

<div class="modal fade" id="modal-addfridges" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Add Fridge</h4>
            </div>
            <div class="modal-body">
                <form action="" method="post" id="myfridge" class="form-horizontal">

                    <div class="row form-group has-feedback">
                        <div class="col-xs-6">
                            <select id="model" name="model" class=" form-control custom-select">
                                <option value="">- Select Fridge Model -</option>
                                <?php
                                foreach ($fridges as $key => $value) {
                                    $name = $value->model;
                                    $id = $value ->id;
                                    echo "<option value='$id'>$name</option>";
                                }

                                ?>
                            </select>
                        </div>

                        <div class="col-xs-6">
                            <select class="form-control" style="width: 100%;" name="power_source[]" id="example-getting-started"  multiple="multiple">
                                <option value="Electricity">Electricity</option>
                                <option value="Solar">Solar</option>
                                <option value="Gas">Gas</option>

                            </select>
                        </div>

                    </div>

                    <div class="row form-group has-feedback">
                        <div class="col-xs-6">
                            <select id="status" name="status" class=" form-control custom-select">
                                <option value="">- Select Fridge Status -</option>
                                <option value="Functional">Functional</option>
                                <option value="Nonfunctional">Non-Functional</option>

                            </select>
                        </div>

                        <div class="col-xs-6">
                            <input type="hidden" id="store_id" name="store_id" value="<?php echo $mystore ->id; ?>">

                            <input type="text" id="temp_monitor_no" name="temp_monitor_no" class="form-control" placeholder="Temp. Monitor No.">
                        </div>
                    </div>

                    <div class="row form-group has-feedback">
                        <div class="col-xs-6">
                            <select id="installation_year" name="installation_year" class=" form-control custom-select">
                                <option value="">- Select Year Installed -</option>
                            </select>
                        </div>


                    </div>


                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default  btn-flat" data-dismiss="modal">Close</button>
                <button type="submit" id="storefridgesubmit" name="storefridgesubmit" class="btn btn-primary  btn-flat"><i class="fa fa-plus"></i>  Add</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<div class="modal fade" id="modal-addstore" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Add Store</h4>
            </div>
            <div class="modal-body">
                <form action="" method="post" id="store" class="form-horizontal">

                    <div class="row form-group has-feedback">
                        <div class="col-xs-4">
                            <select id="subcounties" name="subcounties" class=" form-control custom-select">
                                <option value="NULL">- Select Sub-county -</option>
                                <?php
                                foreach ($subcounties as $key => $value) {
                                    $name = $value->subcounty_name;
                                    $id = $value ->id;
                                    $region_id = $value ->region_id;
                                    $county_id = $value ->county_id;
                                    echo "<option value='$id' data-region='$region_id' data-county='$county_id'>$name</option>";
                                }

                                ?>
                            </select>
                            <input type="hidden" value="" name="counties" id="counties">
                            <input type="hidden" value="" name="regions" id="regions">
                            <input type="hidden" class="form-control" name="name" id="name" placeholder="Store Name">
                        </div>
                        <div class="col-xs-4">
                            <input type="number" name="catchment_pop" class="form-control" min="0" placeholder="Catchment Population">
                        </div>
                        <div class="col-xs-4">
                            <input type="number" name="live_birth_pop" class="form-control" min="0" placeholder="Live Birth Population">
                        </div>
                    </div>

                    <div class="row form-group has-feedback">
                        <div class="col-xs-4">
                            <input type="number" name="cold_boxes" class="form-control" name="name" min="0" placeholder="Cold Boxes">
                        </div>
                        <div class="col-xs-4">
                            <input type="number" name="vaccine_carriers" class="form-control" min="0" placeholder="Vaccine Carriers">
                        </div>
                        <div class="col-xs-4">
                            <input type="number" name="ice_packs" class="form-control" min="0" placeholder="Ice Packs">
                        </div>
                    </div>







                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default  btn-flat" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary  btn-flat" id="storesubmit"><i class="fa fa-plus"></i>  Add</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->



<div class="modal fade" id="modal-showstorefridges" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Store Fridges</h4>
            </div>
            <div class="modal-body" >

                <table class="table no-margin">
                    <thead>
                    <tr>
                        <th>Fridge-ID</th>
                        <th>Model</th>
                        <th>Power Source</th>
                        <th>Temp Mon No</th>
                        <th>Year Installed</th>
                        <th>Status</th>

                    </tr>
                    </thead>
                    <tbody id="display">

                    </tbody>
                </table>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default  btn-flat" data-dismiss="modal">Close</button>
                <!--                <button type="submit" class="btn btn-primary  btn-flat"><i class="fa fa-plus"></i>  Add</button>-->
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="text/javascript">

    $( document ).ready(function() {

        $('#example-getting-started').multiselect(
            {
                includeSelectAllOption: true,
                enableFiltering: false,
                maxHeight: 200,
                filterPlaceholder: 'Select Power Source(s)'
            }
        );

        $('.stores').click(function(){

            var store_id=$(this).closest('td').attr('id');
            //console.log(store_id);

            var siteurl="<?php echo base_url(); ?>";
            var url="<?php echo base_url(); ?>fridges/getById/"+store_id;

            var loading_icon=siteurl+"assets/images/ring.gif";
            $.ajax({
                type: "POST",
                url: url,
                beforeSend: function() {
                    // $('#display').html("<img style='margin:5% 50% 0 50%;' src="+loading_icon+">");
                },
                success: function(data)
                {
                    $('#display').empty();
                    $('#modal-showstorefridges').modal('show');

                    console.log(data.length);
                    for (var i = 0; i < data.length; i++) {
                        tr = $('<tr/>');
                        tr.append("<td>" + data[i].equipment_id + "</td>");
                        tr.append("<td>" + data[i].model + "</td>");
                        tr.append("<td>" + data[i].power_source + "</td>");
                        tr.append("<td>" + data[i].temperature_monitor_no + "</td>");
                        tr.append("<td>" + data[i].installation_year + "</td>");
                        tr.append("<td>" + data[i].fridge_status + "</td>");
                        $('#display').append(tr);
                    }

                },error: function (jqXHR) {
                    console.log(jqXHR);
                    toastr.error(jqXHR.responseText);
                }
            });
        }) ;



        var max = new Date().getFullYear(),
            min = max - 10,
            select = document.getElementById('installation_year');

        for (var i = min; i<=max; i++){
            var opt = document.createElement('option');
            opt.value = i;
            opt.innerHTML = i;
            select.appendChild(opt);
        }

        toastr.options = {
            "closeButton": false,
            "debug": false,
            "newestOnTop": true,
            "progressBar": false,
            "positionClass": "toast-top-right",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        }



        $('#subcounties').on('change', function(){

            var region= $("option:selected", this).attr('data-region');
            var county= $("option:selected", this).attr('data-county');
            var name= $("option:selected", this).text();
            $("#regions").val(region);
            $("#counties").val(county);
            $("#name").val(name)
            console.log(region);


        });

        $("#storesubmit").click(function(e) {

            var url="<?php echo base_url(); ?>store/add"; // the script where you handle the form input.
            var formData ='#store';

            processData(formData,url);

            e.preventDefault(); // avoid to execute the actual submit of the form.
        });

        $("#storefridgesubmit").click(function(e) {

            var url="<?php echo base_url(); ?>fridges/add"; // the script where you handle the form input.
            var formData ='#myfridge';

//        console.log( $( '#myfridge' ).serialize() );
//        return;

            processData(formData,url);

            e.preventDefault(); // avoid to execute the actual submit of the form.

        });

        function processData(formData,url){
            $.ajax({
                type: "POST",
                url: url,
                data: $(formData).serialize(), // serializes the form's elements.
                success: function(data,status, jqXHR)
                {console.log(jqXHR);

                    setTimeout(location.reload.bind(location), 800);

                    toastr.success('Successfully Added.') // show response from the php script.

                },error: function (jqXHR, status, err,data) {
                    console.log(jqXHR);
                    toastr.error(jqXHR.responseText)
                }
            });

        }


    });
</script>