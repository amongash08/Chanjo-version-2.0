<?php if($user_info->level_id == 3): ?>
<head>
    <META HTTP-EQUIV="Access-Control-Allow-Origin" CONTENT="<?php echo base_url('county/home'); ?>">
</head>

<div class="box-body" style="height: 300px">
    <div class="table-responsive">
        <table class="table no-margin">
            <thead>
            <tr>
                <th>Order Number</th>
                <th>From</th>
                <th>Collection Date</th>
                <th>Status</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            <?php if ($other_orders):
                foreach ($other_orders as $key => $value):
                    if ($value->status != 'received'):
                        if ($key == 4) break; ?>
                        <tr>
                            <td><?php echo $value->id; ?></td>
                            <td><?php echo $value->from; ?></td>
                            <td><?php echo date('Y-m-d', strtotime($value->timestamp)); ?></td>
                            <?php if ($value->status == 'pending'): ?>
                                <td><span class="label label-danger">Pending</span></td>
                                <td><a href="<?php echo site_url('order/order_sheet/' . $value->id); ?>"
                                       class="btn btn-primary state-change btn-xs">View</a>&nbsp;
                                </td>
                            <?php elseif ($value->status == 'packing'): ?>
                                <td><span class="label label-warning">Packing</span></td>
                                <td><a href="<?php echo site_url('order/packing_list/' . $value->id); ?>"
                                       class="btn btn-primary state-change btn-xs">View</a>&nbsp;
                                </td>
                            <?php elseif ($value->status == 'issued'): ?>
                                <td><span class="label label-success">Issued</span></td>
                                <td><a href="<?php echo site_url('order/issued/' . $value->id); ?>"
                                       class="btn btn-primary state-change btn-xs">View</a>&nbsp;
                                </td>
                            <?php endif; ?>
                        </tr>
                    <?php endif;
                endforeach;
            endif; ?>
            </tbody>
        </table>
    </div>
    <!-- /.table-responsive -->
</div>
<!-- /.box-body -->
<div class="box-footer clearfix">

</div>
<!-- /.box-footer -->
<?php else: ?>
    <section class="content">
        <div class="text-center">

            <div class="error-content" style="padding-top:28px;">
                <h1><i class="fa fa-warning text-red"></i> Oops! An error was encountered.</h1>

                <p style="font-size: 22px;">
                    You are not authorized to view this section.
                    Please contact your supervisor.
                </p>


            </div>
            <!-- /.error-content -->
        </div>
        <!-- /.error-page -->
    </section>
<?php endif; ?>