<div class="row">

    <section class="col-lg-12 connectedSortable ui-sortable">

        <!-- stock levels box -->
        <div class="box box-solid ">
            <div class="box-header with-border">
                <i class="fa fa-calendar"></i>

                <h3 class="box-title">
                    Immunization Performance
                </h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
            </div>
            <div class="box-body">

                <div class="row">

                    <div class="form-inline row" style="margin-left:2%;">
                        <select id="county" class=" form-control custom-select" style="margin-left:50px;" >
                            <option value="NULL"> Select County </option>
                            <?php
                            foreach ($counties as $key => $value) {
                                $name = $value->county_name;
                                $id = $value->id;
                                echo "<option value='$name' data-id='$id'>$name</option>";
                            }

                            ?>
                        </select>
                        <label for="Date"  style="margin-left:10px;">Month</label>
                        <input type="text" id="date_filter" class="form-control"  name="date_filter" />

                        <button type="submit" style="margin-left:5px;" id="submit" name="submit" class="btn btn-success">Submit</button>
                    </div>

                </div>

                <div class="chart">
                    <div id="report_listing" ></div>
                </div>


            </div>


        </div>
        <!-- /.box -->


    </section>


</div>


<script type="text/javascript">

    var url="<?php echo base_url(); ?>";


    ajax_fill_data('reports/report_listing',"#report_listing");

    $(document).ready(function(){

        $("#date_filter").datepicker( {
            format: "dd-M-yyyy",
            viewMode: "months",
            minViewMode: "months",
            autoclose: true,
        });


    });

    $( "#submit" ).click(function() {

        var period=$('#date_filter').val();
        var county=$('option:selected', '#county').val();
        //console.log(county);
        ajax_fill_data('reports/report_listing/'+county+'/'+period,"#report_listing");


    });


    function ajax_fill_data(function_url,div){
        var function_url =url+function_url;
        var loading_icon=url+"assets/images/ring.gif";
        $.ajax({
            type: "POST",
            url: function_url,
            beforeSend: function() {
                $(div).html("<img style='margin:10% 50% 10% ;' src="+loading_icon+">");
            },
            success: function(msg) {
                $(div).html(msg);
            }
        });
    }
</script>
<?php
/**
 * Created by PhpStorm.
 * User: mureithi
 * Date: 7/18/17
 * Time: 10:30 AM
 */