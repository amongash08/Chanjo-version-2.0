<div class="margin">
    <div style="min-height: 400px;" id="reports_display">
        <table class="table table-bordered table-hover table-striped" id="">
            <thead style="background-color: white">
            <tr>
                <th>Antigen</th>
                <th>Balance (Doses)</th>
                <th>Last Updated</th>
            </tr>
            </thead>

            <tbody>
            <?php foreach ($query as $key => $value) :

                $maxdate = date('Y-m-d');
                $mindate = new DateTime(date('Y-m-d'));
                $interval = new DateInterval('P1M');
                $mindate = $mindate->sub($interval)->format('Y-m-d');
                $date = '';
                $balance = '';
                if ($value):
                    foreach ($value as $data):
                        if (array_key_exists('timestamp', $value)) {
                            $timestamp = date('Y-m-d', strtotime($value['timestamp']));
                            $date = date('d , M Y', strtotime($value['timestamp']));
                        }
                        $balance = (array_key_exists('balance', $value)) ? $value['balance'] : '';
                    endforeach;
                endif;

                $color = ($date <= $mindate) ? 'red' : 'black';
                ?>
                <tr style="color:<?php echo $color; ?>">
                    <td><?php echo $key; ?></td>
                    <td><?php echo $balance; ?></td>
                    <td style="font-weight:600;">
                        <?php echo $date; ?></td>
                </tr>
            <?php endforeach; ?>


            </tbody>
        </table>
    </div>

</div>

<script>

    $(document).ready(function () {

        $("td").each(function () {
            var text = $(this).html();
            if (text == '' || text == ' ') {

                $(this).html("<span class='label label-danger'><i class='fa fa-times' aria-hidden='true'></i></span>");

            }

        });

    });

</script>
