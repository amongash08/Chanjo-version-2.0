<link href="<?php echo base_url(); ?>assets/plugins/jquery-month-picker/MonthPicker.css" rel="stylesheet"
      type="text/css"/>
<div class="row">

    <section class="col-lg-12 connectedSortable ui-sortable">

        <!-- stock levels box -->
        <div class="box box-solid ">
            <div class="box-header with-border">
                <i class="fa fa-calendar"></i>

                <h3 class="box-title">
                    Stock Analysis
                </h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
                    </button>
                </div>
            </div>
            <div class="box-body">

                <div class="row">

                    <div class="form-group col-md-3">
                        <select class="form-control" id="subcounties">
                            <option data-name='NULL'>Select Sub County</option>

                            <?php
                            foreach ($subcounties as $key => $value) {
                                echo "<option value='$key' data-name='$value'>$value</option>";
                            }
                            ?>

                        </select>
                    </div>

                    <div class="form-group col-md-3">
                        <select class="form-control" id="antigen">
                            <option data-name='NULL'>Select Antigen</option>

                            <?php
                            foreach ($vaccines as $key => $value) {
                                echo "<option value='$key' data-name='$value'>$value</option>";
                            }
                            ?>

                        </select>
                    </div>
                    <div class="form-group col-md-3">
                        <input id="monthpicker" class="form-control">
                    </div>

                    <div class="form-group">
                        <div class="col-sm-1">
                           <span class="input-group-btn">
                            <button type="button" class="btn bg-purple btn-flat" id="stock">Submit</button>
                           </span>
                        </div>
                    </div>

                </div>


                <div class="chart">
                    <div id="areaChart"></div>
                </div>


            </div>


        </div>
        <!-- /.box -->


        <!-- stock levels box -->
        <div class="box box-solid ">
            <div class="box-header with-border">
                <i class="fa fa-chevron-circle-down"></i>

                <h3 class="box-title">
                  DPT Dropout rate
                </h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
                    </button>
                </div>
            </div>
            <div class="box-body">
                <div class="chart">
                    <div id="dropout" class="row"></div>
                </div>


            </div>


        </div>
        <!-- /.box -->


    </section>


</div>

<script type="text/javascript">

    $(document).ready(function () {
        var url = "<?php echo base_url(); ?>";

        ajax_fill_data('stock/stock_by_month', "#areaChart");
        ajax_fill_data('county/reports/plot_dpt_dropout', "#dropout");

        $('#monthpicker').MonthPicker({StartYear: 2016, ShowIcon: false, MonthFormat: 'yy-mm'});

        $('#stock').click(function () {

            var location = $('option:selected', '#subcounties').attr('value');
            var antigen = $('option:selected', '#antigen').attr('value');
            var date = $('#monthpicker').val();
            data = {'location': location, 'date': date, 'antigen': antigen};
            ajax_fill_data('stock/stock_by_month', "#areaChart", data);
        });

        function ajax_fill_data(function_url, div, data) {
            var function_url = url + function_url;
            var loading_icon = url + "assets/images/ring.gif";
            $.ajax({
                type: "POST",
                url: function_url,
                data: data,
                beforeSend: function () {
                    $(div).html("<img style='margin:10% 50% 10% ;' src=" + loading_icon + ">");
                },
                success: function (msg) {
                    $(div).html(msg);
                }
            });
        }

    });


</script>
