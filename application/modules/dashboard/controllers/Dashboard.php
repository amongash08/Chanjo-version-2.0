<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MX_Controller
{

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *        http://example.com/index.php/dashboard
     *    - or -
     *        http://example.com/index.php/dashboard/index
     *    - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/dashboard/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    function __construct()
    {
        parent::__construct();
        if (!$this->ion_auth->logged_in()) {
            //redirect them to the login page
            redirect('users');
        }
    }


    public function index()
    {


        $data['title'] = 'Dashboard';
        $data['page_title'] = 'Dashboard';
        $data['module'] = 'dashboard';
        $data['view_file'] = 'dashboard';

        echo Modules::run('templates', $data);


    }

    public function user()
    {
        if (!$this->ion_auth->logged_in()) {
            redirect('users/login');
        }

        $data['title'] = 'Dashboard';
        $data['page_title'] = 'Dashboard';
        $data['module'] = 'dashboard';
        $data['view_file'] = 'dashboard_user';

        echo Modules::run('templates', $data);


    }

    public function admin()
    {
        $data['title'] = 'Dashboard';
        $data['page_title'] = 'Dashboard';
        $data['module'] = 'dashboard';
        $data['view_file'] = 'dashboard_admin';

        if (!$this->ion_auth->logged_in()) {
            $data['message'] = 'You must be logged in to view this page';
            $this->session->set_flashdata('message', $data['message']);
            redirect('users/login');
        } elseif (!$this->ion_auth->is_admin()) {
            $data['message'] = 'Administrator rights are required to view this page';
            $data['title'] = 'Insufficient Rights';
            $data['page_title'] = 'Insufficient Rights';
            $data['module'] = 'users';
            $data['view_file'] = 'insufficient_rights';
        } else {
            $data['title'] = 'Dashboard';
            $data['page_title'] = 'Dashboard';
            $data['module'] = 'dashboard';
            $data['view_file'] = 'dashboard_admin';
        }
        echo Modules::run('templates', $data);


    }
}
