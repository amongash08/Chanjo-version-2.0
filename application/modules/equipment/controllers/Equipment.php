<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Equipment extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();
        if (!$this->ion_auth->logged_in()) {
            //redirect them to the login page
            redirect('users');
        }
        $this->load->module('users');
        $this->load->model('mdl_equipment');
        $this->load->model('mdl_equipment_ext');
        $this->load->model('fridges/mdl_fridges');
    }


    public function index()
    {
        $data['title'] = 'Cold Chain';
        $data['subtitle'] = 'Spare parts';
        $data['module'] = 'equipment';
//        $data['view_file'] = 'equipment';
        $data['view_file'] = 'spareparts';
        $data['type'] = $this->mdl_equipment_ext->get_spare_part_type();
        $data['fridge'] = $this->mdl_fridges->get_all();

        echo Modules::run('templates', $data);

    }

    public function add_spare_parts()
    {
        //$user_info = $this->users->user_info();

            $category = $this->input->post('category');
            $spare_part_name = $this->input->post('spare_part_name');
            $equipment = $this->input->post('associated_equipment');
            $description = $this->input->post('description');


            $spare_part_data = array(
                'spare_part_category_id' => $category,
                'name' => $spare_part_name,
                'associated_equipment_id' => json_encode($equipment),
                'description' => $description
            );

            $this->mdl_equipment_ext->add_spare_part($spare_part_data);
           // echo '<pre>',print_r($spare_part_data),'</pre>';exit;
            echo 'Spare part has been added to the inventory.';



    }

    public function edit_spare_part()
    {
        $user_info = $this->users->user_info();
        if ($this->input->post()) {
            $category = $this->input->post('category');
            $spare_part_name = $this->input->post('spare_part_name');
            $equipment = $this->input->post('associated_equipment');
            $quantity = $this->input->post('quantity');
            $description = $this->input->post('description');
            $id = $this->input->post('DT_RowId');

            $exists = $this->mdl_equipment_ext->get_spare_part($id);
            if($exists){
                $this->mdl_equipment->update($id, ['deleted'=> 1]);
            }
            $transaction_data = array(
                'location_id' => $user_info->location_id,
                'user_id' => $user_info->id,
                'level' => $user_info->level_id,
                'transaction_type' => 'receive',
            );

            $spare_part_data = array(
                'spare_part_type_id' => $category,
                'spare_part_name' => $spare_part_name,
                'associated_equipment_id' => json_encode($equipment),
                'transaction_quantity' => $quantity,
                'description' => $description
            );
            $query = $this->mdl_equipment_ext->transact_spare_parts($transaction_data, $spare_part_data);
        }
        if($query){
            echo 'Spare part has been updated in the inventory.';
        }
    }
    public function spare_part_data()
    {
        if ($this->input->post()) {
            if (array_key_exists('action', $this->input->post())) {

                if ($_POST['action'] === 'remove') {
                    $del = null;
                    foreach ($_POST['data'] as $key => $value) {
                        if($key =='order'){
                            $del = $value;
                        }
                    }
                    $this->mdl_equipment_ext->delete_record($del);
                }
            }
        }
        $user_info = $this->users->user_info();
        $data = $this->mdl_equipment_ext->get_spare_part_data($user_info->location_id);
        $output['data'] = [];
        if ($data) {
            foreach ($data as $key => $value) {
                $equipment = $this->mdl_equipment_ext->get_associated_equipment(json_decode($value->associated_equipment_id));
                $equipment = str_replace(["[", "]", "'", '"'], '', $equipment);
                $equipment = str_replace([","], ', ', $equipment);
                $associated_equipment_id = str_replace(["[", "]", "'", '"'], '', $value->associated_equipment_id);
                $array[] = [
                    'DT_RowId' => $value->id,
                    'category' => $value->name,
                    'category_id' => $value->spare_part_type_id,
                    'spare_part_name' => $value->spare_part_name,
                    'associated_equipment' => $equipment,
                    'equipment_id' => json_decode($value->associated_equipment_id),
                    'quantity' => $value->transaction_quantity,
                    'description' => $value->description,
                ];
            }
            $output['data'] = $array;
            echo json_encode($output);
        }else{
            echo json_encode($output);
        }

    }


    public function receive_spare_part()
    {
        $user_info = $this->users->user_info();

        $this->form_validation->set_rules('receive_category[]', 'Category', 'required');
        $this->form_validation->set_rules('receive_spare_parts[]', 'Spare Parts', 'required');
        $this->form_validation->set_rules('receive_quantity[]', 'Quantity', 'required');
//        $this->form_validation->set_rules('receive_date[]', 'Receive Date', 'required');



        if ($this->form_validation->run() == FALSE)
        {
            echo validation_errors();
            http_response_code(401);
            return;
        }

        //transaction_data

        $transaction_data = array(
            'location_id' => $user_info->location_id,
            'level' => $user_info->level_id,
            'user_id' => $user_info->id,
            'transaction_type' => 'receive'
        );

        $parts = $this->input->post('receive_spare_parts[]');
        $quantity = $this->input->post('receive_quantity');



        $spare_part_data = array(
                'spare_part_id' => $parts,
                'quantity_received' => $quantity,
            );

        $this->mdl_equipment_ext->transact_spare_parts($transaction_data, $parts,$quantity);

//        echo '<pre>',print_r($spare_part_data),'</pre>';exit;




    }

    public function issue_spare_parts()
    {
        $user_info = $this->users->user_info();

        $this->form_validation->set_rules('quantity_issued[]', 'Quantity', 'required');


        if ($this->form_validation->run() == FALSE)
        {
            echo validation_errors();
            http_response_code(401);
            return;
        }

        $item_id = $this->input->post('item_id[]');
        $quantity = $this->input->post('quantity_issued[]');
        $parts = $this->input->post('spare_part_id[]');
        $jobcard = $this->input->post('jobcard_id');

        //transaction_data

        $transaction_data = array(
            'location_id' => $user_info->location_id,
            'level' => $user_info->level_id,
            'user_id' => $user_info->id,
            'transaction_type' => 'issue'
        );


        $this->mdl_equipment_ext->issue_spare_parts($transaction_data,$quantity,$item_id,$parts,$jobcard);

//        echo '<pre>',print_r($spare_part_data),'</pre>';exit;


    }

}