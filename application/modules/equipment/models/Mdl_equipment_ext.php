<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mdl_equipment_ext extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function get_spare_part_type()
    {
        $query = $this->db->select('id, name')
            ->get('spare_part_type');
        $options = array();
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $options[$row->id] = $row->name;
            }
            return $options;
        }
        return false;
    }


    public function get_fridge()
    {
        $query = $this->db->select('id, name')
            ->get('spare_part_type');
        $options = array();
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $options[$row->id] = $row->name;
            }
            return $options;
        }
        return false;
    }

    public function get_associated_equipment($data)
    {
        $query = $this->db->select('Model as model')
            ->where_in('id', $data)
            ->get('fridges');
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $key => $value){
                $options[] = $value->model;
            }
            return json_encode($options);
        }
        return false;
    }

    public function transact_spare_parts($transaction_data, $parts,$quantity)
    {
        $this->db->trans_begin();
        $this->db->insert('spare_part_transactions', $transaction_data);
        $transaction_id = $this->db->insert_id();

        foreach ($parts as $key => $value) {

            $spare_part_data = array(
                'spare_part_id' => $value,
                'quantity' => $quantity[$key],
                'spare_part_transaction_id' => $transaction_id,
            );
            $this->db->insert('spare_part_transaction_items', $spare_part_data);


        }


        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return FALSE;
        } else {
            $this->db->trans_commit();
            return TRUE;
        }
    }

    public function get_spare_part_data($location_id)
    {

        $query = $this->db->select('spare_part_transactions.id, spare_part_type.name, spare_part_type_id, spare_part_name, transaction_quantity, associated_equipment_id, spare_part_transaction_items.description, transaction_type')
            ->from('spare_part_transactions')
            ->join('spare_part_transaction_items', 'spare_part_transactions.id = spare_part_transaction_items.spare_part_transaction_id')
            ->join('spare_part_type', 'spare_part_type.id = spare_part_transaction_items.spare_part_type_id')
            ->where('location_id', $location_id)
            ->where('deleted','0')
            ->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        }
        return false;
    }

    public function get_spare_part($id)
    {

        $query = $this->db->select('spare_part_transactions.id, spare_part_type.name, spare_part_type_id, spare_part_name, transaction_quantity, associated_equipment_id, spare_part_transaction_items.description, transaction_type')
            ->from('spare_part_transactions')
            ->join('spare_part_transaction_items', 'spare_part_transactions.id = spare_part_transaction_items.spare_part_transaction_id')
            ->join('spare_part_type', 'spare_part_type.id = spare_part_transaction_items.spare_part_type_id')
            ->where('spare_part_transactions.id', $id)
            ->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        }
        return false;
    }

    public function delete_record($id)
    {
        $tables = ['transactions', 'transaction_items', 'balances', 'batch_balances'];
        foreach ($tables as $key => $value) {
            if ($value == 'transactions') {
                $this->db->where('id', $id);
                $this->db->delete($value);
            } else {
                $this->db->where('transaction_id', $id);
                $this->db->delete($value);
            }
        }
    }

    public function add_spare_part($spare_part_data)
    {
        $this->db->insert('spare_parts', $spare_part_data);

    }

    public function issue_spare_parts($transaction_data,$quantity,$item_id,$parts,$jobcard)
    {
        $this->db->trans_begin();
        $this->db->insert('spare_part_transactions', $transaction_data);
        $transaction_id = $this->db->insert_id();


        foreach ($quantity as $key => $value) {
            //echo '<pre>',print_r($value),'</pre>';exit;


            $this->db->set('quantity_issued',$value);
            $this->db->where('id', $item_id[$key]);
            $this->db->update('jobcard_repair_parts');

            $spare_part_data = array(
                'spare_part_id' => $parts[$key],
                'quantity' => $quantity[$key],
                'spare_part_transaction_id' => $transaction_id,
            );
            $this->db->insert('spare_part_transaction_items', $spare_part_data);

        }

        //change jobcard status
        $this->db->set('status','closed');
        $this->db->where('id', $jobcard);
        $this->db->update('jobcard');

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return FALSE;
        } else {
            $this->db->trans_commit();
            return TRUE;
        }
    }
}