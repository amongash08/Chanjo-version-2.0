<!-- DataTables -->
<link href="<?php echo base_url() ?>assets/plugins/datatables/datatables.css" rel="stylesheet"/>
<script src="<?php echo base_url() ?>assets/plugins/datatables/datatables.min.js"></script>

<div class="row">
    <div class="col-sm-12">
        <div class="box box-solid">
            <div class="box-body">
                <button type="button" id="add" class="btn btn-primary">
                    Add Spare parts
                </button>
            </div>

            <div class="box-body">
                <div class="table-responsive">
                    <table id="table" class="table table-bordered table-hover table-striped" cellspacing="0"
                           width="100%">
                        <thead>
                        <tr>
                            <th></th>
                            <th>Category</th>
                            <th>Spare part Name</th>
                            <th>Associated Equipment</th>
                            <th>Quantity</th>
                            <th>Description</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                        <tfoot>
                        <tr>
                            <th></th>
                            <th>Category</th>
                            <th>Spare part Name</th>
                            <th>Associated Equipment</th>
                            <th>Quantity</th>
                            <th>Description</th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="add-modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button aria-label="Close" data-dismiss="modal" class="close" type="button">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Add Spare Parts</h4>
                </div>
                <div class="modal-body">
                    <form id="spare_part_form" method="post"
                          action="<?php echo base_url('/equipment/add_spare_parts'); ?>">
                        <div class="form-group-sm">
                            <label class="form-control-label">Category:</label>
                            <select class="form-control" id="category" name="category">
                                <option value="">Select Category</option>
                                <?php foreach ($type as $key => $value): ?>
                                    <option value="<?php echo $key; ?>"
                                            data-name="<?php echo $value ?>"><?php echo $value ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="form-group-sm">
                            <label class="form-control-label">Spare part Name:</label>
                            <input type="text" class="form-control" id="spare_part_name" name="spare_part_name">
                        </div>
                        <div class="form-group-sm">
                            <label class="form-control-label">Associated Equipment:</label>
                            <select class="form-control associated_equipment" multiple="multiple" style="width: 100%;"
                                    id="associated_equipment"
                                    name="associated_equipment">
                                <?php foreach ($fridge as $key => $value): ?>
                                    <option value="<?php echo $value->id; ?>"
                                            data-name="<?php echo $value->model ?>"><?php echo $value->model ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="form-group-sm">
                            <label class="form-control-label">Description:</label>
                            <textarea class="form-control" id="description" name="description"></textarea>
                        </div>
                        <div class="form-group-sm">
                            <label class="form-control-label">Quantity:</label>
                            <input type="number" class="form-control" name="quantity" id="quantity">
                        </div>
                </div>
                <div class="modal-footer">
                    <button data-dismiss="modal" class="btn btn-default pull-left" type="button">Cancel</button>
                    <button type="submit" id="validate" class="btn bg-navy margin">
                        Submit
                    </button>
                    </form>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

</div>

<script type="text/javascript">
    $(document).ready(function () {

        editor = new $.fn.dataTable.Editor({
            ajax: {
                remove: {
                    type: 'POST',
                    url: '<?php echo site_url('equipment/spare_part_data') ?>',
                    complete: function () {
                        dt = $('#table').DataTable();
                        dt.ajax.reload();
                    }
                }
            },
            table: "#table"
        });


        var table = $('#table').DataTable({
            dom: "Bfrtip",
            pageResize: true,
            paging: true,
            ajax: {
                url: '<?php echo site_url('equipment/spare_part_data') ?>',
                type: 'GET'
            },
            columns: [{
                data: null,
                defaultContent: '',
                className: 'select-checkbox',
                orderable: false
            }, {
                data: "category"
            }, {
                data: "spare_part_name"
            }, {
                data: "associated_equipment"
            }, {
                data: "quantity",
                render: $.fn.dataTable.render.number(',', '.', 0)
            }, {
                data: "description"
            }],
            order: [
                [1, "desc"]
            ],
            select: {
                style: 'os',
                selector: 'td:first-child'
            },
            buttons: [{
                extend: 'remove',
                editor: editor,

                action: function (e, dt, button, config) {
                    var selected = dt.row({selected: true}).data();
                    swal({
                            title: "Are you sure?",
                            text: "You will not be able to undo this action!",
                            type: "warning",
                            showCancelButton: true,
                            confirmButtonText: "Yes",
                            cancelButtonText: "Cancel",
                            closeOnConfirm: false,
                            closeOnCancel: true,
                            showLoaderOnConfirm: true
                        },
                        function (isConfirm) {
                            if (isConfirm) {
                                table.row('.selected').remove().draw(false);
                                remove(selected);
                            }
                        });
                }
            }, {
                extend: 'collection',
                text: 'Export',
                buttons: [
                    'copyHtml5',
                    'excelHtml5',
                    'csvHtml5'
                ]
            }, {
                text: 'Edit',
                extend: 'edit',
                action: function (e, dt, button, config) {
                    var selected = dt.row({selected: true}).data();
                    $('#spare_part_form')[0].reset(); // reset form on modals

                    $('#category').val(selected.category_id);
                    $('[name="spare_part_name"]').val(selected.spare_part_name);
                    var selected_values = [];
                    $.each(selected.equipment_id, function (key, value) {
                        selected_values.push(value);
                    });
                    $('[name="associated_equipment"]').val(selected_values).trigger("change");
                    $('[name="description"]').val(selected.description);
                    $('[name="quantity"]').val(selected.quantity);
                    $('<input>').attr({
                        type: 'hidden',
                        id: 'DT_RowId',
                        name: 'DT_RowId',
                        value: selected.DT_RowId
                    }).appendTo('#spare_part_form');
                    $('.modal-title').text('Edit Spare Part'); // Set title to Bootstrap modal title
                    $('#spare_part_form').attr('action', '<?php echo base_url('equipment/edit_spare_part');?>');
                    $('#add-modal').modal('show'); // show bootstrap modal when complete loaded

                }
            }]
        });

        function remove(data) {
            $.ajax({
                url: '<?php echo site_url('equipment/spare_part_data') ?>',
                type: 'POST',
                data: {data, 'action': 'remove'},
                success: function (data) {
                    table.ajax.reload();
                    swal.close();
                }
            });
        }


        $('#add').on('click', function () {
            $('#add-modal').modal('show');
        });

        $("#spare_part_form").find('[name="associated_equipment"]')
            .select2()
            // Revalidate the color when it is changed
            .change(function (e) {
                $('#spare_part_form').formValidation('revalidateField', 'associated_equipment');
            })
            .end()
            .formValidation({
                framework: 'bootstrap',
                // This option will not ignore invisible fields which belong to inactive panels
                excluded: ':hidden, :not(:visible)',
                fields: {
                    category: {
                        row: '.form-group-sm',
                        validators: {
                            notEmpty: {
                                message: '*required'
                            }
                        }
                    },
                    spare_part_name: {
                        row: '.form-group-sm',
                        validators: {
                            notEmpty: {
                                message: '*required'
                            }
                        }
                    },
                    associated_equipment: {
                        row: '.form-group-sm',
                        validators: {
                            notEmpty: {
                                message: '*required'
                            }
                        }
                    },
                    quantity: {
                        row: '.form-group-sm',
                        validators: {
                            notEmpty: {
                                message: '*required'
                            },
                            numeric: {
                                message: '*not a number'
                            },
                            greaterThan: {
                                value: 0,
                                message: '*not a valid number'
                            }
                        }
                    },
                }
            }).on('success.form.fv', function (e) {
            // Prevent form submission
            e.preventDefault();
            submit();

        });

        function submit() {
            var category = retrieveFormValues('category');
            var spare_part_name = retrieveFormValues('spare_part_name');
            var associated_equipment = retrieveFormValues('associated_equipment');
            var quantity = retrieveFormValues('quantity');
            var description = retrieveFormValues('description');
            var DT_RowId = retrieveFormValues('DT_RowId');

            $.ajax({
                url: $('#spare_part_form').attr('action'),
                type: 'POST',
                data: {
                    'category': category,
                    'spare_part_name': spare_part_name,
                    'associated_equipment': associated_equipment,
                    'quantity': quantity,
                    'description': description,
                    'DT_RowId': DT_RowId
                },
                beforeSend: function () {
                    $('#add-modal').modal('hide');
                },
                success: function (data) {
                    $('#add-modal').on('hidden.bs.modal', function () {
                        $('#add-modal').formValidation('resetForm', true);
                    });
                    toastr.options = {
                        "closeButton": false,
                        "debug": false,
                        "newestOnTop": false,
                        "progressBar": false,
                        "positionClass": "toast-top-right",
                        "preventDuplicates": false,
                        "onclick": null,
                        "showDuration": "300",
                        "hideDuration": "1000",
                        "timeOut": "5000",
                        "extendedTimeOut": "1000",
                        "showEasing": "swing",
                        "hideEasing": "linear",
                        "showMethod": "fadeIn",
                        "hideMethod": "fadeOut"
                    };
                    toastr["success"](data, "Success!");
                    table.rows().deselect();
                    table.ajax.reload();
                }
            });
        }

        function retrieveFormValues(name) {
            var dump;
            $.each($("input[name=" + name + "], select[name=" + name + "], textarea[name=" + name + "]"), function (i, v) {
                var theTag = v.tagName;
                var theElement = $(v);
                var theValue = theElement.val();
                dump = theValue;
            });
            return dump;
        }


    });

</script>
