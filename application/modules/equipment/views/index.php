<div class="row">
    <div class="col-sm-12">

        <div class="col-md-4 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-light-blue"><i class=""></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Total Equipment</span>
                    <span class="info-box-number">#</span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>

        <div class="col-md-4 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-light-blue"><i class=""></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Allocated Equipment</span>
                    <span class="info-box-number">#</span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>

        <div class="col-md-4 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-light-blue"><i class=""></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Remaining Equipment</span>
                    <span class="info-box-number">#</span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>

    </div>
</div>

<div class="row">
    <div class="col-sm-12">

    </div>
</div>

<script>
    $(document).ready(function () {

    });
</script>