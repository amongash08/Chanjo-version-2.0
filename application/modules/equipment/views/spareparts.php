<!-- DataTables -->
<link href="<?php echo base_url() ?>assets/plugins/datatables/datatables.css" rel="stylesheet"/>
<script src="<?php echo base_url() ?>assets/plugins/datatables/datatables.js"></script>
<script src="<?php echo base_url() ?>assets/plugins/tabledit/jquery.tabledit.js"></script>
<!--<div class="row">-->
<!--    <div class="col-md-3 col-sm-6 col-xs-12">-->
<!--        <div class="info-box">-->
<!--            <span class="info-box-icon bg-aqua"><i class="ion ion-ios-gear-outline"></i></span>-->
<!---->
<!--            <div class="info-box-content">-->
<!--                <span class="info-box-text">Indicator</span>-->
<!--                <span class="info-box-number">90<small>%</small></span>-->
<!--            </div>-->
<!--            <!-- /.info-box-content -->
<!--        </div>-->
<!--        <!-- /.info-box -->
<!--    </div>-->
<!--    <!-- /.col -->
<!--    <div class="col-md-3 col-sm-6 col-xs-12">-->
<!--        <div class="info-box">-->
<!--            <span class="info-box-icon bg-red"><i class="ion ion-ios-gear-outline"></i></span>-->
<!---->
<!--            <div class="info-box-content">-->
<!--                <span class="info-box-text">Indicator</span>-->
<!--                <span class="info-box-number">41,410</span>-->
<!--            </div>-->
<!--            <!-- /.info-box-content -->
<!--        </div>-->
<!--        <!-- /.info-box -->
<!--    </div>-->
<!--    <!-- /.col -->
<!---->
<!--    <!-- fix for small devices only -->
<!--    <div class="clearfix visible-sm-block"></div>-->
<!---->
<!--    <div class="col-md-3 col-sm-6 col-xs-12">-->
<!--        <div class="info-box">-->
<!--            <span class="info-box-icon bg-green"><i class="ion ion-ios-gear-outline"></i></span>-->
<!---->
<!--            <div class="info-box-content">-->
<!--                <span class="info-box-text">Indicator</span>-->
<!--                <span class="info-box-number">760</span>-->
<!--            </div>-->
<!--            <!-- /.info-box-content -->
<!--        </div>-->
<!--        <!-- /.info-box -->
<!--    </div>-->
<!--    <!-- /.col -->
<!--    <div class="col-md-3 col-sm-6 col-xs-12">-->
<!--        <div class="info-box">-->
<!--            <span class="info-box-icon bg-yellow"><i class="ion ion-ios-gear-outline"></i></span>-->
<!---->
<!--            <div class="info-box-content">-->
<!--                <span class="info-box-text">Indicator</span>-->
<!--                <span class="info-box-number">2,000</span>-->
<!--            </div>-->
<!--            <!-- /.info-box-content -->
<!--        </div>-->
<!--        <!-- /.info-box -->
<!--    </div>-->
<!--    <!-- /.col -->
<!--</div>-->

<div class="row">

    <section class="col-lg-12 connectedSortable ui-sortable">

        <!-- stock levels box -->
        <div class="box box-solid ">
            <div class="box-header with-border">
                <i class="fa  fa-book"></i>

                <h3 class="box-title">
                    Spare Parts Ledger
                </h3>

                <div class="pull-right">
                    <button type="button" id="receive" class="btn btn-danger btn-flat" style="margin-right: 5px;">
                        <i class="fa "></i> Receive Spare Parts
                    </button>

                    <div class="btn-group" role="group">
                        <button type="button" class="btn btn-info btn-flat dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span class="fa fa-cogs"></span><span class=""></span>
                        </button>
                        <ul class="dropdown-menu pull-right">
                            <li><a href="#">Spare Parts</a></li>
                            <li><a href="#" id="add">Add Spare Part</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="box-body">

                <div class="row">

                    <div class="form-group col-md-4">
                        <select id="spare_part_category" name="spare_part_category" class=" form-control col-md-6 category">
                            <option value="NULL">- Select Category -</option>
                            <?php
                            foreach ($type as $key => $value) {
                                $name = $value;
                                $id = $key;

                                echo "<option value='$id'>$name</option>";
                            }

                            ?>

                        </select>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-4">
                            <select id="parts" name="parts" class=" form-control col-md-6 parts">
                                <option value="NULL">- Select Spare Part -</option>

                            </select>
                        </div>
                        <div class="col-sm-3">
                        <span class="input-group-btn">
                          <button type="button" class="btn bg-purple btn-flat" id="stock">
                              <span class="fa fa-filter"></span> Filter</button>
                        </span>
                        </div>
                    </div>

                </div>


                <div class="ledger-container" style="min-height: 215px;">

                    <div class="row myledger">
                        <div class="col-md-12">
                            <div class="box">
                                <div class="box-body">
                                    <div class="table-responsive">
                                        <table id="ledger" data-order='[[ 0, "desc" ]]' class="table table-striped table-hover dataTable"
                                               cellspacing="0" width="100%">
                                            <thead style="background-color: white">
                                            <tr>
                                                <th>Date</th>
                                                <th>Type</th>
                                                <th>Location</th>
                                                <th>Quantity</th>
<!--                                                <th>Stock Balance</th>-->
                                            </tr>
                                            </thead>

                                            <tbody id="display">
                                            <td>
                                            <td style='margin:1%;font-size:2em;font-weight:400;'> &#128559; Please Select Filters To View Ledgers.</td>
                                            </td>


                                            </tbody>

                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
<!--                    <div >-->
<!--                        <div style="margin:5%;font-size:3em;font-weight:400;"> Filter Above to view Ledger.</div>-->
<!---->
<!--                    </div>-->
                </div>


            </div>


        </div>
        <!-- /.box -->


    </section>

</div>

<div class="modal fade" id="add-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-label="Close" data-dismiss="modal" class="close" type="button">
                    <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Add Spare Parts</h4>
            </div>
            <div class="modal-body">
                <form id="spare_part_form" method="post"
                      action="<?php echo base_url('/equipment/add_spare_parts'); ?>">
                    <div class="form-group-sm">
                        <label class="form-control-label">Category:</label>
                        <select class="form-control" id="category" name="category">
                            <option value="">Select Category</option>
                            <?php foreach ($type as $key => $value): ?>
                                <option value="<?php echo $key; ?>"
                                        data-name="<?php echo $value ?>"><?php echo $value ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="form-group-sm">
                        <label class="form-control-label">Spare part Name:</label>
                        <input type="text" class="form-control" id="spare_part_name" name="spare_part_name">
                    </div>
                    <div class="form-group-sm">
                        <label class="form-control-label">Associated Equipment:</label>
                        <select class="form-control associated_equipment" multiple="multiple" style="width: 100%;"
                                id="associated_equipment"
                                name="associated_equipment">
                            <?php foreach ($fridge as $key => $value): ?>
                                <option value="<?php echo $value->id; ?>"
                                        data-name="<?php echo $value->model ?>"><?php echo $value->model ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="form-group-sm">
                        <label class="form-control-label">Description:</label>
                        <textarea class="form-control" id="description" name="description"></textarea>
                    </div>

            </div>
            <div class="modal-footer">
                <button data-dismiss="modal" class="btn btn-default pull-left" type="button">Cancel</button>
                <button type="submit" id="validate" class="btn bg-purple btn-flat">
                    Add
                </button>
                </form>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<div class="modal fade" id="receive-modal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-label="Close" data-dismiss="modal" class="close" type="button">
                    <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Receive Spare Parts</h4>
            </div>
            <div class="modal-body">
                <form id="receive_spare_part_form" method="post"
                      action="">
                    <table class="table table-striped" id="table">
                        <thead>
                        <tr>
                            <th>Spare Part Category</th>
                            <th>Spare Part Required</th>
<!--                            <th>Date Received</th>-->
                            <th>Quantity</th>
                            <th>Action</th>

                        </tr>
                        </thead>
                        <tbody>
                        <tr row="0" class="form-group-sm">
                            <td>
                                <select id="receive_category[]" name="receive_category[]" class=" form-control col-md-6 category">
                                    <option value="NULL">- Select Category -</option>
                                    <?php
                                    foreach ($type as $key => $value) {
                                        $name = $value;
                                        $id = $key;

                                        echo "<option value='$id'>$name</option>";
                                    }

                                    ?>

                                </select>
                            </td>
                            <td>
                                <select id="receive_spare_parts[]" name="receive_spare_parts[]" class=" form-control col-md-6 parts">


                                </select>
                            </td>
<!--                            <td><input type="text" name="receive_date[]" id="receive_date[]" placeholder="YYYY-MM-DD" class="form-control mydate"></td>-->

                            <td><input type="number" name="receive_quantity[]" id="receive_quantity[]" class="form-control"></td>
                            <td><a class="add btn-xs btn-flat" style="cursor: pointer; cursor: hand;"><span
                                        class="label label-success"><i
                                            class="fa fa-plus-square"></i> <b>ADD</b></span></a><br>
                                <a class="remove btn-xs btn-flat" style="cursor: pointer; cursor: hand;"><span
                                        class="label label-danger"><i
                                            class="fa fa-minus-square"></i> <b>REMOVE</b></span></a></td>

                        </tr>

                        <tr id="template" class="form-group-sm" hidden>

                            <td>
                                <select disabled id="receive_category[]" name="receive_category[]" class=" form-control col-md-6 category">
                                    <option value="NULL">- Select Category -</option>
                                    <?php
                                    foreach ($type as $key => $value) {
                                        $name = $value;
                                        $id = $key;


                                        echo "<option value='$id'>$name</option>";
                                    }

                                    ?>

                                </select>
                            </td>
                            <td>
                                <select disabled id="receive_spare_parts[]" name="receive_spare_parts[]" class=" form-control col-md-6 parts">
                                    <option value="NULL">- Select Part -</option>

                                </select>
                            </td>
<!--                            <td><input disabled type="text" name="receive_date[]" id="receive_date[]" class="form-control mydate"></td>-->

                            <td><input disabled type="number" name="receive_quantity[]" id="receive_quantity[]" class="form-control"></td>
                            <td><a class="add btn-xs btn-flat" style="cursor: pointer; cursor: hand;"><span
                                        class="label label-success"><i
                                            class="fa fa-plus-square"></i> <b>ADD</b></span></a><br>
                                <a class="remove btn-xs btn-flat" style="cursor: pointer; cursor: hand;"><span
                                        class="label label-danger"><i
                                            class="fa fa-minus-square"></i> <b>REMOVE</b></span></a></td>

                        </tr>

                        </tbody>
                    </table>

            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default  btn-flat" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary  btn-flat" id="receive_form"><i class="fa "></i>  Submit</button>
            </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->



<script type="text/javascript">
    $(document).ready(function () {

        $('.mydate')
            .datepicker({
                dateFormat: "yy-mm-dd",
                maxDate: 0,
                beforeShow: function (textbox, instance) {
                    var txtBoxOffset = $(this).offset();
                    var top = txtBoxOffset.top;
                    var left = txtBoxOffset.left;
                    var textBoxWidth = $(this).outerWidth();
                    setTimeout(function () {
                        instance.dpDiv.css({
                            top: top - 100, //you can adjust this value accordingly
                            left: left + textBoxWidth//show at the end of textBox
                        });
                    }, 0);

                }
            });


        $(document).on("change", "#spare_part_category", function(event){


            var category_id=$(this).val();
            var closest=$(this).parent().parent().closest('.parts');
            console.log(closest);


            var siteurl="<?php echo base_url(); ?>";
            var url="<?php echo base_url(); ?>jobcard/getByCategoty/"+category_id;
            var drop_down='';

            $.ajax({
                type: "POST",
                url: url,
                beforeSend: function() {
                    // something
                },
                success: function(data)
                {

                    $("#parts").html('<option value="NULL" selected="selected">Select Spare Part</option>');
                    console.log(data.length);
                    if(data.length>=0){
                        $.each(data, function( key, val ) {
                            drop_down +="<option value='"+data[key]["id"]+"'>"+data[key]["description"]+"</option>";
                        });
                    }

                    $("#parts").append(drop_down);

                },error: function (jqXHR) {
                    console.log(jqXHR)

                }
            });



        });


        $(document).on("click", "#table .add", function(event){

            var thisRow = $(this).closest('tr');


            var template = $('#template');
            var cloned_object = template.clone(true).removeAttr('hidden').removeAttr('id');

            var multiple_row = thisRow.attr("row");
            var row_index = parseInt(multiple_row) + 1;
            //console.log(multiple_row)
            var new_row = cloned_object.attr("row", row_index);


            new_row.insertBefore(template);
            //console.log(new_row.find('input[name="quantity"]'));

            new_row.find('input[name="receive_quantity[]"]').removeAttr('disabled');
            new_row.find('select[name="receive_spare_parts[]"]').removeAttr('disabled');
            new_row.find('select[name="receive_category[]"]').removeAttr('disabled');
//            new_row.find('input[name="receive_date[]"]').removeAttr('disabled');

            new_row.slideDown();


        });

        $(document).on("click", "#table .remove", function(event){


            //console.log($('#table tbody tr').length);

            if ($('#table tbody tr').length === 2) return;
            $(this).parents("tr").fadeOut('slow', function () {
                $(this).remove();
            });

        });

        $(document).on("change", ".category", function(event){


            var category_id=$(this).val();
            var locator=$('option:selected', this);
            //console.log(locator.closest("tr").find(".parts"));


            var siteurl="<?php echo base_url(); ?>";
            var url="<?php echo base_url(); ?>jobcard/getByCategoty/"+category_id;

            $.ajax({
                type: "POST",
                url: url,
                beforeSend: function() {
                    // something
                },
                success: function(data)
                {
                    var drop_down='';
                    $(locator.closest("tr").find(".parts")).html('<option value="NULL" selected="selected">Select Spare Part</option>');
                    $.each(data, function( key, val ) {
                        drop_down +="<option value='"+data[key]["id"]+"'>"+data[key]["description"]+"</option>";
                    });
                    $(locator.closest("tr").find(".parts")).append(drop_down);

                },error: function (jqXHR) {
                    console.log(jqXHR)

                }
            });



        });


        function remove(data) {
            $.ajax({
                url: '<?php echo site_url('equipment/spare_part_data') ?>',
                type: 'POST',
                data: {data, 'action': 'remove'},
                success: function (data) {
                    table.ajax.reload();
                    swal.close();
                }
            });
        }


        $('#add').on('click', function () {
            $('#add-modal').modal('show');
        });


        $("#spare_part_form").find('[name="associated_equipment"]')
            .select2()
            // Revalidate the color when it is changed
            .change(function (e) {
                $('#spare_part_form').formValidation('revalidateField', 'associated_equipment');
            })
            .end()
            .formValidation({
                framework: 'bootstrap',
                // This option will not ignore invisible fields which belong to inactive panels
                excluded: ':hidden, :not(:visible)',
                fields: {
                    category: {
                        row: '.form-group-sm',
                        validators: {
                            notEmpty: {
                                message: '*required'
                            }
                        }
                    },
                    spare_part_name: {
                        row: '.form-group-sm',
                        validators: {
                            notEmpty: {
                                message: '*required'
                            }
                        }
                    },
                    associated_equipment: {
                        row: '.form-group-sm',
                        validators: {
                            notEmpty: {
                                message: '*required'
                            }
                        }
                    },
                    quantity: {
                        row: '.form-group-sm',
                        validators: {
                            notEmpty: {
                                message: '*required'
                            },
                            numeric: {
                                message: '*not a number'
                            },
                            greaterThan: {
                                value: 0,
                                message: '*not a valid number'
                            }
                        }
                    },
                }
            }).on('success.form.fv', function (e) {
            // Prevent form submission
            e.preventDefault();
            submit();

        });

        function submit() {
            var category = retrieveFormValues('category');
            var spare_part_name = retrieveFormValues('spare_part_name');
            var associated_equipment = retrieveFormValues('associated_equipment');
            var description = retrieveFormValues('description');
            var DT_RowId = retrieveFormValues('DT_RowId');

            $.ajax({
                url: $('#spare_part_form').attr('action'),
                type: 'POST',
                data: {
                    'category': category,
                    'spare_part_name': spare_part_name,
                    'associated_equipment': associated_equipment,
                    'description': description,
                    'DT_RowId': DT_RowId
                },
                beforeSend: function () {
                    $('#add-modal').modal('hide');
                },
                success: function (data) {
                    console.log(data);
                    $('#add-modal').on('hidden.bs.modal', function () {
                        $('#add-modal').formValidation('resetForm', true);
                    });
                    toastr.options = {
                        "closeButton": false,
                        "debug": false,
                        "newestOnTop": false,
                        "progressBar": false,
                        "positionClass": "toast-top-right",
                        "preventDuplicates": false,
                        "onclick": null,
                        "showDuration": "300",
                        "hideDuration": "1000",
                        "timeOut": "5000",
                        "extendedTimeOut": "1000",
                        "showEasing": "swing",
                        "hideEasing": "linear",
                        "showMethod": "fadeIn",
                        "hideMethod": "fadeOut"
                    };
                    swal("Good job!", data, "success");
                    setTimeout(document.location.replace("<?php echo base_url(); ?>equipment"), 1000);
//                    table.rows().deselect();
//                    table.ajax.reload();
                }
            });
        }

        function retrieveFormValues(name) {
            var dump;
            $.each($("input[name=" + name + "], select[name=" + name + "], textarea[name=" + name + "]"), function (i, v) {
                var theTag = v.tagName;
                var theElement = $(v);
                var theValue = theElement.val();
                dump = theValue;
            });
            return dump;
        }

        $('#receive').on('click', function () {
            $('#receive-modal').modal('show');
        });

        $("#receive_form").click(function(e) {

            var url="<?php echo base_url(); ?>equipment/receive_spare_part"; // the script where you handle the form input.
            var formData ='#receive_spare_part_form';
            var msg ='Successfully received Sparepart(s)';

//        console.log( $( '#jobcard' ).serialize() );
//        return;

            processData(formData,url,msg);

            e.preventDefault(); // avoid to execute the actual submit of the form.

        });

        function processData(formData,url,msg){
            $.ajax({
                type: "POST",
                url: url,
                data: $(formData).serialize(), // serializes the form's elements.
                success: function(data,status, jqXHR)
                {
                    swal("Good job!", msg, "success");//show response from the php script.
//
                    setTimeout(document.location.replace("<?php echo base_url(); ?>equipment"), 1000);
                    console.log(jqXHR);


                },error: function (jqXHR, status, err,data) {
//                    console.log(jqXHR);
                    toastr.error(jqXHR.responseText)
                }
            });

        }

        $(document).on("click", "#stock", function(event){


            var sparepart_id=$('#parts').val();
           // $("#ledger").html();


            var siteurl="<?php echo base_url(); ?>";
            var url="<?php echo base_url(); ?>jobcard/getAllsparepartStock/"+sparepart_id;

            $.ajax({
                type: "POST",
                url: url,
                success: function(data,status, jqXHR)
                {
                    $('#display').empty();


                    if(data.length>0){
                        for (var i = 0; i < data.length; i++) {

                            var sourcedate = data[i].date_created;
                            var location_id = data[i].location_id;

                            tr = $('<tr/>');
                            tr.append("<td class=''>" + sourcedate + "</td>");
                            tr.append("<td class='trrype'>" + data[i].transaction_type + "</td>");

                            tr.append("<td class='location' id='" + data[i].location_id + "'>" + data[i].location_id + "</td>");


                            tr.append("<td class='qty'>" + data[i].quantity + "</td>");
//                            tr.append("<td class='curqty'>" + data[i].quantity + "</td>");

                            $('#display').append(tr);


                        }
                    }else{
                        console.log(jqXHR);

                        $('#display').html( "<div style='margin:1%;font-size:2em;font-weight:400;'> &#128559; No data for the selected filters.</div>");

                    }



                },error: function (jqXHR, status, err,data) {
                    console.log(jqXHR);
                }
            });


        });



    });

</script>
