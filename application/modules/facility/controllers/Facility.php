<?php defined('BASEPATH') or exit('No direct script access allowed');

class Facility extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();
        if (!$this->ion_auth->logged_in()) {
            //redirect them to the login page
            redirect('users');
        }
        $this->load->module('users');
        $this->load->model('mdl_facility');
        $this->load->model('mdl_facility_ext');
        $this->load->model('users/mdl_location', 'location');
        $this->load->model('store/mdl_store');
    }

    public function index()
    {
        $data['title'] = 'Configurations';
        $data['subtitle'] = 'View Facilities';
        $data['module'] = 'facility';
        $data['view_file'] = 'facility/index';

        echo Modules::run('templates', $data);
    }

    public function edit($id = null)
    {
        $data['title'] = 'Configurations';
        $data['subtitle'] = 'Edit Facility';
        $data['module'] = 'facility';
        $data['view_file'] = 'facility/edit';
        $data['id'] = (is_numeric($id)) ? $id : null;

        if (!$this->ion_auth->logged_in() || (!$this->ion_auth->is_admin())) {
            redirect('users', 'refresh');
        }

        $selected_facility = $this->mdl_facility->get($id);

        //validate form input
        $this->form_validation->set_rules('women_population', 'Women Population', 'required');
        $this->form_validation->set_rules('under_one_population', 'Under One Population', 'required');
        $this->form_validation->set_rules('total_population', 'Total Population', 'required');

        if (isset($_POST) && !empty($_POST)) {
            // do we have a valid request?
            if ($id !== $this->input->post('id')) {
                $this->session->set_flashdata('error_message', 'An error has occurred.');
                redirect('facility', 'refresh');
            }

            if ($this->form_validation->run() === true) {
                $data = array(
                    'women_population' => $this->input->post('women_population'),
                    'under_one_population' => $this->input->post('under_one_population'),
                    'total_population' => $this->input->post('total_population'),
                );


                //check to see if we are updating the user
                if ($this->mdl_facility->update($selected_facility->id, $data, true)) {

                    //redirect them back to the admin page if admin, or to the base url if non admin
                    $this->session->set_flashdata('success_message', 'Facility updated successfully.');
                    if ($this->ion_auth->is_admin()) {
                        redirect('facility', 'refresh');
                    } else {
                        redirect('/', 'refresh');
                    }
                } else {
                    //redirect them back to the admin page if admin, or to the base url if non admin
                    $this->session->set_flashdata('error_message', 'An error has occurred.');
                    if ($this->ion_auth->is_admin()) {
                        redirect('facility', 'refresh');
                    } else {
                        redirect('/', 'refresh');
                    }
                }
            }
        }

        //pass the facility to the view
        $data['selected_facility'] = $selected_facility;

        if (!is_null($selected_facility)) {
            $data['facility_name'] = array(
                'name' => 'facility_name',
                'id' => 'facility_name',
                'type' => 'text',
                'class' => 'form-control',
                'disabled' => '',
                'value' => $this->form_validation->set_value('county_name', $selected_facility->facility_name),
            );
            $data['women_population'] = array(
                'name' => 'women_population',
                'id' => 'women_population',
                'type' => 'text',
                'class' => 'form-control',
                'value' => $this->form_validation->set_value('women_population', $selected_facility->women_population),
            );
            $data['under_one_population'] = array(
                'name' => 'under_one_population',
                'id' => 'under_one_population',
                'type' => 'text',
                'class' => 'form-control',
                'value' => $this->form_validation->set_value('under_one_population', $selected_facility->under_one_population),
            );
            $data['total_population'] = array(
                'name' => 'total_population',
                'id' => 'total_population',
                'type' => 'text',
                'class' => 'form-control',
                'value' => $this->form_validation->set_value('total_population', $selected_facility->total_population),
            );
        }

        echo Modules::run('templates', $data);
    }


    public function facility_data()
    {
        //retrieve facility data array

        $user_info = $this->users->user_info();
        if ($user_info->level_id == 1) {
            $facilities = $this->mdl_facility->get_all();
        } elseif ($user_info->level_id == 2) {
          $station = json_decode($user_info->json_location)->region;
          $facilities = $this->mdl_facility->get_by_region($station);
        } elseif ($user_info->level_id == 4) {
          $station = json_decode($user_info->json_location)->subcounty;
          $facilities = $this->mdl_facility->get_by_subcounty($station);
        }

        $output['data'] = $facilities;
        echo json_encode($output);
    }

    public function facility_dropdown()
    {
        $facilities = $this->mdl_facility->dropdown('facility_name');
        asort($facilities);
        return $facilities;
    }

    /** Get facility by subcounty */
    public function get_by_subcounty($subcounty_id = false)
    {
        if (isset($_POST) && !empty($_POST)) {
            if ($this->input->post('id') && is_numeric($this->input->post('id'))) {
                $id = $this->input->post('id');
                $data = $this->mdl_facility->get_many_by('subcounty_id', $id);

                if (!is_null($data)) {
                    echo json_encode($data);
                }
            }
        } else {
            $id = $subcounty_id;
            $data = $this->mdl_facility->get_many_by('subcounty_id', $id);

            if (!is_null($data)) {
                return $data;
            }
        }
    }

    public function get_by_subcounty_dropdown($subcounty_id = false)
    {
        if ($subcounty_id && is_numeric($subcounty_id)) {
            $id = $subcounty_id;
            $data = $this->mdl_facility_ext->get_by_field_dropdown('subcounty_id', $id);

            if (!is_null($data)) {
                return $data;
            }
        }
    }

    public function upload_excel()
    {

        //load the excel library
        header('Content-Type: text/html; charset=UTF-8');
        ini_set('memory_limit', '-1');
        $this->load->library('excel');

        $inputFileName = FCPATH.'docs/CCI.xlsx';
        $file_name = time() . '.xlsx';

        $ext = pathinfo($inputFileName, PATHINFO_EXTENSION);


        if ($ext == 'xls') {
            $excel2 = PHPExcel_IOFactory::createReader('Excel5');
        } elseif ($ext == 'xlsx') {
            $excel2 = PHPExcel_IOFactory::createReader('Excel2007');
        } else {
            die('Invalid file format given' . $_FILES['file']);
        }

        $excel2 = $objPHPExcel = PHPExcel_IOFactory::load($inputFileName);

        $sheet = $objPHPExcel -> getSheet(0);
        $highestRow = $sheet -> getHighestRow();
        $highestColumn = $sheet -> getHighestColumn();
        $temp = array();//exit;

        $this->load->dbforge();

      // Produces: DROP TABLE IF EXISTS table_name
        $this->dbforge->drop_table('dummy2', true);

//         $fields = array(
//
//         'facility_name' => array(
//                 'type' => 'VARCHAR',
//                 'constraint' => '100',
//
//         ),
//         'level' => array(
//                 'type' =>'VARCHAR',
//                 'constraint' => '100',
//                 'null' => TRUE,
//         ),
//         'unit_id' => array(
//           'type' =>'VARCHAR',
//           'constraint' => '100',
//           'null' => TRUE,
//         ),
//         'is_facility' => array(
//           'type' =>'VARCHAR',
//           'constraint' => '100',
//           'null' => TRUE,
//         ),
//
//
// );
//         $attributes = array('ENGINE' => 'InnoDB');
//         $this->dbforge->add_field($fields);
//
//         $this->dbforge->create_table('dummy2', false, $attributes);

         echo '<pre>',print_r($excel2),'</pre>';exit;

      //$array_code=array();
            for ($row = 2; $row <= $highestRow; $row++) {
                $rowData = $objPHPExcel -> getActiveSheet() -> rangeToArray('A' . $row . ':' . $highestColumn . $row, null, true, false);

                foreach ($rowData as $key => $value) {
                    $facilityArray = array('facility_name'=>$value[2],'level'=>$value[1],'unit_id'=>$value[0],'is_facility'=>$value[7]

          );
                }
                $this->db->insert('dummy2', $facilityArray);
                echo $row ,' - ', memory_get_usage() ,'<br />';

                // echo '<pre>',print_r($facilityArray),'</pre>';

            }

    }

    public function add()
    {

        $this->form_validation->set_rules('facilityname', 'Facility Name', 'required');
        // $this->form_validation->set_rules('facilityunitid', 'Facility Organization Unit ID', 'required');
        $this->form_validation->set_rules('mfl', 'MFL', 'required');
        $this->form_validation->set_rules('status', 'Immunizing status', 'required');
        $this->form_validation->set_rules('catchment_pop', 'Catchment population', 'required');
        $this->form_validation->set_rules('live_birth_pop', 'Live Birth Population', 'required');
        $this->form_validation->set_rules('pop_pregnant_women', 'Pregnant women Population', 'required');
        $this->form_validation->set_rules('pop_surviving_infants', 'Surviving Infant Population', 'required');
        $this->form_validation->set_rules('pop_adolescent_girls', 'Adolescent Girl Population', 'required');

        if ($this->form_validation->run() == FALSE)
        {
            echo validation_errors();
            http_response_code(401);
            return;
        }


        $user_info = $this->users->user_info();
        $location_data = $user_info ->json_location;

        //check if facility exists.

        //

        //save facility

        $facilitydata = array(

            'facility_name' => $this->input->post('facilityname'),
            'organisationunitid' => $this->input->post('facilityunitid'),
            'subcounty_id' => (int)json_decode($user_info->json_location)->subcounty,
            'region_id' => (int)json_decode($user_info->json_location)->region,
            'county_id' => (int)json_decode($user_info->json_location)->county,
            'mfl' => $this->input->post('mfl'),
            'immunizing_status' => $this->input->post('status'),
            'catchment_population' => $this->input->post('catchment_pop'),
            'live_birth_population' => $this->input->post('live_birth_pop'),
            'women_population' => $this->input->post('pop_pregnant_women'),


        );

        $facility_id = $this->mdl_facility->_insert($facilitydata);
        // echo '<pre>',print_r($facility_id),'</pre>';exit;

        //save location

        $location_data = array(
            'national' => (int)json_decode($user_info->json_location)->national,
            'region' => (int)json_decode($user_info->json_location)->region,
            'county' => (int)json_decode($user_info->json_location)->county,
            'subcounty' => (int)json_decode($user_info->json_location)->subcounty,
            'facility' =>$facility_id,
        );
        $location_final = array(
            'json_location' => json_encode($location_data),
            'location_name' => $this->input->post('facilityname'),
            'nation_id' => (int)json_decode($user_info->json_location)->national,
            'region_id' => (int)json_decode($user_info->json_location)->region,
            'county_id' => (int)json_decode($user_info->json_location)->county,
            'subcounty_id' => (int)json_decode($user_info->json_location)->subcounty,
            'facility_id' =>$facility_id,
        );

        $location_id = $this->location->_insert($location_final);



          //save store

        $storedata = array(

          'name' => $this->input->post('facilityname'),
          'location_id' => $location_id,
          'catchment_population' => $this->input->post('catchment_pop'),
          'live_birth_population' => $this->input->post('live_birth_pop'),
          'pop_pregnant_women' => $this->input->post('pop_pregnant_women'),
          'pop_surviving_infants' => $this->input->post('pop_surviving_infants'),
          'pop_adolescent_girls' => $this->input->post('pop_adolescent_girls')

        );

        $this->mdl_store->insert($storedata);

        // echo '<pre>',print_r($location_data),'</pre>';exit;

    }

}
