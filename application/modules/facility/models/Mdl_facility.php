<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mdl_facility extends MY_Model
{

    public $_table = 'facilities';

    public function __construct()
    {
        parent::__construct();
    }

    public function get_table()
    {
        $table = 'facilities';

        return $table;
    }
    public function get_all()
    {
        $table = $this->get_table();
        $this->db->order_by('facility_name', 'asc');
        $query = $this->db->get($table);

        return $query->result();
    }

    public function get_population($station)
    {
        $this->db->select('under_one_population as population');
        $this->db->from('facilities');
        $this->db->where('id', $station);
        $this->db->limit('1');
        $query = $this->db->get();
        return $query->result();
    }

    public function get_by_subcounty($station)
    {
        $this->db->select('');
        $this->db->from('facilities');
        $this->db->where('subcounty_id', $station);
        $this->db->where('immunizing_status', 'Yes');
        $this->db->group_by('facility_name');
        $query = $this->db->get();
        return $query->result();
    }

    public function get_by_region($station)
    {
        $this->db->select('');
        $this->db->from('facilities');
        $this->db->where('region_id', $station);
        $this->db->where('immunizing_status', 'Yes');
        $this->db->group_by('facility_name');
        $query = $this->db->get();
        return $query->result();
    }

    public function _insert($data)
    {
        $this->db->insert('facilities',$data);
        $insert_id = $this->db->insert_id();
        return  $insert_id;
    }

}
