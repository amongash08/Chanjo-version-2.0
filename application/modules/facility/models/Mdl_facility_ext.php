<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mdl_facility_ext extends CI_Model
{

    public $_table = 'facilities';

    public function __construct()
    {
        parent::__construct();
    }

    public function get_by_field_dropdown($field, $id)
    {
        $query = $this->db->select('id, facility_name')
            ->where($field, $id)->where('immunizing_status', 'Yes')->group_by('facility_name')
            ->get($this->_table);
        $options = array();
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $options[$row->id] = $row->facility_name;
            }
            return $options;
        }
        return false;
    }


}
