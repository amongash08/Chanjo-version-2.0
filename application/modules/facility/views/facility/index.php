<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<!-- DataTables -->
<link href="<?php echo base_url() ?>assets/plugins/datatables/datatables.css" rel="stylesheet"/>
<script src="<?php echo base_url() ?>assets/plugins/datatables/datatables.js"></script>
<div class="box">
	<div class="box-header" >
	<h4 class="box-data">Below is a list of Facilities</h4>
	</div>
	<!-- /.box-header -->
	<div class="box-footer" style="padding:0">
	<!-- <a href="<?php echo base_url(); ?>facility/create" class="btn bg-navy margin">Add Facility</a> -->
	<button type="button" class="btn btn-sm bg-primary btn-flat margin pull-right" data-toggle="modal" data-target="#modal-addfacility">
	Add Facility <i class="fa fa-stethoscope"></i>  </button>

	</div>
	<div class="box-body">
			<table id="facilities" class="table table-bordered table-striped" cellspacing="0" width="100%">
					<thead>
							<tr>
								<th>Facility Name</th>
								<th>Total population</th>
								<th>Population Under One</th>
								<th>Women Population</th>
								<th align="center">Action</th>
						</tr>
					</thead>
			<tbody>

			</tbody>
				<tfoot>
					<tr>
						<th>Facility Name</th>
						<th>Total population</th>
						<th>Population Under One</th>
						<th>Women Population</th>
						<th align="center">Action</th>
					</tr>
				</tfoot>
			</table>
	</div>
	<!-- /.box-body -->


</div>

<div class="modal fade" id="modal-addfacility" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-lg" role="document">
	<div class="modal-content">
	<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	<h4 class="modal-title">Add Facility</h4>
	</div>
	<div class="modal-body">
				<form action="" method="post" id="add-Facility" class="form-horizontal">

				<div class="row form-group has-feedback">
					<div class="col-xs-6">

						<label>Facility Name</label>
						<input type="text" class="form-control" name="facilityname" id="facilityname">
					</div>

				<div class="col-xs-6">
					<label>Facility Organization Unit ID</label>
					<input type="text" class="form-control" name="facilityunitid" id="facilityunitid">
				</div>


				</div>

				<div class="row form-group has-feedback">
					<div class="col-xs-6">
						<label>Facility MFL</label>
						<input type="text" class="form-control" name="mfl" id="mfl">

					</div>
				<div class="col-xs-6">
					<label>Immunizing</label>
						<select id="status" name="status" class=" form-control custom-select">
						<option value="Yes"> Yes </option>
						<option value="No"> No </option>
						</select>
				</div>


				</div>

				<div class="row form-group has-feedback">

				<div class="col-xs-6">

				<label>Catchment Population</label>
				<input type="text" class="form-control" name="catchment_pop" id="catchment_pop">

				</div>

				<div class="col-xs-6">

				<label>Live Birth Population</label>
				<input type="text" class="form-control" name="live_birth_pop" id="live_birth_pop">

				</div>


				</div>

				<div class="row form-group has-feedback">

				<div class="col-xs-6">

				<label>Pregnant Women Population</label>
				<input type="text" class="form-control" name="pop_pregnant_women" id="pop_pregnant_women">

				</div>

				<div class="col-xs-6">

				<label>Surviving Infant Population</label>
				<input type="text" class="form-control" name="pop_surviving_infants" id="pop_surviving_infants">

				</div>


				</div>

				<div class="row form-group has-feedback">

				<div class="col-xs-6">

				<label>Adolescent Girl Population</label>
				<input type="text" class="form-control" name="pop_adolescent_girls" id="pop_adolescent_girls">

				</div>


				</div>

				<div class="box-footer" style="padding:10px 10px 0 0;">
				<!-- Previous/Next buttons -->
				<button type="button" class="btn btn-default  btn-flat margin pull-right" data-dismiss="modal">Close</button>
				<button type="submit" class="btn btn-primary  btn-flat margin pull-right" id="addFacility"><i class="fa fa-plus"></i>  Add</button>
				</div>
				<!-- /.box-footer -->

				</div>

				</form>

	</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- /.box -->

<div class="modal fade" id="modal-editfacility" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-lg" role="document">
	<div class="modal-content">
	<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	<h4 class="modal-title">Edit Facility</h4>
	</div>
	<div class="modal-body">
				<form action="" method="post" id="add-Facility" class="form-horizontal">

				<div class="row form-group has-feedback">
					<div class="col-xs-6">

						<label>Facility Name</label>
						<input type="text" class="form-control" name="edit_facilityname" id="edit_facilityname">
					</div>

				<div class="col-xs-6">
					<label>Facility Organization Unit ID</label>
					<input type="text" class="form-control" name="edit_facilityunitid" id="edit_facilityunitid">
				</div>


				</div>

				<div class="row form-group has-feedback">
					<div class="col-xs-6">
						<label>Facility MFL</label>
						<input type="text" class="form-control" name="edit_mfl" id="edit_mfl">

					</div>
				<div class="col-xs-6">
					<label>Immunizing</label>
						<select id="edit_status" name="edit_status" class=" form-control custom-select">
						<option value="Yes"> Yes </option>
						<option value="No"> No </option>
						</select>
				</div>


				</div>

				<div class="row form-group has-feedback">

				<div class="col-xs-6">

				<label>Catchment Population</label>
				<input type="text" class="form-control" name="edit_catchment_pop" id="edit_catchment_pop">

				</div>

				<div class="col-xs-6">

				<label>Live Birth Population</label>
				<input type="text" class="form-control" name="edit_live_birth_pop" id="edit_live_birth_pop">

				</div>


				</div>

				<div class="row form-group has-feedback">

				<div class="col-xs-6">

				<label>Pregnant Women Population</label>
				<input type="text" class="form-control" name="edit_pop_pregnant_women" id="edit_pop_pregnant_women">

				</div>

				<div class="col-xs-6">

				<label>Surviving Infant Population</label>
				<input type="text" class="form-control" name="edit_pop_surviving_infants" id="edit_pop_surviving_infants">

				</div>


				</div>

				<div class="row form-group has-feedback">

				<div class="col-xs-6">

				<label>Adolescent Girl Population</label>
				<input type="text" class="form-control" name="edit_pop_adolescent_girls" id="edit_pop_adolescent_girls">

				</div>


				</div>

				<div class="box-footer" style="padding:10px 10px 0 0;">
				<!-- Previous/Next buttons -->
				<button type="button" class="btn btn-default  btn-flat margin pull-right" data-dismiss="modal">Close</button>
				<button type="submit" class="btn btn-primary  btn-flat margin pull-right" id="editFacility"><i class="fa fa-plus"></i>  Add</button>
				</div>
				<!-- /.box-footer -->

				</div>

				</form>

	</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<script type="text/javascript">
	$(document).ready(function () {
		$("#facilities").DataTable({

			serverSide: false,
			ajax: {
				url: "/facility/facility_data",
				type: "POST"
			},
			columns: [
				{data: "facility_name"},
				{data: "women_population"},
				{data: "under_one_population"},
				{data: "total_population"},

				{
			data: {},
			render: function (data, type, full) {
				//console.log(data);
			return '<button type="button"  class="btn btn-sm btn-flat bg-maroon edit editFacility" id="'+data['id']+'" data-facility="'+data['facility_name']+'" data-live_birth_population="'+data['live_birth_population']+'" ><i class="fa fa-edit"></i> Edit </button>';
				}

			}
		]
	});

	$("#addFacility").click(function(e) {

			var url="<?php echo base_url(); ?>facility/add"; // the script where you handle the form input.
			var formData ='#add-Facility';
			var msg ='Successfully added a Facility';

//        console.log( $( '#myfridge' ).serialize() );
//        return;

			processData(formData,url,msg);

			e.preventDefault(); // avoid to execute the actual submit of the form.

	});

	$(document).on("click", ".editFacility", function(event){

		var facility_id=$(this).closest('tr').find('.edit').attr('id');
		var facility_name=$(this).closest('tr').find('.edit').attr('data-facility');


		console.log(facility_name);

		});

	function processData(formData,url,msg){
			$.ajax({
					type: "POST",
					url: url,
					data: $(formData).serialize(), // serializes the form's elements.
					success: function(data,status, jqXHR)
					{
							swal("Good job!", msg, "success");//show response from the php script.
//
							setTimeout(location.reload.bind(location), 1000);
							 console.log(jqXHR);


					},error: function (jqXHR, status, err,data) {
                   // console.log(jqXHR);
							toastr.error(jqXHR.responseText)
					}
			});

	}

	});

</script>
