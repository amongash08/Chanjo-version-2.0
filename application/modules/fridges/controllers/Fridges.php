<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Fridges extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        if (!$this->ion_auth->logged_in()) {
            //redirect them to the login page
            redirect('users');
        }
        $this->load->module('users');
        $this->load->module('region');
        $this->load->module('county');
        $this->load->module('subcounty');
        $this->load->module('facility');
        $this->load->model('mdl_fridges');

    }

    public function add()
    {

        $this->form_validation->set_rules('model', 'Fridge Model', 'required');
        $this->form_validation->set_rules('power_source', 'Power Source', 'required');
        $this->form_validation->set_rules('status', 'Status', 'required');
        $this->form_validation->set_rules('temp_monitor_no', 'Temprature Monitoring Number', 'required');
        $this->form_validation->set_rules('store_id', 'Store ID', 'required');
        $this->form_validation->set_rules('installation_year', 'Installation Year', 'required');
        $this->form_validation->set_rules('alias', 'Alias', 'required');

        if ($this->form_validation->run() == FALSE)
        {
            echo validation_errors();
            http_response_code(401);
            return;
        }

        $user_info = $this->users->user_info();
        $region=json_decode($user_info->json_location)->region;
        $county=json_decode($user_info->json_location)->county;
        $subcounty=json_decode($user_info->json_location)->subcounty;
        $facility=json_decode($user_info->json_location)->facility;
        $random= mt_rand();

        $uniqueeqID= $random.'-AK-'.$region.'-'.$county.'-'.$subcounty.'-'.$facility ;


        $data = array(
            'temperature_monitor_no' => $this->input->post('temp_monitor_no'),
            'fridge_id' => $this->input->post('model'),
            'equipment_id' => $uniqueeqID,
            'store_id' => $this->input->post('store_id'),
            'power_source' => $this->input->post('power_source'),
            'fridge_status' => $this->input->post('status'),
            'installation_year' => $this->input->post('installation_year'),
            'fridge_alias' => $this->input->post('alias')

        );
        //echo '<pre>',print_r($data),'</pre>';exit;

        $this->mdl_fridges->insert_store_fridge($data);

    }

    public function edit()
    {

        $this->form_validation->set_rules('edit_power_source', 'Power Source', 'required');
        $this->form_validation->set_rules('edit_alias', 'Alias', 'required');
        $this->form_validation->set_rules('edit_temp_monitor_no', 'Temprature Monitoring Number', 'required');
        //$this->form_validation->set_rules('edit_installation_year', 'Installation Year', 'required');

        if ($this->form_validation->run() == FALSE)
        {
            echo validation_errors();
            http_response_code(401);
            return;
        }

        $data = array(
            'id' => $this->input->post('edit_fridge_id'),
            'temperature_monitor_no' => $this->input->post('edit_temp_monitor_no'),
            'power_source' => $this->input->post('edit_power_source'),
            'fridge_alias' => $this->input->post('edit_alias')
            //'installation_year' => $this->input->post('edit_installation_year')

        );

        $this->mdl_fridges->update_store_fridge($data);
        //echo '<pre>',print_r($data),'</pre>';exit;



    }

    public function getById($id)
    {
        header('Content-Type: application/json');

        $fridges=$this->mdl_fridges->get_by_store($id);

        if (count($fridges)==0){

            echo 'No Fridges';
//            http_response_code(401);
//            return;

        }else{



            echo json_encode($fridges);


        }



    }

    public function addStoreFridge()
    {

        $this->form_validation->set_rules('model2', 'Fridge Model', 'required');
        $this->form_validation->set_rules('power_source2', 'Power Source', 'required');
        $this->form_validation->set_rules('status2', 'Status', 'required');
        $this->form_validation->set_rules('temp_monitor_no2', 'Temprature Monitoring Number', 'required');
        $this->form_validation->set_rules('store_id2', 'Store ID', 'required');
        $this->form_validation->set_rules('installation_year2', 'Installation Year', 'required');
        $this->form_validation->set_rules('alias2', 'Alias', 'required');

        if ($this->form_validation->run() == FALSE)
        {
            echo validation_errors();
            http_response_code(401);
            return;
        }

        $user_info = $this->users->user_info();
        $region=json_decode($user_info->json_location)->region;
        $county=json_decode($user_info->json_location)->county;
        $subcounty=json_decode($user_info->json_location)->subcounty;
        $facility=json_decode($user_info->json_location)->facility;
        $random= mt_rand();

        $uniqueeqID= $random.'-AK-'.$region.'-'.$county.'-'.$subcounty.'-'.$facility ;

        //echo '<pre>',print_r($region),'</pre>';exit;

        $data = array(
            'temperature_monitor_no' => $this->input->post('temp_monitor_no2'),
            'fridge_id' => $this->input->post('model2'),
            'equipment_id' => $uniqueeqID,
            'store_id' => $this->input->post('store_id2'),
            'power_source' => $this->input->post('power_source2'),
            'fridge_status' => $this->input->post('status2'),
            'installation_year' => $this->input->post('installation_year2'),
            'fridge_alias' => $this->input->post('alias2')

        );
        //echo '<pre>',print_r($data),'</pre>';exit;

        $this->mdl_fridges->insert_store_fridge($data);

    }
    


    public function submit()
    {
       
    }

    public function delete($id)
    {

        $this->mdl_fridges->delete($id);

    }


}
