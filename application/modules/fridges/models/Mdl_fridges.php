<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Mdl_fridges extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function get_table()
    {
        $table = 'fridges';

        return $table;
    }
    public function get_all()
    {
        $table = $this->get_table();
        $this->db->select('id,model,manufacturer,`Technology Type` as technology_type');
        $query = $this->db->get($table);

        return $query->result();
    }

    public function get_store_fridge_by_id($id)
    {
        $this->db->select('store_fridges.id as id ,equipment_id,store_id,temperature_monitor_no,fridge_status,power_source,installation_year,
        delete_status,Model,`Freezer_capacity` as fridge_capacity,`Vaccine storage volume (L)` as fridge_volume');
        $this->db->from('store_fridges');
        $this->db->join('fridges', 'fridges.id = store_fridges.fridge_id');
        $this->db->where('store_fridges.id', $id);
        $query = $this->db->get();

        return $query->result();
    }

    public function get_by_store($store_id)
    {
        $query = $this->db->select('store_fridges.id,store_id as store_id,fridges.id as fridge_id,model,manufacturer,`Technology Type` as technology_type
        ,equipment_id,temperature_monitor_no,power_source,fridge_status,installation_year,fridge_alias')
        ->from('store_fridges')
        ->join('fridges', 'fridges.id = store_fridges.fridge_id')
        ->where('store_id', $store_id)->where('delete_status', 1)
        ->get();
        return $query->result();

    }

    function insert_store_fridge($data)
    {

        $this->db->insert('store_fridges',$data);
    }

    function update_store_fridge($data)
    {
        $this->db->set('temperature_monitor_no', $data['temperature_monitor_no']);
        $this->db->set('power_source', $data['power_source']);
        $this->db->set('fridge_alias', $data['fridge_alias']);
        //$this->db->set('installation_year', $data['installation_year']);
        $this->db->where('id', $data['id']);
        $this->db->update('store_fridges');


    }
    function delete($id)
    {

        $this->db->set('delete_status', 0);
        $this->db->where('id', $id);
        $this->db->update('store_fridges');
    }

    function update_fridge_status($id,$status)
    {
        $this->db->set('fridge_status', $status);
        $this->db->where('id', $id);
        $this->db->update('store_fridges');


    }



}
