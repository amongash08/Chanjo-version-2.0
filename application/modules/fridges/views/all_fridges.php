<style>


</style>
<div class="">
    <div style="" id="reports_display">
        <table  class="table table-bordered table-hover table-striped" id="" >
            <thead style="background-color: white">
            <tr>
                <th>Refrigerator Model</th>
                <th>Manufacturer</th>
                <th>Temp. Monitor No.</th>
                <th>Main Power Source</th>
                <th>Year of Installation</th>
                <th>Status</th>
                <th>Action</th>

            </tr>
            </thead>

            <tbody>

            <?php

            foreach ($fridges as $key => $value) {

                if($value->refrigerator_status=='Functional'){

                    $label='label-success';

                }else{

                    $label='label-danger';

                }



                ?>
                <tr style="">

                    <td ><?php echo $value->Model; ?></td>
                    <td ><?php echo $value->Manufacturer; ?></td>
                    <td ><?php echo $value->temperature_monitor_no; ?></td>
                    <td ><?php echo $value->main_power_source; ?></td>
                    <td ><?php echo $value->age; ?></td>
                    <td ><span class="label <?php echo $label; ?>"><?php echo $value->refrigerator_status; ?></span>
                        </td>
                    <td ><button type="button" class="btn btn-xs btn-flat bg-purple">
                            <i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></td>




                </tr>
                <?php
            }
            ?>


            </tbody>
        </table>
    </div>

</div>

<script>



</script>
