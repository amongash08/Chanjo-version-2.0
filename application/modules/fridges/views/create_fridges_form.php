<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
  <div class="row">
    <div class="col-lg-4 col-lg-offset-4">
   
      <h1>Add Fridge</h1>
      <?php echo form_open('fridges/submit', array('class' => 'form-horizontal')); ?>
      <div class="form-group">
        <?php
        echo form_label('Select Make/Model', 'Model');
        echo '<br>';
        echo form_error('Model');
        echo form_dropdown('Model', $array, $Model, 'id="Model" class="form-control"');
        ?>
      </div>
      <button class="btn btn-lg btn-danger btn-block" name="submit" type="submit">Submit</button>

      <?php 
      if (isset($update_id)) {
          echo form_hidden('update_id', $update_id);
      }
      echo form_close(); ?>
    </div>
  </div>
