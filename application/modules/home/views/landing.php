<!DOCTYPE html>
<html>
<head>
    <meta charset='utf-8' />
    <meta name='viewport' content='initial-scale=1,maximum-scale=1,user-scalable=no' />

    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url() ?>assets/images/favicons/favicon-16x16.png">
    <link rel="manifest" href="<?php echo base_url() ?>assets/images/favicons/manifest.json">

    <meta name="theme-color" content="#ffffff">
    <title>CHANJO eLMIS</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/ionicons/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/AdminLTE.min.css">
    <!-- Select2 -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/select2/select2.min.css">
    <!-- DataTables -->
    <!-- FormValidation CSS file -->



    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script async src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script async src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>

    <![endif]-->

    <!-- Google Font -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    <style>
        body { margin:0; padding:0; }
        /*#map { position:relative; top:0; bottom:0; width:100%; }*/

        .user-header {
            height: auto !important;
        }
        .main-header .navbar .nav>li>a>.label {
            font-size: 12px !important;
        }

        .mapboxgl-popup {
            max-width: 400px;
            font: 12px/20px 'Helvetica Neue', Arial, Helvetica, sans-serif;
        }
        .content-wrapper, .right-side, .main-footer{
            margin:0;
        }
        .btn{

            -webkit-border-radius: 40px;
            -moz-border-radius: 40px;
            border-radius: 40px;
        }
        .parallax {
            /* The image used */
            background-image: url("../assets/images/parallax-min.jpg");

            /* Set a specific height */
            height: 500px;

            /* Create the parallax scrolling effect */
            background-attachment: fixed;
            background-position: center;
            background-repeat: no-repeat;
            background-size: cover;
            /*opacity: 0.5;*/
            /*filter: alpha(opacity=100);*/
        }
        .h-large{
            font-size: 6em;
            font-weight: 300;
        }
    </style>

</head>

<!-- the fixed layout is not compatible with sidebar-mini -->
<body class="hold-transition skin-black-light fixed sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

    <header class="main-header">
        <!-- Logo -->
        <a href="<?php echo base_url(); ?>county/home" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini"><b>CHANJO </b>eLMIS</span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg"><b>CHANJO eLMIS</b><img src="<?php echo base_url() ?>assets/images/coat_of_arms.png" width="45" height="40" /></span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top">


            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">

                    <li class="user user-menu">

                        <a href="<?php echo base_url(); ?>users" class="">
                            <span class="hidden-xs" style="text-transform: capitalize;">
                                <i class="fa fa-user" style="padding-right: 1rem;"></i> Sign In</span>
                        </a>

                    </li>

                </ul>
            </div>
        </nav>
    </header>

    <!-- =============================================== -->


    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper parallax">
        <!-- Content Header (Page header) -->
        <section class="">
            <h1>

                <small></small>
            </h1>

        </section>

        <!-- Main content -->
        <section class="content " style="padding-top: 0;">

            <div class="row">


                <div class="col-md-12">

                    <div style="padding: 5% 10% 0 10%; color:#ffffff;margin: auto;text-align: center">
                        <h1 class="h-large wow bounceInUp" data-wow-duration="2s">CHANJO eLMIS</h1>

                        <p class="wow bounceInUp" style="font-size: 3em;" data-wow-duration="2s">SUPPLY CHAIN | COLD CHAIN | ANALYTICS | DECISIONS</p>
                        <p style="margin-top: 3vh;">

                            <a class="btn bg-purple btn-flat margin wow slideInLeft btn-lg " role="button" href="<?php echo base_url(); ?>users/create_user"> Sign Up</a>
                            <a class="btn btn-success btn-flat margin wow slideInLeft btn-lg " style="margin: 0 3px 0 6px" role="button" href="<?php echo base_url(); ?>users">Sign In</a>
                        </p>
                        <p></p>

                        <p> <a class="btn btn-default btn-flat margin wow slideInLeft btn-lg " style="" role="button" href="<?php echo base_url(); ?>users/forgot_password">Forgot Password</a>
                        </p>
                    </div>

                </div>
            </div>




        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <footer class="footer" style="padding: 15px;">

        <strong>Copyright &copy; 2017 <a href=""></a>.</strong> All rights reserved.
    </footer>

    <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery UI CSS -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/jquery-ui-1.12.1/jquery-ui.min.css">
<!-- AdminLTE Skins. Choose a skin from the css/skins
     folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/skins/_all-skins.min.css">

<link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/wow/css/libs/animate.css">

<!-- jQuery 3.1.1 -->
<script  src="<?php echo base_url() ?>assets/plugins/jQuery/jquery-3.1.1.js"></script>
<!-- jQuery UI 1.21.1 -->
<script  src="<?php echo base_url() ?>assets/plugins/jquery-ui-1.12.1/jquery-ui.js"></script>
<!-- Bootstrap 3.3.6 -->
<script  src="<?php echo base_url() ?>assets/bootstrap/js/bootstrap.min.js"></script>

<script  src="<?php echo base_url() ?>assets/plugins/wow/dist/wow.min.js"></script>


<!--<script src="--><?php //echo base_url(); ?><!--assets/plugins/formvalidation/dist/js/framework/bootstrap.min.js"></script>-->
<!-- Select2 -->
<script src="<?php echo base_url(); ?>assets/plugins/select2/select2.full.min.js"></script>
<!-- moment -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<!-- Toastr -->
<script src="<?php echo base_url(); ?>assets/plugins/toastr/build/toastr.min.js"></script>
<!-- HighCharts -->
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/maps/modules/map.js"></script>

<script src="https://code.highcharts.com/maps/modules/exporting.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url(); ?>assets/js/adminlte.js"></script>
<!--<script src="--><?php //echo base_url(); ?><!--nodejs/node_modules/socket.io-client/dist/socket.io.js"></script>-->
<!--<script src="--><?php //echo base_url();?><!--nodejs/main.js" type="text/javascript"></script>-->

<script>
      new WOW().init();
//    var url="<?php ////echo base_url(); ?>//";
//    var x='';

//
//    $.getJSON( url+"docs/json/kenya.json", function( data ) {
//
//        $.getJSON( url+"docs/json/counties.json", function( counties ) {
//            //console.log(data);
//            //x=data;
//
//            // Initiate the chart
//            Highcharts.mapChart('map', {
//
//                title: {
//                    text: ''
//                },
//                chart: {
//                    backgroundColor: null
//                },
//
//                mapNavigation: {
//                    enabled: true,
//                    enableMouseWheelZoom:false,
//                    buttonOptions: {
//                        verticalAlign: 'bottom'
//                    }
//                },
//                tooltip: {
//                    formatter: function(){
////console.log(this.point);
//                        var c='';
//                        c += '<b>'+this.point.code+'</b><br/>';
//                        var county="Currently Using Chanjo";
//                        c += county;
//                        return c;
//                    },
//                    pointFormat: '{point.code}'
//                },
//
//                credits: {
//                    enabled: false
//                },
//                exporting: { enabled: false },
//                series: [{
//                    showInLegend: false,
//                    animation: true,
//                    data: counties,
//                    mapData: data,
//                    joinBy: ['COUNTY', 'code'],
//                    keys: ['COUNTY', 'code'],
//                    //name: 'County',
//                    color: '#5bc0de',
//                    states: {
//                        hover: {
//                            color: '#ff7f50'
//                        }
//                    },
//                    dataLabels: {
//                        enabled: false,
//                        format: '{point.code}',
//                        style: {
//                            fontFamily: 'Varela Round',
//                            fontColor:'#cccccc',
//                        },
//                    }
//                }]
//            });
//
//        });
//
//    });
//
//
//
//
//
</script>

</body>
</html>