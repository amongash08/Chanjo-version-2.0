<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Jobcard extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        if (!$this->ion_auth->logged_in()) {
            //redirect them to the login page
            redirect('users');
        }
        $this->load->module('users');
        $this->load->module('vaccine');
        $this->load->module('region');
        $this->load->module('county');
        $this->load->module('subcounty');
        $this->load->module('facility');
        $this->load->module('jobcard');
        $this->load->module('fridges');
        $this->load->module('store');
        $this->load->model('mdl_fridges');
        $this->load->model('mdl_store');
        $this->load->model('mdl_jobcard');

    }


    public function index()
    {
        $user_info = $this->users->user_info();
        $level=$user_info->level_id;
        $data['title'] = 'Jobcard';
        $data['page_title'] = 'Jobcard';
        $data['subtitle'] = 'Jobcard';
        $data['module'] = 'jobcard';
        $data['view_file'] = 'home';



        if ($level == 1){

            //get all open jobcards

            $data['sidebar']='nation_sidebar';
            $data['open_cards']=$this->mdl_jobcard->get_jobcards_national($status='open');

            //get all closed jobcards
            $data['closed_cards'] = $this->mdl_jobcard->get_jobcards_national($status='closed');
            $data['all_cards'] = $this->mdl_jobcard->get_all_jobcards_national();

        }

        if ($level == 2){

            //get all open jobcards
            $station=json_decode($user_info->json_location)->region;

            $data['sidebar']='region_sidebar';
            $data['open_cards']=$this->mdl_jobcard->get_jobcards_region($status='open',$station);

            //get all closed jobcards
            $data['closed_cards'] = $this->mdl_jobcard->get_jobcards_region($status='closed',$station);
            $data['all_cards'] = $this->mdl_jobcard->get_all_jobcards_region($station);

        }

        if ($level == 3){
            $station=json_decode($user_info->json_location)->county;
            $data['sidebar']='county_sidebar';
            $data['open_cards']=$this->mdl_jobcard->get_jobcards_county($status='open',$station);

            //get all closed jobcards
            $data['closed_cards'] = $this->mdl_jobcard->get_jobcards_county($status='closed',$station);
            $data['all_cards'] = $this->mdl_jobcard->get_all_jobcards_county($station);

        }

        if ($level == 4){
            $station=json_decode($user_info->json_location)->subcounty;
            $data['sidebar']='subcounty_sidebar';
            $data['open_cards']=$this->mdl_jobcard->get_jobcards_subcounty($status='open',$station);

            //get all closed jobcards
            $data['closed_cards'] = $this->mdl_jobcard->get_jobcards_subcounty($status='closed',$station);
            $data['all_cards'] = $this->mdl_jobcard->get_all_jobcards_subcounty($station);

        }




        echo Modules::run('templates', $data);

    }

    public function create()
    {
        $user_info = $this->users->user_info();
        $level=$user_info->level_id;
        $location = $user_info->location_id;

        $data['title'] = 'Jobcard';
        $data['page_title'] = 'Jobcard';
        $data['subtitle'] = 'Jobcard';
        $data['module'] = 'jobcard';
        $data['view_file'] = 'create';

        if ($level == 1){

            $data['sidebar']='nation_sidebar';


        }

        if ($level == 2){
            $data['sidebar']='region_sidebar';
            $station=json_decode($user_info->json_location)->region;

        }

        if ($level == 3){
            $data['sidebar']='county_sidebar';
            $station=json_decode($user_info->json_location)->county;

        }

        if ($level == 4){
            $data['sidebar']='subcounty_sidebar';
            $station=json_decode($user_info->json_location)->subcounty;

        }

        echo Modules::run('templates', $data);

    }

    public function generate($id)
    {
        //check pending jobcard

        $pending = $this->mdl_jobcard->get_by_fridge_id($id);

        //get fridge details

        $fridge_infor=$this->mdl_fridges->get_store_fridge_by_id($id);

        $data['store_infor']=$this->mdl_store->get_by_id($fridge_infor[0]->store_id);
        $data['fridge_infor'] = $fridge_infor;
        $data['failure_reasons']=$this->mdl_jobcard->get_reasons();
        $data['tests']=$this->mdl_jobcard->get_tests();
        $data['categories']=$this->mdl_jobcard->get_spare_part_categories();
        $data['pending']= count($pending);
        //echo '<pre>',print_r($e),'</pre>';exit;

        $user_info = $this->users->user_info();
        $data['title'] = 'Jobcard';
        $data['sidebar']='nation_sidebar';
        $data['module'] = 'jobcard';
        $data['view_file'] = 'jobcard';
        echo Modules::run('templates', $data);

    }

    public function add()
    {

        $this->form_validation->set_rules('fridge_id', '', 'required');
        $this->form_validation->set_rules('reasons_failure[]', 'Reason for failure.(At least 1)', 'required');
        $this->form_validation->set_rules('tests[]', 'Test Conducted.(At least 1)', 'required');
        $this->form_validation->set_rules('store_id', 'Store ID', 'required');
        $this->form_validation->set_rules('diagnosis', 'Diagnosis', 'required');
        $this->form_validation->set_rules('spare_parts[]', 'Spare parts', 'required');
        $this->form_validation->set_rules('quantity[]', 'Quantity', 'required');
        $this->form_validation->set_rules('fridge_status', 'Fridge Status', 'required');

        $other=$this->input->post('other_specify');

        if(isset($other)){

            $this->form_validation->set_rules('other_specify', 'Specify Other reason for failure.', 'required');
        }

        if ($this->form_validation->run() == FALSE)
        {
            echo validation_errors();
            http_response_code(401);
            return;
        }

        //save jobcard

        $data = array(
            'store_id' => $this->input->post('store_id'),
            'fridge_id' => $this->input->post('fridge_id'),
            'diagnosis' => $this->input->post('diagnosis'),
        );


        $jb_id=$this->mdl_jobcard->create($data);

        $tests=$this->input->post('tests');
        $reasons_failure=$this->input->post('reasons_failure');
        $spare_parts=$this->input->post('spare_parts');
        $quantity=$this->input->post('quantity');

        //save tests

        foreach ($tests as $value){

            $data = array(
                'test_id' => $value,
                'jobcard_id' => $jb_id,
            );


            $this->mdl_jobcard->create_tests($data);
        }

        //save reasons for failure

        foreach ($reasons_failure as $value){

            $data = array(
                'reason_id' => $value,
                'jobcard_id' => $jb_id,
            );


            $this->mdl_jobcard->create_reasons_for_failure($data);
        }

        //save parts added

        foreach ($spare_parts as $key => $value){

            $data = array(
                'sparepart_id' => $value,
                'quantity_requested' => $quantity[$key],
                'jobcard_id' => $jb_id,
            );


            $this->mdl_jobcard->create_repair_parts($data);
        }

        //change fridge status if not Functional

        $fridge=(int)$this->input->post('fridge_status');
        //echo '<pre>',print_r($fridge),'</pre>';exit;

        $id=$this->input->post('fridge_id');

        if($fridge==0){

            $status='Nonfunctional';

        }else{

            $status='Functional';

        }

        $this->mdl_fridges->update_fridge_status($id,$status);

        //send email


    }

    public function view($id='NULL')
    {
        $user_info = $this->users->user_info();
        $level=$user_info->level_id;
        $location_id = $user_info->location_id;

        $user_info = $this->users->user_info();

        //Get Jobcard

        $jobcard=$this->mdl_jobcard->get_by_id($id);

        //Get Jobcard Associates

            //Repair parts

        $this->mdl_jobcard->get_spare_parts($id);

        //Repair parts



        //Tests

        $data['tests'] = $this->mdl_jobcard->get_jobcard_tests($id);

        //Failure Reasons

        $data['failure_reasons']= $this->mdl_jobcard->get_jobcard_failure_reasons($id);

        //get fridge details

        $fridge_infor=$this->mdl_fridges->get_store_fridge_by_id($jobcard[0]->fridge_id);

        $repair_parts=$this->mdl_jobcard->get_jobcard_repair_parts($id);




//        echo '<pre>',print_r($stock),'</pre>';exit;


        $data['store_infor']=$this->mdl_store->get_by_id($fridge_infor[0]->store_id);
        $data['fridge_infor'] = $fridge_infor;
        $data['jobcard'] = $jobcard;
        $data['repair_parts'] = $repair_parts;
        $data['level'] = $user_info->level_id;


        $data['title'] = 'Jobcard';
        $data['sidebar']='nation_sidebar';
        $data['module'] = 'jobcard';
        $data['view_file'] = 'view_one';

        echo Modules::run('templates', $data);

    }

    public function getByCategoty($category_id)
    {
        header('Content-Type: application/json');

        $parts=$this->mdl_jobcard->get_spare_part_by_category($category_id);

        if (count($parts)==0){

            $parts=[];

            echo json_encode($parts);


        }else{

            echo json_encode($parts);

        }



    }

    public function getStock($category_id,$location_id)
    {
        header('Content-Type: application/json');

        $parts=$this->mdl_jobcard->get_spare_part_by_category($category_id);

        if (count($parts)==0){

            echo 'No Parts';
            http_response_code(401);
            return;

        }else{

            echo json_encode($parts);

        }

    }

    public function getsparepartStock($spare_part_id)
    {
        header('Content-Type: application/json');
        $user_info = $this->users->user_info();
        $level=$user_info->level_id;
        $location_id = $user_info->location_id;

        //Get spare parts stock

        $stock=$this->mdl_jobcard->get_spare_part_stock($location_id,$spare_part_id);

        if (count($stock)==0){

            echo 'No Stock';
            http_response_code(401);
            return;

        }else{

            echo json_encode($stock);

        }

    }

    public function getAllsparepartStock($spare_part_id)
    {
        header('Content-Type: application/json');
        $user_info = $this->users->user_info();
        $level=$user_info->level_id;
        $location_id = $user_info->location_id;

        //Get spare parts stock

        $stock=$this->mdl_jobcard->get_spare_part_stock_all($location_id,$spare_part_id);

        if (count($stock)==0){
            $stock=[];


            echo json_encode($stock);
        }else{

            foreach ($stock as $key => $value){

                $value->date_created=date('Y M d',strtotime($value->date_created));
            }

            echo json_encode($stock);

        }

    }



  }
