<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Mdl_Jobcard extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function get_table()
    {
        $table = 'jobcard';

        return $table;
    }
    public function create($data)
    {
        $this->db->insert('jobcard',$data);
        $insert_id = $this->db->insert_id();

        return  $insert_id;

    }
    public function create_tests($data)
    {
        $this->db->insert('jobcard_tests',$data);

    }

    public function create_reasons_for_failure($data)
    {
        $this->db->insert('jobcard_failure_reasons',$data);

    }

    public function create_repair_parts($data)
    {
        $this->db->insert('jobcard_repair_parts',$data);

    }

    public function get_all()
    {
        $table = $this->get_table();
        $this->db->select('');
        $query = $this->db->get($table);

        return $query->result();
    }

    public function get_reasons()
    {
        $this->db->select('');
        $this->db->from('failure_reasons');
        $query = $this->db->get();
        return $query->result();
    }

    public function get_tests()
    {
        $this->db->select('');
        $this->db->from('fridge_tests');
        $query = $this->db->get();
        return $query->result();

    }

    public function get_by_id($id)
    {
        $query = $this->db->select('')->from('jobcard')->where('id', $id)->get();
        return $query->result();

    }

    public function get_by_fridge_id($id)
    {
        $query = $this->db->select('')->from('jobcard')->where('fridge_id', $id)->where('status', 'open')->get();
        return $query->result();

    }

    public function get_spare_parts($jobcard_id)
    {
        $this->db->select('');
        $this->db->from('jobcard_repair_parts');
        $this->db->where('jobcard_id', $jobcard_id);
        $query = $this->db->get();
        return $query->result();


    }

    public function get_spare_part_by_category($category_id)
    {
        $this->db->select('');
        $this->db->from('spare_parts');
        $this->db->where('spare_part_category_id', $category_id);
        $query = $this->db->get();
        return $query->result();


    }

    public function get_spare_part_categories()
    {
        $this->db->select('');
        $this->db->from('spare_part_type');
        $query = $this->db->get();
        return $query->result();


    }

    public function get_jobcards_national($status)
    {
        $query = $this->db->select('jobcard.id as jobcard_id,jobcard.store_id as store_id,
        store.name,json_location,location_id,jobcard.status as jobcard_status')->from('jobcard')
        ->join('store', 'store.id = jobcard.store_id')->join('locations', 'locations.id = store.location_id')
            ->where('JSON_EXTRACT(`json_location`, "$.national") =', 1)
        ->where('jobcard.status', $status)
        ->get();
        return $query->result();



    }

    public function get_all_jobcards_national()
    {
        $query = $this->db->select('jobcard.id as jobcard_id,jobcard.store_id as store_id,
        store.name,json_location,location_id,jobcard.status as jobcard_status')->from('jobcard')
            ->join('store', 'store.id = jobcard.store_id')->join('locations', 'locations.id = store.location_id')
            ->where('JSON_EXTRACT(`json_location`, "$.national") =', 1)
            ->get();
        return $query->result();



    }

    public function get_jobcards_region($status,$station)
    {
        $query = $this->db->select('jobcard.id as jobcard_id,jobcard.store_id as store_id,
        store.name,json_location,location_id,jobcard.status as jobcard_status')->from('jobcard')
            ->join('store', 'store.id = jobcard.store_id')->join('locations', 'locations.id = store.location_id')
            ->where('JSON_EXTRACT(`json_location`, "$.region") =', $station)
            ->where('jobcard.status', $status)
            ->get();
        return $query->result();



    }

    public function get_all_jobcards_region($station)
    {
        $query = $this->db->select('jobcard.id as jobcard_id,jobcard.store_id as store_id,
        store.name,json_location,location_id,jobcard.status as jobcard_status')->from('jobcard')
            ->join('store', 'store.id = jobcard.store_id')->join('locations', 'locations.id = store.location_id')
            ->where('JSON_EXTRACT(`json_location`, "$.region") =', $station)
            ->get();
        return $query->result();


    }

    public function get_jobcards_county($status,$station)
    {
        $query = $this->db->select('jobcard.id as jobcard_id,jobcard.store_id as store_id,
        store.name,json_location,location_id,jobcard.status as jobcard_status')->from('jobcard')
            ->join('store', 'store.id = jobcard.store_id')->join('locations', 'locations.id = store.location_id')
            ->where('JSON_EXTRACT(`json_location`, "$.county") =', $station)
            ->where('jobcard.status', $status)
            ->get();
        return $query->result();



    }

    public function get_all_jobcards_county($station)
    {
        $query = $this->db->select('jobcard.id as jobcard_id,jobcard.store_id as store_id,
        store.name,json_location,location_id,jobcard.status as jobcard_status')->from('jobcard')
            ->join('store', 'store.id = jobcard.store_id')->join('locations', 'locations.id = store.location_id')
            ->where('JSON_EXTRACT(`json_location`, "$.county") =', $station)
            ->get();
        return $query->result();


    }

    public function get_jobcards_subcounty($status,$station)
    {
        $query = $this->db->select('jobcard.id as jobcard_id,jobcard.store_id as store_id,
        store.name,json_location,location_id,jobcard.status as jobcard_status')->from('jobcard')
            ->join('store', 'store.id = jobcard.store_id')->join('locations', 'locations.id = store.location_id')
            ->where('JSON_EXTRACT(`json_location`, "$.subcounty") =', $station)
            ->where('jobcard.status', $status)
            ->get();
        return $query->result();



    }

    public function get_all_jobcards_subcounty($station)
    {
        $query = $this->db->select('jobcard.id as jobcard_id,jobcard.store_id as store_id,
        store.name,json_location,location_id,jobcard.status as jobcard_status')->from('jobcard')
            ->join('store', 'store.id = jobcard.store_id')->join('locations', 'locations.id = store.location_id')
            ->where('JSON_EXTRACT(`json_location`, "$.subcounty") =', $station)
            ->get();
        return $query->result();



    }

    public function get_jobcard_repair_parts($id)
    {
        $query = $this->db->select('jobcard_repair_parts.id,jobcard_id,spare_part_type.name as category,spare_part_type.id as category_id,spare_parts.name as sparepart,spare_parts.id as spare_part_id,quantity_requested')
        ->from('jobcard_repair_parts')
            ->join('spare_parts', 'jobcard_repair_parts.sparepart_id = spare_parts.id')
            ->join('spare_part_type', 'spare_part_type.id = spare_parts.spare_part_category_id')
            ->where('jobcard_id =', $id)->get();

        return $query->result();

    }

    public function get_jobcard_failure_reasons($id)
    {
        $query = $this->db->select('')
            ->from('jobcard_failure_reasons')
            ->join('failure_reasons', 'failure_reasons.id = jobcard_failure_reasons.reason_id')
            ->where('jobcard_id =', $id)->get();

        return $query->result();

    }

    public function get_jobcard_tests($id)
    {
        $query = $this->db->select('')
            ->from('jobcard_tests')
            ->join('fridge_tests', 'fridge_tests.id = jobcard_tests.test_id')
            ->where('jobcard_id =', $id)->get();

        return $query->result();

    }

    public function get_spare_part_stock_all($location_id,$spare_part_id)
    {
        $query = $this->db->select('quantity, `spare_part_id`, `transaction_type`,location_id,spare_part_transactions.date_created')
            ->from('spare_part_transactions')
            ->join('spare_part_transaction_items',
                'spare_part_transaction_items.spare_part_transaction_id = spare_part_transactions.id')
            ->where('spare_part_transactions.location_id =', $location_id)
            ->where('spare_part_transaction_items.spare_part_id =', $spare_part_id)
            ->order_by('spare_part_transactions.date_created DESC')->get();

        return $query->result();

    }

    public function get_spare_part_stock($location_id,$spare_part_id)
    {
        $query = $this->db->select('sum(quantity) as quantity,spare_part_id,transaction_type')
            ->from('spare_part_transactions')
            ->join('spare_part_transaction_items',
                'spare_part_transaction_items.spare_part_transaction_id = spare_part_transactions.id')
            ->where('spare_part_transactions.location_id =', $location_id)
            ->where('spare_part_transaction_items.spare_part_id =', $spare_part_id)
            ->group_by('spare_part_id ,transaction_type')->get();

        return $query->result();

    }




}
