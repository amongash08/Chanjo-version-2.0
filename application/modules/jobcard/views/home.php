<div class="row">

    <section class="col-lg-12 connectedSortable ui-sortable">

        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="false">Open</a></li>
                <li class=""><a href="#tab_2" data-toggle="tab" aria-expanded="false">Closed</a></li>
                <li class=""><a href="#tab_3" data-toggle="tab" aria-expanded="true">History</a></li>

<!--                <li class="pull-right"><a href="#" class="text-muted"><i class="fa fa-gear"></i></a></li>-->
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="tab_1">

                    <div class="table-responsive">
                        <?php if(count($open_cards)==0){

                            echo '<div style="margin:5%;font-size:3em;font-weight:400;"> No <span class="label bg-maroon margin">Open</span> Jobcards at this time.</div>';
                        }else{
                            ?>
                        <table class="table no-margin">
                            <thead>
                            <tr>
                                <th>Jobcard ID</th>
                                <th>Location</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php

                                foreach ($open_cards as $key=> $value) {
                                    // echo '<pre>',print_r($myfridges),'</pre>';exit;
                                    ?>

                                    <tr>

                                        <td>
                                            <?php echo $value->jobcard_id; ?>
                                        </td>
                                        <td><?php echo $value->name; ?></td>
                                        <td><span class="label bg-maroon"><?php echo $value->jobcard_status; ?></span></td>

                                        <td><a href="<?php echo base_url().'jobcard/view/'.$value->jobcard_id ?>" id="<?php echo $value->jobcard_id; ?>" class="btn btn-xs btn-flat bg-primary "><i class="fa fa-eye"></i> View</a>

                                        </td>

                                    </tr>
                                <?php } }?>


                            </tbody>
                        </table>
                    </div>
                    <!-- /.table-responsive -->

                </div>
                <!-- /.tab-pane -->
                <div class="tab-pane" id="tab_2">

                    <div class="table-responsive">
                        <?php
                        if(count($closed_cards)==0){

                        echo '<div style="margin:5%;font-size:3em;font-weight:400;"> No <span class="label btn-flat bg-navy margin">Closed</span> Jobcards at this time.</div>';
                        }else {?>
                        <table class="table no-margin">
                            <thead>
                            <tr>
                                <th>Jobcard ID</th>
                                <th>Location</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php

                                foreach ($closed_cards as $key=> $value) {
                                    // echo '<pre>',print_r($myfridges),'</pre>';exit;
                                    ?>

                                    <tr>

                                        <td>
                                            <?php echo $value->jobcard_id; ?>
                                        </td>
                                        <td><?php echo $value->name; ?></td>
                                        <td><span class="label bg-navy"><?php echo $value->jobcard_status; ?></span></td>

                                        <td><a href="<?php echo base_url().'jobcard/view/'.$value->jobcard_id ?>" id="<?php echo $value->jobcard_id; ?>" class="btn btn-xs btn-flat bg-primary "><i class="fa fa-eye"></i> View</a>

                                        </td>

                                    </tr>
                                <?php } }?>


                            </tbody>
                        </table>
                    </div>
                    <!-- /.table-responsive -->

                </div>
                <!-- /.tab-pane -->
                <div class="tab-pane" id="tab_3">

                    <div class="table-responsive">
                        <?php if(count($all_cards)==0){

                            echo '<div style="margin:5%;font-size:3em;font-weight:400;"> Could not find any Jobcards.</div>';
                        }else{

                        ?>
                        <table class="table no-margin">
                            <thead>
                            <tr>
                                <th>Jobcard ID</th>
                                <th>Location</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php

                                foreach ($all_cards as $key=> $value) {
                                    // echo '<pre>',print_r($myfridges),'</pre>';exit;
                                    ?>

                                    <tr>

                                        <td>
                                            <?php echo $value->jobcard_id; ?>
                                        </td>
                                        <td><?php echo $value->name; ?></td>
                                        <td>
                                            <?php if($value->jobcard_status =='open'){?>
                                            <span class="label bg-maroon"><?php echo $value->jobcard_status; ?></span>
                                            <?php }else { ?>

                                            <span class="label bg-navy"><?php echo $value->jobcard_status; ?></span>
                                    <?php } ?>

                                        </td>

                                        <td><a href="<?php echo base_url().'jobcard/view/'.$value->jobcard_id ?>" id="<?php echo $value->jobcard_id; ?>" class="btn btn-xs btn-flat bg-primary "><i class="fa fa-eye"></i> View</a>

                                        </td>

                                    </tr>
                                <?php } }?>


                            </tbody>
                        </table>
                    </div>
                    <!-- /.table-responsive -->

                </div>
                <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
        </div>

    </section>

</div>