<section class="content">
    <div class="row">

        <?php
        //echo '<pre>',print_r($pending),'</pre>';exit;
        if($pending >= 2) {


            echo '<div class="callout callout-danger" style="margin-bottom: 0!important;">
                <h2><i class="fa fa-info"></i> Note: You seem to have reached the limit(2) for pending Jobcards.</h2>
            
            </div>';return;

        }elseif ($pending > 0 && $pending <= 1){

            echo '<div class="callout callout-info" style="margin-bottom: 0!important;">
                <h2><i class="fa fa-info"></i> Note: You have a pending Jobcard</h2>
            
            </div>';

        }


        ?>

        <section class="invoice">

            <div class="row">
                <div class="col-xs-12">
                    <h2 class="page-header">
                        <i class="fa fa-briefcase"></i> Job Card, <?php echo $store_infor[0]->name?>
                        <small class="pull-right">Date: Today</small>
                    </h2>
                </div>
                <!-- /.col -->
            </div>
            <!-- info row -->
            <div class="row invoice-info">
                <div class="col-sm-6 invoice-col">
                    From
                    <address>
                        <strong><?php echo $store_infor[0]->name?>.</strong><br>
                        Store Manager Name: <?php  if($store_infor[0]->manager_name==''){ echo "<i><b class='text-warning'>".'Please add store manager details'."</b></i>";}else{echo $store_infor[0]->manager_name;}?><br>
                        Store Manager Phone: <?php  if($store_infor[0]->manager_phone==''){ echo "<i><b class='text-warning'>".'Please add store manager details'."</b></i>";}else{echo $store_infor[0]->manager_phone;}?>

                    </address>
                </div>
                <!-- /.col -->
                <div class="col-sm-6 invoice-col">
                    <b>JOBCARD #007612</b><br>
                    <br>
                    <b>Fridge ID:</b> <?php echo $fridge_infor[0]->equipment_id?><br>
                    <b>Fridge Model:</b> Model Here<br>
                    <b>Previous Repair Date:</b> 2/22/2014<br>
                    <b>Previous Repair By:</b> John Doe<br>

                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->

            <!-- Table row -->
            <div class="row">

                <form action="" method="post" id="jobcard" class="">
                    <input type="hidden" id="store_id" name="store_id" value="<?php echo $store_infor[0]->id?>">
                    <input type="hidden" id="fridge_id" name="fridge_id" value="<?php echo $fridge_infor[0]->id?>">

                    <div class="row" style="margin: 1% 0 1% 2%;">
                        <div class="col-md-5">

                            <label for="Diagnosis">Fridge Status</label>

                            <select id="fridge_status" name="fridge_status" class=" form-control col-md-6">
                                <option value="NULL">- Select Status -</option>
                                <option value="1">Functional</option>
                                <option value="0">Not Functional</option>


                            </select>

                        </div>


                    </div>


                    <div class="row" style="margin: 1% 0 1% 2%;">
                        <h3 class="page-header">Tests Administered</h3>
                        <?php foreach ($tests as $key => $value){

                        ?>
                        <div class="col-md-3">

                            <div class="checkbox">
                                <label>
                                    <input id="<?php echo $value->alias; ?>" name="tests[]" onclick="$(this).attr('value', this.checked ? <?php echo $value->id; ?> : 0)" type="checkbox"> <?php echo $value->description; ?>
                                </label>
                            </div>
                        </div>

                        <?php  } ?>


                        <div class="col-md-8" style="margin-bottom: 10px;">

                            <label for="Diagnosis">Diagnosis</label>
                            <textarea class="form-control" rows="5" id="diagnosis" name="diagnosis"></textarea>
                        </div>

                    </div>

                    <div class="row" style="margin: 1% 0 1% 2%;">
                        <h3 class="page-header">Reason For Failure</h3>

                        <?php foreach ($failure_reasons as $key => $value){

                            ?>
                            <div class="col-md-3">

                                <div class="checkbox">
                                    <label>
                                        <input id="<?php echo $value->alias; ?>" onclick="$(this).attr('value', this.checked ? <?php echo $value->id; ?> : 0)" name="reasons_failure[]" type="checkbox"> <?php echo $value->description; ?>
                                    </label>
                                </div>
                            </div>

                        <?php  } ?>


                        <div class="col-md-8" style="margin-bottom: 10px;" name="specify" id="specify">

                            <label for="other">Specify Other</label>
                            <textarea class="form-control" rows="5" name="other_specify" id="other_specify"></textarea>
                        </div>

                    </div>

                    <div class="row" style="margin: 1% 2% 1% 2%;">
                        <h3 class="page-header">Spare Parts Required</h3>
                        <div class="col-xs-12 table-responsive">
                            <table class="table table-striped" id="table">
                                <thead>
                                <tr>
                                    <th>Spare Part Category</th>
                                    <th>Spare Part Required</th>
                                    <th>Quantity</th>
                                    <th>Action</th>

                                </tr>
                                </thead>
                                <tbody>
                                <tr row="0" class="form-group-sm">
                                    <td>
                                        <select id="category[]" name="category[]" class=" form-control col-md-6 category">
                                            <option value="NULL">- Select Category -</option>
                                            <?php
                                            foreach ($categories as $key => $value) {
                                                $name = $value->name;
                                                $id = $value ->id;

                                                echo "<option value='$id'>$name</option>";
                                            }

                                            ?>

                                        </select>
                                    </td>
                                    <td>
                                        <select id="spare_parts[]" name="spare_parts[]" class=" form-control col-md-6 parts">


                                        </select>
                                    </td>
                                    <td><input type="number" name="quantity[]" id="quantity[]" class="form-control"></td>
                                    <td><a class="add btn-xs btn-flat" style="cursor: pointer; cursor: hand;"><span
                                                class="label label-success"><i
                                                    class="fa fa-plus-square"></i> <b>ADD</b></span></a><br>
                                        <a class="remove btn-xs btn-flat" style="cursor: pointer; cursor: hand;"><span
                                                class="label label-danger"><i
                                                    class="fa fa-minus-square"></i> <b>REMOVE</b></span></a></td>

                                </tr>

                                <tr id="template" class="form-group-sm" hidden>

                                    <td>
                                        <select disabled id="category[]" name="category[]" class=" form-control col-md-6 category">
                                            <option value="NULL">- Select Category -</option>
                                            <?php
                                            foreach ($categories as $key => $value) {
                                                $name = $value->name;
                                                $id = $value ->id;

                                                echo "<option value='$id'>$name</option>";
                                            }

                                            ?>

                                        </select>
                                    </td>
                                    <td>
                                        <select disabled id="spare_parts[]" name="spare_parts[]" class=" form-control col-md-6 parts">
                                            <option value="NULL">- Select Part -</option>

                                        </select>
                                    </td>
                                    <td><input disabled type="number" name="quantity[]" id="quantity[]" class="form-control"></td>
                                    <td><a class="add btn-xs btn-flat" style="cursor: pointer; cursor: hand;"><span
                                                    class="label label-success"><i
                                                        class="fa fa-plus-square"></i> <b>ADD</b></span></a><br>
                                        <a class="remove btn-xs btn-flat" style="cursor: pointer; cursor: hand;"><span
                                                    class="label label-danger"><i
                                                        class="fa fa-minus-square"></i> <b>REMOVE</b></span></a></td>

                                </tr>

                                </tbody>
                            </table>
                        </div>



                    </div>

                </form>

            </div>
            <!-- /.row -->

            <div class="row no-print">
                <div class="col-xs-12">

                    <button type="button" id="generate" class="btn btn-primary btn-flat pull-right" style="margin-right: 5px;">
                        <i class="fa "></i> Generate Jobcard
                    </button>
                </div>
            </div>
        </section>


    </div>
</section>


<script type="text/javascript">

    $( document ).ready(function() {

        $('#specify').hide();
        $('#other_specify').prop('disabled', true);

        $("#generate").click(function(e) {

            var url="<?php echo base_url(); ?>jobcard/add"; // the script where you handle the form input.
            var formData ='#jobcard';
            var msg ='Successfully created a Jobcard';

//        console.log( $( '#jobcard' ).serialize() );
//        return;

            processData(formData,url,msg);

            e.preventDefault(); // avoid to execute the actual submit of the form.

        });

        $('#other').change(function() {
            if ($(this).is(':checked')){

                $('#specify').show();
                $('#other_specify').prop('disabled', false);

            }else{
                $('#specify').hide();
                $('#other_specify').prop('disabled', true);
            }

        });

            $(document).on("click", "#table .add", function(event){

                var thisRow = $(this).closest('tr');


                var template = $('#template');
                var cloned_object = template.clone().removeAttr('hidden').removeAttr('id');

                var multiple_row = thisRow.attr("row");
                var row_index = parseInt(multiple_row) + 1;
                //console.log(multiple_row)
                var new_row = cloned_object.attr("row", row_index);


                new_row.insertBefore(template);
                //console.log(new_row.find('input[name="quantity"]'));

                new_row.find('input[name="quantity[]"]').removeAttr('disabled');
                new_row.find('select[name="spare_parts[]"]').removeAttr('disabled');
                new_row.find('select[name="category[]"]').removeAttr('disabled');

                new_row.slideDown();


            });

        $(document).on("click", "#table .remove", function(event){


            //console.log($('#table tbody tr').length);

            if ($('#table tbody tr').length === 2) return;
            $(this).parents("tr").fadeOut('slow', function () {
                $(this).remove();
            });

        });

        $(document).on("change", ".category", function(event){


            var category_id=$(this).val();
            var locator=$('option:selected', this);
            //console.log(locator.closest("tr").find(".parts"));


            var siteurl="<?php echo base_url(); ?>";
            var url="<?php echo base_url(); ?>jobcard/getByCategoty/"+category_id;

            $.ajax({
                type: "POST",
                url: url,
                beforeSend: function() {
                    // something
                },
                success: function(data)
                {
                    var drop_down='';
                    $(locator.closest("tr").find(".parts")).html('<option value="NULL" selected="selected">Select Spare Part</option>');
                    $.each(data, function( key, val ) {
                        drop_down +="<option value='"+data[key]["id"]+"'>"+data[key]["name"]+"</option>";
                    });
                    $(locator.closest("tr").find(".parts")).append(drop_down);

                },error: function (jqXHR) {
                    console.log(jqXHR)

                }
            });



        });


        function processData(formData,url,msg){
            $.ajax({
                type: "POST",
                url: url,
                data: $(formData).serialize(), // serializes the form's elements.
                success: function(data,status, jqXHR)
                {
                    swal("Good job!", msg, "success");//show response from the php script.
//
                    setTimeout(document.location.replace("<?php echo base_url(); ?>jobcard"), 1000);
                    console.log(jqXHR);


                },error: function (jqXHR, status, err,data) {
//                    console.log(jqXHR);
                    toastr.error(jqXHR.responseText)
                }
            });

        }
    })
</script>