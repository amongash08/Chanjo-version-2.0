<script type="text/javascript" src="<?php echo base_url() ?>assets/js/printThis.js"></script>

<div class="row">

    <section class="col-lg-12 connectedSortable ui-sortable">

        <ol class="breadcrumb">
            <li><a href="<?php echo base_url().'jobcard'?>"><i class="fa fa-arrow-left"></i> Back </a></li>
        </ol>

        <section class="invoice">
            <!-- title row -->
            <div class="row">
                <div class="col-xs-12">
                    <h2 class="page-header">
                        <i class="fa fa-map-marker"></i>Job Card, <?php echo $store_infor[0]->name?>
                        <span class="pull-right">JOBCARD # <?php echo $jobcard[0]->id;?>
                            , Date: <?php echo date('M d Y' ,strtotime($jobcard[0]->date_created));?>
                        </span>
                    </h2>
                </div>
                <!-- /.col -->
            </div>
            <!-- info row -->
            <div class="row invoice-info">
                <div class="col-sm-6 invoice-col">
                    <address>
<!--                        <strong>--><?php //echo $store_infor[0]->name?><!--.</strong><br>-->
                        <b>Store Manager Name:</b> <?php  if($store_infor[0]->manager_name==''){ echo "<i><b class='text-warning'>".'Please add store manager details'."</b></i>";}else{echo $store_infor[0]->manager_name;}?><br>
                            <b>Store Manager Phone:</b> <?php  if($store_infor[0]->manager_phone==''){ echo "<i><b class='text-warning'>".'Please add store manager details'."</b></i>";}else{echo $store_infor[0]->manager_phone;}?>

                    </address>
                </div>
                <!-- /.col -->
                <div class="col-sm-6 invoice-col">

                    <b>Fridge ID:</b> <?php echo $fridge_infor[0]->equipment_id?><br>
                    <b>Fridge Model:</b> <?php echo $fridge_infor[0]->Model?><br>
                    <b>Previous Repair Date:</b> <?php echo date('M d Y' ,strtotime($jobcard[0]->date_created));?><br>
                    <b>Previous Repair By:</b> John Doe<br>

                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
            <div class="row" style="margin-top: 1.5%;">
                <div class="col-md-6">
                    <div class="box " style="margin-top: 1%;height:200px;">
                        <div class="box-header with-border">
                            <h3 class="box-title">Tests Administered</h3>

                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                </button>
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <ul class="products-list product-list-in-box">
                                <?php foreach ($tests as $key => $value){?>

                                <li class="item">
                                    <div class="product-img">
                                        <i class="fa fa-circle-o text-green"></i>
                                    </div>
                                    <div class="product-info">
                                        <a href="javascript:void(0)" class="product-title"><?php echo $value->description ?>
<!--                                            <span class="label label-success pull-right">DONE</span>-->
                                        </a>

                                    </div>
                                </li><?php } ?>
                                <!-- /.item -->

                            </ul>
                        </div>
                        <!-- /.box-body -->
<!--                        <div class="box-footer text-center">-->
<!--                            <a href="javascript:void(0)" class="uppercase">View All Products</a>-->
<!--                        </div>-->
                        <!-- /.box-footer -->
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="box " style="margin-top: 1%;height:200px;">
                        <div class="box-header with-border">
                            <h3 class="box-title">Reasons for Failure</h3>

                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                </button>
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <ul class="products-list product-list-in-box">
                                <!-- /.box-header -->
                                <div class="box-body">
                                    <ul class="products-list product-list-in-box">
                                        <?php foreach ($failure_reasons as $key => $value){?>

                                            <li class="item">
                                            <div class="product-img">
                                                <i class="fa fa-circle-o text-green"></i>
                                            </div>
                                            <div class="product-info">
                                                <a href="javascript:void(0)" class="product-title"><?php echo $value->description ?>
<!--                                                    <span class="label label-success pull-right">DONE</span>-->
                                                </a>

                                            </div>
                                            </li><?php } ?>
                                        <!-- /.item -->

                            </ul>
                        </div>
                        <!-- /.box-body -->

                    </div>
                </div>


            </div>

                <div class="row">

                    <div class="callout callout-default" style="margin-bottom: 0!important;">
                        <h4><i class="fa fa-"></i> <strong>Diagnosis: </strong><?php echo $jobcard[0]->diagnosis;?></h4>
                    </div>

                </div>

            <!-- Table row -->

                <div class="row" style="margin: 1% 2% 1% 2%;">
                    <h3 class="page-header">Spare Parts Required</h3>
                    <div class="col-xs-12 table-responsive">
                        <table class="table table-striped" id="table">
                            <thead>
                            <tr>
                                <th>Spare Part Category</th>
                                <th>Spare Part Required</th>
                                <th>Quantity Requested</th>

                            </tr>
                            </thead>

                            <tbody>
                            <?php foreach ($repair_parts as $key => $value){?>
                                <tr>
                                    <td><?php echo $value->category ?></td>
                                    <td><?php echo $value->sparepart ?></td>
                                    <td><?php echo $value->quantity_requested ?></td>
                                </tr>




                            <?php } ?>


                            </tbody>
                        </table>
                    </div>



                </div>

            <!-- /.row -->


            <!-- this row will not appear when printing -->

        </section>
        <div class="row no-print">
            <div class="col-xs-12">
                <button id="print" type="button"  class="btn btn-success btn-flat pull-right" style="margin-right: 5px"><i class="fa fa-print"></i> Print
                </button>

                <?php if ($level==1){?>
                    <button type="button" class="btn btn-danger btn-flat pull-right reject" style="margin-right: 5px"><i class="fa fa-ban"></i> Reject
                    </button>
                    <button type="button" class="btn btn-primary btn-flat pull-right issue" style="margin-right: 5px;">
                        <i class="fa fa-hand-o-right"></i> Issue
                    </button>
                <?php }?>

            </div>
        </div>

        <div class="row">

        </div>

    </section>

</div>

<div class="modal fade" id="modal-issueparts" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document" style="width:86%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Issue Parts</h4>

            </div>
            <div class="modal-body" >
                <form id="issue_spare_part_form">

                <table class="table no-margin">
                    <thead>
                    <tr>
                        <th>Spare Category</th>
                        <th>Spare Part Name</th>
                        <th>Quantity Requested</th>
                        <th>Quantity Available</th>
                        <th>Quantity to Issued</th>

                    </tr>
                    </thead>
                    <tbody id="display">

                    <?php foreach ($repair_parts as $key => $value){?>
                        <tr id="<?php echo $value->spare_part_id ?>" class="checkid">
                            <td><?php echo $value->category ?></td>
                            <td><?php echo $value->sparepart ?></td>
                            <td><input type="number" readonly name="" id="" value="<?php echo $value->quantity_requested ?>" class="form-control"></td>

                            <td><input type="number" readonly name="" id="<?php echo $value->spare_part_id ?>" class="form-control quantity_available">
                                <input type="hidden" readonly name="item_id[]" id="item_id[]" value="<?php echo $value->id ?>" >
                                <input type="hidden" readonly name="spare_part_id[]" id="spare_part_id[]" value="<?php echo $value->spare_part_id ?>" >

                            </td>

                            <td><input type="number" name="quantity_issued[]" id="quantity_issued[]" class="form-control quantity_issued"></td>



                        </tr>




                    <?php } ?>

                    </tbody>
                </table>
                    <input type="hidden" readonly name="jobcard_id" id="jobcard_id" value="<?php echo $value->jobcard_id ?>" >

                </form>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default  btn-flat" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary  btn-flat" id="issue"><i class="fa fa-plus"></i>  Issue</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<script>

    $(document).on("click", "#print", function(event){

        console.log('fff');


            setTimeout($('.invoice').printThis({
                importCSS: true,            // import page CSS
                importStyle: true
            }),200)


    }) ;

    $(".checkid").each(function() {
        var spare_part_id=$(this).attr('id');
        var locator=$(this);
        var receive=0;
        var issue=0;



        var siteurl="<?php echo base_url(); ?>";
        var url="<?php echo base_url(); ?>jobcard/getsparepartStock/"+spare_part_id;

        $.ajax({
            type: "POST",
            url: url,
            beforeSend: function() {
                // something
            },
            success: function(data)
            {

                if(data.length>=0){


                    $.each(data, function( key, val ) {
                        if(data[key]["transaction_type"]=='issue'){

                            data[key]["quantity"]=data[key]["quantity"]*-1;

                        }

                    });

                    var sum=0;

                    for(var i = 0; i < data.length; i++) {
                        sum += parseInt(data[i]["quantity"]);
                    }
                    locator.closest("tr").find(".quantity_available").val(sum);
                    console.log(sum);

                }

            },error: function (jqXHR) {

            }
        });

    });

    $(document).on("keyup", ".quantity_issued", function(event){

        var quantity_available=$(this).closest("tr").find(".quantity_available").val();
        var quantity_issued=$(this).val();
        console.log(quantity_available);

        if(parseInt(quantity_issued) > parseInt(quantity_available)){
            console.log(quantity_issued+'>'+quantity_available);

            toastr.error('Your cant issue more than is available.');
            $(this).val('')

        }


    }) ;


    $(document).on("click", ".issue", function(event){

        var fr_id=$(this).attr('id');


        $('#modal-issueparts').modal('show');

    });

    $("#issue").click(function(e) {

        var url="<?php echo base_url(); ?>equipment/issue_spare_parts"; // the script where you handle the form input.
        var formData ='#issue_spare_part_form';
        var msg ='Successfully Issued Sparepart(s)';

//        console.log( $( '#jobcard' ).serialize() );
//        return;

        processData(formData,url,msg);

        e.preventDefault(); // avoid to execute the actual submit of the form.

    });

    function processData(formData,url,msg){
        $.ajax({
            type: "POST",
            url: url,
            data: $(formData).serialize(), // serializes the form's elements.
            success: function(data,status, jqXHR)
            {
                swal("Good job!", msg, "success");//show response from the php script.
//
                setTimeout(document.location.replace("<?php echo base_url(); ?>jobcard"), 1000);
                console.log(jqXHR);


            },error: function (jqXHR, status, err,data) {
//                    console.log(jqXHR);
                toastr.error(jqXHR.responseText)
            }
        });

    }

    $(document).on("click", ".reject", function(event){

        var url="<?php echo base_url(); ?>jobcards/reject/"+jobcardid;

        swal({
                title: "Are you sure?",
                text: "You will not be able to undo this action!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, reject it!",
                cancelButtonText: "No, cancel!",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function(isConfirm){
                if (isConfirm) {

                    $.ajax({
                        type: "POST",
                        url: url,
                        beforeSend: function() {
                        },
                        success: function(data)
                        {

                            console.log(data);


                        },error: function (jqXHR) {
                            console.log(jqXHR);
                            toastr.error(jqXHR.responseText);
                        }
                    });

                    swal("Rejected!", "This Jobcard has been rejected.", "success");
                    setTimeout(location.reload.bind(location), 800);

                } else {
                    swal("Cancelled", "This Jobcard is safe :)", "error");
                }
            });



    });

</script>