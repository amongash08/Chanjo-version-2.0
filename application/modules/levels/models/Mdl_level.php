<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Mdl_level extends CI_Model {

    public function __construct()
    {
        parent::__construct();
    }

    public function get_table()
    {
        $table = 'levels';

        return $table;
    }
    public function get_all()
    {
        $table = $this->get_table();
        $this->db->order_by('id', 'asc');
        $query = $this->db->get($table);

        return $query->result();
    }


}
