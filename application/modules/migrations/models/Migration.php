<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Migration extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function get_national_coverage($maxdate, $mindate)
    {
        $this->db->select('periodname as months,sum(`measles 1`) as measles1,sum(`measles 2`) as measles2,sum(`measles 3`) as measles3, sum(bcg) as BCG,sum(dpt1) as DPT1,sum(dpt2) as DPT2
        ,sum(dpt3) as DPT3,sum(opv) as OPV,sum(opv1) as OPV1,sum(opv2) as OPV2,sum(opv3) as OPV3,sum(pcv1) as PCV1
        ,sum(pcv2) as PCV2,sum(pcv3) as PCV3,sum(rota1) as ROTA1,sum(rota2) as ROTA2,population');
        $this->db->from('dhis_overview');
        $this->db->where('periodname >=', date('Y-m',strtotime($mindate)));
        $this->db->where('periodname <=', date('Y-m',strtotime($maxdate)));
        $this->db->order_by(`periodname`, 'asc');
        $query = $this->db->get();
        return $query->result();
    }



}
