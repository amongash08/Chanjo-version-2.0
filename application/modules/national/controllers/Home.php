<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->module('users');
        $this->load->module('vaccine');
        $this->load->module('region');
        $this->load->module('facility');
        $this->load->model('mdl_user_ext');
        $this->load->model('mdl_vaccine');
    }

    public function index()
    {

        $data['title'] = 'Home';
        $data['subtitle'] = 'Dashboard';
        $data['module'] = 'national';
        $data['view_file'] = 'home/dashboard';
        $data['sidebar']='nation_sidebar';

        echo Modules::run('templates', $data);

    }

    public function ledger()
    {
        $user_info = $this->users->user_info();
        $data['title'] = 'Manage Stock';
        $data['subtitle'] = 'View Stock Ledger';
        $data['module'] = 'stock';
        $data['level_id'] = $user_info->level_id;



        $data['title'] = 'Stock';
        $data['view_file'] = 'stock/ledger_by_location';
        $data['subtitle'] = 'View Ledger';
        $data['regions'] = $this->mdl_region->get_all();
        $data['vaccines'] = $this->vaccine->vaccine_dropdown();




      echo Modules::run('templates', $data);
    }
}
