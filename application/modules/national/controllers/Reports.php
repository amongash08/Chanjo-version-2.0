<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reports extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        if (!$this->ion_auth->logged_in()) {
            //redirect them to the login page
            redirect('users');
        }
        $this->load->module('region');
    }

    public function supply_chain()
    {

        $data['module'] = 'national';
        $data['view_file'] = 'reports/supply_chain';
        $data['title'] = 'Reports';
        $data['subtitle'] = 'Supply Chain';
        // $data['regions'] = $this->mdl_region->get_all();
        // echo '<pre>', print_r($this->mdl_region->get_all());exit;

        echo Modules::run('templates', $data);

    }

    public function cold_chain()
    {
        $data['module'] = 'national';
        $data['view_file'] = 'reports/cold_chain';
        $data['title'] = 'Reports';
        $data['subtitle'] = 'Cold Chain';
        echo Modules::run('templates',$data);

    }

    public function program_management()
    {
        $data['module'] = 'national';
        $data['view_file'] = 'reports/program_management';
        $data['title'] = 'Reports';
        $data['subtitle'] = 'Program management';
        echo Modules::run('templates',$data);

    }

    public function general_admin()
    {

        $data['module'] = 'national';
        $data['view_file'] = 'reports/general_admin';
        $data['title'] = 'Reports';
        $data['subtitle'] = 'General administration';

        echo Modules::run('templates',$data);

    }

}
