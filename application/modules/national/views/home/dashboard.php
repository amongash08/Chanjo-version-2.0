<script src="//cdnjs.cloudflare.com/ajax/libs/numeral.js/2.0.6/numeral.min.js"></script>

<div class="row">

        <div class="col-md-6" style="">

            <div class="box box-solid ">
                <div class="box-header with-border">
                    <i class="fa fa-calendar"></i>

                    <h3 class="box-title">
                        Penta 1 Coverage By County <small>For Q1</small>
                    </h3>

                </div>

                <div class="box-body">

                    <div class="chart">
                        <div id="map" ></div>
                    </div>


                </div>


            </div>

        </div>

        <div class="col-md-6" style="">

            <div class="box box-solid ">
                <div class="box-header with-border">
                    <i class="fa fa-calendar"></i>

                    <h3 class="box-title">
                        Penta 3 Coverage By County <small>For Q1</small>
                    </h3>

                </div>

                <div class="box-body">

                    <div class="chart">
                        <div id="map2" ></div>
                    </div>


                </div>


            </div>
        </div>


    </div>

    <div class="row">

        <div class="col-md-6" style="">

            <div class="box box-solid ">
                <div class="box-header with-border">
                    <i class="fa fa-calendar"></i>

                    <h3 class="box-title">
                        MR 2 Coverage By County <small>For Q1</small>
                    </h3>

                </div>

                <div class="box-body">

                    <div class="chart">
                        <div id="map3" ></div>
                    </div>


                </div>


            </div>

            <!-- /.box -->
        </div>

        <div class="col-md-6" style="">

            <div class="box box-solid ">
                <div class="box-header with-border">
                    <i class="fa fa-calendar"></i>

                    <h3 class="box-title">
                        FIC Coverage By County <small>For Q1</small>
                    </h3>

                </div>

                <div class="box-body">

                    <div class="chart">
                        <div id="map4" ></div>
                    </div>


                </div>


            </div>
            <!-- /.box -->
        </div>


    </div>

<div class="row">

    <section class="col-lg-12">

        <!-- stock levels box -->
        <div class="box box-solid ">
            <div class="box-header with-border">
                <i class="fa fa-calendar"></i>

                <h3 class="box-title">
                    Stock Levels In Doses
                </h3>

            </div>

            <div class="box-body">

                <div class="chart">
                    <div id="doses" ></div>
                </div>


            </div>


        </div>
        <!-- /.box -->

    </section>


</div>

<div class="row">
    <section class="col-lg-12">

        <!-- stock levels box -->
        <div class="box box-solid ">
            <div class="box-header with-border">
                <i class="fa fa-podcast"></i>

                <h3 class="box-title">
                    Coverage
                </h3>

            </div>

            <div class="box-body">

                <div class="chart">
                    <div id="coverage" ></div>
                </div>


            </div>


        </div>
        <!-- /.box -->

        <!-- stock levels box -->
        <div class="box box-solid ">
            <div class="box-header with-border">
                <i class="fa fa-clock-o"></i>

                <h3 class="box-title">
                    Stocks Last Updated
                </h3>


            </div>
            <div class="box-body">

                <div class="row">

                </div>

                <div class="chart">
                    <div id="stocks" ></div>
                </div>


            </div>


        </div>
        <!-- /.box -->

        <div class="box box-solid ">
            <div class="box-header with-border">
                <i class="fa fa-clock-o"></i>

                <h3 class="box-title">
                    Cold Chain
                </h3>


            </div>
            <div class="box-body">

                <div class="row">

                </div>

                <div class="chart">
                    <div id="coldchain" ></div>
                </div>


            </div>


        </div>



    </section>


</div>




    <script>

        var url="<?php echo base_url(); ?>";

        ajax_fill_data('stock/plot_stock_location/NULL/column',"#doses");
        ajax_fill_data('stock/plot_stock_mos_location/NULL/NULL/column',"#mos");
        ajax_fill_data('reports/current_coverage/NULL/NULL',"#coverage");
        ajax_fill_data('stock/stock_last_updated',"#stocks");
        ajax_fill_data('reports/plot_fridge_capacity',"#coldchain");

        $.getJSON( url+"docs/json/kenya.json", function( data ) {

            $.getJSON( url+"docs/json/counties.json", function( counties ) {
                //x=data;
                var coverage = [];
                $.each(counties, function (i, val) {
                    coverage.push({
                        code: val.code,
                        value: parseFloat(val.penta1)

                    });
                });

                //console.log(coverage);

                // Initiate the chart
                Highcharts.mapChart('map', {

                    title: {
                        text: ''
                    },
                    chart: {
                        backgroundColor: null,
                    },

                    mapNavigation: {
                        enabled: true,
                        enableMouseWheelZoom:false,
                        buttonOptions: {
                            verticalAlign: 'bottom'
                        }
                    },
                    tooltip: {
                        formatter: function(){
//console.log(this.point);
                            var c='';
                            c += '<b>'+this.point.code+'</b><br/>';
                            var county="Coverage Penta 1: "+ "<b>"+numeral(this.point.value).format('0,0')+ "</b>";
                            c += county;
                            return c;
                        },
                        pointFormat: '{point.penta1}'
                    },

                    credits: {
                        enabled: false
                    },
                    exporting: { enabled: false },
                    legend: {
                        title: {
                            text: 'Coverage %',
                            style: {
                                color: (Highcharts.theme && Highcharts.theme.textColor) || 'black'
                            }
                        },
                        align: 'left',
                        verticalAlign: 'bottom',
                        floating: true,
                        layout: 'vertical',
                        valueDecimals: 0,
                        backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || 'rgba(255, 255, 255, 0.85)',
                        symbolRadius: 0,
                        symbolHeight: 14
                    },
                    colorAxis: {
                        dataClasses: [
                            {
                                to: 49,
                                color:'#ee0109'
                            },
                            {
                                from: 50,
                                to: 79,
                                color:'#e8e644'
                            },
                            {
                                from: 80,
                                to: 100,
                                color:'#79db34'
                            },
                            {
                                from: 101,
                                color:'#288113'
                            }
                        ]
                    },

                    series: [{
                        showInLegend: true,
                        animation: true,
                        data: coverage,
                        mapData: data,
                        joinBy: ['COUNTY', 'code'],
                        keys: ['COUNTY', 'code'],
                        //name: 'County',
                        //color: '#5bc0de',
                        animation: true,
                        states: {
                            hover: {
                                color: '#a4edba'
                            }
                        }
//                        dataLabels: {
//                            enabled: true,
//                            format: '{point.code}',
//                            style: {
//                                fontFamily: 'Varela Round',
//                                fontColor:'#cccccc',
//                            },
//                        }
                    }]
                });

            });

        });

        //map 2

        $.getJSON( url+"docs/json/kenya.json", function( data ) {

            $.getJSON( url+"docs/json/counties.json", function( counties ) {
                //x=data;
                var penta3 = [];
                $.each(counties, function (i, val) {
                    penta3.push({
                        code: val.code,
                        value: parseFloat(val.penta3)

                    });
                });

                //console.log(coverage);

                // Initiate the chart
                Highcharts.mapChart('map2', {

                    title: {
                        text: ''
                    },
                    chart: {
                        backgroundColor: null,
                    },

                    mapNavigation: {
                        enabled: true,
                        enableMouseWheelZoom:false,
                        buttonOptions: {
                            verticalAlign: 'bottom'
                        }
                    },
                    tooltip: {
                        formatter: function(){
//console.log(this.point);
                            var c='';
                            c += '<b>'+this.point.code+'</b><br/>';
                            var county="Coverage Penta 3: "+ "<b>"+numeral(this.point.value).format('0,0')+ "</b>";
                            c += county;
                            return c;
                        },
                        pointFormat: '{point.penta3}'
                    },

                    credits: {
                        enabled: false
                    },
                    exporting: { enabled: false },
                    legend: {
                        title: {
                            text: 'Coverage %',
                            style: {
                                color: (Highcharts.theme && Highcharts.theme.textColor) || 'black'
                            }
                        },
                        align: 'left',
                        verticalAlign: 'bottom',
                        floating: true,
                        layout: 'vertical',
                        valueDecimals: 0,
                        backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || 'rgba(255, 255, 255, 0.85)',
                        symbolRadius: 0,
                        symbolHeight: 14
                    },
                    colorAxis: {
                        dataClasses: [
                            {
                                to: 49,
                                color:'#ee0109'
                            },
                            {
                                from: 50,
                                to: 79,
                                color:'#e8e644'
                            },
                            {
                                from: 80,
                                to: 100,
                                color:'#79db34'
                            },
                            {
                                from: 101,
                                color:'#288113'
                            }
                        ]
                    },

                    series: [{
                        showInLegend: true,
                        animation: true,
                        data: penta3,
                        mapData: data,
                        joinBy: ['COUNTY', 'code'],
                        keys: ['COUNTY', 'code'],
                        //name: 'County',
                        //color: '#5bc0de',
                        animation: true,
                        states: {
                            hover: {
                                color: '#a4edba'
                            }
                        }
//                        dataLabels: {
//                            enabled: true,
//                            format: '{point.code}',
//                            style: {
//                                fontFamily: 'Varela Round',
//                                fontColor:'#cccccc',
//                            },
//                        }
                    }]
                });

            });

        });

        //map 3

        $.getJSON( url+"docs/json/kenya.json", function( data ) {

            $.getJSON( url+"docs/json/counties.json", function( counties ) {
                //x=data;
                var mr2 = [];
                $.each(counties, function (i, val) {
                    mr2.push({
                        code: val.code,
                        value: parseFloat(val.mr2)

                    });
                });

                //console.log(coverage);

                // Initiate the chart
                Highcharts.mapChart('map3', {

                    title: {
                        text: ''
                    },
                    chart: {
                        backgroundColor: null,
                    },

                    mapNavigation: {
                        enabled: true,
                        enableMouseWheelZoom:false,
                        buttonOptions: {
                            verticalAlign: 'bottom'
                        }
                    },
                    tooltip: {
                        formatter: function(){
//console.log(this.point);
                            var c='';
                            c += '<b>'+this.point.code+'</b><br/>';
                            var county="Coverage MR 2: "+ "<b>"+numeral(this.point.value).format('0,0')+ "</b>";
                            c += county;
                            return c;
                        },
                        pointFormat: '{point.mr2}'
                    },

                    credits: {
                        enabled: false
                    },
                    exporting: { enabled: false },
                    legend: {
                        title: {
                            text: 'Coverage %',
                            style: {
                                color: (Highcharts.theme && Highcharts.theme.textColor) || 'black'
                            }
                        },
                        align: 'left',
                        verticalAlign: 'bottom',
                        floating: true,
                        layout: 'vertical',
                        valueDecimals: 0,
                        backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || 'rgba(255, 255, 255, 0.85)',
                        symbolRadius: 0,
                        symbolHeight: 14
                    },
                    colorAxis: {
                        dataClasses: [
                            {
                                to: 49,
                                color:'#ee0109'
                            },
                            {
                                from: 50,
                                to: 79,
                                color:'#e8e644'
                            },
                            {
                                from: 80,
                                to: 100,
                                color:'#79db34'
                            },
                            {
                                from: 101,
                                color:'#288113'
                            }
                        ]
                    },

                    series: [{
                        showInLegend: true,
                        animation: true,
                        data: mr2,
                        mapData: data,
                        joinBy: ['COUNTY', 'code'],
                        keys: ['COUNTY', 'code'],
                        //name: 'County',
                        //color: '#5bc0de',
                        animation: true,
                        states: {
                            hover: {
                                color: '#a4edba'
                            }
                        }
//                        dataLabels: {
//                            enabled: true,
//                            format: '{point.code}',
//                            style: {
//                                fontFamily: 'Varela Round',
//                                fontColor:'#cccccc',
//                            },
//                        }
                    }]
                });

            });

        });

        //map4

        $.getJSON( url+"docs/json/kenya.json", function( data ) {

            $.getJSON( url+"docs/json/counties.json", function( counties ) {
                //x=data;
                var fic = [];
                $.each(counties, function (i, val) {
                    fic.push({
                        code: val.code,
                        value: parseFloat(val.fic)

                    });
                });

                //console.log(coverage);

                // Initiate the chart
                Highcharts.mapChart('map4', {

                    title: {
                        text: ''
                    },
                    chart: {
                        backgroundColor: null,
                    },

                    mapNavigation: {
                        enabled: true,
                        enableMouseWheelZoom:false,
                        buttonOptions: {
                            verticalAlign: 'bottom'
                        }
                    },
                    tooltip: {
                        formatter: function(){
//console.log(this.point);
                            var c='';
                            c += '<b>'+this.point.code+'</b><br/>';
                            var county="Coverage FIC: "+ "<b>"+numeral(this.point.value).format('0,0')+ "</b>";
                            c += county;
                            return c;
                        },
                        pointFormat: '{point.fic}'
                    },

                    credits: {
                        enabled: false
                    },
                    exporting: { enabled: false },
                    legend: {
                        title: {
                            text: 'Coverage %',
                            style: {
                                color: (Highcharts.theme && Highcharts.theme.textColor) || 'black'
                            }
                        },
                        align: 'left',
                        verticalAlign: 'bottom',
                        floating: true,
                        layout: 'vertical',
                        valueDecimals: 0,
                        backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || 'rgba(255, 255, 255, 0.85)',
                        symbolRadius: 0,
                        symbolHeight: 14
                    },
                    colorAxis: {
                        dataClasses: [
                            {
                                to: 49,
                                color:'#ee0109'
                            },
                            {
                                from: 50,
                                to: 79,
                                color:'#e8e644'
                            },
                            {
                                from: 80,
                                to: 100,
                                color:'#79db34'
                            },
                            {
                                from: 101,
                                color:'#288113'
                            }
                        ]
                    },

                    series: [{
                        showInLegend: true,
                        animation: true,
                        data: fic,
                        mapData: data,
                        joinBy: ['COUNTY', 'code'],
                        keys: ['COUNTY', 'code'],
                        //name: 'County',
                        //color: '#5bc0de',
                        animation: true,
                        states: {
                            hover: {
                                color: '#a4edba'
                            }
                        }
//                        dataLabels: {
//                            enabled: true,
//                            format: '{point.code}',
//                            style: {
//                                fontFamily: 'Varela Round',
//                                fontColor:'#cccccc',
//                            },
//                        }
                    }]
                });

            });

        });

        function ajax_fill_data(function_url,div){
            var function_url =url+function_url;
            var loading_icon=url+"assets/images/ring.gif";
            $.ajax({
                type: "POST",
                url: function_url,
                beforeSend: function() {
                    $(div).html("<img style='margin:10% 50% 10% ;' src="+loading_icon+">");
                },
                success: function(msg) {
                    $(div).html(msg);
                }
            });
        }




    </script>
