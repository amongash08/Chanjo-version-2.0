<style>
    .small-box .icon {font-size: 50px;}
    .small-box:hover .icon {font-size: 54px;}
    .select2-hidden-accessible {
        border: 1px solid #ccc;}
    .small-box h3 {font-size: 22px;}

    .page {
        width: 21cm;
        min-height: 29.7cm;
        padding: 2cm;
        margin: 1cm auto;
        border: 1px #D3D3D3 solid;
        border-radius: 5px;
        background: white;
        box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
    }
    .subpage {
        padding: 1cm;
        border: 5px red solid;
        height: 256mm;
        outline: 2cm #FFEAEA solid;
    }

    /*@page {*/
        /*size: A4;*/
        /*margin: 0;*/
    /*}*/
    @media print {
        html, body {
            width: 150mm;
            height: 50mm;
        }
        .page {
            margin: 0;
            border: initial;
            border-radius: initial;
            width: initial;
            min-height: initial;
            box-shadow: initial;
            background: initial;
            page-break-after: always;
        }
    }
    legend {
        font-size: 13px;
        font-weight: 600;
    }

    .fridgeNo .label {

        top: 0;
        left: 0;
        font-size: 12px;
        padding: 9px;
        left: -22%;
    }

</style>
<script type="text/javascript" src="https://cdn.jsdelivr.net/jsbarcode/3.6.0/JsBarcode.all.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/plugins/bootstrap-multiselect/dist/js/bootstrap-multiselect.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/js/printThis.js"></script>

<link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/bootstrap-multiselect/dist/css/bootstrap-multiselect.css" type="text/css"/>
<div class="row">

    <section class="col-lg-12 connectedSortable ui-sortable">


        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active links"><a href="#activity" data-id="activity" data-toggle="tab" aria-expanded="true">MY STORE</a></li>
                <li class="links"><a href="#timeline" data-id="timeline" data-toggle="tab" aria-expanded="true">REGIONAL STORES</a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="activity">

                    <div class="row this-store" id="<?php echo $mystore->id; ?>" data-store="<?php echo $mystore->name; ?>"
                         data-catchment-pop="<?php echo $mystore->catchment_population; ?>" data-live-births="<?php echo $mystore->live_birth_population; ?>"
                         data-vaccine-carriers="<?php echo $mystore->vaccine_carriers; ?>" data-cold-boxes="<?php echo $mystore->cold_boxes; ?>"
                         data-ice-packs="<?php echo $mystore->ice_packs; ?>"
                         data-time="<?php echo $mystore->time_to_store; ?>" data-distance="<?php echo $mystore->distance_to_store; ?>"
                         data-electricity="<?php echo $mystore->electricity_status; ?>" data-roof="<?php echo $mystore->roof_type; ?>"
                         data-mgname="<?php echo $mystore->manager_name; ?>" data-mgphone="<?php echo $mystore->manager_phone; ?>">
                        <div class="col-lg-2 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-aqua">
                                <div class="inner">
                                    <h3><?php echo count($myfridges); ?></h3>

                                    <p>Fridges</p>
                                </div>
                                <div class="icon">
                                    <i class="ion ion-thermometer"></i>
                                </div>
                            </div>
                        </div>
                        <!-- ./col -->
                        <div class="col-lg-2 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-green ">
                                <div class="inner">
                                    <h3><?php echo $mystore ->vaccine_carriers; ?><sup style="font-size: 20px"></sup></h3>

                                    <p>Vaccine Carriers</p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-archive" aria-hidden="true"></i>
                                </div>
<!--                                <a href="#" class="small-box-footer editmystore">Edit <i class="fa fa-arrow-circle-right"></i></a>-->
                            </div>
                        </div>
                        <!-- ./col -->
                        <div class="col-lg-2 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-yellow">
                                <div class="inner">
                                    <h3><?php echo $mystore ->live_birth_population; ?></h3>

                                    <p>Live Births</p>
                                </div>
                                <div class="icon">
                                    <i class="ion ion-ios-people"></i>
                                </div>
<!--                                <a href="#" class="small-box-footer editmystore">Edit <i class="fa fa-arrow-circle-right"></i></a>-->

                            </div>
                        </div>
                        <!-- ./col -->
                        <div class="col-lg-2 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-red">
                                <div class="inner">
                                    <h3><?php echo $mystore ->catchment_population; ?></h3>

                                    <p>Catchment Pop</p>
                                </div>
                                <div class="icon">
                                    <i class="ion ion-person-stalker"></i>
                                </div>
<!--                                <a href="#" class="small-box-footer editmystore">Edit <i class="fa fa-arrow-circle-right"></i></a>-->

                            </div>
                        </div>
                        <!-- ./col -->
                        <div class="col-lg-2 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-purple">
                                <div class="inner">
                                    <h3><?php echo $mystore ->cold_boxes; ?></h3>

                                    <p>Cold Boxes</p>
                                </div>
                                <div class="icon">
                                    <i class="ion ion-cube"></i>
                                </div>
<!--                                <a href="#" class="small-box-footer editmystore">Edit <i class="fa fa-arrow-circle-right"></i></a>-->

                            </div>
                        </div>
                        <!-- ./col -->

                        <div class="col-lg-2 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-teal">
                                <div class="inner">
                                    <h3><?php echo $mystore ->ice_packs; ?></h3>

                                    <p>Ice Packs</p>
                                </div>
                                <div class="icon">
                                    <i class="ion ion-ios-snowy"></i>
                                </div>
<!--                                <a href="#" class="small-box-footer editmystore">Edit <i class="fa fa-arrow-circle-right"></i></a>-->

                            </div>
                        </div>
                        <!-- ./col -->
                    </div>

                    <div class="box box-solid ">
                        <div class="box-header with-border">

                            <i class="fa fa-thermometer"></i>

                            <h3 class="box-title">
                                My Fridges
                            </h3>

                           <button type="button" id="myfridgesubmit" name="myfridgesubmit" class="btn btn-sm bg-olive btn-flat margin pull-right" data-toggle="modal" data-target="#modal-addfridges">
                                <i class="fa fa-thermometer-full"></i>  Add Refrigerator
                            </button>


                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="table-responsive">
                                <table class="table no-margin">
                                    <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Fridge ID</th>
                                        <th>Model</th>
                                        <th>Temp. Monitor No.</th>
                                        <th>Power Source</th>
                                        <th>Year of Installation</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    if(count($myfridges)==0){

                                        echo '<div style="margin:5%;font-size:3em;font-weight:400;"> No Fridges at this time.</div>';
                                    }else{


                                        foreach ($myfridges as $key=> $value) {
                                            // echo '<pre>',print_r($myfridges),'</pre>';exit;
                                            ?>



                                            <tr>
                                                <td class="alias"><?php echo $value->fridge_alias; ?></td>

                                                <td style="">
                                                    <?php echo $value->equipment_id; ?>
                                                </td>
                                                <td class="model" id="<?php echo $value->fridge_id; ?>"><?php echo $value->model; ?></td>
                                                <td class="tmp_mon_no"><span class="label label-danger"><?php echo $value->temperature_monitor_no; ?></span></td>

                                                <td class="powersrc"><?php echo $value->power_source; ?></td>
                                                <td class="install_year"><?php echo $value->installation_year; ?></td>

                                                <td class="fr_status"><?php echo $value->fridge_status; ?></td>
                                                <td><button type="button" id="<?php echo $value->id; ?>" class="btn btn-xs btn-flat bg-maroon editfridge"><i class="fa fa-edit"></i> Edit</button>
                                                    <button type="button" id="<?php echo $value->equipment_id; ?>" class="btn btn-xs btn-flat btn-primary barcode"><i class="fa fa-print"></i> Print Label</button>
                                                <a href="<?php echo base_url().'jobcard/generate/'.$value->id ?>" id="<?php echo $value->id; ?>" class='btn btn-xs btn-flat bg-purple jobcard '><i class='fa fa-edit'></i> Generate JobCard</a></td>

                                            </tr>
                                        <?php } }?>


                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer clearfix">

<!--                            <a href="javascript:void(0)" class="btn btn-sm btn-default btn-flat pull-right">View</a>-->
                        </div>
                        <!-- /.box-footer -->
                    </div>

                </div>
                <!-- /.tab-pane -->
                <div class="tab-pane" id="timeline">
                    <!-- The timeline -->

                    <div class="box box-info">
                        <div class="box-header with-border">
                            <i class="fa fa-podcast"></i>

                            <h3 class="box-title">
                                Regional Stores
                            </h3>

<!--                            <button type="button" class="btn btn-sm bg-olive btn-flat margin pull-right" data-toggle="modal" data-target="#">-->
<!--                                <i class="fa fa-print"></i>  Print-->
<!--                            </button>-->

                            <button type="button" class="btn btn-sm bg-olive btn-flat margin pull-right" data-toggle="modal" data-target="#modal-addstore">
                                <i class="fa fa-thermometer-full"></i>  Add Store
                            </button>

                            <button type="button" class="btn btn-sm bg-maroon btn-flat margin pull-right" data-toggle="modal" data-target="#modal-addstorefridge">
                                <i class="fa fa-thermometer-full"></i>  Add Store Refrigerator
                            </button>



                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="table-responsive">
                                <table class="table no-margin" id="otherstores">
                                    <thead>
                                    <tr>
                                        <th>Region</th>
                                        <th>Catchment Pop</th>
                                        <th>Live Births</th>
                                        <th>Pop preg women</th>
                                        <th>Pop surv infants</th>
                                        <th>Pop adolescent girls</th>
                                        <th>Cold Boxes</th>
                                        <th>Vaccine Carriers</th>
                                        <th>Ice Packs</th>
                                        <th>Fridges</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    if(count($stores)==0){

                                        echo '<div style="margin:5%;font-size:3em;font-weight:400;"> No Stores Found at this time.
                                            </div>';
                                    }else{


                                        foreach ($stores as $key=> $value) { ?>



                                            <tr class="store-container">

                                                <td class="" ><?php echo $value->name; ?></td>
                                                <td class="catchment_pop"><?php echo $value->catchment_population; ?></td>
                                                <td class="live_birth_pop"><?php echo $value->live_birth_population; ?></td>
                                                <td class="pop_pregnant_women"><?php echo $value->pop_pregnant_women; ?></td>
                                                <td class="pop_surviving_infants"><?php echo $value->pop_surviving_infants; ?></td>
                                                <td class="pop_adolescent_girls"><?php echo $value->pop_adolescent_girls; ?></td>
                                                <td class="cold_boxes"><?php echo $value->cold_boxes; ?></td>
                                                <td class="vaccine_carriers"><?php echo $value->vaccine_carriers; ?></td>
                                                <td class="ice_packs"><?php echo $value->ice_packs; ?></td>
                                                <td class="fridgeNo stores" id="<?php echo $value->id; ?>"></td>

                                                <td class="store" id="<?php echo $value->id; ?>" data-store="<?php echo $value->name; ?>"
                                                    data-time="<?php echo $value->time_to_store; ?>" data-distance="<?php echo $value->distance_to_store; ?>"
                                                    data-electricity="<?php echo $value->electricity_status; ?>" data-roof="<?php echo $value->roof_type; ?>"
                                                    data-mgname="<?php echo $value->manager_name; ?>" data-mgphone="<?php echo $value->manager_phone; ?>"
                                                    data-nearest-town="<?php echo $value->nearest_town; ?>">

                                                    <button type="button"  class="btn btn-sm btn-flat bg-maroon editstore " ><i class="fa fa-edit"></i> Edit Store </button></td>

                                            </tr>
                                        <?php } }?>


                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.box-body -->

                    </div>

                </div>
                <!-- /.tab-pane -->



            </div>
            <!-- /.tab-content -->
        </div>

    </section>


</div>

<div class="modal fade" id="modal-addstorefridge" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Add Refrigerator</h4>
            </div>
            <div class="modal-body">
                <form action="" method="post" id="add-Store-Fridge" class="form-horizontal">

                    <div class="row form-group has-feedback">
                        <div class="col-xs-6">
                            <select id="store_id2" name="store_id2" class=" form-control custom-select">
                                <option value="NULL">- Select Store -</option>
                                <?php
                                foreach ($stores as $key => $value) {
                                    $name = $value->name;
                                    $id = $value ->id;
                                    $location_id = $value ->location_id;
//                                    $county_id = $value ->county_id;
                                    echo "<option value='$id' data-location='$location_id' data-county=''>$name</option>";
                                }

                                ?>
                            </select>

                            <input type="hidden" class="form-control" name="name2" id="name2">
                        </div>

                        <div class="col-xs-6">
                            <select id="model2" name="model2" class=" form-control custom-select">
                                <option value="">- Select Fridge Model -</option>
                                <?php
                                foreach ($fridges as $key => $value) {
                                    $name = $value->model;
                                    $id = $value ->id;
                                    echo "<option value='$id'>$name</option>";
                                }

                                ?>
                            </select>
                        </div>


                    </div>

                    <div class="row form-group has-feedback">
                        <div class="col-xs-6">
                            <select class="form-control custom-select" style="" name="power_source2" id="power_source2" >
                                <option value="Electricity">Electricity</option>
                                <option value="Solar">Solar</option>
                                <option value="Gas">Gas</option>

                            </select>                        </div>
                        <div class="col-xs-6">
                            <select id="status2" name="status2" class=" form-control custom-select">
                                <option value="">- Select Fridge Status -</option>
                                <option value="Functional">Functional</option>
                                <option value="Nonfunctional">Non-Functional</option>

                            </select>
                        </div>


                    </div>

                    <div class="row form-group has-feedback">

                        <div class="col-xs-6">

                            <input type="text" id="temp_monitor_no2" name="temp_monitor_no2" class="form-control" placeholder="Temp. Monitor No.">
                        </div>
                        <div class="col-xs-6">
                            <select id="installation_year2" name="installation_year2" class=" form-control custom-select">
                                <option value="">- Select Year Installed -</option>
                            </select>
                        </div>


                    </div>

                    <div class="row form-group has-feedback">

                        <div class="col-xs-6">

                            <input type="text" id="alias2" name="alias2" class="form-control" placeholder="Alias E.g. TCW-Pharmacy1A">
                        </div>


                    </div>






                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default  btn-flat" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary  btn-flat" id="addStoreFridge"><i class="fa fa-plus"></i>  Add</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="modal-addfridges" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Add Refrigerator</h4>
            </div>
            <div class="modal-body">
                <form action="" method="post" id="myfridge" class="form-horizontal">

                    <div class="row form-group has-feedback">
                        <div class="col-xs-6">
                            <label>Refrigerator Model</label>
                            <select id="model" name="model" class=" form-control custom-select">
                                <option value="">- Select Fridge Model -</option>
                                <?php
                                foreach ($fridges as $key => $value) {
                                    $name = $value->model;
                                    $id = $value ->id;
                                    echo "<option value='$id'>$name</option>";
                                }

                                ?>
                            </select>
                        </div>

                        <div class="col-xs-6">
                            <label>Refrigerator Power Source</label>
                            <select class="form-control custom-select" style="" name="power_source" id="power_source"  >
                                <option value="Electricity">Electricity</option>
                                <option value="Solar">Solar</option>
                                <option value="Gas">Gas</option>

                            </select>
                        </div>

                    </div>

                    <div class="row form-group has-feedback">
                        <div class="col-xs-6">
                            <label>Refrigerator Status</label>
                            <select id="status" name="status" class=" form-control custom-select">
                                <option value="">- Select Fridge Status -</option>
                                <option value="Functional">Functional</option>
                                <option value="Nonfunctional">Non-Functional</option>

                            </select>
                        </div>

                        <div class="col-xs-6">
                            <input type="hidden" id="store_id" name="store_id" value="<?php echo $mystore ->id; ?>">
                            <label>Temp Monitoring No</label>

                            <input type="text" id="temp_monitor_no" name="temp_monitor_no" class="form-control" placeholder="Temp. Monitor No.">
                        </div>
                    </div>

                    <div class="row form-group has-feedback">
                        <div class="col-xs-6">
                            <label>Year Installed</label>
                            <select id="installation_year" name="installation_year" class=" form-control custom-select">
                                <option value="">- Select Year Installed -</option>
                            </select>
                        </div>

                        <div class="col-xs-6">
                            <label>Refrigerator Alias</label>
                            <input type="text" id="alias" name="alias" class="form-control" placeholder="Alias. E.g. Cold-Room 1A">

                        </div>


                    </div>


                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default  btn-flat" data-dismiss="modal">Close</button>
                <button type="submit" id="storefridgesubmit" name="storefridgesubmit" class="btn btn-primary  btn-flat"><i class="fa fa-plus"></i>  Add</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<div class="modal fade" id="modal-addstore" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Add Store</h4>
            </div>
            <div class="modal-body">

                <!-- Horizontal Form -->
                <div class="">
                   <!-- form start -->
                    <form action="" method="post" id="store" class="">
                        <ul class="nav nav-pills">
                        <li class="active"><a href="#store-tab" data-toggle="tab">Store information</a></li>
                        <li><a href="#cc-tab" data-toggle="tab">Cold Chain Information</a></li>
                        <li><a href="#logistics-tab" data-toggle="tab">Logistics Information</a></li>
                        <li><a href="#wastage-tab" data-toggle="tab">Vaccine Wastage Information</a></li>
                    </ul>
                    <div class="box-body" style="max-height: 400px;">
                        <div class="tab-content">
                            <!-- First tab -->
                            <div class="tab-pane active" id="store-tab">

                                <div class="row form-group has-feedback">
                                    <div class="col-xs-6" data-toggle="tooltip" data-placement="left" title="Store">
                                        <label for="firstName" class="control-label">Region</label>
                                        <select id="regions" name="regions" class=" form-control custom-select">
                                            <option value="NULL">- Select Region -</option>
                                            <?php
                                            foreach ($regions as $key => $value) {
                                                $name = $value->region_name;
                                                $id = $value ->id;
                                                echo "<option value='$id'>$name</option>";
                                            }

                                            ?>
                                        </select>
                                        <input type="hidden" class="form-control" name="name" id="name" placeholder="Store Name">
                                    </div>

                                    <div class="col-xs-6">
                                        <label for="firstName" class="control-label">Store Manager Name</label>
                                        <input type="text" data-toggle="tooltip" data-placement="left" title="Store Manager Name" name="manager_name" id="manager_name" class="form-control" placeholder="Store Manager Name">
                                    </div>




                                </div>

                                <div class="row form-group has-feedback">

                                    <div class="col-xs-6">
                                        <label for="manager_phone" class="control-label">Store Manager Phone</label>
                                        <input type="number" data-toggle="tooltip" data-placement="left" title="Store Manager Phone" name="manager_phone" id="manager_phone" class="form-control" placeholder="Store Manager Phone">
                                    </div>

                                    <div class="col-xs-6">
                                        <label for="nearesttown" class="control-label">Nearest Town</label>
                                        <input type="text" data-toggle="tooltip" data-placement="left" title="Nearest Town" name="nearesttown" id="nearesttown" class="form-control" min="0" placeholder="Nearest Town">
                                    </div>


                                </div>

                                <div class="row form-group has-feedback">
                                    <div class="col-xs-6">
                                        <label for="electricity" class="control-label">Electricity Availability</label>
                                        <select id="electricity" data-toggle="tooltip" data-placement="left" title="Electricity Availability" name="electricity" class=" form-control custom-select">
                                            <option value="NULL">- Electicity -</option>
                                            <option value="Yes"> Yes </option>
                                            <option value="No"> No </option>

                                        </select>
                                    </div>
                                    <div class="col-xs-6">
                                        <label for="roof" class="control-label">Roof Type</label>
                                        <select id="roof" data-toggle="tooltip" data-placement="left" title="Type of Roof" name="roof" class=" form-control custom-select">
                                            <option value="NULL">- Roof Type  -</option>
                                            <option value="Asbestos">Asbestos</option>
                                            <option value="Concrete">Concrete</option>
                                            <option value="Iron Sheets">Iron Sheets</option>
                                            <option value="Tiles">Tiles</option>

                                        </select>
                                    </div>
                                </div>

                                <!-- end form -->
                                <div class="box-footer" style="padding:10px 10px 0 0;">
                                    <!-- Previous/Next buttons -->
                                    <button type="button" class="btn bg-maroon btn-flat margin pull-right" data-dismiss="modal">Close</button>
                                    <button type="button" class="btn bg-olive btn-flat margin pull-right" style="margin-right:5px;" href="#cc-tab" data-toggle="tab">Next</button>

                                </div>
                                <!-- /.box-footer -->




                            </div>

                            <!-- Second tab -->
                            <div class="tab-pane " id="cc-tab">

                                <div class="row form-group has-feedback">
                                    <div class="col-xs-6">
                                        <label for="time" class="control-label">Time To Main Store</label>
                                        <input type="number" data-toggle="tooltip" data-placement="left" title="Time To Main Store" name="time" id="time" class="form-control" min="0" placeholder="Time To Main Store Hrs">
                                    </div>
                                    <div class="col-xs-6">
                                        <label for="distance" class="control-label">Distance To Main Store</label>
                                        <input type="number" data-toggle="tooltip" data-placement="left" title="Distance To Main Store" name="distance" id="distance" class="form-control" min="0" placeholder="Distance To Main Store (Km)">
                                    </div>
                                </div>
                                <div class="row form-group has-feedback">
                                    <div class="col-xs-4">
                                        <label for="live_birth_pop" class="control-label">Live Birth Population</label>
                                        <input type="number" data-toggle="tooltip" data-placement="left" title="Live Birth Pop" name="live_birth_pop" id="live_birth_pop" class="form-control" min="0" placeholder="Catchment Population">
                                    </div>
                                    <div class="col-xs-4">
                                        <label for="pop_pregnant_women" class="control-label">Pregnant Women Population</label>
                                        <input type="number" data-toggle="tooltip" data-placement="left" title="Pregnant Women Pop" name="pop_pregnant_women" id="pop_pregnant_women" class="form-control" min="0" placeholder="Catchment Population">
                                    </div>
                                    <div class="col-xs-4">
                                        <label for="catchment_pop" class="control-label">Catchment Population</label>
                                        <input type="number" data-toggle="tooltip" data-placement="left" title="Catchment Pop" name="catchment_pop" id="catchment_pop" class="form-control" min="0" placeholder="Catchment Population">
                                    </div>
                                </div>
                                <div class="row form-group has-feedback">
                                    <div class="col-xs-4">
                                        <label for="pop_surviving_infants" class="control-label">Surviving Infants Population</label>
                                        <input type="number" data-toggle="tooltip" data-placement="left" title="Surviving Infants" name="pop_surviving_infants" id="pop_surviving_infants" class="form-control" min="0" placeholder="Catchment Population">
                                    </div>
                                    <div class="col-xs-4">
                                        <label for="pop_adolescent_girls" class="control-label">Cohort Girls Population</label>
                                        <input type="number" data-toggle="tooltip" data-placement="left" title="Cohort Girls Pop" name="pop_adolescent_girls" id="pop_adolescent_girls" class="form-control" min="0" placeholder="Catchment Population">
                                    </div>
                                </div>

                                <!-- end form -->
                                <div class="box-footer" style="padding:10px 10px 0 0;">
                                    <!-- Previous/Next buttons -->
                                    <button type="button" class="btn bg-olive btn-flat margin  btn-flat" href="#store-tab" data-toggle="tab" >Previous</button>
                                    <button type="button" class="btn bg-maroon btn-flat margin pull-right" data-dismiss="modal">Close</button>
                                    <button type="button" class="btn bg-olive btn-flat margin pull-right" href="#logistics-tab" data-toggle="tab" style="margin-right:5px;">Next</button>

                                </div>
                                <!-- /.box-footer -->

                            </div>

                            <!-- Third tab -->
                            <div class="tab-pane " id="logistics-tab">

                                <div class="row form-group has-feedback">
                                    <div class="col-xs-4">
                                        <label for="cold_boxes" class="control-label">Cold Boxes</label>
                                        <input type="number" data-toggle="tooltip" data-placement="left" title="Cold Boxes" name="cold_boxes" id="cold_boxes" class="form-control" min="0" placeholder="Cold boxes">
                                    </div>
                                    <div class="col-xs-4">
                                        <label for="vaccine_carriers" class="control-label">Vaccine Carriers</label>
                                        <input type="number" data-toggle="tooltip" data-placement="left" title="Vaccine Carriers" name="vaccine_carriers" id="vaccine_carriers" class="form-control" min="0" placeholder="Vaccine Carriers">
                                    </div>

                                    <div class="col-xs-4">
                                        <label for="ice_packs" class="control-label">Ice Packs</label>
                                        <input type="number" data-toggle="tooltip" data-placement="left" title="Ice Packs" name="ice_packs" id="ice_packs" class="form-control" min="0" placeholder="Ice Packs">
                                    </div>



                                </div>

                                <!-- end form -->
                                <div class="box-footer" style="padding:10px 10px 0 0;">
                                    <!-- Previous/Next buttons -->
                                    <button type="button" class="btn bg-olive btn-flat margin  btn-flat" href="#cc-tab" data-toggle="tab">Previous</button>
                                    <button type="button" class="btn bg-maroon btn-flat margin pull-right" data-dismiss="modal">Close</button>
                                    <button type="button" class="btn bg-olive btn-flat margin pull-right" style="margin-right:5px;" href="#wastage-tab" data-toggle="tab">Next</button>

                                </div>

                            </div>


                            <div class="tab-pane" id="wastage-tab">

                                <div class="row form-group has-feedback">
                                    <?php
                                   /// echo '<pre>',print_r($vaccines),'</pre>';exit;
                                        foreach ($vaccines as $value => $key){


                                    ?>

                                            <div class="col-xs-4">
                                                <label for="<?php echo $key->vaccine_name; ?>" class="control-label"><?php echo $key->vaccine_name; ?></label>
                                                <input type="number" data-toggle="tooltip" data-placement="left" title="<?php echo $key->vaccine_name; ?>" name="wastage[]" id="wastage[]" class="form-control" min="0" placeholder="Wastage Factor">
                                                <input type="hidden" class="form-control" id="vaccine_id[]" name="vaccine_id[]" value="<?php echo $key->id; ?>" placeholder="Store Name">

                                            </div>


                                    <?php } ?>
                                </div>

                                <!-- end form -->
                                <div class="box-footer" style="padding:10px 10px 0 0;">
                                    <!-- Previous/Next buttons -->
                                    <button type="button" class="btn bg-olive btn-flat margin" href="#logistics-tab" data-toggle="tab">Previous</button>
                                    <button type="button" class="btn bg-navy btn-flat margin pull-right" id="storesubmit"  data-dismiss="modal">Finish</button>
                                    <button type="button" class="btn bg-maroon btn-flat margin pull-right" data-dismiss="modal" style="margin-right:5px;">Close</button>


                                </div>
                                <!-- /.box-footer -->


                            </div><!-- Fourth tab -->

                        </div>


                    </div>
                        <!-- /.box-body -->
                    </form>

                    <?php echo form_close(); ?>
                </div>
                <!-- /.box -->

            </div>

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->



<div class="modal fade" id="modal-showstorefridges" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document" style="width:86%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Store Fridges</h4>

            </div>
            <div class="modal-body" >

                <table class="table no-margin">
                    <thead>
                    <tr>
                        <th>Fridge Name</th>
                        <th>Fridge-ID</th>
                        <th>Model</th>
                        <th>Power Source</th>
                        <th>Temp Mon No</th>
                        <th>Year Installed</th>
                        <th>Status</th>
                        <th>Action</th>

                    </tr>
                    </thead>
                    <tbody id="display">

                    </tbody>
                </table>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default  btn-flat" data-dismiss="modal">Close</button>
                <!--                <button type="submit" class="btn btn-primary  btn-flat"><i class="fa fa-plus"></i>  Add</button>-->
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<div class="modal fade" id="modal-editfridge" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-md" role="document" style="width: 40%">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Edit Refrigerator</h4>

            </div>
            <div class="modal-body" >

                <form action="" method="post" id="editmyfridge" class="form-horizontal">

                    <div class="row form-group has-feedback">
                        <div class="col-xs-6">
                            <label>Refrigerator Model</label>
                            <select id="edit_model" name="edit_model" disabled class=" form-control custom-select">
                                <option value="">- Select Refrigerator Model -</option>
                                <?php
                                foreach ($fridges as $key => $value) {
                                    $name = $value->model;
                                    $id = $value ->id;
                                    echo "<option value='$id'>$name</option>";
                                }

                                ?>
                            </select>

                            <input type="hidden" id="edit_fridge_id" name="edit_fridge_id" value="">

                        </div>

                        <div class="col-xs-6">
                            <label style="width:100%">Power Source</label>
                            <select class="form-control custom-select" style="" name="edit_power_source" id="edit_power_source"  >
                                <option value="">- Select Power Source -</option>
                                <option value="Electricity">Electricity</option>
                                <option value="Solar">Solar</option>
                                <option value="Gas">Gas</option>

                            </select>
                        </div>

                    </div>

                    <div class="row form-group has-feedback">
                        <div class="col-xs-6">
                            <label>Fridge Status</label>
                            <select id="edit_status" disabled name="edit_status" class=" form-control custom-select">
                                <option value="">- Select Fridge Status -</option>
                                <option value="Functional">Functional</option>
                                <option value="Nonfunctional">Non-Functional</option>

                            </select>
                        </div>

                        <div class="col-xs-6">
                            <label>Temprature Monitoring No</label>
                            <input type="hidden" id="edit_store_id" name="edit_store_id" value="<?php echo $mystore ->id; ?>">

                            <input type="text" id="edit_temp_monitor_no" name="edit_temp_monitor_no" class="form-control" placeholder="Temp. Monitor No.">
                        </div>
                    </div>

                    <div class="row form-group has-feedback">
                        <div class="col-xs-6">
                            <label>Year Installed</label>
                            <select id="edit_installation_year" disabled name="edit_installation_year" class=" form-control custom-select">
                                <option value="">- Select Year Installed -</option>
                            </select>
                        </div>

                        <div class="col-xs-6">
                            <label>Refrigerator Alias</label>
                            <input type="text" id="edit_alias" name="edit_alias" class="form-control" placeholder="Alias">

                            </select>
                        </div>


                    </div>


                </form>



            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-danger  btn-flat pull-left delFridge" id=""><i class="fa fa-trash-o"></i>  Delete</button>

                <button type="submit" class="btn btn-success  btn-flat" id="editFridge"><i class="fa fa-save"></i>  Save</button>

                <button type="button" class="btn btn-default  btn-flat" data-dismiss="modal">Close</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="sticker-contain page" hidden >

    <div class="sticker subpage" style="margin:auto;">

        <div id="location-text" style="padding-left:18%;font-size: 1.6em "><small><strong>
                    <?php echo $location_breadcrumb; ?></strong></small></div>

        <svg id="bar-code"
             jsbarcode-value=""
             jsbarcode-textmargin="1"
             jsbarcode-fontoptions="bold"
             jsbarcode-lineColor="black">
        </svg>





    </div>
</div>


<div class="modal fade" id="modal-editmystore" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Edit Store</h4>
            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default  btn-flat" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-success  btn-flat" id="mystore_edit" data-dismiss="modal"><i class="fa fa-edit"></i>  Update</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="modal-editstore" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Edit Store</h4>
            </div>
            <div class="modal-body">

                <!-- Horizontal Form -->
                <div class="">
                    <!-- form start -->
                    <form action="" method="post" id="modify_store" class="">
                        <ul class="nav nav-pills">
                            <li class="active"><a href="#store-tab_edit" data-toggle="tab">Store information</a></li>
                            <li><a href="#cc-tab_edit" data-toggle="tab">Cold Chain Information</a></li>
                            <li><a href="#logistics-tab_edit" data-toggle="tab">Logistics Information</a></li>
                            <li><a href="#wastage-tab_edit" data-toggle="tab">Vaccine Wastage Information</a></li>
                        </ul>
                        <div class="box-body" style="max-height: 400px;">
                            <div class="tab-content">
                                <!-- First tab -->
                                <div class="tab-pane active" id="store-tab_edit">

                                    <div class="row form-group has-feedback">
                                        <div class="col-xs-6" data-toggle="tooltip" data-placement="left" title="Store">
                                            <label for="region" class="control-label">Region</label>
                                            <select id="edit_stores"  name="edit_stores" disabled class=" form-control custom-select">
                                      <option value="NULL">- Select Region -</option>
                                      <?php
                                      foreach ($stores as $key => $value) {
                                          $name = $value->name;
                                          $id = $value ->id;
                                          $location_id = $value ->location_id;
  //                                    $county_id = $value ->county_id;
                                          echo "<option value='$id' data-location='$location_id' data-county=''>$name</option>";
                                      }

                                      ?>
                                  </select>

                                            <input type="hidden" class="form-control" name="store_" id="store_" placeholder="Store Name">
                                        </div>

                                        <div class="col-xs-6">
                                            <label for="Store Manager Name" class="control-label">Store Manager Name</label>
                                            <input type="text" name="edit_manager_name" id="edit_manager_name" class="form-control" min="0" placeholder="Manager Name">
                                        </div>




                                    </div>

                                    <div class="row form-group has-feedback">

                                        <div class="col-xs-6">
                                            <label for="edit_manager_phone" class="control-label">Store Manager Phone</label>
                                            <input type="number"  name="edit_manager_phone" id="edit_manager_phone" class="form-control" placeholder="Store Manager Phone">
                                        </div>

                                        <div class="col-xs-6">
                                            <label for="edit_nearesttown" class="control-label">Nearest Town</label>
                                            <input type="text" name="edit_nearesttown" id="edit_nearesttown" class="form-control" placeholder="Nearest Town">
                                        </div>


                                    </div>

                                    <div class="row form-group has-feedback">
                                        <div class="col-xs-6">
                                            <label for="edit_electricity" class="control-label">Electricity Availability</label>
                                            <select id="edit_electricity" data-toggle="tooltip" data-placement="left" title="Electricity Availability" name="edit_electricity" class=" form-control custom-select">
                                                <option value="NULL">- Electicity -</option>
                                                <option value="Yes"> Yes </option>
                                                <option value="No"> No </option>

                                            </select>
                                        </div>
                                        <div class="col-xs-6">
                                            <label for="edit_roof" class="control-label">Roof Type</label>
                                            <select id="edit_roof" data-toggle="tooltip" data-placement="left" title="Type of Roof" name="edit_roof" class=" form-control custom-select">
                                                <option value="NULL">- Roof Type  -</option>
                                                <option value="Asbestos">Asbestos</option>
                                                <option value="Concrete">Concrete</option>
                                                <option value="Iron Sheets">Iron Sheets</option>
                                                <option value="Tiles">Tiles</option>

                                            </select>
                                        </div>
                                    </div>

                                    <!-- end form -->
                                    <div class="box-footer" style="padding:10px 10px 0 0;">
                                        <!-- Previous/Next buttons -->
                                        <button type="button" class="btn bg-maroon btn-flat margin pull-right" data-dismiss="modal">Close</button>
                                        <button type="button" class="btn bg-olive btn-flat margin pull-right" style="margin-right:5px;" href="#cc-tab_edit" data-toggle="tab">Next</button>

                                    </div>
                                    <!-- /.box-footer -->




                                </div>

                                <!-- Second tab -->
                                <div class="tab-pane " id="cc-tab_edit">

                                    <div class="row form-group has-feedback">
                                        <div class="col-xs-6">
                                            <label for="edit_time" class="control-label">Time To Main Store</label>
                                            <input type="number" data-toggle="tooltip" data-placement="left" title="Time To Main Store" name="edit_time" id="edit_time" class="form-control" min="0" placeholder="Time To Main Store Hrs">
                                        </div>
                                        <div class="col-xs-6">
                                            <label for="edit_distance" class="control-label">Distance To Main Store</label>
                                            <input type="number" data-toggle="tooltip" data-placement="left" title="Distance To Main Store" name="edit_distance" id="edit_distance" class="form-control" min="0" placeholder="Distance To Main Store (Km)">
                                        </div>
                                    </div>
                                    <div class="row form-group has-feedback">
                                        <div class="col-xs-4">
                                            <label for="edit_live_birth_pop" class="control-label">Live Birth Population</label>
                                            <input type="number" data-toggle="tooltip" data-placement="left" title="Live Birth Pop" name="edit_live_birth_pop" id="edit_live_birth_pop" class="form-control" min="0" placeholder="Catchment Population">
                                        </div>
                                        <div class="col-xs-4">
                                            <label for="edit_pop_pregnant_women" class="control-label">Pregnant Women Population</label>
                                            <input type="number" data-toggle="tooltip" data-placement="left" title="Pregnant Women Pop" name="edit_pop_pregnant_women" id="edit_pop_pregnant_women" class="form-control" min="0" placeholder="Catchment Population">
                                        </div>
                                        <div class="col-xs-4">
                                            <label for="edit_catchment_pop" class="control-label">Catchment Population</label>
                                            <input type="number" data-toggle="tooltip" data-placement="left" title="Catchment Pop" name="edit_catchment_pop" id="edit_catchment_pop" class="form-control" min="0" placeholder="Catchment Population">
                                        </div>
                                    </div>
                                    <div class="row form-group has-feedback">
                                        <div class="col-xs-4">
                                            <label for="edit_pop_surviving_infants" class="control-label">Surviving Infants Population</label>
                                            <input type="number" data-toggle="tooltip" data-placement="left" title="Surviving Infants" name="edit_pop_surviving_infants" id="edit_pop_surviving_infants" class="form-control" min="0" placeholder="Catchment Population">
                                        </div>
                                        <div class="col-xs-4">
                                            <label for="edit_pop_adolescent_girls" class="control-label">Cohort Girls Population</label>
                                            <input type="number" data-toggle="tooltip" data-placement="left" title="Cohort Girls Pop" name="edit_pop_adolescent_girls" id="edit_pop_adolescent_girls" class="form-control" min="0" placeholder="Catchment Population">
                                        </div>
                                    </div>

                                    <!-- end form -->
                                    <div class="box-footer" style="padding:10px 10px 0 0;">
                                        <!-- Previous/Next buttons -->
                                        <button type="button" class="btn bg-olive btn-flat margin  btn-flat" href="#store-tab_edit" data-toggle="tab" >Previous</button>
                                        <button type="button" class="btn bg-maroon btn-flat margin pull-right" data-dismiss="modal">Close</button>
                                        <button type="button" class="btn bg-olive btn-flat margin pull-right" href="#logistics-tab_edit" data-toggle="tab" style="margin-right:5px;">Next</button>

                                    </div>
                                    <!-- /.box-footer -->

                                </div>

                                <!-- Third tab -->
                                <div class="tab-pane " id="logistics-tab_edit">

                                    <div class="row form-group has-feedback">
                                        <div class="col-xs-4">
                                            <label for="cold_boxes" class="control-label">Cold Boxes</label>
                                            <input type="number" data-toggle="tooltip" data-placement="left" title="Cold Boxes" name="edit_cold_boxes" id="edit_cold_boxes" class="form-control" min="0" placeholder="Cold boxes">
                                        </div>
                                        <div class="col-xs-4">
                                            <label for="vaccine_carriers" class="control-label">Vaccine Carriers</label>
                                            <input type="number" data-toggle="tooltip" data-placement="left" title="Vaccine Carriers" name="edit_vaccine_carriers" id="edit_vaccine_carriers" class="form-control" min="0" placeholder="Vaccine Carriers">
                                        </div>

                                        <div class="col-xs-4">
                                            <label for="ice_packs" class="control-label">Ice Packs</label>
                                            <input type="number" data-toggle="tooltip" data-placement="left" title="Ice Packs" name="edit_ice_packs" id="edit_ice_packs" class="form-control" min="0" placeholder="Ice Packs">
                                        </div>



                                    </div>

                                    <!-- end form -->
                                    <div class="box-footer" style="padding:10px 10px 0 0;">
                                        <!-- Previous/Next buttons -->
                                        <button type="button" class="btn bg-olive btn-flat margin  btn-flat" href="#cc-tab_edit" data-toggle="tab">Previous</button>
                                        <button type="button" class="btn bg-maroon btn-flat margin pull-right" data-dismiss="modal">Close</button>
                                        <button type="button" class="btn bg-olive btn-flat margin pull-right" style="margin-right:5px;" href="#wastage-tab_edit" data-toggle="tab">Next</button>

                                    </div>

                                </div>


                                <div class="tab-pane" id="wastage-tab_edit">

                                    <div class="row form-group has-feedback display_edit">


                                    </div>

                                    <!-- end form -->
                                    <div class="box-footer" style="padding:10px 10px 0 0;">
                                        <!-- Previous/Next buttons -->
                                        <button type="button" class="btn bg-olive btn-flat margin" href="#logistics-tab_edit" data-toggle="tab">Previous</button>
                                        <button type="button" class="btn bg-navy btn-flat margin pull-right" id="stores_edit"  data-dismiss="modal">Finish</button>
                                        <button type="button" class="btn bg-maroon btn-flat margin pull-right" data-dismiss="modal" style="margin-right:5px;">Close</button>


                                    </div>
                                    <!-- /.box-footer -->


                                </div><!-- Fourth tab -->

                            </div>


                        </div>
                        <!-- /.box-body -->
                    </form>

                    <?php echo form_close(); ?>
                </div>
                <!-- /.box -->

            </div>

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->





<script type="text/javascript">

    $( document ).ready(function() {

      $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
          //save the latest tab;
          localStorage.setItem('selectedTab', $(e.target).attr('data-id'));
      });

      //var hash = window.location.hash;
      //switching to correct tab
      var hash = localStorage.getItem('selectedTab');
      var selectedTab = $('.nav li a[href="#' + hash + '"]');
      selectedTab.trigger('click', true);

        toastr.options = {
            "closeButton": false,
            "debug": false,
            "newestOnTop": true,
            "progressBar": false,
            "positionClass": "toast-top-right",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        }

        $(".fridgeNo").each(function() {
            var store_id=$(this).attr('id');
            var obj=$(this);



            var siteurl="<?php echo base_url(); ?>";
            var url="<?php echo base_url(); ?>fridges/getById/"+store_id;

            $.ajax({
                type: "POST",
                url: url,
                beforeSend: function() {
                    // something
                },
                success: function(data)
                {
                    obj.html('<button type="button"  class="btn btn-sm btn-flat btn-primary " ><small class="label bg-blue">'+data.length+'</small>View</button>');

                },error: function (jqXHR) {

                }
            });

        });


        $('#example-getting-started,#example2,#example3').multiselect(
            {
                includeSelectAllOption: true,
                enableFiltering: false,
                maxHeight: 200,
                filterPlaceholder: 'Select Power Source(s)'
            }
        );


        $('.barcode').click(function(){

            var eid=$(this).attr('id');
            $("#bar-code").attr('jsbarcode-value',eid);
            JsBarcode("#bar-code").init();

//            console.log(eid);

            setTimeout($('.sticker').printThis({
                importCSS: true,            // import page CSS
                importStyle: true
            }),200)


        }) ;

        $(document).on("keyup", "#live_birth_pop", function(event){

            var catchment_pop=$('#catchment_pop').val();
            var live_birth_pop=$('#live_birth_pop').val();

            if(parseInt(live_birth_pop) > parseInt(catchment_pop)){
                console.log(live_birth_pop+'>'+catchment_pop);

                toastr.error('Your catchment population should be higher than live births.');
                $('#live_birth_pop').val('')

            }


        }) ;

        $(document).on("keyup", "#edit_live_birth_pop", function(event){

            var catchment_pop=$('#edit_catchment_pop').val();
            var live_birth_pop=$('#edit_live_birth_pop').val();


            if(parseInt(live_birth_pop) > parseInt(catchment_pop)){
                console.log(live_birth_pop+'>'+catchment_pop);

                toastr.error('Your catchment population should be higher than live births.');
                $('#edit_live_birth_pop').val('')

            }


        }) ;

        $(document).on("keyup", "#edit_mylive_birth_pop", function(event){

            var catchment_pop=$('#edit_mycatchment_pop').val();
            var live_birth_pop=$('#edit_mylive_birth_pop').val();


            if(parseInt(live_birth_pop) > parseInt(catchment_pop)){
                console.log(live_birth_pop+'>'+catchment_pop);

                toastr.error('Your catchment population should be higher than live births.');
                $('#edit_mylive_birth_pop').val('')

            }


        }) ;

        $(document).on("keyup", "#time", function(event){
            var time = $("#time").val();
            var distance = 0;

            distance = time*56.77;

            $("#distance").val(distance);


        }) ;

        $(document).on("keyup", "#edit_time", function(event){
            var time = $("#edit_time").val();
            var distance = 0;

            distance = time*56.77;

            $("#edit_distance").val(distance);


        }) ;

        $(document).on("keyup", "#edit_mytime", function(event){
            var time = $("#edit_mytime").val();
            var distance = 0;

            distance = time*56.77;

            $("#edit_mydistance").val(distance.toFixed(2));


        }) ;

        $(document).on("click", ".facilitybarcode", function(event){

            var store_eid=$(this).attr('id');
//            console.log(store_eid);
            $("#bar-code").attr('jsbarcode-value',store_eid);
            JsBarcode("#bar-code").init();

            //console.log('haha');

            setTimeout($('.sticker').printThis({
                importCSS: true,            // import page CSS
                importStyle: true
            }),200)
        });

        $('#regions').on('change', function(){

            var name= $("option:selected", this).text();
            $("#name").val(name)
            console.log(name);


        });

        $('#store_id2').on('change', function(){

            var location= $("option:selected", this).attr('data-location');
            var name= $("option:selected", this).text();
            $("#name2").val(name);


        });

        $(document).on("click", ".editfridge", function(event){

            var fr_id=$(this).attr('id');
            var model=$(this).closest('tr').find('.model').attr('id');
            var tmp_mon_no=$(this).closest('tr').find('.tmp_mon_no').text();
            var status=$(this).closest('tr').find('.fr_status').text();
            var year=$(this).closest('tr').find('.install_year').text();
            var powersrc=$(this).closest('tr').find('.powersrc').text();
            var alias=$(this).closest('tr').find('.alias').text();
            $("#edit_installation_year").val(year);
            $("#edit_status").val(status);
            $("#edit_temp_monitor_no").val(tmp_mon_no);
            $("#edit_model").val(model);
            $("#edit_fridge_id").val(fr_id);
            $("#edit_alias").val(alias);
            $("#edit_power_source").val(powersrc);


            $('#modal-editfridge').modal('show');

        });

        $(document).on("click", ".delFridge", function(event){

            var fr_id=$('#edit_fridge_id').val();
            var url="<?php echo base_url(); ?>fridges/delete/"+fr_id;

            swal({
                    title: "Are you sure?",
                    text: "You will not be able to recover this Fridge!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, delete it!",
                    cancelButtonText: "No, cancel!",
                    closeOnConfirm: false,
                    closeOnCancel: false
                },
                function(isConfirm){
                    if (isConfirm) {

                        $.ajax({
                            type: "POST",
                            url: url,
                            beforeSend: function() {
                            },
                            success: function(data)
                            {

                                console.log(data);


                            },error: function (jqXHR) {
                                console.log(jqXHR);
                                toastr.error(jqXHR.responseText);
                            }
                        });

                        swal("Deleted!", "This Fridge has been deleted.", "success");
                        setTimeout(location.reload.bind(location), 800);

                    } else {
                        swal("Cancelled", "Your Fridge is safe :)", "error");
                    }
                });


        });

        $(document).on("click", "#editFridge", function(event){

            var url="<?php echo base_url(); ?>fridges/edit"; // the script where you handle the form input.
            var formData ='#editmyfridge';
            var msg ='Successfully edited Fridge';

//        console.log( $( '#editmyfridge' ).serialize() );
//        return;

            processData(formData,url,msg);

            event.preventDefault(); // avoid to execute the actual submit of the form.


        });

        $(document).on("click", "#stores_edit", function(event){

            var url="<?php echo base_url(); ?>store/edit"; // the script where you handle the form input.
            var formData ='#modify_store';
            var msg ='Successfully edited Store';

//        console.log( $( '#modify_store' ).serialize() );
//        return;

            processData(formData,url,msg);

            event.preventDefault(); // avoid to execute the actual submit of the form.


        });

        $(document).on("click", "#mystore_edit", function(event){

            var url="<?php echo base_url(); ?>store/editmystore"; // the script where you handle the form input.
            var formData ='#modify_mystore';
            var msg ='Successfully edited Store';

//        console.log( $( '#modify_store' ).serialize() );
//        return;

            processData(formData,url,msg);

            event.preventDefault(); // avoid to execute the actual submit of the form.


        });

        $(document).on("click", ".editstorefridge", function(event){

            $('#modal-showstorefridges').modal('hide');

            var fr_id=$(this).attr('id');
            var model=$(this).closest('tr').find('.model').attr('id');
            var tmp_mon_no=$(this).closest('tr').find('.tmp_mon_no').text();
            var status=$(this).closest('tr').find('.fr_status').text();
            var year=$(this).closest('tr').find('.install_year').text();
            var powersrc=$(this).closest('tr').find('.powersrc').text();
            $("#edit_installation_year").val(year);
            $("#edit_status").val(status);
            $("#edit_temp_monitor_no").val(tmp_mon_no);
            $("#edit_model").val(model);
            $("#edit_fridge_id").val(fr_id);


            var multi = powersrc.split(',');

            $("#example3").multiselect('refresh');

            for (var i = 0; i < multi.length; i++) {

                $("#example3").multiselect('select', multi[i]);

            }


            $('#modal-editfridge').modal('show');

        });

        $(document).on("click", ".editstore", function(event){

            var store_id=$(this).closest('tr').find('.store').attr('id');
            var catchment_pop=$(this).closest('tr').find('.catchment_pop').text();
            var live_birth_pop=$(this).closest('tr').find('.live_birth_pop').text();
            var pop_pregnant_women=$(this).closest('tr').find('.pop_pregnant_women').text();
            var pop_surviving_infants=$(this).closest('tr').find('.pop_surviving_infants').text();
            var pop_adolescent_girls=$(this).closest('tr').find('.pop_adolescent_girls').text();
            var cold_boxes=$(this).closest('tr').find('.cold_boxes').text();
            var vaccine_carriers=$(this).closest('tr').find('.vaccine_carriers').text();
            var ice_packs=$(this).closest('tr').find('.ice_packs').text();
            var time=$(this).closest('tr').find('.store').attr('data-time');
            var distance=$(this).closest('tr').find('.store').attr('data-distance');
            var electricity=$(this).closest('tr').find('.store').attr('data-electricity');
            var roof=$(this).closest('tr').find('.store').attr('data-roof');
            var mgname=$(this).closest('tr').find('.store').attr('data-mgname');
            var mgphone=$(this).closest('tr').find('.store').attr('data-mgphone');
            var town=$(this).closest('tr').find('.store').attr('data-nearest-town');

            var url="<?php echo base_url(); ?>store/getWastageStore/"+store_id;
            var url2="<?php echo base_url(); ?>store/getVaccinesJson/";

          $.get(url, function (data) {


                    $('.display_edit').empty();


                    if(data.length == 0){

                      $.get(url2, function (r) {
                          // use response
                          for (var i = 0; i < r.length; i++) {

                            div = $('<div class="col-xs-4">');
                            div.append("<label for='' class='control-label'> " + r[i].vaccine_name + "</label>");
                            div.append("<input type='number' name='edit_wastage[]' id='edit_wastage[]' value='0' class='form-control' placeholder='Wastage Factor'>");
                            div.append("<input type='hidden' name='edit_vaccine_id[]' id='edit_vaccine_id[]' value='" + r[i].id + "' class='form-control' >");
                            div.append("<input type='hidden' name='tr_id[]' id='tr_id[]' value='NULL' class='form-control' >");


                            $('.display_edit').append(div);

                          }
                      });

                    }

                    for (var i = 0; i < data.length; i++) {

                        div = $('<div class="col-xs-4">');
                        div.append("<label for='' class='control-label'> " + data[i].vaccine_name + "</label>");
                        div.append("<input type='number' name='edit_wastage[]' id='edit_wastage[]' value='" + data[i].wastage + "' class='form-control' placeholder='Wastage Factor'>");
                        div.append("<input type='hidden' name='edit_vaccine_id[]' id='edit_vaccine_id[]' value='" + data[i].vaccine_id + "' class='form-control' >");
                        div.append("<input type='hidden' name='tr_id[]' id='tr_id[]' value='" + data[i].wf_id + "' class='form-control' >");


                        $('.display_edit').append(div);

                      }

            });


            $("#edit_stores").val(store_id);
            $("#store_").val(store_id);

            $("#edit_catchment_pop").val(parseInt(catchment_pop));
            $("#edit_live_birth_pop").val(parseInt(live_birth_pop));
            $("#edit_cold_boxes").val(parseInt(cold_boxes));
            $("#edit_vaccine_carriers").val(parseInt(vaccine_carriers));
            $("#edit_ice_packs").val(parseInt(ice_packs));
            $("#edit_manager_name").val(mgname);
            $("#edit_manager_phone").val(mgphone);


            $("#edit_electricity").val(electricity);
            $("#edit_roof").val(roof);
            $("#edit_time").val(time);
            $("#edit_distance").val(distance);
            $("#edit_nearesttown").val(town);

            $("#edit_pop_pregnant_women").val(pop_pregnant_women);
            $("#edit_pop_adolescent_girls").val(pop_surviving_infants);
            $("#edit_pop_surviving_infants").val(pop_adolescent_girls);
            // $("#edit_ipv").val(ipv);
            // $("#edit_measles").val(measles);
            // $("#edit_measles_diluent").val(measlesdiluent);
            // $("#edit_yf").val(yf);
            // $("#edit_pcv").val(pcv);
            // $("#edit_dpt").val(dpt);




            $('#modal-editstore').modal('show');

        });

        $(document).on("click", ".editmystore", function(event){

            var store_id=$('.this-store').attr('id');
            var catchment_pop=$('.this-store').attr('data-catchment-pop');
            var live_birth_pop=$('.this-store').attr('data-live-births');
            var cold_boxes=$('.this-store').attr('data-cold-boxes');
            var vaccine_carriers=$('.this-store').attr('data-vaccine-carriers');
            var ice_packs=$('.this-store').attr('data-ice-packs');
            var time=$('.this-store').attr('data-time');
            var distance=$('.this-store').attr('data-distance');
            var electricity=$('.this-store').attr('data-electricity');
            var roof=$('.this-store').attr('data-roof');
            var mgname=$('.this-store').attr('data-mgname');
            var mgphone=$('.this-store').attr('data-mgphone');
            var rota=$('.this-store').attr('data-rota');
            var bcg=$('.this-store').attr('data-bcg');
            var bcgdiluent=$('.this-store').attr('data-bcgdiluent');
            var opv=$('.this-store').attr('data-opv');
            var tt=$('.this-store').attr('data-tt');
            var ipv=$('.this-store').attr('data-ipv');
            var measles=$('.this-store').attr('data-measles');
            var measlesdiluent=$('.this-store').attr('data-measlesdiluent');
            var yf=$('.this-store').attr('data-yf');
            var pcv=$('.this-store').attr('data-pcv');
            var dpt=$('.this-store').attr('data-dpt');

            $("#edit_mystore").val('NVIP');
            $("#mystore_").val(store_id);

            $("#edit_mycatchment_pop").val(parseInt(catchment_pop));
            $("#edit_mylive_birth_pop").val(parseInt(live_birth_pop));
            $("#edit_mycold_boxes").val(parseInt(cold_boxes));
            $("#edit_myvaccine_carriers").val(parseInt(vaccine_carriers));
            $("#edit_myice_packs").val(parseInt(ice_packs));
            $("#edit_mymg_name").val(parseInt(mgname));
            $("#edit_mymg_phone").val(parseInt(mgphone));


            $("#edit_myelectricity").val(electricity);
            $("#edit_myroof").val(roof);
            $("#edit_mytime").val(time);
            $("#edit_mydistance").val(distance);
            $("#edit_myrota").val(rota);

            $("#edit_mybcg").val(bcg);
            $("#edit_mybcg_diluent").val(bcgdiluent);
            $("#edit_myopv").val(opv);
            $("#edit_mytt").val(tt);
            $("#edit_myipv").val(ipv);
            $("#edit_mymeasles").val(measles);
            $("#edit_mymeasles_diluent").val(measlesdiluent);
            $("#edit_myyf").val(yf);
            $("#edit_mypcv").val(pcv);
            $("#edit_mydpt").val(dpt);

            $('#modal-editmystore').modal('show');

        });

        $('.stores').click(function(){

            var store_id=$(this).closest('td').attr('id');
            var storename=$(this).closest('td').attr('data-store');

            $("#location-text").html();
            $("#location-text").html(storename);

            var siteurl="<?php echo base_url(); ?>";
            var url="<?php echo base_url(); ?>fridges/getById/"+store_id;

            var loading_icon=siteurl+"assets/images/ring.gif";
            $.ajax({
                type: "POST",
                url: url,
                beforeSend: function() {
                },
                success: function(data)
                {
                    $('#display').empty();
                    $('#modal-showstorefridges').modal('show');


                    for (var i = 0; i < data.length; i++) {

                        tr = $('<tr/>');
                        tr.append("<td class='alias'>" + data[i].fridge_alias + "</td>");
                        tr.append("<td class=''>" + data[i].equipment_id + "</td>");
                        tr.append("<td class='model' id='" + data[i].fridge_id + "'>" + data[i].model + "</td>");

                        tr.append("<td class='powersrc'>" + data[i].power_source + "</td>");


                        tr.append("<td class='tmp_mon_no'>" + data[i].temperature_monitor_no + "</td>");
                        tr.append("<td class='install_year'>" + data[i].installation_year + "</td>");
                        tr.append("<td class='fr_status'>" + data[i].fridge_status + "</td>");
                        tr.append("<td><button type='button' id='" + data[i].equipment_id + "' class='btn btn-xs btn-flat btn-success facilitybarcode'><i class='fa fa-print'></i> Print Label</button> " +
                            "<a href='"+ siteurl+"jobcard/generate/"+ data[i].id+"' id='" + data[i].id + "' class='btn btn-xs btn-flat bg-purple jobcard '><i class='fa fa-edit'></i> Generate JobCard</a> "+
                            "<button type='button' id='" + data[i].id + "' class='btn btn-xs btn-flat bg-maroon editstorefridge '><i class='fa fa-edit'></i> Edit</button></td>");
                        $('#display').append(tr);
                    }

                },error: function (jqXHR) {
                    toastr.error(jqXHR.responseText);
                }
            });
        }) ;


        var max = new Date().getFullYear();
        var    min = max - 10;
        var    select = document.getElementById('installation_year');
        var    select2 = document.getElementById('installation_year2');
        var    select3 = document.getElementById('edit_installation_year');

        for (var i = min; i<=max; i++){
            var opt = document.createElement('option');
            opt.value = i;
            opt.innerHTML = i;
            select.appendChild(opt);
        }

        for (var i = min; i<=max; i++){
            var opt = document.createElement('option');
            opt.value = i;
            opt.innerHTML = i;
            select2.appendChild(opt);
        }

        for (var i = min; i<=max; i++){
            var opt = document.createElement('option');
            opt.value = i;
            opt.innerHTML = i;
            select3.appendChild(opt);
        }

        $("#addStoreFridge").click(function(e) {

            var url="<?php echo base_url(); ?>fridges/addStoreFridge"; // the script where you handle the form input.
            var formData ='#add-Store-Fridge';
            var msg ='Successfully added a Fridge';

            processData(formData,url,msg);

            e.preventDefault(); // avoid to execute the actual submit of the form.
        });



    $("#storesubmit").click(function(e) {

        var url="<?php echo base_url(); ?>store/add"; // the script where you handle the form input.
        var formData ='#store';

        processData(formData,url);
        //console.log($(formData).serialize())

        e.preventDefault(); // avoid to execute the actual submit of the form.
    });

        $("#storefridgesubmit").click(function(e) {

            var url="<?php echo base_url(); ?>fridges/add"; // the script where you handle the form input.
            var formData ='#myfridge';
            var msg ='Successfully added a Fridge';

//        console.log( $( '#myfridge' ).serialize() );
//        return;

            processData(formData,url,msg);

            e.preventDefault(); // avoid to execute the actual submit of the form.

        });


        function processData(formData,url,msg){
            $.ajax({
                type: "POST",
                url: url,
                data: $(formData).serialize(), // serializes the form's elements.
                success: function(data,status, jqXHR)
                {
                    swal("Good job!", msg, "success");//show response from the php script.
//
                    setTimeout(location.reload.bind(location), 1000);
                    console.log(jqXHR);


                },error: function (jqXHR, status, err,data) {
//                    console.log(jqXHR);
                    toastr.error(jqXHR.responseText)
                }
            });

        }



    });
</script>
