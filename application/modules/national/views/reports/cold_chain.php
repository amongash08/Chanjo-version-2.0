<div class="row">

    <section class="col-lg-12 connectedSortable ui-sortable">

        <!-- stock levels box -->
        <div class="box box-solid ">
            <div class="box-header with-border">
                <i class="fa fa-calendar"></i>

                <h3 class="box-title">
                    Positive Cold Chain Utilization
                </h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
            </div>
            <div class="box-body">

                <div class="row">

                    <div class="form-inline row" style="margin-left:2%;">
                        <select id="levels" class=" form-control custom-select" >
                            <option value="NULL">- Select Level -</option>
                            <?php
                            foreach ($user_levels as $key => $value) {
                                $name = $value['name'];
                                $id = $value['id'];
                                echo "<option value='$name' data-id='$id'>$name</option>";
                            }

                            ?>
                        </select>


                        <select id="regions" class=" form-control custom-select">
                            <option value="NULL">- Select Region -</option>
                            <?php
                            foreach ($regions as $key => $value) {
                                $name = $value['region_name'];
                                $id = $value['id'];
                                echo "<option value='$id'>$name</option>";
                            }

                            ?>
                        </select>

                        <select id="counties" class=" form-control custom-select">
                            <option value="NULL">- Select County -</option>

                        </select>

                        <select id="subcounties" class=" form-control custom-select">
                            <option value="NULL">- Select Sub-County -</option>

                        </select>

                        <button type="button" class="btn bg-purple btn-flat" id="filter_all" name="filter_all">Filter</button>

                    </div>
<!--                    <div class="form-group col-md-4">-->
<!--                        <select id="facilities" class=" form-control custom-select">-->
<!--                            <option value="NULL">- Select Facility -</option>-->
<!---->
<!--                        </select>-->
<!--                    </div>-->







                </div>




                <div class="chart">
                    <div class="col-md-6 card">
                        <h3 class="title">Positive Cold Chain Utilization</h3>
                        <div id="positive">

                        </div>
                    </div>

                    <div class="col-md-6 card">
                        <h3 class="title">Negative Cold Chain Utilization</h3>
                        <div id="negative">

                        </div>
                    </div>
                </div>


            </div>


        </div>
        <!-- /.box -->




        <!-- stock levels box -->
        <div class="box box-solid ">
            <div class="box-header with-border">
                <i class="fa fa-truck"></i>

                <h3 class="box-title">
                    Heat Excursions By County
                </h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
            </div>
            <div class="box-body">

<!--                <div class="row">-->
<!---->
<!--                    -->
<!---->
<!--                    -->
<!---->
<!--                </div>-->




                <div class="chart">
                    <div id="heat_excursions" ></div>
                </div>


            </div>


        </div>
        <!-- /.box -->



    </section>


</div>


<script type="text/javascript">

    var url="<?php echo base_url(); ?>";
    $('#regions,#counties,#subcounties,#facilities').hide();


    ajax_fill_data('reports/heat_excursions',"#heat_excursions");
    ajax_fill_data('dashboard/positiveColdchain/NULL/NULL',"#positive");
    ajax_fill_data('dashboard/negativeColdchain/NULL/NULL',"#negative");

    $('#levels').on('change', function(){
        $('#regions,#counties,#subcounties,#facilities').val('NULL');

        if ($(this).val()==='Region') {

            $('#regions').show();
            $('#counties,#subcounties,#facilities').hide();
        }else if ($(this).val()==='County') {

            $('#counties').show();
            $('#regions,#subcounties,#facilities').hide();
            var drop_down='';
            var county_select = "<?php echo base_url(); ?>reports/getallCountiesjson/";
            $.getJSON( county_select ,function( json ) {
                $("#counties").html('<option value="NULL" selected="selected">Select Counties</option>');
                $.each(json, function( key, val ) {
                    drop_down +="<option value='"+json[key]["id"]+"'>"+json[key]["county_name"]+"</option>";
                });
                $("#counties").append(drop_down);
            });


        }else if ($(this).val()==='Sub County') {

            $('#subcounties').show();
            $('#regions,#counties,#facilities').hide();

            var drop_down='';
            var subcounty_select = "<?php echo base_url(); ?>reports/getallSubcountiesjson/";
            $.getJSON( subcounty_select ,function( json ) {
                $("#subcounties").html('<option value="NULL" selected="selected">Select Sub-Counties</option>');
                $.each(json, function( key, val ) {
                    drop_down +="<option value='"+json[key]["id"]+"'>"+json[key]["subcounty_name"]+"</option>";
                });
                $("#subcounties").append(drop_down);
            });

        }else if ($(this).val()==='Facility') {

            $('#facilities').show();
            $('#regions,#counties,#subcounties').hide();

            var drop_down='';
            var facility_select = "<?php echo base_url(); ?>reports/getallFacilitiesjson/";
            $.getJSON( facility_select ,function( json ) {
                $("#facilities").html('<option value="NULL" selected="selected">Select Facility</option>');
                $.each(json, function( key, val ) {
                    drop_down +="<option value='"+json[key]["id"]+"'>"+json[key]["facility_name"]+"</option>";
                });
                $("#facilities").append(drop_down);
            });

        }
        else if ($(this).val()==='National') {

            $('#regions,#counties,#subcounties,#facilities').hide();

        }

    });


    $( "#filter_all" ).click(function() {

        var vaccine_antigens = [];
        $(':checkbox:checked').each(function(i){
            vaccine_antigens[i] = $(this).val();
        });
        console.log(vaccine_antigens);

        var level=$('option:selected', '#levels').attr('data-id');
        var region_name=$('option:selected', '#regions').text();
        var region_id=$('option:selected', '#regions').val();
        var county=$('#counties').val();
        var subcounty=$('#subcounties').val();
        var facility=$('#facilities').val();
        if ($('option:selected', '#levels').val()==='NULL'||$('option:selected', '#levels').val()==='National') {
            var station='NVIP';
            var station_name='NVIP';
        }
        if ($('option:selected', '#regions').val()!='NULL') {
            var station=$('option:selected', '#regions').val();
            var station_name=$('option:selected', '#regions').text();
        }
        if ($('option:selected', '#counties').val()!='NULL') {
            var station=$('option:selected', '#counties').val();
            var station_name=$('option:selected', '#counties').text();
        }
        if ($('option:selected', '#subcounties').val()!='NULL') {
            var station=$('option:selected', '#subcounties').val();
            var station_name=$('option:selected', '#subcounties').text();
        }
        if ($('option:selected', '#facilities').val()!='NULL') {
            var station=$('option:selected', '#facilities').val();
            var station_name=$('option:selected', '#facilities').text();
        }
        console.log(station);
        console.log(station_name);

        ajax_fill_data('dashboard/negativeColdchain/'+level+'/'+station_name,"#negative");
        ajax_fill_data('dashboard/positiveColdchain/'+level+'/'+station_name,"#positive");


    });



    function ajax_fill_data(function_url,div){
        var function_url =url+function_url;
        var loading_icon=url+"assets/images/ring.gif";
        $.ajax({
            type: "POST",
            url: function_url,
            beforeSend: function() {
                $(div).html("<img style='margin:10% 50% 10% ;' src="+loading_icon+">");
            },
            success: function(msg) {
                $(div).html(msg);
            }
        });
    }
</script>
