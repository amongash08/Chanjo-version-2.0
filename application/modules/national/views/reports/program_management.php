<div class="row">

    <section class="col-lg-12 connectedSortable ui-sortable">

        <!-- stock levels box -->
        <div class="box box-solid ">
            <div class="box-header with-border">
                <i class="fa fa-calendar"></i>

                <h3 class="box-title">
                    Immunization Performance
                </h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
            </div>
            <div class="box-body">

                <div class="chart">
                    <div id="immunization_performance" ></div>
                </div>


            </div>


        </div>
        <!-- /.box -->




        <!-- stock levels box -->
        <div class="box box-solid ">
            <div class="box-header with-border">
                <i class="fa fa-truck"></i>

                <h3 class="box-title">
                    Default Tracing
                </h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
            </div>
            <div class="box-body">


                <div class="chart">
                    <div id="default_tracing" ></div>
                </div>


            </div>


        </div>
        <!-- /.box -->



    </section>


</div>


<script type="text/javascript">

    var url="<?php echo base_url(); ?>";


    //ajax_fill_data('reports/immunization_performance',"#immunization_performance");
    ajax_fill_data('reports/default_tracing',"#default_tracing");
    ajax_fill_data('dashboard/coverage/NULL/NULL',"#immunization_performance");


    function ajax_fill_data(function_url,div){
        var function_url =url+function_url;
        var loading_icon=url+"assets/images/ring.gif";
        $.ajax({
            type: "POST",
            url: function_url,
            beforeSend: function() {
                $(div).html("<img style='margin:10% 50% 10% ;' src="+loading_icon+">");
            },
            success: function(msg) {
                $(div).html(msg);
            }
        });
    }
</script>
<?php
/**
 * Created by PhpStorm.
 * User: mureithi
 * Date: 7/18/17
 * Time: 10:30 AM
 */