<div class="row">

    <section class="col-lg-12 connectedSortable ui-sortable">

        <!-- stock levels box -->
        <div class="box box-solid ">
            <div class="box-header with-border">
                <i class="fa fa-calendar"></i>

                <h3 class="box-title">
                    Stock Levels
                </h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
                    </button>
                </div>
            </div>
            <div class="box-body">


              <div class="" id="template" >

                  <table class="table no-margin">
                      <thead>
                      <tr>
                          <th>Region Name</th>
                          <th>Rota</th>
                          <th>BCG</th>
                          <th>BCG Diluent</th>
                          <th>TT</th>
                          <th>OPV</th>
                          <th>IPV</th>
                          <th>YF</th>
                          <th>YF Diluent</th>
                          <th>PCV</th>
                          <th>DPT</th>
                          <th>MR</th>
                          <th>MR Diluent</th>

                      </tr>
                      </thead>
                      <tbody id="display">

                      </tbody>
                  </table>

              </div>


            </div>


        </div>
        <!-- /.box -->

    </section>


</div>



<script type="text/javascript">

    $(document).ready(function () {

        var url = '<?php echo  base_url(); ?>';
        // ajax_fill_data('stock/stock_last_updated/NULL', "#stock_updated");
        ajax_fill_data('stock/get_stock_region', "#stock_levels");

        // $('#stock_level').click(function () {
        //     var level = $('option:selected', '#level').val();
        //     data = {'level': level};
        //     ajax_fill_data('reports/table_stock_level', "#stock_levels", data);
        // });
        //
        // $('#stock_last_updated').click(function () {
        //     var interval = $('option:selected', '#interval').attr('data-value');
        //     data = {'location': location};
        //     ajax_fill_data('reports/stock_last_updated', "#stock_updated", data);
        // });


        function ajax_fill_data(function_url, div) {
            var function_url = url + function_url;
            var loading_icon = url + "assets/images/ring.gif";
            $.ajax({
                type: "POST",
                url: function_url,
                beforeSend: function () {
                    $(div).html("<img style='margin:10% 50% 10% ;' src=" + loading_icon + ">");
                },
                success: function (data) {
                  console.log(data);

                  $('#display').empty();
                  // $('#modal-showstorefridges').modal('show');
                  console.log(data);


                  for (var i = 0; i < data.length; i++) {

                      tr = $('<tr/>');
                      tr.append("<td class=''>" + data[i].region_name + "</td>");
                      tr.append("<td class=''>" + data[i].ROTA + "</td>");
                      tr.append("<td class=''>" + data[i].BCG + "</td>");

                      tr.append("<td class=''>" + data[i].bcg_dil + "</td>");


                      tr.append("<td class=''>" + data[i].TT + "</td>");
                      tr.append("<td class=''>" + data[i].OPV + "</td>");
                      tr.append("<td class=''>" + data[i].IPV + "</td>");
                      tr.append("<td class=''>" + data[i].YF + "</td>");
                      tr.append("<td class=''>" + data[i].yf_dil + "</td>");
                      tr.append("<td class=''>" + data[i].PCV + "</td>");

                      tr.append("<td class=''>" + data[i].DPT + "</td>");


                      tr.append("<td class=''>" + data[i].MR + "</td>");
                      tr.append("<td class=''>" + data[i].mr_dil + "</td>");
                      $('#display').append(tr);
                }

          },error: function (jqXHR) {
              toastr.error(jqXHR.responseText);
          }
            });
        }
    });

</script>
