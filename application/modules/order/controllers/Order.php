<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Order extends Auth_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->module('users');
        $this->load->module('vaccine');
        $this->load->module('stock');
        $this->load->model('stock/mdl_stock_ext');
        $this->load->model('order/mdl_order');
        $this->load->model('order/mdl_order_ext');
        $this->load->model('users/mdl_user_ext');
        $this->load->model('vaccine/mdl_vaccine');
    }

    public function index()
    {
        $data['title'] = 'Order';
        $data['page_title'] = 'Order';
        $data['subtitle'] = 'Order';
        $data['module'] = 'order';
        $user_info = $this->user_info();
        if ($user_info->level_id == 1) {
            $data['sidebar'] = 'nation_sidebar';
            $data['view_file'] = 'nation_index';
            $data['other_orders'] = $this->get_orders()['other_orders'];
            if ($user_info->level_id == 1) {
                $data['location'] = $this->region->region_dropdown();
            }

        } else if ($user_info->level_id == 4) {
            $data['view_file'] = 'subcounty_index';
            $data['my_orders'] = $this->get_orders()['my_orders'];
        } else {
            $data['view_file'] = 'index';
            $data['my_orders'] = $this->get_orders()['my_orders'];
            $data['other_orders'] = $this->get_orders()['other_orders'];
        }


        echo Modules::run('templates', $data);

    }

    public function create($id = false)
    {
        $user_info = $this->user_info();
        $data['title'] = 'Create Order';
        $data['page_title'] = 'Order';
        $data['subtitle'] = 'Order';
        $data['module'] = 'order';
        $data['view_file'] = 'create_order';
        $data['id'] = $id;

        if ($this->input->post()) {
            if ($this->input->post('id') && !empty($this->input->post('id'))) {
                $to_from = $user_info->location_nation_id;
                $location = $this->input->post('id');
                $level = 2;
            } elseif ($this->input->post('destination') && !empty($this->input->post('destination'))) {
                $to_from = $user_info->location_nation_id;
                $location = $this->input->post('destination');
                $level = $this->input->post('level');
            } else {
                $to_from = ($user_info->level_id !== '2') ? $user_info->location_region_id : $user_info->location_nation_id;
                $location = $user_info->location_id;
                $level = $user_info->level_id;
            }
            $order = [
                'pickup_date' => $this->input->post('pickup_date'),
                'location_id' => $location,
                'level_id' => $level,
                'user_id' => $user_info->id,
                'to_from' => $to_from
            ];

            $batch = json_decode(stripcslashes($this->input->post('batch')), true);

            $order_items = array();
            $counter = 0;

            foreach ($batch as $item) {
                $order_items[$counter]['vaccine_id'] = $item['vaccine_id'];
                $order_items[$counter]['calculated_quantity'] = $item['quantity'];
                $order_items[$counter]['transaction_quantity'] = $item['order'];
                $order_items[$counter]['current_quantity'] = $item['current'];
                $order_items[$counter]['max_quantity'] = $item['max_stock'];
                $order_items[$counter]['min_quantity'] = $item['min_stock'];
                $order_items[$counter]['request_id'] = '';
                ++$counter;
            }
            $query = $this->mdl_order_ext->_create_order($order, $order_items);
            if ($query) {
                $this->session->set_flashdata('success_message', 'Order created successfully');
                redirect('order', 'refresh');
            }

        } else {
            if ($id) {
                $data['population'] = $this->mdl_stock_ext->get_population($id, 2);
                $data['title'] = 'Vaccine Requests';
                $data['subtitle'] = 'Push stocks to ' . $this->location_name($id);
                $quantities = array();
                $vaccines = $this->vaccine->vaccine_dropdown();
                foreach ($vaccines as $key => $value) {
                    $quantities[$value] = $this->calculate_order($key, $id, 2);
                }
                $data['vaccines'] = $quantities;

            } else {
                if ($user_info->level_id == 1) {

                    $data['vaccines'] = $this->vaccine_dropdown();
                    $data['view_file'] = 'create_order_other';
                } else {
                    $data['population'] = $this->mdl_stock_ext->get_population($user_info->location_id, $user_info->level_id);
                    $quantities = array();
                    $vaccines = $this->vaccine->vaccine_dropdown();
                    foreach ($vaccines as $key => $value) {
                        $quantities[$value] = $this->calculate_order($key, $user_info->location_id, $user_info->level_id);
                    }
                    $data['vaccines'] = $quantities;
                }

            }
            $data['user_info'] = $user_info;


        }
        echo Modules::run('templates', $data);


    }

    public function pack($id = false)
    {
        $data['title'] = 'Vaccine Request';
        $data['subtitle'] = 'Packing List';
        $data['module'] = 'order';
        $data['view_file'] = 'pack';
        $user_info = $this->user_info();
        $data['user_info'] = $user_info;
        if (is_numeric($id)) {
            $data['user_details'] = $this->get_order($id)['user_details'];
            $data['order'] = $this->get_order($id)['order'];
            $data['items'] = $this->get_order($id)['order_items'];
            $data['item_count'] = $this->get_order($id)['item_count'];
            $data['new_item_count'] = $this->get_order($id)['new_item_count'];
            $data['id'] = $id;
        }
        if ($this->input->post()) {
            $id = (int)$this->input->post('id');
            $order = [
                'packing_date' => $this->input->post('packing_date'),
                'location_id' => $user_info->location_id,
                'level_id' => $user_info->level_id,
                'user_id' => $user_info->id,
                'request_id' => $id,
                'from_id' => $this->get_order($id)['user_details']['from_id'],
            ];

            $batch = json_decode(stripcslashes($this->input->post('batch')), true);

            $order_items = array();
            $counter = 0;

            foreach ($batch as $item) {
                $order_items[$counter]['vaccine_id'] = $item['vaccine_id'];
                $order_items[$counter]['batch'] = $item['batch_no'];
                $order_items[$counter]['expiry_date'] = $item['expiry_date'];
                $order_items[$counter]['order_quantity'] = $item['order_quantity'];
                $order_items[$counter]['packing_quantity'] = $item['packing_quantity'];
                $order_items[$counter]['packing_id'] = '';
                ++$counter;
            }
            $query = $this->mdl_order_ext->_pack($order, $order_items);
            if ($query) {
                $this->mdl_order->update($id, ['status' => 'packing']);
                $this->session->set_flashdata('success_message', 'Packing list created successfully');
                redirect('order', 'refresh');
            }
        } else {
            $data['vaccine'] = array(
                'name' => 'vaccine',
                'id' => 'vaccine',
                'class' => 'form-control vaccine',
                'options' => '',
                'value' => $this->form_validation->set_value('vaccine'),
            );
        }

        echo Modules::run('templates', $data);
    }

    public function update_pack($id = false)
    {
        $data['title'] = 'Packing List';
        $data['page_title'] = 'Order';
        $data['subtitle'] = 'Packing List';
        $data['module'] = 'order';
        $data['view_file'] = 'update_pack';
        $user_info = $this->user_info();
        $data['user_info'] = $user_info;
        if (is_numeric($id)) {
            $data['user_details'] = $this->get_packing_list($id)['user_details'];
            $data['order'] = $this->get_packing_list($id)['packing'];
            $data['items'] = $this->get_packing_list($id)['items'];
            $data['id'] = $id;
        }
        if ($this->input->post()) {
            $id = (int)$this->input->post('id');
            $order = [
                'packing_date' => $this->input->post('packing_date'),
            ];

            $query = $this->db->where('id', $id)->update('packing', $order);
            $batch = json_decode(stripcslashes($this->input->post('batch')), true);

            $order_item = array();
            foreach ($batch as $item) {
                if ($this->mdl_order_ext->get_packing_item($id, $item['row_id'])) {
                    $order_item['vaccine_id'] = $item['vaccine_id'];
                    $order_item['batch'] = $item['batch_no'];
                    $order_item['expiry_date'] = $item['expiry_date'];
                    $order_item['order_quantity'] = $item['order_quantity'];
                    $order_item['packing_quantity'] = $item['packing_quantity'];
                    $query = $this->db->where('id', $item['row_id'])->update('packing_items', $order_item);
                } else {
                    $order_item['vaccine_id'] = $item['vaccine_id'];
                    $order_item['batch'] = $item['batch_no'];
                    $order_item['expiry_date'] = $item['expiry_date'];
                    $order_item['order_quantity'] = $item['order_quantity'];
                    $order_item['packing_quantity'] = $item['packing_quantity'];
                    $order_item['packing_id'] = $id;
                    $query = $this->db->insert('packing_items', $order_item);
                }
            }

            if ($query) {
                $this->mdl_order->update($id, ['status' => 'packing']);
                $this->session->set_flashdata('success_message', 'Packing list updated successfully');
                redirect('order', 'refresh');
            }
        }

        echo Modules::run('templates', $data);
    }

    public function receive($id = false)
    {
        $data['title'] = 'Receive Order';
        $data['page_title'] = 'Order';
        $data['subtitle'] = 'Receive Order';
        $data['module'] = 'order';
        $data['view_file'] = 'receive_order';
        $user_info = $this->user_info();
        $data['user_info'] = $user_info;
        if (is_numeric($id)) {
            $data['user_details'] = $this->get_issued($id)['user_details'];
            $data['details'] = $this->get_issued($id)['details'];
            $data['items'] = $this->get_issued($id)['items'];
            $data['id'] = $id;
        }
        if ($this->input->post()) {
            $id = (int)$this->input->post('id');
            $transaction = null;
            $items = null;
            $transaction_details = null;
            $type = 1;
            $transaction = array(
                'transaction_voucher' => $this->input->post('voucher'),
                'transaction_date' => $this->input->post('receive_date'),
                'to_from' => $this->input->post('from'),
                'type' => $type,
                'status' => 'received',
                'request_id' => $id,
                'location_id' => $user_info->location_id,
                'user_id' => $user_info->id,
                'level' => $user_info->level_id,
            );
            $batch = json_decode(stripcslashes($this->input->post('batch')), true);
            foreach ($batch as $item) {
                $items = array(
                    'vaccine_id' => $item['vaccine_id'],
                    'batch' => strtoupper($item['batch_no']),
                    'expiry_date' => date('Y-m-28', strtotime($item['expiry_date'])),
                    'transaction_quantity' => $item['receive_quantity'],
                    'vvm' => $item['vvm'],
                    'comment' => '',
                    'transaction_id' => '',
                );
                $transaction_details = array(
                    'date' => $this->input->post('receive_date'),
                    'vaccine_id' => $item['vaccine_id'],
                    'batch' => strtoupper($item['batch_no']),
                    'expiry_date' => date('Y-m-28', strtotime($item['expiry_date'])),
                    'vvm' => $item['vvm'],
                    'balance' => $item['receive_quantity'],
                    'location_id' => $user_info->location_id,
                    'user_id' => $user_info->id,
                    'type' => $type,
                    'transaction_id' => '',
                );
                $balance = $this->stock->_check_transaction_balances($transaction_details);

                $balance_array[] = $balance;
                $items_array[] = $items;
            }
            $query = $this->mdl_stock_ext->_issue_items($transaction, $items_array, $balance_array);
            if ($query) {
                $this->mdl_order->update($id, ['status' => 'received']);
                $this->session->flashdata('success_message', 'Order has been received');
                redirect('order', 'refresh');
            }

        } else {
            $vvm_option = array('' => 'Select vvm');
            $data['vvm'] = array(
                'name' => 'vvm',
                'id' => 'vvm',
                'class' => 'form-control',
                'options' => $vvm_option + $this->vaccine->vvm_dropdown()
            );
        }

        echo Modules::run('templates', $data);
    }

    public function issue($id = false)
    {
        $data['title'] = 'Issue Order';
        $data['page_title'] = 'Order';
        $data['subtitle'] = 'Order';
        $data['module'] = 'order';
        $data['view_file'] = 'issue_order';
        $user_info = $this->user_info();
        $data['user_info'] = $user_info;
        if (is_numeric($id)) {
            $data['user_details'] = $this->get_packing_list($id)['user_details'];
            $data['packing'] = $this->get_packing_list($id)['packing'];
            $data['items'] = $this->get_packing_list($id)['items'];
            $data['id'] = $id;
        }
        if ($this->input->post()) {
            $id = (int)$this->input->post('id');
            $to_from = (int)$this->input->post('from');
            $transaction = null;
            $items = null;
            $transaction_details = null;
            $type = 2;
            $transaction = array(
                'transaction_voucher' => '',
                'transaction_date' => $this->input->post('issue_date'),
                'to_from' => ($id !== false) ? $to_from : $id,
                'type' => $type,
                'status' => 'issued',
                'request_id' => $id,
                'location_id' => $user_info->location_id,
                'user_id' => $user_info->id,
                'level' => $user_info->level_id
            );
            $batch = json_decode(stripcslashes($this->input->post('batch')), true);
            foreach ($batch as $item) {
                $items = array(
                    'vaccine_id' => $item['vaccine_id'],
                    'batch' => strtoupper($item['batch_no']),
                    'expiry_date' => date('Y-m-28', strtotime($item['expiry_date'])),
                    'transaction_quantity' => $item['issue_quantity'],
                    'current_quantity' => '',
                    'vvm' => $item['vvm'],
                    'comment' => '',
                    'transaction_id' => '',
                );
                $transaction_details = array(
                    'date' => $this->input->post('issue_date'),
                    'vaccine_id' => $item['vaccine_id'],
                    'batch' => strtoupper($item['batch_no']),
                    'expiry_date' => date('Y-m-28', strtotime($item['expiry_date'])),
                    'vvm' => $item['vvm'],
                    'balance' => $item['issue_quantity'],
                    'location_id' => $user_info->location_id,
                    'user_id' => $user_info->id,
                    'type' => $type,
                    'transaction_id' => '',
                );
                $balance = $this->stock->_check_transaction_balances($transaction_details);
                $query = $this->stock->_insert_transactions($transaction, $items, $balance);
            }
            if ($query) {
                $this->mdl_order->update($id, ['status' => 'issued']);
                $this->session->flashdata('success_message', 'Order has been issued');
                redirect('order', 'refresh');
            }
        } else {
            $vvm_option = array('' => 'Select vvm');
            $data['vvm'] = array(
                'name' => 'vvm',
                'id' => 'vvm',
                'class' => 'form-control',
                'options' => $vvm_option + $this->vaccine->vvm_dropdown()
            );
        }

        echo Modules::run('templates', $data);
    }

    public function order_sheet($id = false)
    {
        $data['title'] = 'Vaccine Requests';
        $data['subtitle'] = 'View Order';
        $data['module'] = 'order';
        $data['view_file'] = 'order_sheet';
        $user_info = $this->user_info();
        $data['user_info'] = $user_info;
        if (is_numeric($id)) {
            $data['id'] = $id;
            $data['user_details'] = $this->get_order($id)['user_details'];
            $data['order'] = $this->get_order($id)['order'];
            $data['order_items'] = $this->get_order($id)['order_items'];
        }

        echo Modules::run('templates', $data);
    }

    public function issued($id = false)
    {
        $data['title'] = 'View Order';
        $data['page_title'] = 'Order';
        $data['subtitle'] = 'Issued';
        $data['module'] = 'order';
        $data['view_file'] = 'issued_order';
        $user_info = $this->user_info();
        $data['user_info'] = $user_info;
        if (is_numeric($id)) {
            $data['id'] = $id;
            $data['user_details'] = $this->get_issued($id)['user_details'];
            $data['details'] = $this->get_issued($id)['details'];
            $data['items'] = $this->get_issued($id)['items'];
        }

        echo Modules::run('templates', $data);
    }

    public function print_order_sheet($id = false)
    {
        $data['title'] = 'Order Sheet';
        $user_info = $this->user_info();
        if ($user_info->level_id == '1') {
            $location = $user_info->nation;
        } elseif ($user_info->level_id == '2') {
            $location = $user_info->region;
        } elseif ($user_info->level_id == '3') {
            $location = $user_info->region;
        } elseif ($user_info->level_id == '4') {
            $location = $user_info->subcounty;
        }

        if (is_numeric($id)) {
            $data['id'] = $id;
            $data['user_details'] = $this->get_order($id)['user_details'];
            $data['order'] = $this->get_order($id)['order'];
            $data['order_items'] = $this->get_order($id)['order_items'];
        }
        $this->load->view('order/print_order_sheet', $data);
    }

    public function print_packing_list($id = false)
    {
        $data['title'] = 'Packing List';
        $user_info = $this->user_info();
        $region = json_decode($user_info->json_location)->region;
        $county = json_decode($user_info->json_location)->county;
        $subcounty = json_decode($user_info->json_location)->subcounty;
        $facility = json_decode($user_info->json_location)->facility;

        $loc = $region . $county . $subcounty . $facility;
        $data['user_info'] = $user_info;
        $data['location'] = $loc;
        if (is_numeric($id)) {
            $data['id'] = $id;
            $data['user_details'] = $this->get_packing_list($id)['user_details'];
            $data['packing'] = $this->get_packing_list($id)['packing'];
            $data['items'] = $this->get_packing_list($id)['items'];
        }
        $this->load->view('order/print_packing_list', $data);
    }

    public function print_issue_sheet($id = false)
    {
        $data['title'] = 'Vaccine Issue Form';
        $user_info = $this->user_info();
        $region = json_decode($user_info->json_location)->region;
        $county = json_decode($user_info->json_location)->county;
        $subcounty = json_decode($user_info->json_location)->subcounty;
        $facility = json_decode($user_info->json_location)->facility;

        $loc = $region . $county . $subcounty . $facility;
        $data['location'] = $loc;
        $data['user_info'] = $user_info;
        if (is_numeric($id)) {
            $data['id'] = $id;
            $data['user_details'] = $this->get_issued($id)['user_details'];
            $data['details'] = $this->get_issued($id)['details'];
            $data['items'] = $this->get_issued($id)['items'];
        }
        $this->load->view('order/print_issue_sheet', $data);
    }

    public function packing_list($id = false)
    {
        $data['title'] = 'Vaccine Requests';
        $data['subtitle'] = 'View Packing List';
        $data['module'] = 'order';
        $data['view_file'] = 'packing_list';
        $user_info = $this->user_info();
        $data['user_info'] = $user_info;
        if (is_numeric($id)) {
            $data['id'] = $id;
            $data['user_details'] = $this->get_packing_list($id)['user_details'];
            $data['packing'] = $this->get_packing_list($id)['packing'];
            $data['items'] = $this->get_packing_list($id)['items'];
        }

        echo Modules::run('templates', $data);
    }

    public function calculate_order($vaccine = false, $location = false, $level = false)
    {
        if ($this->input->post()) {
            $vaccines = $this->input->post('vaccines');
            $location = $this->input->post('destination');
            $level = $this->input->post('level');
            foreach ($vaccines as $key => $vaccine) {
                if ($vaccine) {
                    $period_stock = $this->mdl_stock_ext->calculate_period_stock($location, $level, $vaccine);
                    $vaccine_balance = $this->mdl_stock_ext->get_stock_balance($vaccine, $location);
                    $vaccine_presentation = $this->mdl_stock_ext->get_vaccine_presentation($vaccine);
                    $dose_volume = $this->mdl_stock_ext->get_vaccine_volume($vaccine);
                    $max_stock = (int)($period_stock * 1.25);
                    $min_stock = (int)($period_stock * 0.25);
                    $order_quantity = null;
                    $max_stock += ($vaccine_presentation - ($max_stock % $vaccine_presentation)) % $vaccine_presentation;
                    $min_stock += ($vaccine_presentation - ($min_stock % $vaccine_presentation)) % $vaccine_presentation;

                    if ((int)$vaccine_balance[0]['stock_balance'] == 0) {
                        $order_quantity = $max_stock;
                    } elseif ((int)$vaccine_balance[0]['stock_balance'] > 0) {
                        $order = $max_stock - (int)$vaccine_balance[0]['stock_balance'];
                        $order_quantity = ($order < 0) ? 0 : $order;
                    }


                    $data[$vaccine] = [
                        'vaccine_id' => $vaccine,
                        'current_stock' => $vaccine_balance[0]['stock_balance'],
                        'max_stock' => $max_stock,
                        'min_stock' => $min_stock,
                        'doses' => $vaccine_presentation,
                        'volume' => $dose_volume,
                        'order' => $order_quantity,
                    ];


                }
            }

            echo json_encode($data);
        } else {
            if (!$location && !$level) {
                $location = $this->user_info()->location_id;
                $level = $this->user_info()->level_id;
            }
            if ($vaccine) {
                $period_stock = $this->mdl_stock_ext->calculate_period_stock($location, $level, $vaccine);
                $vaccine_balance = $this->mdl_stock_ext->get_stock_balance($vaccine, $location);
                $vaccine_presentation = $this->mdl_stock_ext->get_vaccine_presentation($vaccine);
                $dose_volume = $this->mdl_stock_ext->get_vaccine_volume($vaccine);
                $max_stock = (int)($period_stock * 1.25);
                $min_stock = (int)($period_stock * 0.25);
                $order_quantity = null;
                $max_stock += ($vaccine_presentation - ($max_stock % $vaccine_presentation)) % $vaccine_presentation;
                $min_stock += ($vaccine_presentation - ($min_stock % $vaccine_presentation)) % $vaccine_presentation;

                if ((int)$vaccine_balance[0]['stock_balance'] == 0) {
                    $order_quantity = $max_stock;
                } elseif ((int)$vaccine_balance[0]['stock_balance'] > 0) {
                    $order = $max_stock - (int)$vaccine_balance[0]['stock_balance'];
                    $order_quantity = ($order < 0) ? 0 : $order;
                }


                $data = [
                    'vaccine_id' => $vaccine,
                    'current_stock' => $vaccine_balance[0]['stock_balance'],
                    'max_stock' => $max_stock,
                    'min_stock' => $min_stock,
                    'doses' => $vaccine_presentation,
                    'volume' => $dose_volume,
                    'order' => $order_quantity,
                ];

                return $data;
            }
        }
    }

    public function get_orders()
    {
        $data = null;
        $user_info = $this->user_info();
        $my_orders = $this->mdl_order->get_where(array('location_id' => $user_info->location_id));
        $other_orders = $this->mdl_order->get_where(array('to_from' => $user_info->location_id));
        $user_id = null;
        $format_other_orders = null;

        if ($other_orders) {
            foreach ($other_orders as $key => $value) {
                $user_id = $value->user_id;
            }
            $info = $this->mdl_user_ext->get_user_info($user_id);
            if ($info) {
                if ($info->level_id == '2') {
                    $from = $info->region;
                    $location = $value->location_id;
                } elseif ($info->level_id == '3') {
                    $from = $info->county;
                    $location = $value->location_id;
                } elseif ($info->level_id == '4') {
                    $from = $info->subcounty;
                    $location = $value->location_id;
                }
                foreach ($other_orders as $key => $value) {
                    if ($info->level_id == '1') {
                        $from = $this->users->location_name($value->location_id);
                        $location = $value->to_from;
                    }
                    $data = [
                        'id' => $value->id,
                        'location_id' => $location,
                        'user_id' => $value->user_id,
                        'pickup_date' => $value->pickup_date,
                        'status' => $value->status,
                        'timestamp' => $value->timestamp,
                        'from' => $from,
                    ];
                    $format_other_orders[] = (object)$data;
                }

            }
        }

        $data = [
            'my_orders' => $my_orders,
            'other_orders' => $format_other_orders
        ];

        return $data;

    }

    public function get_order($id)
    {
        $data = null;
        $user_info = $this->user_info();
        $order = $this->mdl_order_ext->get_order($id);
        $order_items = $this->mdl_order_ext->get_order_items($id);
        $data['item_count'] = count($order_items);
        $data['order_items'] = $order_items;
        foreach ($order_items as $key => $value) {
            $balance = $this->mdl_stock_ext->get_stock_balance($value->vaccine_id, $user_info->location_id);
            foreach ($balance as $k => $v) {
                if (is_null($v['stock_balance'])) {
                    unset($order_items[$key]);
                }
            }
        }
        $data['new_item_count'] = count($order_items);
        $user_id = null;

        if ($order) {
            $data['order'] = $order;
            $user_id = $order->user_id;
        }
        if ($order_items) {
            $data['items'] = $order_items;
        }

        $user = $this->mdl_user_ext->get_user_info($user_id);
        if ($user) {
            if ($user_info->level_id == '1') {
                $to = 'NVIP';
                $from = $this->users->location_name($order->location_id);
                $from_id = $order->location_id;
            } elseif ($user_info->level_id == '2') {
                $to = $user->region;
                $from = $user->subcounty;
                $from_id = $user->location_id;
            } elseif ($user_info->level_id == '3') {
                $to = $user->region;
                $from = $user->county;
                $from_id = $user->location_id;
            } elseif ($user_info->level_id == '4') {
                $to = $user->region;
                $from = $user->subcounty;
                $from_id = $user->location_id;
            }

            $data['user_details'] = [
                'name' => $user->first_name . ' ' . $user->last_name,
                'email' => $user->email,
                'phone' => $user->phone,
                'from' => $from,
                'from_id' => $from_id,
                'to' => $to
            ];
        }

        return $data;
    }

    public function get_packing_list($id)
    {
        $data = null;
        $packing = $this->mdl_order_ext->get_packing_list($id);
        $items = $this->mdl_order_ext->get_packing_items($id);
        $user_id = null;

        if ($packing) {
            $data['packing'] = $packing;
            $user_id = $packing->user;
        }
        if ($items) {
            $data['items'] = $items;
        }

        $user = $this->mdl_user_ext->get_user_info($user_id);
        if ($user) {

            if ($user->level_id == '1') {
                $to = $this->location_name($packing->location_id);
                $from = $this->location_name($packing->from_id);
                $from_id = $packing->from_id;
            } elseif ($user->level_id == '2') {
                $to = $user->nation;
                $from = $user->region;
                $from_id = $user->location_id;
            } elseif ($user->level_id == '3') {
                $to = $user->region;
                $from = $user->county;
                $from_id = $user->location_id;
            } elseif ($user->level_id == '4') {
                $to = $user->region;
                $from = $user->subcounty;
                $from_id = $user->location_id;
            } else {
                $to = $this->location_name($packing->location_id);
                $from = $this->location_name($packing->from_id);
                $from_id = $user->location_id;
            }


            $data['user_details'] = [
                'name' => $user->first_name . ' ' . $user->last_name,
                'email' => $user->email,
                'phone' => $user->phone,
                'from' => $from,
                'from_id' => $from_id,
                'to' => $to,
            ];
        }

        return $data;

    }

    public function get_issued($id)
    {
        $data = null;
        $user_info = $this->user_info();
        $details = $this->mdl_order_ext->get_issue_details($id);
        $items = $this->mdl_order_ext->get_issue_items($id);
        $user_id = null;

        if ($details) {
            $data['details'] = $details;
            $r_user = $this->mdl_user_ext->get_user_info($details->r_user);
            $i_user = $this->mdl_user_ext->get_user_info($details->i_user);
        }
        if ($items) {
            $data['items'] = $items;
        }

        if ($user_info->location_id !== $details->location_id) {
            $name = $i_user->first_name . ' ' . $i_user->last_name;
            $email = $i_user->email;
            $phone = $i_user->phone;
            $from_id = $i_user->location_id;
            if ($user_info->level_id == '1') {
                $to = $i_user->nation;
                $from = $this->location_name($details->location_id);
            } elseif ($user_info->level_id == '2') {
                $from = $i_user->region;
                $to = $r_user->subcounty;
            } elseif ($user_info->level_id == '3') {
                $from = $i_user->region;
                $to = $r_user->subcounty;
            } elseif ($user_info->level_id == '4') {
                $from = $i_user->region;
                $to = $r_user->subcounty;
            }
        } elseif ($user_info->location_id == $details->location_id) {
            $name = $r_user->first_name . ' ' . $r_user->last_name;
            $email = $r_user->email;
            $phone = $r_user->phone;
            $from_id = $r_user->location_id;
            if ($user_info->level_id == '1') {
                $to = $i_user->nation;
                $from = $r_user->region;
            } elseif ($user_info->level_id == '2') {
                $to = $i_user->region;
                $from = $r_user->subcounty;
            } elseif ($user_info->level_id == '3') {
                $to = $i_user->region;
                $from = $r_user->subcounty;
            } elseif ($user_info->level_id == '4') {
                $to = $i_user->region;
                $from = $r_user->subcounty;
            }
        }


        $data['user_details'] = [
            'name' => $name,
            'email' => $email,
            'phone' => $phone,
            'from' => $from,
            'from_id' => $from_id,
            'to' => $to,
        ];

        return ($data);

    }

    public function packing_batch()
    {
        if (isset($_POST) && !empty($_POST)) {
            $user_info = $this->user_info();
            $data_array = array();
            if ($this->input->post('vaccine') && is_array($this->input->post('vaccine'))) {

                $array = $this->input->post('vaccine');
                foreach ($array as $key => $value) {
                    $query = $this->mdl_stock_ext->get_batches($value['vaccine'], $user_info->location_id);
                    if (!is_null($query)) {
                        foreach ($query as $row) {
                            $data['batch_no'] = $row['batch_number'];
                            $data['balance'] = number_format($row['balance']);
                            $data_array['row' . $value['vaccine']][] = $data;
                        }
                    }
                }
                echo json_encode($data_array);
            }
        }
    }

    public function batch()
    {
        if (isset($_POST) && !empty($_POST)) {
            $user_info = $this->user_info();
            $data_array = array();
            if ($this->input->post('vaccine') && is_array($this->input->post('vaccine'))) {

                $array = $this->input->post('vaccine');
                array_shift($array);
                foreach ($array as $key => $value) {
                    $query = $this->mdl_stock_ext->get_batches($value['vaccine'], $user_info->location_id);
                    if (!is_null($query)) {
                        foreach ($query as $row) {
                            $data['batch_no'] = $row['batch_number'];
                            $data['balance'] = number_format($row['balance']);
                            $data_array[$value['row']][] = $data;
                        }
                    }
                }
                echo json_encode($data_array);
            }
        }
    }

    public function order_id($order_id)
    {
        $this->_order_id = $order_id;
        return $this;
    }

    public function mail_order()
    {
        $this->load->library('mailgun');
        $this->load->library('pdfgenerator');
        var_dump($this->_order_id);
        if (is_numeric($this->_order_id)) {
            $data['user_details'] = $this->get_order($this->_order_id)['user_details'];
            $data['order'] = $this->get_order($this->_order_id)['order'];
            $data['items'] = $this->get_order($this->_order_id)['items'];
        }
        $html = $this->load->view('order/emails/order_sheet', $data, true);
        $filename = 'Vaccine Order Sheet-' . time();
        $file = $this->pdfgenerator->generate($html, $filename, false);
        file_put_contents('docs/reports' . $filename . '.pdf', $file);
        $body = $this->load->view('emails/order');
        $this->mailgun
            ->to('amongash08@gmail.com')
            ->subject('Vaccine Order')
            ->message($body)
            ->attachments($file)
            ->send();

    }

    function test()
    {
        $this->load->library('mailgun');
        $this->load->library('pdfgenerator');
        $this->load->helper('file');
        if (is_numeric(1)) {
            $data['user_details'] = $this->get_order(1)['user_details'];
            $data['order'] = $this->get_order(1)['order'];
            $data['items'] = $this->get_order(1)['items'];
        }
        $html = $this->load->view('order/emails/order_sheet', $data, true);
        $filename = 'Vaccine Order Sheet-' . time();
        $file = $this->pdfgenerator->generate($html, $filename, false);
        file_put_contents('docs/reports/' . $filename . '.pdf', $file);
//
        $body = $this->load->view('emails/order', '', true);
        $this->mailgun
            ->to('amongash08@gmail.com')
            ->subject('Vaccine Order')
            ->message($body)
            ->attachments('docs/reports/' . $filename . '.pdf')
            ->send();
    }

    public function print_invoice($id = false)
    {
        $data['title'] = 'Proforma Invoice';
        $user_info = $this->user_info();
        $region = json_decode($user_info->json_location)->region;
        $county = json_decode($user_info->json_location)->county;
        $subcounty = json_decode($user_info->json_location)->subcounty;
        $facility = json_decode($user_info->json_location)->facility;

        $loc = $region . $county . $subcounty . $facility;
        $data['location'] = $loc;
        $data['user_info'] = $user_info;
        if (is_numeric($id)) {
            $data['id'] = $id;
            $data['user_details'] = $this->get_issued($id)['user_details'];
            $data['details'] = $this->get_issued($id)['details'];
            $data['items'] = $this->get_issued($id)['items'];
        }
        $this->load->view('order/proforma_invoice', $data);
    }

}