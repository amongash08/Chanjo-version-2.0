<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mdl_order_ext extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function _create_order($order, $items)
    {
        $this->db->trans_begin();
        $this->db->insert('requests', $order);
        $order_id = $this->db->insert_id();

        foreach ($items as $item) {
            foreach ($item as $key => $value) {
                if ($key == 'request_id') {
                    $item['request_id'] = $order_id;
                }
            }
            $this->db->insert('request_items', $item);
        }

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return FALSE;
        } else {
            $this->db->trans_commit();
            return $order_id;
        }
    }


    public function _pack($order, $items)
    {
        $this->db->trans_begin();
        $this->db->insert('packing', $order);
        $packing_id = $this->db->insert_id();

        foreach ($items as $item) {
            foreach ($item as $key => $value) {
                if ($key == 'packing_id') {
                    $item['packing_id'] = $packing_id;
                }
            }
            $this->db->insert('packing_items', $item);
        }

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return FALSE;
        } else {
            $this->db->trans_commit();
            return TRUE;
        }
    }

    public function get_order($id)
    {
        $query = $this->db->select()
            ->from('requests')
            ->where('id', $id)
            ->get();
        return $query->row();
    }

    public function get_order_items($id)
    {
        $query = $this->db->select('vaccine_name, request_items.*')
            ->from('request_items')
            ->join('vaccines', 'vaccines.id = request_items.vaccine_id')
            ->where('request_id', $id)
            ->get();
        return $query->result();
    }

    public function get_packing_list($id)
    {
        $query = $this->db->select('packing.*,requests.user_id as user')
            ->from('packing')
            ->join('requests', 'requests.id = packing.request_id')
            ->where('request_id', $id)
            ->get();
        return $query->row();
    }

    public function get_packing_items($id)
    {
        $query = $this->db->select('vaccine_name, packing_items.*')
            ->from('packing')
            ->join('packing_items', 'packing.id = packing_items.packing_id')
            ->join('vaccines', 'vaccines.id = packing_items.vaccine_id')
            ->where('request_id', $id)
            ->get();
        return $query->result();
    }

    public function get_packing_item($request_id, $id)
    {
        $query = $this->db->select('packing_items.*')
            ->from('packing')
            ->join('packing_items', 'packing.id = packing_items.packing_id')
            ->where('request_id', $request_id)
            ->where('packing_items.id', $id)
            ->get();

        if ($query->num_rows() > 0) {
            return $query->row();
        }else{
            return false;
        }


    }


    public function get_issue_items($id)
    {
        $query = $this->db->distinct()
            ->select('vaccine_name, ri.vaccine_id, ti.batch, ti.expiry_date, 
                     ti.transaction_quantity as issue_quantity, ri.transaction_quantity as order_quantity,ti.vvm, vaccine_price_dose')
            ->from('transactions t')
            ->join('requests r', 'r.id = t.request_id')
            ->join('transaction_items ti', 't.id = ti.transaction_id')
            ->join('request_items ri', 'r.id = ri.request_id AND ti.vaccine_id = ri.vaccine_id')
            ->join('vaccines v', 'v.id = ri.vaccine_id')
            ->where('r.id', $id)
            ->get();
        return $query->result();
    }

    public function get_issue_details($id)
    {
        $query = $this->db->distinct()
            ->select('transaction_date, r.location_id, r.to_from, r.user_id as r_user, t.user_id as i_user, t.request_id')
            ->from('transactions t')
            ->join('requests r', 'r.id = t.request_id')
            ->where('r.id', $id)
            ->get();
        return $query->row();
    }


}
