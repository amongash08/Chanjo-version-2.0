<?php echo form_open('order/create', array('class' => 'form-horizontal', 'id' => 'order')); ?>

<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Capacities</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                class="fa fa-minus"></i>
                    </button>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="row">
                    <div class="col-md-8">
                        <p class="text-center">
                            <strong></strong>
                        </p>

                        <div class="chart">
                            <!-- Chart Canvas -->
                            <!--                            <div id="coldchain" style="height: 150px;"></div>-->
                        </div>
                        <!-- /.chart-responsive -->
                    </div>
                    <!-- /.col -->
                    <div class="col-md-4">
                        <div class="progress-group">
                            <span class="progress-text">Under One Population</span>
                            <div class="pull-right"><strong><?php echo $population->under_one_population; ?></strong>
                            </div>
                            <hr>
                        </div>
                        <!-- /.progress-group -->
                        <!---->
                        <!--                        <div class="progress-group">-->
                        <!--                            <span class="progress-text">Vaccine Carriers</span>-->
                        <!--                            <span class="progress-number">Number required/<b><a data-toggle="modal">-->
                        <!--                                        Total Carriers</a></b></span>-->
                        <!--                            <hr>-->
                        <!--                            <div class="progress sm">-->
                        <!--                                <div class="progress-bar progress-bar-aqua" style="width: 80%"></div>-->
                        <!--                            </div>-->
                        <!--                        </div>-->
                        <!-- /.progress-group -->
                        <div class="progress-group form-group-sm">
                            <span class="progress-text"><?php echo $user_info->level_id == '1' ? 'Pickup Date' : 'Preferred Pickup Date' ?></span>
                            <input type="text" id="pickup_date" class="form-control" name="pickup_date">
                        </div>
                        <!-- /.progress-group -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- ./box-body -->
        </div>
        <!-- /.box -->
    </div>
    <!-- /.col -->
</div>
<!-- /.row -->

<div class="row">
    <!-- /.box-header -->
    <div class="box-body">
        <div class="box-group" id>
            <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
            <div class="panel box box-primary">
                <div class="box-header with-border">
                    <h4 class="box-title">
                        Request Details
                    </h4>

                </div>

                <div class="box-body">
                    <div class="table-responsive">

                        <div id="request_table">
                            <table id="order" class="table table-bordered table-hover table-striped">
                                <thead>
                                <tr>
                                    <th align="center">Vaccine/Diluents</th>
                                    <th align="center">Stock In Hand</th>
                                    <th align="center">Minimum Stock</th>
                                    <th align="center">Maximum Stock</th>
                                    <th align="center"><?php echo $user_info->level_id == '1' ? 'Allocation Quantity' : 'Order Quantity' ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($vaccines as $key => $value):
                                    if ($user_info->region_id == 2) :
                                        ?>
                                        <tr align="center" class="form-group-sm">

                                            <td class="col-xs-2"><?php echo $key; ?></td>
                                            <td hidden><input type="text" disabled
                                                              class="form-control vaccine" id="vaccine"
                                                              value="<?php echo $value['vaccine_id']; ?>"
                                                              name="vaccine">
                                            </td>
                                            <td class="col-xs-2"><input type="text" disabled
                                                                        class="form-control current_stock"
                                                                        id="current_stock"
                                                                        value="<?php echo $value['current_stock']; ?>"
                                                                        name="current_stock">
                                            </td>
                                            <td class="col-xs-2"><input type="text" disabled
                                                                        class="form-control min_stock"
                                                                        id="min_stock"
                                                                        value="<?php echo $value['min_stock']; ?>"
                                                                        name="min_stock">
                                            </td>
                                            <td class="col-xs-2"><input type="text" disabled
                                                                        class="form-control"
                                                                        id="max_stock"
                                                                        value="<?php echo $value['max_stock']; ?>"
                                                                        name="max_stock">
                                            </td>

                                            <td hidden><input type="text" disabled
                                                              class="form-control" id="doses"
                                                              value="<?php echo $value['doses']; ?>"
                                                              name="doses">
                                            </td>
                                            <td hidden><input type="text" disabled
                                                              class="form-control"
                                                              id="volume"
                                                              value="<?php echo $value['volume']; ?>"
                                                              name="volume">
                                            </td>
                                            <td hidden><input type="number" disabled
                                                              class="form-control"
                                                              id="quantity"
                                                              value="<?php echo $value['order']; ?>"
                                                              name="quantity">
                                            </td>
                                            <td class="col-xs-2"><input type="number"
                                                                        class="form-control"
                                                                        id="order"
                                                                        value="<?php echo $value['order']; ?>"
                                                                        name="order">
                                            </td>
                                        </tr>
                                    <?php else:
                                        if ($value['vaccine_id'] != 8 && $value['vaccine_id'] != 12):?>
                                            <tr align="center" class="form-group-sm">

                                                <td class="col-xs-2"><?php echo $key; ?></td>
                                                <td hidden><input type="text" disabled
                                                                  class="form-control vaccine" id="vaccine"
                                                                  value="<?php echo $value['vaccine_id']; ?>"
                                                                  name="vaccine">
                                                </td>
                                                <td class="col-xs-2"><input type="text" disabled
                                                                            class="form-control current_stock"
                                                                            id="current_stock"
                                                                            value="<?php echo $value['current_stock']; ?>"
                                                                            name="current_stock">
                                                </td>
                                                <td class="col-xs-2"><input type="text" disabled
                                                                            class="form-control min_stock"
                                                                            id="min_stock"
                                                                            value="<?php echo $value['min_stock']; ?>"
                                                                            name="min_stock">
                                                </td>
                                                <td class="col-xs-2"><input type="text" disabled
                                                                            class="form-control"
                                                                            id="max_stock"
                                                                            value="<?php echo $value['max_stock']; ?>"
                                                                            name="max_stock">
                                                </td>

                                                <td hidden><input type="text" disabled
                                                                  class="form-control" id="doses"
                                                                  value="<?php echo $value['doses']; ?>"
                                                                  name="doses">
                                                </td>
                                                <td hidden><input type="text" disabled
                                                                  class="form-control"
                                                                  id="volume"
                                                                  value="<?php echo $value['volume']; ?>"
                                                                  name="volume">
                                                </td>
                                                <td hidden><input type="number" disabled
                                                                  class="form-control"
                                                                  id="quantity"
                                                                  value="<?php echo $value['order']; ?>"
                                                                  name="quantity">
                                                </td>
                                                <td class="col-xs-2"><input type="number"
                                                                            class="form-control"
                                                                            id="order"
                                                                            value="<?php echo $value['order']; ?>"
                                                                            name="order">
                                                </td>
                                            </tr>
                                        <?php endif;
                                    endif;
                                endforeach; ?>

                                </tbody>
                            </table>


                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <button type="submit" id="validate" class="btn bg-navy margin">
                        Submit
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /.box-body -->
<?php echo form_close(); ?>

<script type="text/javascript">

    var url = '<?php echo site_url(); ?>';

    ajax_fill_data('reports/plot_fridge_capacity', "#coldchain", {'location': '<?php echo $id;?>'});

    $(document).ready(function () {

        $('#order')
            .formValidation({
                framework: 'bootstrap',
                err: {
                    container: 'tooltip'
                },
                row: {
                    selector: 'td'
                },
                // This option will not ignore invisible fields which belong to inactive panels
                excluded: ':hidden, :not(:visible)',
                fields: {
                    pickup_date: {
                        row: '.form-group-sm',
                        validators: {
                            notEmpty: {
                                message: '*required'
                            },
                            date: {
                                format: 'YYYY-MM-DD',
                                message: '*date is not valid'
                            }
                        }
                    },
                    min_stock: {
                        validators: {
                            notEmpty: {
                                message: '*required'
                            },
                        }
                    },
                    max_stock: {
                        validators: {
                            notEmpty: {
                                message: '*required'
                            },
                        }
                    },
                    order: {
                        validators: {
                            notEmpty: {
                                message: '*required'
                            },
                            numeric: {
                                message: '*not a number'
                            },
                            greaterThan: {
                                value: -1,
                                message: '*not a valid number'
                            },
                            lessThan: {
                                value: 'max_stock',
                                message: '*exceeds maximum stock'
                            }
                        }
                    }
                }
            }).on('success.form.fv', function (e) {
            // Prevent form submission
            e.preventDefault();
            swal({
                    title: "Confirm Submission",
                    text: "Are you sure you want to submit the entered details?",
                    type: "info",
                    showCancelButton: true,
                    confirmButtonColor: "#54CEF7",
                    confirmButtonText: "Submit",
                    closeOnConfirm: false,
                    showLoaderOnConfirm: true,
                },
                function (isConfirm) {
                    if (isConfirm) {
                        submit();
                    }
                });
        });


        $("#pickup_date").datepicker({
            dateFormat: 'yy-mm-dd',
            minDate: 0,
            beforeShow: function (textbox, instance) {
                var txtBoxOffset = $(this).offset();
                var top = txtBoxOffset.top;
                var left = txtBoxOffset.left;
                var textBoxWidth = $(this).outerWidth();

                setTimeout(function () {
                    instance.dpDiv.css({
                        top: top - 230, //you can adjust this value accordingly
                        left: left //show at the end of textBox
                    });
                }, 0);

            },
            onSelect: function (date, inst) {
                /* Revalidate the field when choosing it from the datepicker */
                $('#order').formValidation('revalidateField', 'pickup_date');
            }
        });


        function submit() {

            var vaccine_count = 0;
            $.each($(".vaccine"), function (i, v) {
                vaccine_count++;
            });


            var pickup_date = retrieveFormValues('pickup_date');
            var vaccines = retrieveFormValues_Array('vaccine');
            var order = retrieveFormValues_Array('order');
            var quantity = retrieveFormValues_Array('quantity');
            var current = retrieveFormValues_Array('current_stock');
            var max = retrieveFormValues_Array('max_stock');
            var min = retrieveFormValues_Array('min_stock');
            var dat = new Array();


            for (var i = 0; i < vaccine_count; i++) {
                var data = new Array();
                var get_vaccine = vaccines[i];
                var get_quantity = quantity[i];
                var get_order = order[i];
                var get_current = current[i];
                var get_max = max[i];
                var get_min = min[i];
                data = {
                    "vaccine_id": get_vaccine,
                    "quantity": get_quantity,
                    "order": get_order,
                    "current": get_current,
                    "max_stock": get_max,
                    "min_stock": get_min
                };
                dat.push(data);
            }
            batch = JSON.stringify(dat);

            $.ajax(
                {
                    url: $('#order').attr('action'),
                    type: "POST",
                    data: {
                        "pickup_date": pickup_date,
                        "batch": batch,
                        "id": '<?php echo $id;?>'
                    },
                    beforeSend: function () {

                    },
                    success: function (data, textStatus, jqXHR) {
                        window.location.replace('<?php echo site_url('order');?>');
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        //if fails
                    }
                });
            // e.unbind(); //unbind. to stop multiple form submit.
        }


        function retrieveFormValues_Array(name) {
            var dump = new Array();
            var counter = 0;
            $.each($("input[name=" + name + "], select[name=" + name + "]"), function (i, v) {
                var theTag = v.tagName;
                var theElement = $(v);
                var theValue = theElement.val();
                dump[counter] = theValue;
                counter++;
            });
            return dump;
        }

        function retrieveFormValues(name) {
            var dump;
            $.each($("input[name=" + name + "], select[name=" + name + "]"), function (i, v) {
                var theTag = v.tagName;
                var theElement = $(v);
                var theValue = theElement.val();
                dump = theValue;
            });
            return dump;
        }


    });


    function ajax_fill_data(function_url, div, data) {
        var function_url = url + function_url;
        var loading_icon = url + "assets/images/ring.gif";
        $.ajax({
            type: "POST",
            data: data,
            url: function_url,
            beforeSend: function () {
                $(div).html("<img style='margin:10% 50% 10% ;' src=" + loading_icon + ">");
            },
            success: function (msg) {
                $(div).html(msg);
            }
        });
    }
</script>

