<?php echo form_open('order/create', array('class' => 'form-horizontal', 'id' => 'order')); ?>

<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Transaction Details</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                class="fa fa-minus"></i>
                    </button>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-lg-3">
                            <div class="form-group-sm">
                                <label>Level</label>
                                <br>
                                <select name="level" class="form-control" id="level">
                                    <option value="">Select Level</option>
                                    <option value="3">County</option>
                                    <option value="4">Subcounty</option>
                                </select>

                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group-sm">
                                <label>Destination</label>
                                <br>
                                <input type="text" name="destination" class="form-control" id="destination" disabled>

                            </div>
                        </div>

                        <div class="col-lg-3">
                            <div class="form-group-sm">
                                <label>Date</label>
                                <br>
                                <input type="text" class="form-control date" id="date"
                                       name="date" placeholder="YYYY-MM-DD">
                            </div>
                        </div>
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- ./box-body -->
        </div>
        <!-- /.box -->
    </div>
    <!-- /.col -->
</div>
<!-- /.row -->

<div class="row">
    <!-- /.box-header -->
    <div class="box-body">
        <div class="box-group" id>
            <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
            <div class="panel box box-primary">
                <div class="box-header with-border">
                    <h4 class="box-title">
                        Vaccine Details
                    </h4>

                </div>

                <div class="box-body">
                    <div class="table-responsive">

                        <div id="request_table">
                            <table id="order" class="table table-bordered table-hover table-striped">
                                <thead>
                                <tr>
                                    <th align="center">Vaccine/Diluents</th>
                                    <th align="center">Stock In Hand</th>
                                    <th align="center">Minimum Stock</th>
                                    <th align="center">Maximum Stock</th>
                                    <th align="center"><?php echo $user_info->level_id == '1' ? 'Allocation Quantity' : 'Order Quantity' ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($vaccines as $key => $value):
                                    if ($user_info->region_id == 2) :
                                        ?>
                                        <tr align="center" class="form-group-sm" id="row<?php echo $key; ?>">

                                            <td class="col-xs-2"><?php echo $value; ?></td>
                                            <td hidden><input type="text" disabled
                                                              class="form-control vaccine" id="vaccine"
                                                              value="<?php echo $key; ?>"
                                                              name="vaccine">
                                            </td>
                                            <td class="col-xs-2"><input type="text" disabled
                                                                        class="form-control current_stock"
                                                                        id="current_stock"
                                                                        name="current_stock">
                                            </td>
                                            <td class="col-xs-2"><input type="text" disabled
                                                                        class="form-control min_stock"
                                                                        id="min_stock"
                                                                        name="min_stock">
                                            </td>
                                            <td class="col-xs-2"><input type="text" disabled
                                                                        class="form-control"
                                                                        id="max_stock"
                                                                        name="max_stock">
                                            </td>

                                            <td hidden><input type="text" disabled
                                                              class="form-control" id="doses"
                                                              name="doses">
                                            </td>
                                            <td hidden><input type="text" disabled
                                                              class="form-control"
                                                              id="volume"
                                                              name="volume">
                                            </td>
                                            <td hidden><input type="number" disabled
                                                              class="form-control"
                                                              id="quantity"
                                                              name="quantity">
                                            </td>
                                            <td class="col-xs-2"><input type="number"
                                                                        class="form-control"
                                                                        id="order"
                                                                        name="order">
                                            </td>
                                        </tr>
                                    <?php else:
                                        if ($value != 8 && $value != 12):?>
                                            <tr align="center" class="form-group-sm" id="row<?php echo $key; ?>">

                                                <td class="col-xs-2"><?php echo $value; ?></td>
                                                <td hidden><input type="text" disabled
                                                                  class="form-control vaccine" id="vaccine"
                                                                  value="<?php echo $key; ?>"
                                                                  name="vaccine">
                                                </td>
                                                <td class="col-xs-2"><input type="text" disabled
                                                                            class="form-control current_stock"
                                                                            id="current_stock"
                                                                            name="current_stock">
                                                </td>
                                                <td class="col-xs-2"><input type="text" disabled
                                                                            class="form-control min_stock"
                                                                            id="min_stock"
                                                                            name="min_stock">
                                                </td>
                                                <td class="col-xs-2"><input type="text" disabled
                                                                            class="form-control"
                                                                            id="max_stock"
                                                                            name="max_stock">
                                                </td>

                                                <td hidden><input type="text" disabled
                                                                  class="form-control" id="doses"
                                                                  name="doses">
                                                </td>
                                                <td hidden><input type="text" disabled
                                                                  class="form-control"
                                                                  id="volume"
                                                                  name="volume">
                                                </td>
                                                <td hidden><input type="number" disabled
                                                                  class="form-control"
                                                                  id="quantity"
                                                                  name="quantity">
                                                </td>
                                                <td class="col-xs-2"><input type="number"
                                                                            class="form-control"
                                                                            id="order"
                                                                            name="order">
                                                </td>
                                            </tr>
                                        <?php endif;
                                    endif;
                                endforeach; ?>

                                </tbody>
                            </table>


                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <button type="submit" id="validate" class="btn bg-navy margin">
                        Submit
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /.box-body -->
<?php echo form_close(); ?>

<script type="text/javascript">

    $(document).ready(function () {

        $('#order')
            .formValidation({
                framework: 'bootstrap',
                err: {
                    container: 'tooltip'
                },
                row: {
                    selector: 'td'
                },
                // This option will not ignore invisible fields which belong to inactive panels
                excluded: ':hidden, :not(:visible)',
                fields: {
                    date: {
                        row: '.form-group-sm',
                        validators: {
                            notEmpty: {
                                message: '*required'
                            },
                            date: {
                                format: 'YYYY-MM-DD',
                                message: '*date is not valid'
                            }
                        }
                    },
                    min_stock: {
                        validators: {
                            notEmpty: {
                                message: '*required'
                            },
                        }
                    },
                    max_stock: {
                        validators: {
                            notEmpty: {
                                message: '*required'
                            },
                        }
                    },
                    order: {
                        validators: {
                            notEmpty: {
                                message: '*required'
                            },
                            numeric: {
                                message: '*not a number'
                            },
                            greaterThan: {
                                value: -1,
                                message: '*not a valid number'
                            },
                            lessThan: {
                                value: 'max_stock',
                                message: '*exceeds maximum stock'
                            }
                        }
                    }
                }
            })
    }).on('change', '#level', function () {
        var level = $(this).val();

        if (level.length > 0) {


            var request = $.ajax({
                url: "<?php echo base_url('stock/get_level'); ?>",
                type: 'post',
                data: {"level": level},

            });

            request.done(function (data) {
                data = JSON.parse(data);
                var temp = [];

                $.each(data, function (key, value) {
                    temp.push({v: value, k: key});
                });
                temp.sort(function (a, b) {
                    if (a.v > b.v) {
                        return 1
                    }
                    if (a.v < b.v) {
                        return -1
                    }
                    return 0;
                });

                var dropdown_start = '<select class="form-control" id="destination" name="destination">';
                var dropdown_option = new Array();
                var default_option = "<option value=''>" + "Select Level" + "</option> ";
                var dropdown_end = '</select>';
                dropdown_option.push(default_option);
                $.each(temp, function (key, obj) {
                    option = "<option value='" + obj.k + "'>" + obj.v + "</option> ";
                    dropdown_option.push(option);
                });
                $("#destination").replaceWith(dropdown_start + dropdown_option.join("") + dropdown_end);


            });
            request.fail(function (jqXHR, textStatus) {

            });
        }

    }).on('change', '#destination', function () {
        var destination = $(this).val();
        var level = $('#level').val();
        var vaccines = new Array();

        $('#order tr').each(function (row, tr) {
            if (row !== 0) {
                vaccines[row] = (tr.id).slice(3);
            }
        });
        load_vaccine_data(level, destination, vaccines);

    }).on('success.form.fv', function (e) {
        // Prevent form submission
        e.preventDefault();
        swal({
                title: "Confirm Submission",
                text: "Are you sure you want to submit the entered details?",
                type: "info",
                showCancelButton: true,
                confirmButtonColor: "#54CEF7",
                confirmButtonText: "Submit",
                closeOnConfirm: false,
                showLoaderOnConfirm: true,
            },
            function (isConfirm) {
                if (isConfirm) {
                    submit();
                }
            });
    });

    function load_vaccine_data(level, destination, vaccines) {

        var request = $.ajax({
            url: '<?php echo site_url('order/calculate_order'); ?>',
            type: 'post',
            data: {
                'level': level,
                'destination': destination,
                'vaccines': vaccines
            },

        });

        request.done(function (data) {
            data = JSON.parse(data);
            $.each(data, function (key, value) {
                var row = $('#row' + value.vaccine_id);
                row.closest("tr").find("#current_stock").val(value.current_stock);
                row.closest("tr").find("#min_stock").val(value.min_stock);
                row.closest("tr").find("#max_stock").val(value.max_stock);
                row.closest("tr").find("#doses").val(value.doses);
                row.closest("tr").find("#volume").val(value.volume);
                row.closest("tr").find("#quantity").val(value.order);
                row.closest("tr").find("#order").val(value.order);
            });

        });
        request.fail(function (jqXHR, textStatus) {

        });

    }


    $("#date").datepicker({
        dateFormat: 'yy-mm-dd',
        minDate: 0,
        beforeShow: function (textbox, instance) {
            var txtBoxOffset = $(this).offset();
            var top = txtBoxOffset.top;
            var left = txtBoxOffset.left;
            var textBoxWidth = $(this).outerWidth();

            setTimeout(function () {
                instance.dpDiv.css({
                    top: top - 230, //you can adjust this value accordingly
                    left: left //show at the end of textBox
                });
            }, 0);

        },
        onSelect: function (date, inst) {
            /* Revalidate the field when choosing it from the datepicker */
            $('#order').formValidation('revalidateField', 'date');
        }
    });


    function submit() {

        var vaccine_count = 0;
        $.each($(".vaccine"), function (i, v) {
            vaccine_count++;
        });


        var destination = retrieveFormValues('destination');
        var level = retrieveFormValues('level');
        var pickup_date = retrieveFormValues('date');
        var vaccines = retrieveFormValues_Array('vaccine');
        var order = retrieveFormValues_Array('order');
        var quantity = retrieveFormValues_Array('quantity');
        var current = retrieveFormValues_Array('current_stock');
        var max = retrieveFormValues_Array('max_stock');
        var min = retrieveFormValues_Array('min_stock');
        var dat = new Array();


        for (var i = 0; i < vaccine_count; i++) {
            var data = new Array();
            var get_vaccine = vaccines[i];
            var get_quantity = quantity[i];
            var get_order = order[i];
            var get_current = current[i];
            var get_max = max[i];
            var get_min = min[i];
            data = {
                "vaccine_id": get_vaccine,
                "quantity": get_quantity,
                "order": get_order,
                "current": get_current,
                "max_stock": get_max,
                "min_stock": get_min
            };
            dat.push(data);
        }
        batch = JSON.stringify(dat);

        $.ajax(
            {
                url: $('#order').attr('action'),
                type: "POST",
                data: {
                    "pickup_date": pickup_date,
                    "batch": batch,
                    "destination": destination,
                    "level": level
                },
                beforeSend: function () {

                },
                success: function (data, textStatus, jqXHR) {
                    window.location.replace('<?php echo site_url('order');?>');
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    //if fails
                }
            });
        // e.unbind(); //unbind. to stop multiple form submit.
    }


    function retrieveFormValues_Array(name) {
        var dump = new Array();
        var counter = 0;
        $.each($("input[name=" + name + "], select[name=" + name + "]"), function (i, v) {
            var theTag = v.tagName;
            var theElement = $(v);
            var theValue = theElement.val();
            dump[counter] = theValue;
            counter++;
        });
        return dump;
    }

    function retrieveFormValues(name) {
        var dump;
        $.each($("input[name=" + name + "], select[name=" + name + "]"), function (i, v) {
            var theTag = v.tagName;
            var theElement = $(v);
            var theValue = theElement.val();
            dump = theValue;
        });
        return dump;
    }

</script>

