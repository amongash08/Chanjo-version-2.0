<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo $title; ?></title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="<?php echo $_SERVER['DOCUMENT_ROOT'];?>/assets/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo $_SERVER['DOCUMENT_ROOT'];?>/assets/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="<?php echo $_SERVER['DOCUMENT_ROOT'];?>/assets/ionicons/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo $_SERVER['DOCUMENT_ROOT'];?>/assets/css/AdminLTE.min.css">
</head>
<body>
<!-- Main content -->
<section class="invoice">
    <!-- title row -->
    <div class="row">
        <div class="col-sm-12">

            <img class="pull-left" width="100" height="80"
                 src="<?php echo $_SERVER['DOCUMENT_ROOT']; ?>/assets/images/logo.png">
            <div class="col-sm-7">
                <div class="margin">
                    <p style="margin-top:-4px;font-size:1.6em;font-weight:600">
                        Ministry of Health.
                    </p>
                    <p style="font-size:1.2em;font-weight:600">
                        National Vaccines &amp; Immunization Programme.
                    </p>
                </div>

            </div>
        </div>
        <!-- /.col -->
    </div>
    <hr>
    <!-- info row -->
    <table class="invoice-info" width="100%">
        <tbody>
        <tr>
            <td width="33%">
                <div class="invoice-col">
                    <b> From</b>
                    <address>
                        <i><?php echo $user_details['from']; ?></i><br>
                        <i><?php echo $user_details['name']; ?></i><br>
                        <i><?php echo $user_details['phone']; ?></i><br>
                        <i><?php echo $user_details['email']; ?></i><br>
                    </address>
                </div>
            </td>
            <td width="33%">
                <div class="invoice-col" style="margin-top: -60px">
                    <b>To</b>
                    <address>
                        <i><?php echo $user_details['to']; ?></i><br>

                    </address>
                </div>
            </td>
            <td width="33%">
                <div class="invoice-col" style="margin-top: -60px">
                    <b>Order ID:</b>
                    <i><?php echo strtoupper(substr($user_details['from'], 0, 3)) . '/' . $order->id; ?></i><br>
                    <b>Order Date:</b> <i><?php echo date('Y-m-d', strtotime($order->timestamp)); ?></i><br>
                    <b>Collection Date:</b> <i><?php echo $order->pickup_date; ?></i>
                </div>
            </td>
        </tr>
        </tbody>
    </table>
    <hr>
    <!-- Table row -->
    <div class="row">
        <div class="col-xs-12 table-responsive">
            <table class="table" width="100%">
                <thead>
                <tr>
                    <th>Vaccine /<br>Diluent</th>
                    <th>Stock On <br> Hand</th>
                    <th>Minimum <br> Stock</th>
                    <th>Maximum <br> Stock</th>
                    <th>Quantity <br> ordered(Doses)</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($items as $key => $value): ?>
                    <tr>
                        <td><?php echo $value->vaccine_name; ?></td>
                        <td><?php echo $value->current_quantity; ?></td>
                        <td><?php echo $value->min_quantity; ?></td>
                        <td><?php echo $value->max_quantity; ?></td>
                        <td><?php echo $value->transaction_quantity; ?></td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</body>
</html>