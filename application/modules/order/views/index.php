<div class="row">
    <div class="box-body">
        <div class="col-sm-12">
            <a href="<?php echo site_url('order/create'); ?>" class="btn btn-primary state_change">Create Order</a>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-6">
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">My Orders</h3>

                <div class="box-tools pull-right">
                    <button data-widget="collapse" class="btn btn-box-tool" type="button"><i class="fa fa-minus"></i>
                    </button>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body" style="height: 300px">
                <div class="table-responsive">
                    <table class="table no-margin">
                        <thead>
                        <tr>
                            <th>Order<br> Number</th>
                            <th>Date</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php if ($my_orders):
                            foreach ($my_orders as $key => $value):
                                if ($value->status != 'received'):
                                    ?>
                                    <tr>
                                    <td>
                                        <?php echo $value->id; ?>
                                    </td>
                                    <td><?php echo date('Y-m-d', strtotime($value->timestamp)); ?></td>
                                    <?php if ($value->status == 'pending' || $value->status == 'packing'): ?>
                                    <td><span class="label label-danger">Pending</span></td>
                                    <td><a href="<?php echo site_url('order/order_sheet/' . $value->id); ?>"
                                           class="btn btn-primary state-change btn-xs">View</a></td>
                                <?php elseif ($value->status == 'issued'): ?>
                                    <td><span class="label label-success">Issued</span></td>
                                    <td><a href="<?php echo site_url('order/receive/' . $value->id); ?>"
                                           class="btn btn-primary state-change btn-xs">View</a></td>
                                    </tr>
                                <?php endif;
                                endif;
                            endforeach;
                        endif; ?>
                        </tbody>
                    </table>
                </div>
                <!-- /.table-responsive -->
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix">

            </div>
            <!-- /.box-footer -->
        </div>
    </div>

    <div class="col-sm-6">
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Other Orders</h3>

                <div class="box-tools pull-right">
                    <button data-widget="collapse" class="btn btn-box-tool" type="button"><i class="fa fa-minus"></i>
                    </button>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body" style="height: 300px; overflow-y: scroll;">
                <div class="table-responsive">
                    <table class="table no-margin">
                        <thead>
                        <tr>
                            <th width="10%">Order Number</th>
                            <th width="10%">From</th>
                            <th width="10%">Collection Date</th>
                            <th width="10%">Status</th>
                            <th width="20%">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php if ($other_orders):
                            foreach ($other_orders as $key => $value):
                                if ($value->status != 'received'):
                                    ?>
                                    <tr>
                                    <td><?php echo $value->id; ?></td>
                                    <td><?php echo $value->from; ?></td>
                                    <td><?php echo date('Y-m-d', strtotime($value->pickup_date)); ?></td>
                                    <?php if ($value->status == 'pending'): ?>
                                    <td><span class="label label-danger">Pending</span></td>
                                    <td><a href="<?php echo site_url('order/order_sheet/' . $value->id); ?>"
                                           class="btn btn-primary state-change btn-xs">View</a>&nbsp;
                                        <a href="<?php echo site_url('order/pack/' . $value->id); ?>"
                                           class="btn btn-primary state-change btn-xs">Create Packing List</a></td>
                                <?php elseif ($value->status == 'packing'): ?>
                                    <td><span class="label label-warning">Packing</span></td>
                                    <td><a href="<?php echo site_url('order/order_sheet/' . $value->id); ?>"
                                           class="btn btn-primary state-change btn-xs">View</a>&nbsp;
                                        <a href="<?php echo site_url('order/packing_list/' . $value->id); ?>"
                                           class="btn btn-primary state-change btn-xs">View Packing List</a></td>

                                <?php elseif ($value->status == 'issued'): ?>
                                    <td><span class="label label-success">Issued</span></td>
                                    <td><a href="<?php echo site_url('order/issued/' . $value->id); ?>"
                                           class="btn btn-primary state-change btn-xs">View</a>&nbsp;
                                    </td>

                                    </tr>
                                <?php endif;
                                endif;
                            endforeach;
                        endif; ?>
                        </tbody>
                    </table>
                </div>
                <!-- /.table-responsive -->
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix">

            </div>
            <!-- /.box-footer -->
        </div>
    </div>

</div>