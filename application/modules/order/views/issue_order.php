<?php echo form_open('order/issue', array('class' => "form-horizontal", 'id' => "issue")); ?>
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Transaction Details</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                class="fa fa-minus"></i>
                    </button>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-lg-3">
                            <div class="form-group-sm">
                                <label>From</label>
                                <br>
                                <input type="text" class="form-control" value="<?php echo $user_details['from']; ?>"
                                       disabled>
                                <input type="text" value="<?php echo $user_details['from_id']; ?>" name="from" id="from"
                                       hidden disabled>

                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group-sm">
                                <label>Order Number</label>
                                <br>
                                <input type="text" class="form-control" value="<?php echo $packing->request_id; ?>"
                                       disabled>

                            </div>
                        </div>

                        <div class="col-lg-3">
                            <div class="form-group-sm">
                                <label>Date</label>
                                <br>
                                <input type="text" class="form-control issue_date" id="issue_date"
                                       name="issue_date">
                            </div>
                        </div>
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- ./box-body -->
        </div>
        <!-- /.box -->
    </div>
    <!-- /.col -->
</div>
<!-- /.row -->
<div class="row">
    <!-- /.box-header -->
    <div class="box-body">
        <div class="box-group">
            <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
            <div class="panel box box-primary">
                <div class="box-header with-border">
                    <h4 class="box-title">
                        Vaccine Details
                    </h4>

                </div>

                <div class="box-body">
                    <div class="table-responsive" id="table">
                        <table class="table table-bordered table-hover table-striped">
                            <thead>
                            <tr align="center" class="form-group-sm">
                                <th style="width:13%;">Vaccine /<br>Diluents</th>
                                <th style="width:13%;">Batch <br>Number</th>
                                <th style="width:13%;">Expiry <br>Date</th>
                                <th style="width:12%;">Amount <br>Ordered</th>
                                <th style="width:12%;">Packing <br>Quantity</th>
                                <th style="width:12%;">Amount <br>Issued</th>
                                <th style="width:13%;">VVM <br>Status</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($items as $key => $value):
                                if ($value->batch):?>
                                    <tr class="form-group-sm">
                                        <td>
                                            <?php echo $value->vaccine_name; ?>
                                        </td>
                                        <td hidden>
                                            <input type="text" class="form-control vaccine" id="vaccine"
                                                   name="vaccine" value="<?php echo $value->vaccine_id; ?>"
                                                   disabled>
                                        </td>
                                        <td>
                                            <input type="text" class="form-control batch_no" id="batch_no"
                                                   name="batch_no"
                                                   value="<?php echo $value->batch; ?>" disabled>
                                        </td>
                                        <td>
                                            <input type="text" class="form-control expiry_date" id="expiry_date"
                                                   name="expiry_date" value="<?php echo $value->expiry_date; ?>"
                                                   disabled>
                                        </td>
                                        <td>
                                            <input type="text" class="form-control order_quantity" id="order_quantity"
                                                   name="order_quantity" value="<?php echo $value->order_quantity; ?>"
                                                   disabled>
                                        </td>
                                        <td>
                                            <input type="text" class="form-control packing_quantity"
                                                   id="packing_quantity"
                                                   name="packing_quantity[<?php echo $value->id; ?>]"
                                                   value="<?php echo $value->packing_quantity; ?>"
                                                   disabled>
                                        </td>
                                        <td>
                                            <input type="text" class="form-control issue_quantity" id="issue_quantity"
                                                   name="issue_quantity[<?php echo $value->id; ?>]"
                                                   value="<?php echo $value->packing_quantity; ?>">
                                        </td>
                                        <td>
                                            <?php echo form_dropdown($vvm); ?>
                                        </td>
                                    </tr>
                                <?php endif;
                            endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="box-footer">
                    <button type="submit" id="validate" class="btn bg-navy margin">
                        Submit
                    </button>
                </div>
            </div>


        </div>

    </div>
</div>
<?php echo form_close(); ?>
<script>
    $(document).ready(function () {

        $('#issue')
            .formValidation({
                framework: 'bootstrap',
                err: {
                    container: 'tooltip'
                },
                row: {
                    selector: 'td'
                },
                // This option will not ignore invisible fields which belong to inactive panels
                excluded: ':hidden, :not(:visible)',
                fields: {
                    issue_date: {
                        row: '.form-group-sm',
                        validators: {
                            notEmpty: {
                                message: '*required'
                            },
                            date: {
                                format: 'YYYY-MM-DD',
                                message: '*date is not valid',
                                max: moment().format('YYYY-MM-DD'),
                                min: moment('2016-06-01').format('YYYY-MM-DD')
                            }
                        }
                    },
                    vaccine: {
                        validators: {
                            notEmpty: {
                                message: '*required'
                            }
                        }
                    },
                    batch_no: {
                        validators: {
                            notEmpty: {
                                message: '*required'
                            }
                        }
                    },
                    expiry_date: {
                        validators: {
                            notEmpty: {
                                message: '*required'
                            }
                        }
                    },
                    order_quantity: {
                        validators: {
                            notEmpty: {
                                message: '*required'
                            }
                        }
                    },
                    <?php foreach ($items as $key => $value):
                    if ($value->batch):?>
                    'packing_quantity[<?php echo $value->id; ?>]': {
                        validators: {
                            notEmpty: {
                                message: '*required'
                            }
                        }
                    },
                    'issue_quantity[<?php echo $value->id; ?>]': {
                        validators: {
                            notEmpty: {
                                message: '*required'
                            },
                            numeric: {
                                message: '*not a number'
                            },
                            between: {
                                min: 0,
                                max: 'packing_quantity[<?php echo $value->id; ?>]',
                                message: '*not a valid number'
                            }
                        }
                    },
                    <?php endif;
                    endforeach; ?>
                    vvm: {
                        validators: {
                            notEmpty: {
                                message: '*required'
                            }
                        }
                    }
                }
            }).on('success.form.fv', function (e) {
            // Prevent form submission
            e.preventDefault();
            swal({
                    title: "Confirm Submission",
                    text: "Are you sure you want to submit the entered details?",
                    type: "info",
                    showCancelButton: true,
                    confirmButtonColor: "#54CEF7",
                    confirmButtonText: "Submit",
                    closeOnConfirm: false,
                    showLoaderOnConfirm: true,
                },
                function (isConfirm) {
                    if (isConfirm) {
                        submit();
                    }
                });
        });


        $('#issue_date')
            .datepicker({
                dateFormat: "yy-mm-dd",
                maxDate: 0,
                beforeShow: function (textbox, instance) {
                    var txtBoxOffset = $(this).offset();
                    var top = txtBoxOffset.top;
                    var left = txtBoxOffset.left;
                    var textBoxWidth = $(this).outerWidth();
                    setTimeout(function () {
                        instance.dpDiv.css({
                            top: top - 100, //you can adjust this value accordingly
                            left: left + textBoxWidth//show at the end of textBox
                        });
                    }, 0);

                },
                onSelect: function (date, inst) {
                    /* Revalidate the field when choosing it from the datepicker */
                    $('#issue').formValidation('revalidateField', 'issue_date');
                }
            }).datepicker("setDate", new Date());


        function submit() {
            var vaccine_count = 0;
            $.each($(".vaccine"), function (i, v) {
                vaccine_count++;
            });

            var issue_date = retrieveFormValues('issue_date');
            var from = retrieveFormValues('from');

            var vaccines = retrieveFormValues_Array('vaccine');
            var batch_no = retrieveFormValues_Array('batch_no');
            var expiry_date = retrieveFormValues_Array('expiry_date');
            var order_quantity = retrieveFormValuesArray_Array('order_quantity');
            var issue_quantity = retrieveFormValuesArray_Array('issue_quantity');
            var vvm = retrieveFormValues_Array('vvm');

            var dat = new Array();

            for (var i = 0; i < vaccine_count; i++) {
                var data = new Array();
                var get_vaccine = vaccines[i];
                var get_batch = batch_no[i];
                var get_expiry = expiry_date[i];
                var get_order_quantity = order_quantity[i];
                var get_issue_quantity = issue_quantity[i];
                var get_vvm = vvm[i];

                data = {
                    "vaccine_id": get_vaccine,
                    "batch_no": get_batch,
                    "expiry_date": get_expiry,
                    "order_quantity": get_order_quantity,
                    "issue_quantity": get_issue_quantity,
                    "vvm": get_vvm
                };
                dat.push(data);
            }


            batch = JSON.stringify(dat);
            $.ajax({
                url: $('#issue').attr('action'),
                type: "POST",
                data: {
                    "id": '<?php echo $id; ?>',
                    "from": from,
                    "issue_date": issue_date,
                    "batch": batch
                },
                beforeSend: function () {
                    $('#validate').fadeOut(300, function () {
                        $(this).remove();
                    });
                },

                success: function (data, textStatus, jqXHR) {
                    window.location.replace('<?php echo site_url('order');?>');
                },

                error: function (jqXHR, textStatus, errorThrown) {
                    //if fails
                }
            });

        }

        function retrieveFormValues(name) {
            var dump;
            $.each($("input[name=" + name + "], select[name=" + name + "]"), function (i, v) {
                var theTag = v.tagName;
                var theElement = $(v);
                var theValue = theElement.val();
                dump = theValue;
            });
            return dump;
        }

        function retrieveFormValues_Array(name) {
            var dump = new Array();
            var counter = 0;
            $.each($("input[name=" + name + "], select[name=" + name + "]"), function (i, v) {
                var theTag = v.tagName;
                var theElement = $(v);
                var theValue = theElement.val();
                dump[counter] = theValue;

                counter++;
            });
            return dump;
        }

        function retrieveFormValuesArray_Array(name) {
            var dump = new Array();
            var counter = 0;
            $.each($("input[id=" + name + "]"), function (i, v) {
                var theTag = v.tagName;
                var theElement = $(v);
                var theValue = theElement.val();
                dump[counter] = theValue;

                counter++;
            });
            return dump;
        }

    });

</script>
