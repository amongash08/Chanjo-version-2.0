<!-- Main content -->
<section class="invoice">
    <!-- Table row -->
    <div class="row">
        <div class="col-xs-12 table-responsive">
            <table class="table table-striped table-condensed table-hover">
                <thead>
                <tr>
                    <th style="width:13%;">Vaccine /<br>Diluents</th>
                    <th style="width:13%;">Batch <br>Number</th>
                    <th style="width:13%;">Expiry <br>Date</th>
                    <th style="width:12%;">Amount <br>Ordered</th>
                    <th style="width:12%;">Amount <br>Issued</th>
                    <th style="width:13%;">VVM <br>Status</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($items as $key => $value): ?>
                    <tr>
                        <td><?php echo $value->vaccine_name; ?></td>
                        <td><?php echo $value->batch; ?></td>
                        <td><?php echo $value->expiry_date; ?></td>
                        <td><?php echo $value->order_quantity; ?></td>
                        <td><?php echo $value->issue_quantity; ?></td>
                        <td>Stage <?php echo $value->vvm; ?></td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
    <hr>
    <!-- this row will not appear when printing -->
    <div class="row no-print">
        <div class="col-xs-12">
            <div class="col-sm-3">
                <a href="<?php echo site_url('order/print_issue_sheet/' . $id); ?>" target="_blank"
                   class="btn btn-default"><i
                            class="fa fa-print"></i> Print Issue Form</a>
            </div>
            <div class="col-sm-3">
                <a href="<?php echo site_url('order/print_invoice/' . $id); ?>" target="_blank"
                   class="btn btn-default"><i
                            class="fa fa-print"></i> Print Invoice</a>
            </div>
            <div class="col-sm-3 pull-right">
                <p>Please print <strong>2 Copies of the Issue Form</strong></p>
            </div>
        </div>


    </div>

</section>
<!-- /.content -->