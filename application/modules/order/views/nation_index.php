<div class="row">
    <div class="box-body">
        <div class="col-sm-12">
            <div class="btn-group">
                <button class="btn btn-primary" type="button">Push Stocks</button>
                <button data-toggle="dropdown" class="btn btn-primary dropdown-toggle" type="button">
                    <span class="caret"></span>
                    <span class="sr-only">Toggle Dropdown</span>
                </button>
                <ul role="menu" class="dropdown-menu">
                    <?php foreach ($location as $key => $value): ?>
                        <li><a href="<?php echo site_url('order/create/' . $key); ?>"><?php echo $value; ?></a></li>
                    <?php endforeach; ?>
                        <li><a href="<?php echo site_url('order/create/'); ?>">Other</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Orders</h3>

                <div class="box-tools pull-right">
                    <button data-widget="collapse" class="btn btn-box-tool" type="button"><i class="fa fa-minus"></i>
                    </button>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body" style="height: 300px; overflow-y: scroll;">
                <div class="table-responsive">
                    <table class="table no-margin">
                        <thead>
                        <tr>
                            <th>Order Number</th>
                            <th>From</th>
                            <th>Collection Date</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php if ($other_orders):
                            foreach ($other_orders as $key => $value):
                             ?>
                                <tr>
                                    <td><?php echo $value->id; ?></td>
                                    <td><?php echo $value->from; ?></td>
                                    <td><?php echo date('Y-m-d', strtotime($value->pickup_date)); ?></td>
                                    <?php if ($value->status == 'pending'): ?>
                                        <td><span class="label label-danger">Pending</span></td>
                                        <td><a href="<?php echo site_url('order/order_sheet/' . $value->id); ?>"
                                               class="btn btn-primary state-change btn-xs">View</a>&nbsp;
                                            <a href="<?php echo site_url('order/pack/' . $value->id); ?>"
                                               class="btn btn-primary state-change btn-xs">Create Packing List</a>
                                        </td>
                                    <?php elseif ($value->status == 'packing'): ?>
                                        <td><span class="label label-warning">Packing</span></td>
                                        <td><a href="<?php echo site_url('order/update_pack/' . $value->id); ?>"
                                               class="btn btn-primary state-change btn-xs">Update Packing List</a>&nbsp;
                                            <a href="<?php echo site_url('order/packing_list/' . $value->id); ?>"
                                               class="btn btn-primary state-change btn-xs">View</a>
                                            <a href="<?php echo site_url('order/issue/' . $value->id); ?>"
                                               class="btn btn-primary state-change btn-xs">Issue</a>
                                            </td>
                                    <?php elseif ($value->status == 'issued'): ?>
                                        <td><span class="label label-success">Issued</span></td>
                                        <td><a href="<?php echo site_url('order/issued/' . $value->id); ?>"
                                               class="btn btn-primary state-change btn-xs">View</a>&nbsp;
                                        </td>

                                    <?php endif; ?>
                                </tr>
                            <?php
                            endforeach;
                        endif; ?>
                        </tbody>
                    </table>
                </div>
                <!-- /.table-responsive -->
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix">

            </div>
            <!-- /.box-footer -->
        </div>
    </div>

</div>