<!-- Main content -->
<section class="invoice">
    <!-- Table row -->
    <div class="row">
        <div class="col-xs-12 table-responsive">
            <table class="table table-striped table-condensed table-hover">
                <thead>
                <tr>
                    <th>Vaccine</th>
                    <th>Stock On Hand</th>
                    <th>Minimum Stock</th>
                    <th>Maximum Stock</th>
                    <th>Quantity ordered(Doses)</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($order_items as $key => $value): ?>
                    <tr>
                        <td><?php echo $value->vaccine_name; ?></td>
                        <td><?php echo $value->current_quantity; ?></td>
                        <td><?php echo $value->min_quantity; ?></td>
                        <td><?php echo $value->max_quantity; ?></td>
                        <td><?php echo $value->transaction_quantity; ?></td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->

    <!-- this row will not appear when printing -->
    <div class="row no-print">
        <div class="col-xs-12">
            <a href="<?php echo site_url('order/print_order_sheet/' . $order->id); ?>" target="_blank"
               class="btn btn-default"><i
                        class="fa fa-print"></i> Print</a>
            <?php if ($order->to_from == $user_info->location_id && $order->status == 'pending'): ?>
                <a href="<?php echo site_url('order/pack/' . $order->id); ?>" class="btn bg-navy pull-right">Create
                    Packing List </a>
            <?php elseif ($order->to_from == $user_info->location_id && $order->status == 'packing'): ?>
                <a href="<?php echo site_url('order/issue/' . $order->id); ?>" class="btn bg-navy pull-right">Issue
                    Order </a>
            <?php else: if ($order->status != 'pending' && $order->status != 'packing'): ?>
                <a href="<?php echo site_url('order/receive/' . $order->id); ?>" class="btn bg-navy pull-right">Receive
                    Order </a>
            <?php endif; endif; ?>
        </div>
    </div>
</section>
<!-- /.content -->