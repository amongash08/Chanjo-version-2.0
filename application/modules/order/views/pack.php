<?php echo form_open('order/pack', array('class' => "form-horizontal", 'id' => "pack")); ?>
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Transaction Details</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                class="fa fa-minus"></i>
                    </button>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-lg-3">
                            <div class="form-group-sm">
                                <label><?php echo ($user_info->level_id == '1') ? 'To' : 'From' ?></label>
                                <br>
                                <input type="text" class="form-control" value="<?php echo $user_details['from']; ?>"
                                       disabled>


                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group-sm">
                                <label>Order Number</label>
                                <br>
                                <input type="text" class="form-control" value="<?php echo $order->id; ?>"
                                       disabled>

                            </div>
                        </div>

                        <div class="col-lg-3">
                            <div class="form-group-sm">
                                <label>Packing Date</label>
                                <br>
                                <input type="text" class="form-control packing_date" id="packing_date"
                                       name="packing_date">
                            </div>
                        </div>
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- ./box-body -->
        </div>
        <!-- /.box -->
    </div>
    <!-- /.col -->
</div>
<!-- /.row -->
<div class="row">
    <!-- /.box-header -->
    <div class="box-body">
        <div class="box-group">
            <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
            <div class="panel box box-primary">
                <div class="box-header with-border">
                    <h4 class="box-title">
                        Vaccine Details
                    </h4>

                </div>

                <div class="box-body">
                    <div class="table-responsive" id="table">
                        <table class="table table-bordered table-hover table-striped">
                            <thead>
                            <tr align="center" class="form-group-sm">
                                <th style="width:13%;">Vaccine /Diluents</th>
                                <th style="width:15%;">Batch Number</th>
                                <th style="width:12%;">Expiry Date</th>
                                <th style="width:13%;">Stock Quantity</th>
                                <th style="width:13%;">Order Quantity</th>
                                <th style="width:13%;">Packing Quantity</th>
                                <th style="width:8%;">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($items as $key => $value):
                                if ($value->transaction_quantity >= 0):?>
                                    <tr class="form-group-sm" id="row<?php echo $value->vaccine_id; ?>">
                                        <td id="vaccine_name">
                                            <?php echo $value->vaccine_name; ?>
                                        </td>
                                        <td hidden>
                                            <input type="text" class="form-control vaccine" id="vaccine"
                                                   name="vaccine" value="<?php echo $value->vaccine_id; ?>"
                                                   disabled>
                                        </td>
                                        <td>
                                            <input type="text" class="form-control batch_no" id="batch_no"
                                                   name="batch_no"
                                                   disabled>
                                        </td>
                                        <td>
                                            <input type="text" class="form-control expiry_date" id="expiry_date"
                                                   name="expiry_date"
                                                   disabled>
                                        </td>
                                        <td>
                                            <input type="text" class="form-control stock_quantity"
                                                   id="stock_quantity"
                                                   name="stock_quantity[<?php echo $value->vaccine_id; ?>]"
                                                   disabled>
                                        </td>
                                        <td>
                                            <input type="text" class="form-control order_quantity"
                                                   id="order_quantity"
                                                   name="order_quantity"
                                                   value="<?php echo $value->transaction_quantity; ?>"
                                                   disabled>
                                        </td>
                                        <td>
                                            <input type="text" class="form-control packing_quantity"
                                                   id="packing_quantity"
                                                   name="packing_quantity[<?php echo $value->vaccine_id; ?>]">
                                        </td>

                                        <td class="small">
                                            <a class="add btn-xs" style="cursor: pointer; cursor: hand;"><span
                                                        class="label label-success"><i
                                                            class="fa fa-plus-square"></i> <b>ADD</b></span></a><br>
                                            <a class="remove btn-xs" style="cursor: pointer; cursor: hand;"><span
                                                        class="label label-danger"><i
                                                            class="fa fa-minus-square"></i> <b>REMOVE</b></span></a>
                                        </td>
                                    </tr>
                                <?php endif;
                            endforeach; ?>
                            <tr id="template" class="form-group-sm" hidden>
                                <td id="vaccine_name">
                                </td>
                                <td hidden>
                                    <input type="text" class="form-control vaccine" id="vaccine"
                                           name="vaccine" value=""
                                           disabled>
                                </td>
                                <td>
                                    <input type="text" class="form-control batch_no" id="batch_no"
                                           name="batch_no"
                                           disabled>
                                </td>
                                <td>
                                    <input type="text" class="form-control expiry_date" id="expiry_date"
                                           name="expiry"
                                           disabled>
                                </td>
                                <td>
                                    <input type="text" class="form-control stock_quantity" id="stock_quantity"
                                           name="stock_quantity" disabled>
                                </td>
                                <td>
                                    <input type="text" class="form-control order_quantity" id="order_quantity"
                                           name="order_quantity"
                                           disabled>
                                </td>
                                <td>
                                    <input type="text" class="form-control packing_quantity"
                                           id="packing_quantity">
                                </td>

                                <td class="small">
                                    <a class="add btn-xs" style="cursor: pointer; cursor: hand;"><span
                                                class="label label-success"><i
                                                    class="fa fa-plus-square"></i> <b>ADD</b></span></a><br>
                                    <a class="remove btn-xs" style="cursor: pointer; cursor: hand;"><span
                                                class="label label-danger"><i
                                                    class="fa fa-minus-square"></i> <b>REMOVE</b></span></a>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="box-footer">
                    <button type="submit" id="validate" class="btn bg-navy margin">
                        Submit
                    </button>
                </div>
            </div>
        </div>


        <div class="modal fade" id="submit-modal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button aria-label="Close" data-dismiss="modal" class="close" type="button">
                            <span aria-hidden="true">×</span></button>
                        <h4 class="modal-title">Confirm Form Submission</h4>
                    </div>
                    <div class="modal-body">
                        <p>Are you sure you want to submit the entered details?</p>
                    </div>
                    <div class="modal-footer">
                        <button data-dismiss="modal" class="btn btn-default pull-left" type="button">Close</button>
                        <button type="button" name="submit" id="submit" class="btn btn-sm btn-danger">
                            <i class="fa fa-paper-plane"></i>&nbsp;Submit
                        </button>

                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
    </div>
</div>
<?php echo form_close(); ?>
<script>
    $(document).ready(function () {

        <?php if($item_count != $new_item_count):?>
        swal({
            title: "Alert!",
            text: "You seem to have insufficient stocks. Packing will be limited to available stock in your store.",
            type: "info",
            showCancelButton: false,
            confirmButtonColor: "#54CEF7",
            confirmButtonText: "Okay",
            closeOnConfirm: true,
        });
        <?php endif;?>

        $('#pack')
            .formValidation({
                framework: 'bootstrap',
                err: {
                    container: 'tooltip'
                },
                row: {
                    selector: 'td'
                },
                // This option will not ignore invisible fields which belong to inactive panels
                excluded: ':hidden, :not(:visible)',
                fields: {
                    packing_date: {
                        row: '.form-group-sm',
                        validators: {
                            notEmpty: {
                                message: '*required'
                            },
                            date: {
                                format: 'YYYY-MM-DD',
                                message: '*date is not valid',
                                max: moment().format('YYYY-MM-DD'),
                                min: moment('2016-06-01').format('YYYY-MM-DD')
                            }
                        }
                    },
                    batch_no: {
                        validators: {
                            notEmpty: {
                                message: '*required'
                            }
                        }
                    },
                    expiry_date: {
                        validators: {
                            notEmpty: {
                                message: '*required'
                            },
                            date: {
                                format: 'YYYY-MM-DD',
                                message: '*date is not valid'
                            }
                        }
                    },
                    <?php foreach ($items as $key => $value):
                    if ($value->transaction_quantity >= 0):?>
                    'stock_quantity[<?php echo $value->vaccine_id; ?>]': {
                        validators: {
                            notEmpty: {
                                message: '*required'
                            },
                            numeric: {
                                message: '*not a number'
                            },
                            greaterThan: {
                                value: 0,
                                message: '*not a valid number'
                            }
                        }
                    },
                    'packing_quantity[<?php echo $value->vaccine_id; ?>]': {
                        validators: {
                            notEmpty: {
                                message: '*required'
                            },
                            numeric: {
                                message: '*not a number'
                            },
                            between: {
                                min: 0,
                                max: 'stock_quantity[<?php echo $value->vaccine_id; ?>]',
                                message: '*not a valid number'
                            }
                        }
                    },
                    <?php endif;
                    endforeach; ?>
                }
            }).on('click', '#table .add', function () {
            var thisRow = $(this).closest('tr').attr('id');
            addRow(thisRow);

        }).on('click', '#table .remove', function () {
            if ($('#table tbody tr').length === 1) return;
            $(this).parents("tr").fadeOut('slow', function () {
                $(this).remove();
            });

        }).on('change', '#batch_no', function () {
            var row = $(this).closest('tr').attr('id');
            var batch = $(this).val();
            load_batch_details(batch, row);

        }).on('success.form.fv', function (e) {
            // Prevent form submission
            e.preventDefault();
            swal({
                    title: "Confirm Submission",
                    text: "Are you sure you want to submit the entered details?",
                    type: "info",
                    showCancelButton: true,
                    confirmButtonColor: "#54CEF7",
                    confirmButtonText: "Submit",
                    closeOnConfirm: false,
                    showLoaderOnConfirm: true,
                },
                function (isConfirm) {
                    if (isConfirm) {
                        submit();
                    }
                });
        });

        var array = new Array();
        $('#table tr').each(function (row, tr) {
            if (tr.id !== 'template') {
                array[row] = {
                    "vaccine": $(tr).find('#vaccine').val()
                };
            }
        });
        load_batches(array);

        function load_batches(vaccine) {

            var request = $.ajax({
                url: '<?php echo site_url('order/packing_batch'); ?>',
                type: 'post',
                data: {'vaccine': vaccine},

            });

            request.done(function (data) {
                data = JSON.parse(data);
                <?php foreach ($items as $key => $value):
                if ($value->transaction_quantity >= 0):?>

                var dropdown_start = '<select class="form-control" id="batch_no" name="batch_no">';
                var dropdown_option = new Array();
                var default_option = "<option value=''>" + "Select batch" + "</option> ";
                var dropdown_end = '</select>';
                dropdown_option.push(default_option);

                var current_row = $('#row<?php echo $value->vaccine_id; ?>');
                fv = $('#pack').data('formValidation');
                if ($.type(data.row<?php echo $value->vaccine_id; ?>) !== 'undefined') {
                    $.each(data.row<?php echo $value->vaccine_id; ?>, function (key, value) {

                        option = "<option value='" + value.batch_no + "'>" + value.batch_no + ' : ' + value.balance + "</option> ";
                        dropdown_option.push(option);
                    });
                    current_row.closest("tr").find("#batch_no").replaceWith(dropdown_start + dropdown_option.join("") + dropdown_end);
                }
                <?php endif;
                endforeach;?>
            });
            request.fail(function (jqXHR, textStatus) {

            });

        }

        function submit() {
            var vaccine_count = 0;
            $.each($(".vaccine"), function (i, v) {
                vaccine_count++;
            });
            vaccine_count = vaccine_count - 1;

            var packing_date = retrieveFormValues('packing_date');

            var vaccines = retrieveFormValues_Array('vaccine');
            var batch_no = retrieveFormValues_Array('batch_no');
            var expiry_date = retrieveFormValues_Array('expiry_date');
            var order_quantity = retrieveFormValues_Array('order_quantity');
            var packing_quantity = retrieveFormValuesArray_Array('packing_quantity');

            var dat = new Array();

            for (var i = 0; i < vaccine_count; i++) {
                var data = new Array();
                var get_vaccine = vaccines[i];
                var get_batch = batch_no[i];
                var get_expiry = expiry_date[i];
                var get_order_quantity = order_quantity[i];
                var get_packing_quantity = packing_quantity[i];

                data = {
                    "vaccine_id": get_vaccine,
                    "batch_no": get_batch,
                    "expiry_date": get_expiry,
                    "order_quantity": get_order_quantity,
                    "packing_quantity": get_packing_quantity,
                };
                dat.push(data);
            }

            batch = JSON.stringify(dat);
            $.ajax({
                url: $('#pack').attr('action'),
                type: "POST",
                data: {
                    "id": '<?php echo $id; ?>',
                    "packing_date": packing_date,
                    "batch": batch
                },
                beforeSend: function () {
                    $('#validate').fadeOut(300, function () {
                        $(this).remove();
                    });
                },

                success: function (data, textStatus, jqXHR) {
                    window.location.replace('<?php echo site_url('order');?>');
                },

                error: function (jqXHR, textStatus, errorThrown) {
                    //if fails
                }
            });
        }

        $('#packing_date')
            .datepicker({
                dateFormat: "yy-mm-dd",
                maxDate: 0,
                beforeShow: function (textbox, instance) {
                    var txtBoxOffset = $(this).offset();
                    var top = txtBoxOffset.top;
                    var left = txtBoxOffset.left;
                    var textBoxWidth = $(this).outerWidth();
                    setTimeout(function () {
                        instance.dpDiv.css({
                            top: top - 100, //you can adjust this value accordingly
                            left: left + textBoxWidth//show at the end of textBox
                        });
                    }, 0);

                },
                onSelect: function (date, inst) {
                    /* Revalidate the field when choosing it from the datepicker */
                    $('#pack').formValidation('revalidateField', 'packing_date');
                }
            }).datepicker("setDate", new Date());


        function load_batch_details(batch, row) {
            var current_row = ('#' + row);
            if (batch.length > 0) {
                var request = $.ajax({
                    url: '<?php echo site_url('stock/batch_detail'); ?>',
                    type: 'post',
                    data: {
                        "batch": batch
                    },

                });

                request.done(function (data) {
                    data = JSON.parse(data);
                    $.each(data, function (key, value) {
                        var expiry = $(current_row + " .expiry_date").val(value.expiry_date);
                        var stock = $(current_row + " #stock_quantity").val(value.balance);
                        $('#pack').formValidation('revalidateField', expiry);
                        $('#pack').formValidation('revalidateField', stock);

                    });
                });
                request.fail(function (jqXHR, textStatus) {

                });
                return;
            } else {
                current_row.closest('tr').find("#expiry_date").val('');
                current_row.closest('tr').find("#stock_quantity").val('');
                return;
            }
        }

        var counter = 20;

        function addRow(thisRow) {

            var template = $('#template');
            var parent = $('#' + thisRow).closest('tr');
            var cloned_object = template.clone().removeAttr('hidden').removeAttr('id');
            var row_id = parseInt(thisRow.slice(3)) + counter;
            var new_row = cloned_object.attr('id', 'row' + row_id);
            new_row.find('td').removeClass('has-success').find('input').val('');
            console.log('click');
            new_row.insertAfter(parent);

            var parent_vaccine_id = parent.find('[name="vaccine"]').val();
            new_row.find('[name="vaccine"]').val(parent_vaccine_id);
            var parent_vaccine_name = parent.find('[id="vaccine_name"]').text();
            new_row.find('[id="vaccine_name"]').text(parent_vaccine_name);
            var parent_batch = parent.find('[id="batch_no"]').clone();
            var batch_no = new_row.find('[id="batch_no"]').replaceWith(parent_batch);
            var expiry_date = new_row.find('[name="expiry"]').attr('name', 'expiry_date');
            var stock_quantity = new_row.find('[id="stock_quantity"]').attr('name', 'stock_quantity[' + row_id + ']');
            var parent_order_quantity = parent.find('[id="order_quantity"]').val();
            new_row.find('[id="order_quantity"]').val(parent_order_quantity);
            var packing_quantity = new_row.find('[id="packing_quantity"]').attr('name', 'packing_quantity[' + row_id + ']');

            $('#pack')
                .formValidation('addField', expiry_date);
            //     .formValidation('addField', stock_quantity);
            //     // .formValidation('resetField', packing_quantity)
            // .formValidation('addField', packing_quantity);


            new_row.slideDown();
            counter = counter + 1;
        }

        function retrieveFormValues(name) {
            var dump;
            $.each($("input[name=" + name + "], select[name=" + name + "]"), function (i, v) {
                var theTag = v.tagName;
                var theElement = $(v);
                var theValue = theElement.val();
                dump = theValue;
            });
            return dump;
        }

        function retrieveFormValues_Array(name) {
            var dump = new Array();
            var counter = 0;
            $.each($("input[name=" + name + "], select[name=" + name + "]"), function (i, v) {
                var theTag = v.tagName;
                var theElement = $(v);
                var theValue = theElement.val();
                dump[counter] = theValue;

                counter++;
            });
            return dump;
        }

        function retrieveFormValuesArray_Array(name) {
            var dump = new Array();
            var counter = 0;
            $.each($("input[id=" + name + "]"), function (i, v) {
                var theTag = v.tagName;
                var theElement = $(v);
                var theValue = theElement.val();
                dump[counter] = theValue;

                counter++;
            });
            return dump;
        }


    });
</script>
