<!-- Main content -->
<section class="invoice">
    <!-- Table row -->
    <div class="row">
        <div class="col-xs-12 table-responsive">
            <table class="table table-striped table-condensed table-hover">
                <thead>
                <tr>
                    <th>Vaccine/Diluents</th>
                    <th>Batch</th>
                    <th>Expiry Date</th>
                    <th>Quantity Ordered</th>
                    <th>Quantity Allocated</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($items as $key => $value): ?>
                    <tr>
                        <td><?php echo $value->vaccine_name; ?></td>
                        <td><?php echo $value->batch; ?></td>
                        <td><?php echo ($value->expiry_date == '0000-00-00') ? '' : $value->expiry_date; ?></td>
                        <td><?php echo $value->order_quantity; ?></td>
                        <td><?php echo $value->packing_quantity; ?></td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->

    <hr>
    <!-- this row will not appear when printing -->
    <div class="row no-print">
        <div class="col-xs-12">
            <div class="col-sm-3">
                <a href="<?php echo site_url('order/print_packing_list/' . $id); ?>" target="_blank"
                   class="btn btn-default"><i
                            class="fa fa-print"></i> Print Packing List </a>
            </div>
            <?php if ($packing->location_id == $user_info->location_id): ?>

                <a href="<?php echo site_url('order/issue/' . $id); ?>" class="btn bg-navy pull-right">Issue
                </a>

            <?php endif; ?>
        </div>
        <div class="col-sm-5 pull-Left" style="padding-top: 10px">
            <p>Please print <strong>2 Copies of the Packing List</strong></p>
        </div>
    </div>
</section>
<!-- /.content -->