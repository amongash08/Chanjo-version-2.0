<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo $title; ?></title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/ionicons/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/AdminLTE.min.css">
</head>
<body onload="window.print();">
<style>
    @media print {
        a[href]:after {
            display: none;
            visibility: hidden;
        }
    }
</style>
<!-- Main content -->
<section class="invoice">
    <!-- title row -->
    <div class="row">
        <div class="col-sm-12">

            <img class="pull-left" width="90" height="75"
                 src="<?php echo base_url() ?>assets/images/logo.png">
            <div class="col-sm-7">
                <div class="margin">
                    <p style="padding-top:14px;font-size:1.6em;font-weight:600;margin:auto">
                        Ministry of Health.
                    </p>
                    <p style="font-size:1.2em;font-weight:600">
                        National Vaccines Depot - Kitengela
                    </p>
                </div>

            </div>
        </div>
        <!-- /.col -->
    </div>
    <hr>
    <!-- info row -->
    <div class="row invoice-info">
        <div class="col-sm-4 invoice-col">
            <b> From</b>
            <address>
                <i><?php echo ($user_info->level_id == 1) ? $user_details['to'] : $user_details['from']; ?></i><br>
                <i><?php echo $user_details['name']; ?></i><br>
                <i><?php echo $user_details['phone']; ?></i><br>
                <i><?php echo $user_details['email']; ?></i><br>
            </address>
        </div>
        <!-- /.col -->
        <div class="col-sm-4 invoice-col">
            <b>To</b>
            <address>
                <i><?php echo ($user_info->level_id == 1) ? $user_details['from'] : $user_details['to']; ?></i><br>
            </address>
        </div>
        <!-- /.col -->
        <div class="col-sm-4 invoice-col">
            <b>Order Number:</b> <i><?php echo $location; ?>-<?php echo $details->request_id; ?></i><br>
            <b>Issue Date:</b> <i><?php echo date('Y-m-d', strtotime($details->transaction_date)); ?></i><br>
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
    <hr>
    <!-- Table row -->
    <div class="row">
        <div class="col-xs-12 table-responsive">
            <table class="table table-striped table-condensed table-hover">
                <thead>
                <tr>
                    <th>Vaccine/Diluents</th>
                    <th>Batch</th>
                    <th>Expiry Date</th>
                    <th>Quantity Ordered</th>
                    <th>Quantity Issued</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($items as $key => $value): ?>
                    <tr>
                        <td><?php echo $value->vaccine_name; ?></td>
                        <td><?php echo $value->batch; ?></td>
                        <td><?php echo ($value->expiry_date == '0000-00-00') ? '' : $value->expiry_date; ?></td>
                        <td><?php echo $value->order_quantity; ?></td>
                        <td><?php echo $value->issue_quantity; ?></td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>

            <table class="table table-striped table-condensed table-hover">
                <tr>
                    <td>Issued By</td>
                    <td>Designation</td>
                    <td>Signature</td>
                </tr>
                <tr>
                    <td>Received By</td>
                    <td>Designation</td>
                    <td>Signature</td>
                </tr>
            </table>
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</body>
</html>