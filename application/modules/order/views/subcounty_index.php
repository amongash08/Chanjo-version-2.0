<!--<div class="row">-->
<!--    <div class="box-body">-->
<!--        <div class="col-sm-12">-->
<!--            <a href="--><?php //echo site_url('order/create'); ?><!--" class="btn btn-primary state_change">Create Order</a>-->
<!--        </div>-->
<!--    </div>-->
<!--</div>-->
<div class="row">
    <div class="col-sm-12">
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">My Orders</h3>

                <div class="box-tools pull-right">
                    <button data-widget="collapse" class="btn btn-box-tool" type="button"><i class="fa fa-minus"></i>
                    </button>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body" style="height: 300px">
                <div class="table-responsive">
                    <table class="table no-margin">
                        <thead>
                        <tr>
                            <th>Order Number</th>
                            <th>Date</th>
                            <th>Collection Date</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php if ($my_orders):
                            foreach ($my_orders as $key => $value):
                                if ($value->status != 'received'):
                                    if ($key == 4) break; ?>
                                    <tr>
                                    <td>
                                        <?php echo $value->id; ?>
                                    </td>
                                    <td><?php echo date('Y-m-d', strtotime($value->pickup_date)); ?></td>
                                    <td><?php echo date('Y-m-d', strtotime($value->timestamp)); ?></td>
                                    <?php if ($value->status == 'pending' || $value->status == 'packing'): ?>
                                    <td><span class="label label-danger">Pending</span></td>
                                    <td><a href="<?php echo site_url('order/order_sheet/' . $value->id); ?>"
                                           class="btn btn-primary state-change btn-xs">View</a></td>
                                <?php elseif ($value->status == 'issued'): ?>
                                    <td><span class="label label-success">Issued</span></td>
                                    <td><a href="<?php echo site_url('order/receive/' . $value->id); ?>"
                                           class="btn btn-primary state-change btn-xs">Receive</a></td>
                                    </tr>
                                <?php endif;
                                endif;
                            endforeach;
                        endif; ?>
                        </tbody>
                    </table>
                </div>
                <!-- /.table-responsive -->
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix">
                <a class="btn btn-sm btn-primary btn-flat pull-left" href="<?php echo site_url('order/create'); ?>">Place New Order</a>
            </div>
            <!-- /.box-footer -->
        </div>
    </div>

</div>