<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {

        $data['title'] = 'Home';
        $data['page_title'] = 'Home';
        $data['subtitle'] = 'Dashboard';
        $data['module'] = 'region';
        $data['view_file'] = 'home/dashboard';
        $data['sidebar']='region_sidebar';

        echo Modules::run('templates', $data);

    }
}
