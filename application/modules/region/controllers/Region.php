<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Region extends MX_Controller
{
    function __construct()
    {
        parent::__construct();
        if (!$this->ion_auth->logged_in()) {
            //redirect them to the login page
            redirect('users');
        }
        $this->load->model('mdl_region');

    }

    public function index()
    {
        $data['title'] = 'View Regions';
        $data['page_title'] = 'Region';
        $data['subtitle'] = 'View Regions';
        $data['module'] = 'region';
        $data['view_file'] = 'region/index';

        echo Modules::run('templates', $data);
    }

    function edit($id = null)
    {
        $data['title'] = "Edit Region";
        $data['subtitle'] = 'Edit Region';
        $data['module'] = 'region';
        $data['view_file'] = 'region/edit';
        $data['id'] = (is_numeric($id)) ? $id : null;

        if (!$this->ion_auth->logged_in() || (!$this->ion_auth->is_admin())) {
            redirect('users', 'refresh');
        }

        $selected_region = $this->mdl_region->get($id);

        //validate form input
        $this->form_validation->set_rules('women_population', 'Women Population', 'required');
        $this->form_validation->set_rules('under_one_population', 'Under One Population', 'required');
        $this->form_validation->set_rules('total_population', 'Total Population', 'required');

        if (isset($_POST) && !empty($_POST)) {
            // do we have a valid request?
            if ($id !== $this->input->post('id')) {
                $this->session->set_flashdata('error_message', 'An error has occurred.');
                redirect('region', 'refresh');
            }

            if ($this->form_validation->run() === TRUE) {

                $data = array(
                    'women_population' => $this->input->post('women_population'),
                    'under_one_population' => $this->input->post('under_one_population'),
                    'total_population' => $this->input->post('total_population'),
                );


                //check to see if we are updating the user
                if ($this->mdl_region->update($selected_region->id, $data, TRUE)) {

                    //redirect them back to the admin page if admin, or to the base url if non admin
                    $this->session->set_flashdata('success_message', 'Region updated successfully.');
                    if ($this->ion_auth->is_admin()) {
                        redirect('region', 'refresh');
                    } else {
                        redirect('/', 'refresh');
                    }

                } else {
                    //redirect them back to the admin page if admin, or to the base url if non admin
                    $this->session->set_flashdata('error_message', 'An error has occurred.');
                    if ($this->ion_auth->is_admin()) {
                        redirect('region', 'refresh');
                    } else {
                        redirect('/', 'refresh');
                    }

                }

            }
        }

        //pass the region to the view
        $data['selected_region'] = $selected_region;

        if (!is_null($selected_region)) {
            $data['region_name'] = array(
                'name' => 'region_name',
                'id' => 'region_name',
                'type' => 'text',
                'class' => 'form-control',
                'disabled' => '',
                'value' => $this->form_validation->set_value('region_name', $selected_region->region_name),
            );
            $data['women_population'] = array(
                'name' => 'women_population',
                'id' => 'women_population',
                'type' => 'text',
                'class' => 'form-control',
                'value' => $this->form_validation->set_value('women_population', $selected_region->women_population),
            );
            $data['under_one_population'] = array(
                'name' => 'under_one_population',
                'id' => 'under_one_population',
                'type' => 'text',
                'class' => 'form-control',
                'value' => $this->form_validation->set_value('under_one_population', $selected_region->under_one_population),
            );
            $data['total_population'] = array(
                'name' => 'total_population',
                'id' => 'total_population',
                'type' => 'text',
                'class' => 'form-control',
                'value' => $this->form_validation->set_value('total_population', $selected_region->total_population),
            );
        }

        echo Modules::run('templates', $data);
    }


    public function region_data()
    {
        //retrieve region data array
        $regions = $this->mdl_region->get_all();
        $output['data'] = $regions;
        echo json_encode($output);
    }

    public function region_dropdown()
    {
        $regions = $this->mdl_region->region_location();
        asort($regions);
        return $regions;
    }

}