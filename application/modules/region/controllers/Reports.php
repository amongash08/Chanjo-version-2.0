<?php

/**
 * Created by PhpStorm.
 * User: mureithi
 * Date: 6/14/17
 * Time: 11:35 AM
 */
class Reports extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function supply_chain()
    {

        $data['module'] = 'region';
        $data['sidebar']='region_sidebar';
        $data['view_file'] = 'reports/supply_chain';
        $data['main_title'] = 'Reports | Supply Chain';
        $data['subtitle'] = 'Reports';
        $this->load->model('region/mdl_region');


//        $userobj = $this->get_user_object();
//        $station_id=$userobj['user_station_id'];

//        $query = $this->mdl_region->get_all();
//        $data['regions'] = $query;
//        echo '<pre>',print_r($query),'</pre>';exit;



        echo Modules::run('templates', $data);

    }

    public function cold_chain()
    {
        $data['module'] = 'region';
        $data['sidebar']='region_sidebar';
        $data['view_file'] = 'reports/cold_chain';
        $data['main_title'] = 'Reports | Cold Chain';
        $data['subtitle'] = 'Reports';
//        $this->load->model('region/mdl_region');
//        $this->load->model('users/mdl_user_levels');
//        $data['user_levels'] = json_decode(json_encode($this->mdl_user_levels->get_all()), true);
//        $data['regions'] = json_decode(json_encode($this->mdl_region->get_all()), true);
//
//        $userobj = $this->get_user_object();
//        $station_id=$userobj['user_station_id'];

        //echo '<pre>',print_r($query),'</pre>';exit;

        echo Modules::run('templates',$data);

    }

    public function program_management()
    {
        $data['module'] = 'region';
        $data['sidebar']='region_sidebar';
        $data['view_file'] = 'reports/program_management';
        $data['main_title'] = 'Reports | Program management';
        $data['subtitle'] = 'Reports';
//        $this->load->model('region/mdl_region');
//        $this->load->model('users/mdl_user_levels');
//        $data['user_levels'] = json_decode(json_encode($this->mdl_user_levels->get_all()), true);
//        $data['regions'] = json_decode(json_encode($this->mdl_region->get_all()), true);
//
//        $userobj = $this->get_user_object();
//        $station_id=$userobj['user_station_id'];

        //echo '<pre>',print_r($query),'</pre>';exit;

        echo Modules::run('templates',$data);

    }

    public function general_admin()
    {

        $data['module'] = 'region';
        $data['sidebar']='region_sidebar';
        $data['view_file'] = 'reports/general_admin';
        $data['main_title'] = 'Reports | General administration';
//        $this->load->model('users/mdl_user_levels');
//        $this->load->model('region/mdl_region');
//        $data['user_levels'] = json_decode(json_encode($this->mdl_user_levels->get_all()), true);
//        $data['regions'] = json_decode(json_encode($this->mdl_region->get_all()), true);
//        $this->load->model('county/mdl_county');
//        $query = $this->mdl_county->get_all();
//        $data['counties'] = $query;
//
//        $userobj = $this->get_user_object();
//        $station_id=$userobj['user_station_id'];

        //echo '<pre>',print_r($query),'</pre>';exit;

        echo Modules::run('templates',$data);

    }

}