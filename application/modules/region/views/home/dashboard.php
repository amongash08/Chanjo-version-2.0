<script src="//cdnjs.cloudflare.com/ajax/libs/numeral.js/2.0.6/numeral.min.js"></script>

<div class="row">

    <section class="col-lg-6">

        <!-- stock levels box -->
        <div class="box box-solid ">
            <div class="box-header with-border">
                <i class="fa fa-calendar"></i>

                <h3 class="box-title">
                    Stock Levels In Doses
                </h3>

            </div>

            <div class="box-body">

                <div class="chart">
                    <div id="doses" ></div>
                </div>


            </div>


        </div>
        <!-- /.box -->

    </section>

    <section class="col-lg-6">

        <!-- stock levels box -->
        <div class="box box-solid ">
            <div class="box-header with-border">
                <i class="fa fa-calendar"></i>

                <h3 class="box-title">
                    Stock Levels In MOS
                </h3>

            </div>

            <div class="box-body">

                <div class="chart">
                    <div id="mos" ></div>
                </div>


            </div>


        </div>
        <!-- /.box -->

    </section>




</div>

<div class="row">
    <section class="col-lg-12">

        <!-- stock levels box -->
        <div class="box box-solid ">
            <div class="box-header with-border">
                <i class="fa fa-clock-o"></i>

                <h3 class="box-title">
                    Stocks Last Updated
                </h3>


            </div>
            <div class="box-body">

                <div class="row">

                </div>

                <div class="chart">
                    <div id="stocks" ></div>
                </div>


            </div>


        </div>
        <!-- /.box -->
<!--        <div class="box box-solid ">-->
<!--            <div class="box-header with-border">-->
<!--                <i class="fa fa-podcast"></i>-->
<!---->
<!--                <h3 class="box-title">-->
<!--                    Equipment-->
<!--                </h3>-->
<!---->
<!--                <a class="btn btn-xs bg-olive btn-flat margin pull-right">-->
<!---->
<!--                    <i class="fa fa-thermometer-full"></i> Add Fridge-->
<!--                </a>-->
<!---->
<!--            </div>-->
<!---->
<!--            <div class="box-body">-->
<!---->
<!--                <div class="chart">-->
<!--                    <div class="table-responsive" id="equipment">-->
<!--                    </div>-->
<!--                </div>-->
<!---->
<!---->
<!--            </div>-->
<!---->
<!---->
<!--        </div>-->
        <!-- /.box -->

    </section>

<!--    <section class="col-md-6">-->
<!---->
<!--        <div class="box box-solid ">-->
<!--            <div class="box-header with-border">-->
<!--                <i class="fa fa-plus-square-o"></i>-->
<!---->
<!--                <h3 class="box-title">-->
<!--                    Positive Cold Chain-->
<!--                </h3>-->
<!---->
<!--            </div>-->
<!---->
<!--            <div class="box-body">-->
<!---->
<!--                <div class="chart">-->
<!--                    <div id="positive" ></div>-->
<!--                </div>-->
<!---->
<!---->
<!--            </div>-->
<!---->
<!---->
<!--        </div>-->
<!--        <!-- /.box -->
<!---->
<!--    </section>-->


<!--    <section class="col-md-6">-->
<!---->
<!--            <div class="box box-solid ">-->
<!--                <div class="box-header with-border">-->
<!--                    <i class="fa fa-minus-square-o"></i>-->
<!---->
<!--                    <h3 class="box-title">-->
<!--                        Negative Cold Chain-->
<!--                    </h3>-->
<!---->
<!---->
<!---->
<!--                </div>-->
<!---->
<!--                <div class="box-body">-->
<!---->
<!--                    <div class="chart">-->
<!--                        <div id="negative" ></div>-->
<!--                    </div>-->
<!---->
<!---->
<!--                </div>-->
<!---->
<!---->
<!--            </div>-->
<!---->
<!---->
<!---->
<!---->
<!---->
<!--    </section>-->


</div>




    <script>

        var url="<?php echo base_url(); ?>";

        ajax_fill_data('stock/plot_stock_location/NULL/column',"#doses");
        ajax_fill_data('stock/plot_stock_mos_location/NULL/NULL/column',"#mos");
        ajax_fill_data('stock/stock_last_updated',"#stocks");
//        ajax_fill_data('dashboard/positiveColdchain/NULL/NULL',"#positive");
//        ajax_fill_data('dashboard/negativeColdchain/NULL/NULL',"#negative");
//        ajax_fill_data('store/get_fridges',"#equipment");




        function ajax_fill_data(function_url,div){
            var function_url =url+function_url;
            var loading_icon=url+"assets/images/ring.gif";
            $.ajax({
                type: "POST",
                url: function_url,
                beforeSend: function() {
                    $(div).html("<img style='margin:10% 50% 10% ;' src="+loading_icon+">");
                },
                success: function(msg) {
                    $(div).html(msg);
                }
            });
        }


    </script>