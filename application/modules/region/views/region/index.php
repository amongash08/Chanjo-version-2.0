<div class="box">
    <div class="box-header">
        <h3 class="box-data">Below is a list of regions</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <table id="regions" class="table table-bordered table-striped" cellspacing="0" width="100%">
            <thead>
            <tr>
                <th>Region Name</th>
                <th>Population</th>
                <th>Population One</th>
                <th>Women Population</th>
                <th align="center">Action</th>
            </tr>
            </thead>
            <tbody>

            </tbody>
            <tfoot>
            <tr>
                <th>Region Name</th>
                <th>Population</th>
                <th>Population One</th>
                <th>Women Population</th>
                <th align="center">Action</th>
            </tr>
            </tfoot>
        </table>
    </div>
    <!-- /.box-body -->

</div>
<!-- /.box -->
<script type="text/javascript">
    $(document).ready(function () {
        $("#regions").DataTable({
            serverSide: false,
            ajax: {
                url: "/region/region_data",
                type: "POST"
            },
            columns: [
                {data: "region_name"},
                {data: "women_population"},
                {data: "under_one_population"},
                {data: "total_population"},
                {
                    data: "id",
                    render: function (data, type, full) {
                        return '<a href="<?php echo base_url();?>region/edit/' + data + '" class="btn btn-primary btn-xs">Edit</a>';
                    }

                }
            ]
        });

    });

</script>

<!-- DataTables -->
<script src="<?php echo base_url() ?>assets/plugins/datatables/dataTables.min.js"></script>
<script src="<?php echo base_url() ?>assets/plugins/datatables/Bootstrap-3.3.7/js/bootstrap.min.js"></script>
