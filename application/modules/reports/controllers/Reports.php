<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reports extends MX_Controller
{
    function __construct()
    {
        parent::__construct();
        if (!$this->ion_auth->logged_in()) {
            //redirect them to the login page
            redirect('users');
        }
        $this->load->module('users');
        $this->load->module('vaccine');
        $this->load->module('region');
        $this->load->module('county');
        $this->load->module('subcounty');
        $this->load->module('facility');
        $this->load->model('mdl_reports');
        $this->load->model('stock/mdl_stock_ext');
        $this->load->model('store/mdl_store');

    }


    public function current_coverage($level = 'NULL', $location = 'NULL')
    {
        $maxdate = date('Y-m-d');
        $mindate = new DateTime(date('Y-m-d'));
        $interval = new DateInterval('P1M');
        $mindate = $mindate->sub($interval)->format('Y-m-d');
        $user_info = $this->users->user_info();
        if ($location == 'NULL') {

            $user_info = $this->users->user_info();
            $location = $user_info->location_id;
        }

        if ($level == 'NULL') {
            $level = $user_info->level_id;
        }

        //echo '<pre>',print_r($level),'</pre>';exit;


        if ($level == 1) {
//            $station=json_decode($user_info->json_location)->region;
//            $query_population = $this->mdl_region->get_population($station);
            $population = 1800000;
            $query = $this->mdl_reports->get_national_coverage($maxdate, $mindate);


        } elseif ($level == 2) {
            $station = json_decode($user_info->json_location)->region;
            $query_population = $this->mdl_region->get_population($station);
            $population = $query_population[0]->population;
            $query = $this->mdl_reports->get_region_coverage($maxdate, $mindate, $station);
        } elseif ($level == 3) {
            $station = json_decode($user_info->json_location)->county;
            $query_population = $this->mdl_county->get_population($station);
            $population = $query_population[0]->population;
            $query = $this->mdl_reports->get_county_coverage($maxdate, $mindate, $station);
        } elseif ($level == 4) {
            $station = json_decode($user_info->json_location)->subcounty;
            $query_population = $this->mdl_subcounty->get_population($station);
            $population = $query_population[0]->population;
            $query = $this->mdl_reports->get_subcounty_coverage($maxdate, $mindate, $station);
        }

        //echo '<pre>',print_r($population),'</pre>';exit;


        $query = json_decode(json_encode($query), true);
        $category_data = [];
        $series_data = [];
        if ($query) {

            //$population = end($query[0]);
            array_pop($query[0]);
            array_shift($query[0]);

            $category_data = array_keys($query[0]);

            $old_series = array_values($query[0]);

            foreach ($old_series as $key => $value) {

                $series_data[] = (float)number_format((($value / $population) * 100), 2);

            }
        }

        $data['graph_type'] = 'column';
        $data['graph_title'] = '';
        $data['graph_yaxis_title'] = '';
        $data['graph_id'] = 'coverage';
        $data['legend'] = 'Doses';
        $data['colors'] = "['#008080','#6AF9C4']";
        $data['series_data'] = json_encode($series_data);
        $data['category_data'] = json_encode($category_data);

        if (count($series_data) == 0) {
            echo '<div style="margin:10%;font-size:3em;font-weight:400;"> No data for the selected filters. </div>';
            exit;
        } else {
            $this->load->view('templates/graphs/basic_bar', $data);
        }

    }

    public function plot_stock_level()
    {
        if ($this->input->post()) {
            $location = $this->input->post('location');
        } else {
            $user_info = $this->users->user_info();
            $location = $user_info->location_id;
        }

        $query = $this->mdl_stock_ext->get_my_balances($location);

        $new = json_decode(json_encode($query), true);
        $category_data = [];
        $series_data = [];
        $time_data = [];

        foreach ($new as $key => $value) {
            $category_data[] = $value['vaccine_name'];
            $series_data[] = (int)$value['balance'];
            $time_data[] = $value['timestamp'];
        }


        $data['graph_type'] = 'column';
        $data['graph_yaxis_title'] = 'Stock Balance (Doses)';
        $data['graph_id'] = 'Stock';
        $data['legend'] = 'Doses';
        $data['colors'] = "['#008080','#6AF9C4']";
        $data['series_data'] = json_encode($series_data);
        $data['category_data'] = json_encode($category_data);

        if (count($series_data) == 0) {
            echo '<div style="margin:5%;font-size:2em;font-weight:400;"> No data for the selected filters. </div>';
            exit;

        } else {
            $data['graph_title'] = 'Balance as of ' . date('d ,M Y', strtotime(max($time_data)));
            $this->load->view('templates/graphs/basic_bar', $data);
        }


    }

    public function table_stock_level($level = false)
    {
        if (is_numeric($level) && $this->input->post('level')) {
            $level = (int)$this->input->post('level');
            if ($level === 2) {
                $data['result'] = $this->mdl_reports->get_region_stock_level();
            } elseif ($level === 3) {
                $data['result'] = $this->mdl_reports->get_county_stock_level();
            } elseif ($level === 4) {
                $data['result'] = $this->mdl_reports->get_subcounty_stock_level();
            }

            $view = 'reports/balances_pivot_table';

            $this->load->view($view, $data);

        }else{
            echo '<div style="margin:5%;font-size:2em;font-weight:400;"> No data for the selected filters. </div>';
            exit;
        }


    }


    public function plot_fridge_capacity()
    {
        if ($this->input->post()) {
            $location = $this->input->post('location');
        } else {
            $user_info = $this->users->user_info();
            $location = $user_info->location_id;
        }

        $fridge_capacity = $this->mdl_store->get_store_capacity_by_station($location,$type='No');
        $freezer_capacity = $this->mdl_store->get_store_capacity_by_station($location,$type='Yes');
        $balances = $this->mdl_stock_ext->get_my_balances($location);

        $opvbalance=[];
        $sum_volume =0;
        $opv_volume =0;
        $fridge_volume =0;
        $freezer_volume =0;

        foreach ($balances as $key => $value){

            if(in_array("OPV", $value)){

                $opvbalance=$value;
                $opv_volume=($value['balance']*$value['vaccine_pck_vol'])/1000;

            }

            //remove diluents
            $pattern = '/Diluent/';
            preg_match($pattern, (string)$value['vaccine_name'], $matches, PREG_OFFSET_CAPTURE);

            if(count($matches)> 0){

                unset($balances[$key]);

            }
            $sum_volume+=($value['balance']*$value['vaccine_pck_vol'])/1000 ;



        }

        if(count($fridge_capacity)!=0){

            $fridge_volume = $fridge_capacity[0]->volume;
        }
        if(count($freezer_capacity)!=0){

            $freezer_volume = $freezer_capacity[0]->volume;
        }

        $vaccines_volume = $sum_volume-$opv_volume;

        $data['freezer_volume'] = json_encode( $freezer_volume);
        $data['vaccines_volume'] = json_encode($vaccines_volume);
        $data['fridge_volume'] = json_encode( $fridge_volume);
        $data['opv_volume'] = json_encode( $opv_volume);


//        echo '<pre>',print_r($freezer_volume),'</pre>';
//        echo '<pre>',print_r($opv_volume),'</pre>';exit;

        $this->load->view('cold_chain_visual', $data);


    }

    public function create_recipient_csv()
    {
        if ($this->input->post()) {
            $location = $this->input->post('location');
        } else {
            $user_info = $this->users->user_info();
            $location = $user_info->location_id;
        }

        //load the excel library
        $this->load->library('excel');

        //activate worksheet number 1
        $this->excel->setActiveSheetIndex(0);
        //name the worksheet
        $this->excel->getActiveSheet()->setTitle('recipient');

        // load database

        if ($user_info->level_id == 1) {
            $stores=$this->mdl_store->get_stores_in_national();
        } elseif ($user_info->level_id == 2) {
            $station=json_decode($user_info->json_location)->region;
            $stores=$this->mdl_store->get_stores_region($station);
        }

        $stores = json_decode(json_encode($stores), True);

        $excelheaders=array ('Recipient','Total Population','Pregnant Women','Live Births','Surviving Infants','Adolescent Girls');
        $exceldata=[];
//        echo '<pre>',print_r($stores),'</pre>';exit;

        foreach ($stores as $key =>$value){

            $exceldata[]= array ('Recipient'=>$value['name'],'Total Population'=>$value['catchment_population'],
                'Pregnant Women'=>$value['pop_pregnant_women'],'Live Births'=>$value['live_birth_population'],
                'Surviving Infants'=>$value['pop_surviving_infants'],'Adolescent Girls'=>$value['pop_adolescent_girls']);

        }

        //echo '<pre>',print_r($exceldata),'</pre>';exit;
        $this->excel->getActiveSheet()->getStyle('1')->getFont()->setSize(12);
        $this->excel->getActiveSheet()->getStyle('1')->getFont()->setBold(true);


        // read data to active sheet
        $this->excel->getActiveSheet()->fromArray(array_keys($exceldata[0]),NULL,'A1');
        $this->excel->getActiveSheet()->fromArray($exceldata,NULL,'A2');

        $styleArray = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            )
        );

        $this->excel->getDefaultStyle()->applyFromArray($styleArray);
        unset($styleArray);

        //autosize all cells

        foreach(range('A','Z') as $columnID) {
            $this->excel->getActiveSheet()->getColumnDimension($columnID)
                ->setAutoSize(true);
        }

        $filename='recipient-'.date('M d Y').'.xls'; //save our workbook as this file name

        header('Content-Type: application/vnd.ms-excel'); //mime type

        header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name

        header('Cache-Control: max-age=0'); //no cache

        //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        //if you want to save it as.XLSX Excel 2007 format

        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');

        //force user to download the Excel file without writing it to server's HD
        $objWriter->save('php://output');

       // echo '<pre>',print_r($stores),'</pre>';exit;


    }

    public function create_arrivals_csv()
    {
        if ($this->input->post()) {
            $location = $this->input->post('location');
        } else {
            $user_info = $this->users->user_info();
            $location = $user_info->location_id;
        }

        //load the excel library
        $this->load->library('excel');

        //activate worksheet number 1
        $this->excel->setActiveSheetIndex(0);
        //name the worksheet
        $this->excel->getActiveSheet()->setTitle('Arrivals');
        $type='Receive';

        $arrivals = $this->mdl_stock_ext->get_stock_by_type($location,$type);

        $arrivals = json_decode(json_encode($arrivals), True);
//        echo '<pre>',print_r($arrivals),'</pre>';exit;


        $exceldata=[];

        if(count($arrivals)!=0) {

            foreach ($arrivals as $key => $value) {

                $dateReceived = strtotime($value['date']);
                $dateReceived = PHPExcel_Shared_Date::PHPToExcel($dateReceived);
                $expiryDate = strtotime($value['expiry']);
                $expiryDate = PHPExcel_Shared_Date::PHPToExcel($expiryDate);

                $exceldata[] = array('Date Received' => $dateReceived, 'Type Received' => 'B/F',
                    'Type Product' => $value['product_type'], 'Vaccines/diluents & Supplies' => $value['alias'],
                    'Batch No' => $value['batch'], 'Date of Expiry' => $expiryDate, 'VVM Type' => $value['vvm_type'],
                    'Type_SIAs' => '', 'doses/vial or units/box' => $value['vaccine_presentation'],
                    'No. vials or boxes' => '', 'No. doses or units' => $value['quantity'],
                    'Manufacturer' => $value['manufacturer'], 'Origin' => $value['origin']);

            }

            //echo '<pre>',print_r($exceldata),'</pre>';exit;
            $this->excel->getActiveSheet()->getStyle('1')->getFont()->setSize(12);
            $this->excel->getActiveSheet()->getStyle('1')->getFont()->setBold(true);



            // read data to active sheet
            $this->excel->getActiveSheet()->fromArray(array_keys($exceldata[0]), NULL, 'A1');
            $this->excel->getActiveSheet()->fromArray($exceldata, NULL, 'A2');
            $this->excel->getActiveSheet()->getStyle('A')
                ->getNumberFormat()
                ->setFormatCode('dd/mm/yy');
            $this->excel->getActiveSheet()->getStyle('F')
                ->getNumberFormat()
                ->setFormatCode('dd/mm/yy');

            $styleArray = array(
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN
                    )
                )
            );

            $this->excel->getDefaultStyle()->applyFromArray($styleArray);
            unset($styleArray);

            //autosize all cells

            foreach (range('A', 'Z') as $columnID) {
                $this->excel->getActiveSheet()->getColumnDimension($columnID)
                    ->setAutoSize(true);
            }

//            $highestRow = count($exceldata);
//
//            for ($row = 0; $row <= $highestRow; ++$row) {
//                // Fetch the data of the columns you need
//                $col1[] = $this->excel->getActiveSheet()->getStyle('A');
//
//
//
//
//                echo '<pre>',print_r($exceldata),'</pre>';exit;
//            }
//            exit;


        }

        $filename='arrivals-'.date('M d Y').'.xls'; //save our workbook as this file name

        header('Content-Type: application/vnd.ms-excel'); //mime type

        header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name

        header('Cache-Control: max-age=0'); //no cache

        //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        //if you want to save it as.XLSX Excel 2007 format

        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');

        //force user to download the Excel file without writing it to server's HD
        $objWriter->save('php://output');

        // load database

//         echo '<pre>',print_r($x),'</pre>';exit;


    }

    public function create_issues_csv()
    {
        if ($this->input->post()) {
            $location = $this->input->post('location');
        } else {
            $user_info = $this->users->user_info();
            $location = $user_info->location_id;
        }

        //load the excel library
        $this->load->library('excel');

        //activate worksheet number 1
        $this->excel->setActiveSheetIndex(0);
        //name the worksheet
        $this->excel->getActiveSheet()->setTitle('Issued');
        $type='Issue';

        // $issues = $this->mdl_stock_ext->get_stock_by_type($location,$type);
        //
        // $issues = json_decode(json_encode($issues), True);

        if ($user_info->level_id == 1) {
          $issues = $this->mdl_stock_ext->get_stock_by_type($location,$type);
          $issues = json_decode(json_encode($issues), True);

        } elseif ($user_info->level_id == 2) {

            $issues = $this->mdl_stock_ext->get_stock_by_type_region($location,$type);
            $issues = json_decode(json_encode($issues), True);
        }
          //echo '<pre>',print_r($issues),'</pre>';exit;



        $exceldata=[];

        if(count($issues)!=0){
            foreach ($issues as $key =>$value){

                $jsonlocation = json_decode($value['json_location'], true);

                if ($user_info->level_id == 1) {
                    $region=json_decode(json_encode($this->mdl_region->get_by_id($jsonlocation['region'])));
                    $recepient_name=$region[0]->region_name;

                } elseif ($user_info->level_id == 2) {
                    $subcounty=$this->mdl_subcounty->get_by_id($jsonlocation['subcounty']);
                    $recepient_name=$subcounty[0]->subcounty_name;

                }


//                echo '<pre>',print_r($recepient_name),'</pre>';exit;
                $dateissued = strtotime($value['date']);
                $dateissued = PHPExcel_Shared_Date::PHPToExcel($dateissued);
                $expiryDate = strtotime($value['expiry']);
                $expiryDate = PHPExcel_Shared_Date::PHPToExcel($expiryDate);

                $exceldata[]= array ('Date Issued'=>$dateissued,'Type_Issue'=>'Requisition',
                    'Main Allocation'=>$recepient_name ,'Commentaries'=>'','Voucher No'=>$value['transaction_voucher'],
                    'Type_SIAs'=>'','Type Product'=>$value['product_type'],'Vaccines/diluents & Supplies'=>$value['alias'],
                    'Date of Expiry'=>$expiryDate,'Batch No'=>$value['batch'],'Qty_Issued'=>$value['quantity']);

            }

        // echo '<pre>',print_r($exceldata),'</pre>';exit;
        $this->excel->getActiveSheet()->getStyle('1')->getFont()->setSize(12);
        $this->excel->getActiveSheet()->getStyle('1')->getFont()->setBold(true);


        // read data to active sheet
        $this->excel->getActiveSheet()->fromArray(array_keys($exceldata[0]),NULL,'A1');
        $this->excel->getActiveSheet()->fromArray($exceldata,NULL,'A2');
        $this->excel->getActiveSheet()->getStyle('A')
            ->getNumberFormat()
            ->setFormatCode('dd/mm/yy');
        $this->excel->getActiveSheet()->getStyle('F')
            ->getNumberFormat()
            ->setFormatCode('dd/mm/yy');

        $styleArray = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            )
        );

        $this->excel->getDefaultStyle()->applyFromArray($styleArray);
        unset($styleArray);

        //autosize all cells

        foreach(range('A','Z') as $columnID) {
            $this->excel->getActiveSheet()->getColumnDimension($columnID)
                ->setAutoSize(true);
        }

        } //end if

        $filename='issues-'.date('M d Y').'.xls'; //save our workbook as this file name

        header('Content-Type: application/vnd.ms-excel'); //mime type

        header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name

        header('Cache-Control: max-age=0'); //no cache

        //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        //if you want to save it as.XLSX Excel 2007 format

        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');

        //force user to download the Excel file without writing it to server's HD
        $objWriter->save('php://output');

        // load database

//         echo '<pre>',print_r($x),'</pre>';exit;


    }


}
