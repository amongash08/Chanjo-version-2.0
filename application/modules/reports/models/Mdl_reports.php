<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Mdl_Reports extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function get_national_coverage($maxdate, $mindate)
    {
        $this->db->select('periodname as months,sum(`measles 1`) as measles1,sum(`measles 2`) as measles2,sum(`measles 3`) as measles3, sum(bcg) as BCG,sum(dpt1) as DPT1,sum(dpt2) as DPT2
        ,sum(dpt3) as DPT3,sum(opv) as OPV,sum(opv1) as OPV1,sum(opv2) as OPV2,sum(opv3) as OPV3,sum(pcv1) as PCV1
        ,sum(pcv2) as PCV2,sum(pcv3) as PCV3,sum(rota1) as ROTA1,sum(rota2) as ROTA2,sum(population) as population');
        $this->db->from('v_coverage_overview');
        $this->db->where('periodname >=', date('Y-m', strtotime($mindate)));
        $this->db->where('periodname <=', date('Y-m', strtotime($maxdate)));
        $this->db->group_by('`periodname`');
        $this->db->order_by(`periodname`, 'asc');
        $query = $this->db->get();
        return $query->result();
    }

    public function get_county_coverage($maxdate, $mindate, $station)
    {
        $this->db->select('periodname as months,sum(`measles 1`) as measles1,sum(`measles 2`) as measles2,sum(`measles 3`) as measles3, sum(bcg) as BCG,sum(dpt1) as DPT1,sum(dpt2) as DPT2
        ,sum(dpt3) as DPT3,sum(opv) as OPV,sum(opv1) as OPV1,sum(opv2) as OPV2,sum(opv3) as OPV3,sum(pcv1) as PCV1
        ,sum(pcv2) as PCV2,sum(pcv3) as PCV3,sum(rota1) as ROTA1,sum(rota2) as ROTA2,sum(population) as population');
        $this->db->from('dhis_overview');
        $this->db->where('county_id', $station);
        $this->db->where('periodname >=', date('Y-m', strtotime($mindate)));
        $this->db->where('periodname <=', date('Y-m', strtotime($maxdate)));
        $this->db->group_by('`periodname`');
        $this->db->order_by(`periodname`, 'asc');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        }
        return false;

    }

    public function get_region_coverage($maxdate, $mindate, $station)
    {
        $this->db->select('periodname as months,sum(`measles 1`) as measles1,sum(`measles 2`) as measles2,sum(`measles 3`) as measles3, sum(bcg) as BCG,sum(dpt1) as DPT1,sum(dpt2) as DPT2
        ,sum(dpt3) as DPT3,sum(opv) as OPV,sum(opv1) as OPV1,sum(opv2) as OPV2,sum(opv3) as OPV3,sum(pcv1) as PCV1
        ,sum(pcv2) as PCV2,sum(pcv3) as PCV3,sum(rota1) as ROTA1,sum(rota2) as ROTA2,sum(population) as population');
        $this->db->from('dhis_overview');
        $this->db->where('region_id', $station);
        $this->db->where('periodname >=', date('Y-m', strtotime($mindate)));
        $this->db->where('periodname <=', date('Y-m', strtotime($maxdate)));
        $this->db->order_by(`periodname`, 'asc');
        $query = $this->db->get();
        return $query->result();
    }

    public function get_region_stock_level()
    {
        $this->db->query('set sql_mode = "";');
        $query = $this->db->select('*')
            ->from('region_balances')
            ->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        }
        return false;
    }

    public function get_county_stock_level()
    {
        $this->db->query('set sql_mode = "";');
        $query = $this->db->select('*')
            ->from('county_balances')
            ->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        }
        return false;
    }


    public function get_subcounty_stock_level()
    {
        $this->db->query('set sql_mode = "";');
        $query = $this->db->select('*')
            ->from('subcounty_balances')
            ->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        }
        return false;
    }

    public function get_subcounty_stock_level_by_county($county_id)
    {
        $this->db->query('set sql_mode = "";');
        $query = $this->db->select('*')
            ->from('subcounty_balances')
            ->where('county_id', $county_id)
            ->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        }
        return false;
    }

    public function get_subcounty_last_updated_by_county($county_id)
    {
        $this->db->query('set sql_mode = "";');
        $query = $this->db->select('*')
            ->from('subcounty_last_updated')
            ->where('county_id', $county_id)
            ->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        }
        return false;
    }

    public function get_subcounty_coverage_by_county($where)
    {
        $this->db->query('set sql_mode = "";');
        $query = $this->db->select('*')
            ->from('v_subcounty_coverage')
            ->where($where)
            ->order_by('period', 'asc')
            ->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        }
        return false;
    }

    public function get_coverage($select, $where)
    {
        $this->db->query('set sql_mode = "";');
        $query = $this->db->select('period, subcounty_name,'. $select)
            ->from('v_subcounty_coverage')
            ->where($where)
            ->order_by('period', 'asc')
            ->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        }
        return false;
    }


}
