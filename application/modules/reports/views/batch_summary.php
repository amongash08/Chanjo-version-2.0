<table class="table table-striped table-bordered" id="" style="margin:auto;">
    <thead style="background-color: white">
        <tr>
            <th>Vaccine Name</th>
            <th>Batch</th>
            <th>Expiry</th>
            <th>Stock Balance </th>

        </tr>
    </thead>

    <tbody>
        <?php foreach ($batch_summary as $key=> $value) { ?>
        <tr>

            <td class="">
                <?php echo $value->vaccine_name; ?>
            </td>
            <td class="">
                <?php echo $value->batch; ?>
            </td>
            <td class="">
                <?php echo date('d M Y', strtotime($value->expiry_date)); ?>
            </td>
            <td class="">
<?php
if ($value->balance < 0) {
$balance=$value->balance * -1;
echo $balance;
} else { echo $value->balance;
}
?>

            </td>
            
        </tr>
        <?php } ?>



        <tfoot>


        </tfoot>


    </tbody>
</table>
