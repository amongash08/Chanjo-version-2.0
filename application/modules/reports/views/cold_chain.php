


<style>
.col-md-6{

  margin-bottom: 2%;
}

.col-md-12{

  margin-bottom: 2%;
}

.title{
  text-align: center;
  font-weight: 300;
  background: whitesmoke;
  margin-bottom: 0px;
  padding:5px;
}
.card{
  min-height:450px;
}

</style>

<div class="row">

  <div class="form-inline row" style="margin:2%;">
    <select id="levels" class=" form-control custom-select" >
      <option value="NULL">- Select Level -</option>
      <?php
      foreach ($user_levels as $key => $value) {
          $name = $value['name'];
          $id = $value['id'];
          echo "<option value='$name' data-id='$id'>$name</option>";
      }

      ?>
    </select>

    <select id="regions" class=" form-control custom-select">
      <option value="NULL">- Select Region -</option>
      <?php
      foreach ($regions as $key => $value) {
          $name = $value['region_name'];
          $id = $value['id'];
          echo "<option value='$id'>$name</option>";
      }

      ?>
    </select>
    <select id="counties" class=" form-control custom-select">
      <option value="NULL">- Select County -</option>

    </select>
    <select id="subcounties" class=" form-control custom-select">
      <option value="NULL">- Select Sub-County -</option>

    </select>
    <select id="facilities" class=" form-control custom-select">
      <option value="NULL">- Select Facility -</option>

    </select>

    <button type="submit" id="filter_all" name="filter_all" class="btn btn-success btn-lg">Filter</button>





  </div>

  <div class="col-md-6 card">
    <h3 class="title">Positive Cold Chain Utilization</h3>
    <div id="positive">

    </div>
  </div>

  <div class="col-md-6 card">
    <h3 class="title">Negative Cold Chain Utilization</h3>
    <div id="negative">

    </div>
  </div>

  <div class="col-md-12 card">
    <h3 class="title">Heat Excursions</h3>
    <div id="heat_excursions">

    </div>
  </div>

  <!-- <div class="col-md-6 card">
    <h3 class="title">Order Fullfill Rate </h3>
  </div>

  <div class="col-md-6 card">
    <h3 class="title">Forecasting (Time Period Selection)</h3>
  </div> -->

</div>


<script type="text/javascript">

var url="<?php echo base_url(); ?>";
$('#regions,#counties,#subcounties,#facilities').hide();


ajax_fill_data('reports/heat_excursions',"#heat_excursions");
ajax_fill_data('dashboard/positiveColdchain/NULL/NULL',"#positive");
ajax_fill_data('dashboard/negativeColdchain/NULL/NULL',"#negative");

$('#levels').on('change', function(){
  $('#regions,#counties,#subcounties,#facilities').val('NULL');

  if ($(this).val()==='Region') {

    $('#regions').show();
    $('#counties,#subcounties,#facilities').hide();
  }else if ($(this).val()==='County') {

    $('#counties').show();
    $('#regions,#subcounties,#facilities').hide();
    var drop_down='';
    var county_select = "<?php echo base_url(); ?>reports/getallCountiesjson/";
$.getJSON( county_select ,function( json ) {
 $("#counties").html('<option value="NULL" selected="selected">Select Counties</option>');
  $.each(json, function( key, val ) {
    drop_down +="<option value='"+json[key]["id"]+"'>"+json[key]["county_name"]+"</option>";
  });
  $("#counties").append(drop_down);
});


  }else if ($(this).val()==='Sub County') {

    $('#subcounties').show();
    $('#regions,#counties,#facilities').hide();

    var drop_down='';
    var subcounty_select = "<?php echo base_url(); ?>reports/getallSubcountiesjson/";
$.getJSON( subcounty_select ,function( json ) {
 $("#subcounties").html('<option value="NULL" selected="selected">Select Sub-Counties</option>');
  $.each(json, function( key, val ) {
    drop_down +="<option value='"+json[key]["id"]+"'>"+json[key]["subcounty_name"]+"</option>";
  });
  $("#subcounties").append(drop_down);
});

}else if ($(this).val()==='Facility') {

    $('#facilities').show();
    $('#regions,#counties,#subcounties').hide();

    var drop_down='';
    var facility_select = "<?php echo base_url(); ?>reports/getallFacilitiesjson/";
$.getJSON( facility_select ,function( json ) {
 $("#facilities").html('<option value="NULL" selected="selected">Select Facility</option>');
  $.each(json, function( key, val ) {
    drop_down +="<option value='"+json[key]["id"]+"'>"+json[key]["facility_name"]+"</option>";
  });
  $("#facilities").append(drop_down);
});

  }
  else if ($(this).val()==='National') {

    $('#regions,#counties,#subcounties,#facilities').hide();

  }

    });


    $( "#filter_all" ).click(function() {

      var vaccine_antigens = [];
        $(':checkbox:checked').each(function(i){
          vaccine_antigens[i] = $(this).val();
        });
        console.log(vaccine_antigens);

      var level=$('option:selected', '#levels').attr('data-id');
      var region_name=$('option:selected', '#regions').text();
      var region_id=$('option:selected', '#regions').val();
      var county=$('#counties').val();
      var subcounty=$('#subcounties').val();
      var facility=$('#facilities').val();
      if ($('option:selected', '#levels').val()==='NULL'||$('option:selected', '#levels').val()==='National') {
        var station='NVIP';
        var station_name='NVIP';
      }
       if ($('option:selected', '#regions').val()!='NULL') {
        var station=$('option:selected', '#regions').val();
        var station_name=$('option:selected', '#regions').text();
      }
      if ($('option:selected', '#counties').val()!='NULL') {
       var station=$('option:selected', '#counties').val();
       var station_name=$('option:selected', '#counties').text();
     }
     if ($('option:selected', '#subcounties').val()!='NULL') {
      var station=$('option:selected', '#subcounties').val();
      var station_name=$('option:selected', '#subcounties').text();
    }
    if ($('option:selected', '#facilities').val()!='NULL') {
     var station=$('option:selected', '#facilities').val();
     var station_name=$('option:selected', '#facilities').text();
   }
    console.log(station);
    console.log(station_name);

      ajax_fill_data('dashboard/negativeColdchain/'+level+'/'+station_name,"#negative");
      ajax_fill_data('dashboard/positiveColdchain/'+level+'/'+station_name,"#positive");


    });



function ajax_fill_data(function_url,div){
      var function_url =url+function_url;
      var loading_icon=url+"assets/images/loader.gif";
        $.ajax({
          type: "POST",
          url: function_url,
          beforeSend: function() {
          $(div).html("<img style='margin:10% 50% 0 50%;' src="+loading_icon+">");
          },
          success: function(msg) {
          $(div).html(msg);
        }
      });
    }
</script>
