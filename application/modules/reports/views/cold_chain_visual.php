<!-- Styles -->
<style>
    #positive,#negative {
        width		: 100%;
        height		: 300px;
        font-size	: 11px;
    }
</style>

<!--<!-- Resources -->
<!--<script src="https://www.amcharts.com/lib/3/amcharts.js"></script>-->
<!--<script src="https://www.amcharts.com/lib/3/serial.js"></script>-->
<!--<script src="https://www.amcharts.com/lib/3/plugins/export/export.min.js"></script>-->
<!--<link rel="stylesheet" href="https://www.amcharts.com/lib/3/plugins/export/export.css" type="text/css" media="all" />-->
<!--<script src="https://www.amcharts.com/lib/3/themes/light.js"></script>-->

<!-- Chart code -->
<script>

    var freezer_volume = <?php echo $freezer_volume; ?>;
    var fridge_volume = <?php echo $fridge_volume; ?>;
    var opv_volume = <?php echo $opv_volume; ?>;
    var vaccines_volume = <?php echo $vaccines_volume; ?>;
    var positiveccData = [ {
        "category": "Positive cold chain capacity (L)",
        "vvol": vaccines_volume,
        "fvol": fridge_volume
    } ];

    var negativeccData = [ {
        "category": "Negative cold chain capacity (L)",
        "value1": opv_volume,
        "value2": freezer_volume
    } ];


    var chart = AmCharts.makeChart( "positive", {
        "theme": "light",
        "type": "serial",
        "depth3D": 40,
        "angle": 50,
        "autoMargins": false,
        "marginBottom": 20,
        "marginLeft": 150,
        "marginRight": 150,
        "dataProvider": positiveccData,
        "valueAxes": [ {
            "stackType": "100%",
            "gridAlpha": 0
//            "recalculateToPercents": true,
//            "dashLength": 0,
//            "min":0


        } ],
        "graphs": [ {
            "type": "column",
            "topRadius": 1,
            "columnWidth": 1,
            "showOnAxis": true,
            "lineThickness": 2,
            "lineAlpha": 0.5,
            "lineColor": "#FFFFFF",
            "fillColors": "#8d003b",
            "fillAlphas": 0.8,
            "valueField": "vvol"
        }, {
            "type": "column",
            "topRadius": 1,
            "columnWidth": 1,
            "showOnAxis": true,
            "lineThickness": 2,
            "lineAlpha": 0.5,
            "lineColor": "#cdcdcd",
            "fillColors": "#cdcdcd",
            "fillAlphas": 0.5,
            "valueField": "fvol"
        } ],

        "categoryField": "category",
        "categoryAxis": {
            "axisAlpha": 0,
            "labelOffset": 40,
            "gridAlpha": 0
        },
        "export": {
            "enabled": true
        }
    } );

    var chart2 = AmCharts.makeChart( "negative", {
        "theme": "light",
        "type": "serial",
        "depth3D": 40,
        "angle": 50,
        "autoMargins": false,
        "marginBottom": 20,
        "marginLeft": 150,
        "marginRight":150,
        "dataProvider": negativeccData,
        "valueAxes": [ {
            "stackType": "100%",
            "gridAlpha": 0
        } ],
        "graphs": [ {
            "type": "column",
            "topRadius": 1,
            "columnWidth": 1,
            "showOnAxis": true,
            "lineThickness": 2,
            "lineAlpha": 0.5,
            "lineColor": "#FFFFFF",
            "fillColors": "#8d003b",
            "fillAlphas": 0.8,
            "valueField": "value1"
        }, {
            "type": "column",
            "topRadius": 1,
            "columnWidth": 1,
            "showOnAxis": true,
            "lineThickness": 2,
            "lineAlpha": 0.5,
            "lineColor": "#cdcdcd",
            "fillColors": "#cdcdcd",
            "fillAlphas": 0.5,
            "valueField": "value2"
        } ],

        "categoryField": "category",
        "categoryAxis": {
            "axisAlpha": 0,
            "labelOffset": 40,
            "gridAlpha": 0
        },
        "export": {
            "enabled": true
        }
    } );

    jQuery(document).ajaxComplete(function() {
        jQuery("a[title='JavaScript charts']").remove();
    });
</script>


<!-- HTML -->

<div class="row">
    <div class="col-md-6">
        <h3 class="box-title">
            Positive  <small>Cold Chain Capacity</small>
        </h3>
        <div id="positive"  ></div>
    </div>
    <div class="col-md-6">
        <h3 class="box-title">
            Negative  <small>Cold Chain Capacity</small>
        </h3>
        <div id="negative"  ></div>

    </div>
</div>
