<div style="min-height: 400px;" id="coverage">

</div>

<script>

    Highcharts.chart('coverage', {
        chart: {
            type: '<?php echo $graph_type; ?>'
        },
        title: {
            text: '<?php echo $graph_title; ?>'
        },
        subtitle: {
            text: 'Source: DHIS'
        },
        xAxis: {
            categories: <?php print_r($category_data);?>
        },
        yAxis: {
            title: {
                text: '<?php echo $yaxis_title;?>'
            }
        },
        plotOptions: {
            line: {
                dataLabels: {
                    enabled: true
                },
                enableMouseTracking: false
            }
        },
        series: <?php print_r($series_data); ?>
    });

</script>
