


<style>
.col-md-6{

  margin-bottom: 2%;
}

.col-md-12{

  margin-bottom: 2%;
}

.title{
  text-align: center;
  font-weight: 300;
  background: whitesmoke;
  margin-bottom: 0px;
  padding:5px;
}
.card{
  min-height:450px;
}

</style>

<div class="row">

  <div class="col-md-12 card">
    <h3 class="title">System Utilization</h3>
    <div id="system_utilization">

    </div>
  </div>

  <div class="col-md-12 card">
    <h3 class="title">Reporting and Completion Rates</h3>
    <div id="reporting_rates">

    </div>
  </div>



</div>


<script type="text/javascript">

var url="<?php echo base_url(); ?>";


ajax_fill_data('reports/system_utilization',"#system_utilization");
ajax_fill_data('reports/reporting_rates',"#reporting_rates");


function ajax_fill_data(function_url,div){
      var function_url =url+function_url;
      var loading_icon=url+"assets/images/loader.gif";
        $.ajax({
          type: "POST",
          url: function_url,
          beforeSend: function() {
          $(div).html("<img style='margin:10% 50% 0 50%;' src="+loading_icon+">");
          },
          success: function(msg) {
          $(div).html(msg);
        }
      });
    }
</script>
