


<style>
.col-md-6{

  margin-bottom: 2%;
}

.col-md-12{

  margin-bottom: 2%;
}

.title{
  text-align: center;
  font-weight: 300;
  background: whitesmoke;
  margin-bottom: 0px;
  padding:5px;
}
.card{
  min-height:450px;
}
.ui-datepicker-calendar {
    display: none !important;
    }

</style>

<div class="row">

  <div class="col-md-12 card">

    <div class="form-inline row">
      <select id="county" class=" form-control custom-select" style="margin-left:50px;" >
        <option value="NULL"> Select County </option>
        <?php
        foreach ($counties as $key => $value) {
            $name = $value->county_name;
            $id = $value->id;
            echo "<option value='$name' data-id='$id'>$name</option>";
        }

        ?>
      </select>
    <label for="Date"  style="margin-left:10px;">Month</label>
        <input type="text" id="date_filter" class="form-control"  name="date_filter" />

        <button type="submit" style="margin-left:5px;" id="submit" name="submit" class="btn btn-success">Submit</button>
  </div>
  <div class="col-md-12 card" id="report_listing" style="margin-top:2%;">

  </div>



</div>


</div>


<script type="text/javascript">

var url="<?php echo base_url(); ?>";


ajax_fill_data('reports/report_listing',"#report_listing");

$(document).ready(function(){

   $("#date_filter").datepicker( {
    format: "dd-M-yyyy",
    viewMode: "months",
    minViewMode: "months",
    autoclose: true,
});


});

$( "#submit" ).click(function() {

  var period=$('#date_filter').val();
  var county=$('option:selected', '#county').val();
  //console.log(county);
  ajax_fill_data('reports/report_listing/'+county+'/'+period,"#report_listing");


});


function ajax_fill_data(function_url,div){
      var function_url =url+function_url;
      var loading_icon=url+"assets/images/loader.gif";
        $.ajax({
          type: "POST",
          url: function_url,
          beforeSend: function() {
          $(div).html("<img style='margin:10% 50% 0 50%;' src="+loading_icon+">");
          },
          success: function(msg) {
          $(div).html(msg);
        }
      });
    }
</script>
