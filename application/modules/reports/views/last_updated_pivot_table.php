<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<!-- DataTables -->
<link href="<?php echo base_url() ?>assets/plugins/datatables/datatables.css" rel="stylesheet"/>
<script src="<?php echo base_url() ?>assets/plugins/datatables/datatables.js"></script>
<div>
    <div class="col-lg-12" style="margin-top: 10px;">
        <div class="table-responsive">
            <table id="stock_last_updated" class="table table-bordered table-hover table-striped">
                <thead>
                <tr>
                    <th>Location</th>
                    <th>ROTA</th>
                    <th>BCG</th>
                    <th>BCG Diluent</th>
                    <th>TT</th>
                    <th>OPV</th>
                    <th>IPV</th>
                    <th>YF</th>
                    <th>YF Diluent</th>
                    <th>PCV</th>
                    <th>DPT</th>
                    <th>MR</th>
                    <th>MR Diluent</th>
                </tr>
                </thead>
                <tbody>

                <?php foreach ($result as $balance): ?>
                    <tr>
                        <?php foreach ($balance as $key => $value):
                            if ($key !== 'county_id'):
                                $maxdate = date('Y-m-d');
                                $mindate = new DateTime(date('Y-m-d'));
                                $interval = new DateInterval('P1M');
                                $mindate = $mindate->sub($interval)->format('Y-m-d');

                                $timestamp=date('Y-m-d',strtotime($value));

                                if ($timestamp <= $mindate){
                                    $color='red';
                                }
                                else{ $color='black';}

                                ?>
                            <td style="color:<?php echo $color; ?>"><?php echo $value; ?> </td>

                        <?php endif;
                        endforeach; ?>
                    </tr>
                <?php endforeach; ?>

                </tbody>
                <tfoot>
                <tr>
                    <th>Location</th>
                    <th>ROTA</th>
                    <th>BCG</th>
                    <th>BCG Diluent</th>
                    <th>TT</th>
                    <th>OPV</th>
                    <th>IPV</th>
                    <th>YF</th>
                    <th>YF Diluent</th>
                    <th>PCV</th>
                    <th>DPT</th>
                    <th>MR</th>
                    <th>MR Diluent</th>
                </tr>
                </tfoot>
            </table>
            <hr>
            </br>
        </div>

    </div>
</div>
<script type="text/javascript">
    var table;
    $(document).ready(function () {
        table = $('#stock_last_updated').DataTable({
            dom: 'Bfrtip',
            buttons: [
                {
                    extend: 'copyHtml5',
                    exportOptions: {
                        columns: [0, ':visible']
                    }
                },
                {
                    extend: 'excelHtml5',
                    exportOptions: {
                        columns: [0, ':visible']
                    }
                },
                {
                    extend: 'pdfHtml5',
                    exportOptions: {
                        columns: [0, ':visible']
                    }
                },
                'colvis'
            ]
        });
    });
</script>
