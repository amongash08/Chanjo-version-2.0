<script src="<?php echo base_url() ?>assets/js/bootstrap-datepicker.min.1.3.0.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/plug-ins/a5734b29083/integration/bootstrap/3/dataTables.bootstrap.css">
<link rel="stylehseet" type="text/css" href="//cdn.datatables.net/responsive/1.0.1/css/dataTables.responsive.css">
<style>
    .rptheader{
        display: none;
    } 
</style>

<div class="row">
    <div class="col-md-3">
        <div class='input-group'>
            <input placeholder="period" type='text' id="txtperiod" class="form-control" />
            <span class="input-group-addon">
                <!--<span class="glyphicon glyphicon-calendar"></span>-->
                <i class="fa fa-calendar"></i>
            </span>
        </div>
    </div>

    <div class="col-md-3">
        <input class="btn btn-info" type="button" id="btngetreport" value="load"/>
        &nbsp;&nbsp;
        <!--<input class="btn btn-info" type="button" id="btnprint" value="print"/>-->
    </div>
</div>
<br/>

<div class="row">
    <center><h3 class="rptheader">National MoS</h3></center>
    <div id="nationalmos_graph"></div>
</div>

<br/><br/>

<div class="row">
    <center><h3 class="rptheader">Total Administered</h3></center>
    <div class="col-md-12">
        <table style="margin: -20px;" class="table table-striped rptheader" data-bind="visible: totaladministered().length > 0">
            <thead>
                <tr>
                    <th>Month</th>
                    <th>BCG</th>
                    <th>DPT1</th>
                    <th>DPT2</th>
                    <th>DPT3</th>
                    <th>IPV</th>
                    <th>MEASLES1</th>
                    <th>MEASLES2</th>
                    <th>MEASLES3</th>
                    <th>OPV1</th>
                    <th>OPV2</th>
                    <th>OPV3</th>
                    <th>PCV1</th>
                    <th>PCV2</th>
                    <th>PCV3</th>
                    <th>TT</th>
                    <th>Yellow Fever</th>
                </tr>
            </thead>
            <tbody data-bind="foreach: totaladministered()">
                <tr>
                    <td><b data-bind="text: formatperiod(periodcode)"></b></td>
                    <td><span data-bind="text: cm(bcg)"></span></td>
                    <td><span data-bind="text: cm(dpt1)"></span></td>
                    <td><span data-bind="text: cm(dpt2)"></span></td>
                    <td><span data-bind="text: cm(dpt3)"></span></td>
                    <td><span data-bind="text: cm(ipv)"></span></td>
                    <td><span data-bind="text: cm(measles1)"></span></td>
                    <td><span data-bind="text: cm(measles2)"></span></td>
                    <td><span data-bind="text: cm(measles3)"></span></td>
                    <td><span data-bind="text: cm(opv1)"></span></td>
                    <td><span data-bind="text: cm(opv2)"></span></td>
                    <td><span data-bind="text: cm(opv3)"></span></td>
                    <td><span data-bind="text: cm(pcv1)"></span></td>
                    <td><span data-bind="text: cm(pcv2)"></span></td>
                    <td><span data-bind="text: cm(pcv3)"></span></td>
                    <td><span data-bind="text: cm(tt)"></span></td>
                    <td><span data-bind="text: cm(yellowfever)"></span></td>
                </tr>
            </tbody>
        </table>
    </div>
</div>

<br/><br/>

<div class="row">
    <center><h3 class="rptheader">Total Administered Graph</h3></center>
    <div id="taline_graph"></div>
</div>

<br/><br/>

<div class="row">
    <center><h3 class="rptheader">County facilities MoS</h3></center>
    <div class="col-md-12">
        <table id="cfmosdata" class="table table-bordered table-condensed hide" data-bind="visible: cfmos().length > 0" style="display: none;">
            <thead>
                <tr>
                    <th>County</th>
                    <th>BCG</th>
                    <th>DPT</th>
                    <th>IPV</th>
                    <th>Measles</th>
                    <th>OPV</th>
                    <th>PCV</th>
                    <th>TT</th>
                    <th>Yellow Fever</th>
                </tr>
            </thead>
            <tbody data-bind="foreach: cfmos()">
                <tr>
                    <td style="background-color: #ffffff;"><span data-bind="text: countyname"></span></td>
                    <td data-bind="style: { backgroundColor: bcg <= 4 ? '#EB9D9E' : 'white' }"><span data-bind="text: bcg"></span></td>
                    <td data-bind="style: { backgroundColor: dpt <= 4 ? '#EB9D9E' : 'white' }"><span data-bind="text: dpt"></span></td>
                    <td data-bind="style: { backgroundColor: ipv <= 4 ? '#EB9D9E' : 'white' }"><span data-bind="text: ipv"></span></td>
                    <td data-bind="style: { backgroundColor: measles <= 4 ? '#EB9D9E' : 'white' }"><span data-bind="text: measles"></span></td>
                    <td data-bind="style: { backgroundColor: opv <= 4 ? '#EB9D9E' : 'white' }"><span data-bind="text: opv"></span></td>
                    <td data-bind="style: { backgroundColor: pcv <= 4 ? '#EB9D9E' : 'white' }"><span data-bind="text: pcv"></span></td>
                    <td data-bind="style: { backgroundColor: tt <= 4 ? '#EB9D9E' : 'white' }"><span data-bind="text: tt"></span></td>
                    <td data-bind="style: { backgroundColor: yellowfever <= 4 ? '#EB9D9E' : 'white' }"><span data-bind="text: yellowfever"></span></td>
                </tr>
            </tbody>
            <tfoot>
                <tr>
                    <td colspan="9">&nbsp;</td>
                </tr>
                <tr>
                    <td style="background-color: #cccccc;">N/A</td><td colspan="8">NoStock data on MoH 710 report in DHIS-2</td>
                </tr>
                <tr>
                    <td style="background-color: #EB9D9E;"></td><td colspan="8">Low stock (if MOS < 3)</td>
                </tr>
            </tfoot>
        </table>

        <div id="cfmosdataout"></div>
    </div>
</div>

<br/><br/>

<div class="row">

    <div class="col-md-6">
        <center><h3 class="rptheader">Top Reporting Rates</h3></center>
        <div id="top5rr"></div>
    </div>
    <div class="col-md-6">
        <center><h3 class="rptheader">Bottom Reporting Rates</h3></center>
        <div id="bottom5rr"></div>
    </div>
</div>
<br/>

<div id="loading-div" class="hide">
    <img src="<?php echo base_url() ?>assets/images/loader.gif" />
</div>


<!-- jQuery DataTables http://datatables.net -->
<script src="//cdn.datatables.net/1.10.2/js/jquery.dataTables.js"></script>

<!-- Bootstrap DataTables http://www.datatables.net/manual/styling/bootstrap -->
<script src="//cdn.datatables.net/plug-ins/a5734b29083/integration/bootstrap/3/dataTables.bootstrap.js"></script>

<!-- Responsive DataTables http://www.datatables.net/extensions/responsive/ -->
<script src="//cdn.datatables.net/responsive/1.0.1/js/dataTables.responsive.js"></script>
<script src="<?php echo base_url(); ?>assets/js/reports/nationalrpt.js"></script>

<script>
    var baseurl = "<?php base_url(); ?>";
    $(function () {
        natApp.init();
    })
</script>