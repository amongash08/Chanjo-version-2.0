<div class="row" >
    <div class="block-web">
        <div class="col-lg-6">
          <h5>Top 10</h5>

          <table class="table table-striped table-bordered" id="" style="margin:auto;">
              <thead style="background-color: white">
                  <tr>
                      <th>Sub County</th>
                      <th>Last Updated</th>


                  </tr>
              </thead>

              <tbody>
                  <?php foreach ($top as $key=> $value) { ?>
                  <tr>

                      <td class="">
                          <?php echo $key; ?>
                      </td>
                      <td class="">
                          <?php echo date('d M Y', strtotime($value)); ?>
                      </td>

                  </tr>
                  <?php } ?>

              </tbody>
          </table>



        </div>

        <div class="col-lg-6">
          <h5>Bottom 10</h5>

          <table class="table table-striped table-bordered" id="" style="margin:auto;">
              <thead style="background-color: white">
                  <tr>
                      <th>Sub County</th>
                      <th>Last Updated</th>


                  </tr>
              </thead>

              <tbody>
                  <?php foreach ($bottom as $key=> $value) { ?>
                  <tr>

                      <td class="">
                          <?php echo $key; ?>
                      </td>
                      <td class="">
                          <?php echo date('d M Y', strtotime($value)); ?>
                      </td>

                  </tr>
                  <?php } ?>

              </tbody>
          </table>



        </div>
    </div>
</div>
