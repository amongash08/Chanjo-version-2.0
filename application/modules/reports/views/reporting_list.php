<style>
    .invoice-box{
        max-width:1000px;
        margin:auto;
        padding:30px 30px 50px 30px;
        border:1px solid #eee;
        box-shadow:0 0 3px rgba(0, 0, 0, .15);
        font-size:16px;
        line-height:24px;
        font-family:'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
        color:#555;
    }

    .invoice-box table{
        width:100%;
        line-height:inherit;
        text-align:left;
    }

    .invoice-box table td{
        padding:5px;
        vertical-align:top;
    }

    .invoice-box table tr td:nth-child(2){
        text-align:right;
    }

    .invoice-box table tr.top table td{
        padding-bottom:20px;
    }

    .invoice-box table tr.top table td.title{
        font-size:45px;
        line-height:45px;
        color:#333;
    }

    .invoice-box table tr.information table td{
        padding-bottom:40px;
    }

    .invoice-box table tr.heading td{
        background:#eee;
        border-bottom:1px solid #ddd;
        font-weight:bold;
    }

    .invoice-box table tr.details td{
        padding-bottom:20px;
    }

    .invoice-box table tr.item td{
        border-bottom:1px solid #eee;
    }

    .invoice-box table tr.item.last td{
        border-bottom:none;
    }

    .invoice-box table tr.total td:nth-child(2){
        border-top:2px solid #eee;
        font-weight:bold;
    }

    @media only screen and (max-width: 600px) {
        .invoice-box table tr.top table td{
            width:100%;
            display:block;
            text-align:center;
        }

        .invoice-box table tr.information table td{
            width:100%;
            display:block;
            text-align:center;
        }
    }
    </style>
</head>

<body>
    <div class="invoice-box">

                  <table  class="table table-bordered" id="" style="margin-bottom:4%;">
                <thead style="background-color: white">
                <tr>

                  <th>Sub County</th>
                  <th>Non Reporting HFs</th>
                  <!-- <th>HFs with Incomplete Data</th>
                  <th>HFs with Erroneous Data</th> -->

                </tr>
                </thead>

                  <tbody>
                    <?php
                        foreach ($facility_count as $key => $value) {


                                ?>
                            <tr>
                              <td><?php echo $key; ?> </td>
                              <td><?php echo $value; ?> </td>


                            </tr>
                          <?php
                        }
                          ?>

                    </tbody>

                 </table>





</div>

  <div class="invoice-box">

            <table  class="table table-striped table-bordered" id="facility_listing" >
          <thead style="background-color: white">
          <tr>

            <th>Sub County</th>
            <th>Ward</th>
            <th>Health Facility</th>
            <th>Facility Incharge</th>
            <th>Phone Number</th>
            <th>Immunizing</th>

          </tr>
          </thead>

            <tbody>
              <?php
          // echo '<pre>',print_r($query),'</pre>';exit;
                  foreach ($query as $key => $value) {

                    $chunk = explode("/", $value->organisationunitname);

                    if(sizeof($chunk)<=5){
                      $ward='-';
                      $facility=$chunk[4];

                    }if(sizeof($chunk)==6) {
                      $ward=$chunk[4];
                      $facility=$chunk[5];

                    }
                    $subcounty=$chunk[3];


                          ?>
                      <tr>
                        <td><?php echo $subcounty; ?> </td>
                        <td><?php echo $ward; ?> </td>
                        <td><?php echo $facility; ?> </td>
                        <td><?php echo '-'; ?> </td>
                        <td><?php echo '-'; ?> </td>
                        <td><?php echo '-'; ?> </td>


                      </tr>
                    <?php
                  }
                    ?>

              </tbody>
           </table>

    </div>

    <script>

    $(document).ready(function(){
      $('#facility_listing').DataTable( {
        dom: 'Bfrtip',
        bDestroy: true,
        extend: 'collection',
        text: 'Export',
        buttons: [
            'copy',
            //'excel',
            'csv',
            'pdf',
            'print'
        ]
    } );
    });

    </script>
