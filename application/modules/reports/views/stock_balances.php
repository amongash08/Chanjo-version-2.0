<style>
    
    .custom-select{
        width:20%  !important;
        height:40px !important;
        font-size: 1.2em !important;
        font-weight: bolder;
        margin-left: 2.6%;
    }
    #filter_all{
        height:40px !important;
        margin-left: 2.6%;
        border-radius: 0 !important;
    }
</style>
<div class="form-inline row">
    <select id="levels" class=" form-control custom-select">
        <option value="NULL">- Select Level -</option>
        <?php 
        foreach ($user_levels as $key=> $value) { 
          $name = $value['name']; $id = $value['id']; 
          if($id != 5 && $id != 1 && $id != 3){ 
            echo "<option value='$id' data-id='$id'>$name</option>"; 
          }
        } 
        ?>
    </select>

    <select id="regions" class=" form-control custom-select">
        <option value="NULL">- Select Region -</option>
        <?php
        foreach ($regions as $key => $value) {
            $name = $value['region_name'];
            $id = $value['id'];
            echo "<option value='$id'>$name</option>";
        }

        ?>
    </select>

    <select id="counties" class=" form-control custom-select">
        <option value="NULL">- Select County -</option>
    </select>

    <select id="subcounties" class=" form-control custom-select">
        <option value="NULL">- Select Sub-County -</option>
    </select>

    <input placeholder="period" type='text' id="period" class="form-control custom-select" />
     
    <button type="submit" id="filter_all" name="filter_all" class="btn btn-success btn-lg">Filter</button>

</div>



<div class="row" style="min-height:300px">
    <div class="block-web">
        <div class="col-lg-12" style="margin-top: 10px;">
            <table id="balance" class="table table-striped " cellspacing="0" width="100%">
                <thead style="background-color: white">
                    <tr style="color: black !important;">
                      <th>Station</th>
                      <th>BCG</th>
                      <th>BCG <br> Diluent</th>
                      <th>DPT</th>
                      <th>IPV</th>
                      <th>MR </th>
                      <th>MR <br> Diluent</th>
                      <th>OPV</th>
                      <th>PCV</th>
                      <th>ROTA</th>
                      <th>TT</th>
                      <th>Yellow <br> Fever</th>
                      <th>Yellow Fever<br> Diluent</th>
                    </tr>
                </thead>

                <tbody>

                   <tr>
                     
                   </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>


<script>
    var table;
    var base_url;
    $(document).ready(function(){
        $('#period').datepicker({
            autoclose: true,
            minViewMode: 1,
            format: 'yyyy-mm-dd'
        });
        base_url="<?php echo base_url(); ?>";
        table = $('#balance').DataTable({
            paging: true,
            ajax: base_url+'reports/list_balance',
            columns: [{
               data: "station",
            },{
                data: "bcg",
                render: $.fn.dataTable.render.number(',', '.', 0)
            },{
                data: "bcg_diluent",
                render: $.fn.dataTable.render.number(',', '.', 0)
            },{
                data: "dpt",
                render: $.fn.dataTable.render.number(',', '.', 0)
            },{
                data: "ipv",
                render: $.fn.dataTable.render.number(',', '.', 0)
            },{
                data: "measles_rubella",
                render: $.fn.dataTable.render.number(',', '.', 0)
            },{
                data: "measles_rubella_diluent",
                render: $.fn.dataTable.render.number(',', '.', 0)
            },{
                data: "opv",
                render: $.fn.dataTable.render.number(',', '.', 0)
            },{
                data: "pcv",
                render: $.fn.dataTable.render.number(',', '.', 0)
            },{
                data: "rota",
                render: $.fn.dataTable.render.number(',', '.', 0)
            },{
                data: "tt",
                render: $.fn.dataTable.render.number(',', '.', 0)
            },{
                data: "yellow_fever",
                render: $.fn.dataTable.render.number(',', '.', 0)
            },{
                data: "yellow_fever_diluent",
                render: $.fn.dataTable.render.number(',', '.', 0)
            }
            ],
            order: [[ 1, "desc" ]]   
        });

    });

    $("#filter_all").click(function (e) {
        _level = $('#levels').val();
        _date = $('#period').val();
        if (_level != '' && _level != 'NULL') {
            if(_date != ''){               
                    url =base_url+'reports/list_balance?level='+ _level+'&date='+_date;
                    url = encodeURI(url);
                    table.ajax.url(url).load();
               
            }else{
                alert('Please check the value entered');
            }
        }else{
            alert('Select level ');
        }



    });

    $('#regions, #counties, #subcounties').hide();
    $('#levels').on('change', function() {
        $('#regions,#counties,#subcounties').val('NULL');

        if ($(this).val() === 'Region') {

            // $('#regions').show();
            // $('#counties,#subcounties').hide();
        } else if ($(this).val() === 'County') {

            // $('#counties').show();
            // $('#regions,#subcounties').hide();
            // var drop_down = '';
            // var county_select = "<?php echo base_url(); ?>reports/getallCountiesjson/";
            // $.getJSON(county_select, function(json) {
            //     $("#counties").html('<option value="NULL" selected="selected">Select County</option>');
            //     $.each(json, function(key, val) {
            //         drop_down += "<option value='" + json[key]["id"] + "'>" + json[key]["county_name"] + "</option>";
            //     });
            //     $("#counties").append(drop_down);
            // });


        } else if ($(this).val() === 'Sub County') {

            // $('#subcounties').show();
            // $('#regions,#counties').hide();

            // var drop_down = '';
            // var subcounty_select = "<?php echo base_url(); ?>reports/getallSubcountiesjson/";
            // $.getJSON(subcounty_select, function(json) {
            //     $("#subcounties").html('<option value="NULL" selected="selected">Select Sub-County</option>');
            //     $.each(json, function(key, val) {
            //         drop_down += "<option value='" + json[key]["id"] + "'>" + json[key]["subcounty_name"] + "</option>";
            //     });
            //     $("#subcounties").append(drop_down);
            // });

        } else if ($(this).val() === 'National') {

            // $('#regions,#counties,#subcounties').hide();

        }

    });
</script>