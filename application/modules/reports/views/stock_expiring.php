<div class="">
  <div style="min-height: 400px;" id="">
    <table  class="table table-bordered table-hover table-striped" id="<?php echo $table_id; ?>" >
  <thead style="background-color: white">
  <tr>
    <th>Vaccine</th>
    <th>Batch</th>
    <th>Balance</th>
    <th>Expiry Date</th>

  </tr>
  </thead>

    <tbody>

    <?php
//echo '<pre>',print_r($query),'</pre>';exit;
        foreach ($query as $key) {

              ?>
            <tr>
              <td><?php echo $key->vaccine_name; ?> </td>
              <td> <?php echo $key->batch;  ?>  </td>
              <td> <?php echo $key->balance;  ?>  </td>
              <td><?php echo date('d M,Y', strtotime($key->expiry_date)); ?></td>

            </tr>
          <?php
        }
          ?>


   </tbody>
</table>
  </div>

</div>

<script>

$(document).ready(function(){
  $('#<?php echo $table_id; ?>').DataTable( {
    dom: 'Bfrtip',
    bDestroy: true,
    extend: 'collection',
    text: 'Export',
    buttons: [
        'copy',
        //'excel',
        'csv',
        'pdf',
        'print'
    ]
} );
});

</script>
