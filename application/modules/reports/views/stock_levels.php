<div class="">
  <div style="min-height: 200px;" id="">
    <table  class="table table-bordered table-hover table-striped" id="<?php echo $table_id; ?>" >
  <thead style="background-color: white">
  <tr>

    <th>County</th>
    <th>Station</th>
    <th>Population</th>
    <th>BCG</th>
    <th>BCG-Diluent</th>
    <th>PCV</th>
    <th>OPV </th>
    <th>DPT </th>
    <th>IPV</th>
    <th>ROTA</th>
    <th>MEASLE-Rubella</th>
    <th>MEASLE</th>
    <th>MEASLE-Diluent</th>
    <th>TT</th>
    <th>YF</th>
    <th>YF-Diluent</th>
    <th>Last Updated</th>
  </tr>
  </thead>

    <tbody>

    <?php
//echo '<pre>',print_r($query),'</pre>';exit;
        foreach ($query as $key => $value) {


                ?>
            <tr>
              <td><?php echo $county_names[$key]; ?> </td>
              <td><?php echo $key; ?> </td>
              <td> <?php echo $population[$key]; ?>  </td>
              <td> <?php for ($i = 0; $i < sizeof($value); ++$i) {
                    if ($value[$i]->vaccine_name == 'BCG') {
                        echo $value[$i]->balance;
                    }
                } ?>
                </td>
                <td> <?php for ($i = 0; $i < sizeof($value); ++$i) {
                      if ($value[$i]->vaccine_name == 'BCG-Diluent') {
                          echo $value[$i]->balance;
                      }
                  } ?>
                  </td>

                  <td> <?php for ($i = 0; $i < sizeof($value); ++$i) {
                        if ($value[$i]->vaccine_name == 'PCV') {
                            echo $value[$i]->balance;
                        }
                    } ?>
                    </td>

              <td><?php for ($i = 0; $i < sizeof($value); ++$i) {
                    if ($value[$i]->vaccine_name == 'OPV') {
                        echo $value[$i]->balance;
                    }
                } ?>
              </td>

              <td><?php for ($i = 0; $i < sizeof($value); ++$i) {
                    if ($value[$i]->vaccine_name == 'DPT-Hib-HepB') {
                        echo $value[$i]->balance;
                    }
                } ?>
              </td>

              <td><?php for ($i = 0; $i < sizeof($value); ++$i) {
                    if ($value[$i]->vaccine_name == 'IPV') {
                        echo $value[$i]->balance;
                    }
                } ?></td>

              <td><?php for ($i = 0; $i < sizeof($value); ++$i) {
                    if ($value[$i]->vaccine_name == 'ROTA') {
                        echo $value[$i]->balance;
                    }
                } ?></td>

                <td><?php for ($i = 0; $i < sizeof($value); ++$i) {
                      if ($value[$i]->vaccine_name == 'Measles Rubella') {
                          echo $value[$i]->balance;
                      }
                  } ?></td>

                <td><?php for ($i = 0; $i < sizeof($value); ++$i) {
                      if ($value[$i]->vaccine_name == 'Measles') {
                          echo $value[$i]->balance;
                      }
                  } ?></td>

              <td><?php for ($i = 0; $i < sizeof($value); ++$i) {
                    if ($value[$i]->vaccine_name == 'Measles-Diluent') {
                        echo $value[$i]->balance;
                    }
                } ?></td>

              <td><?php for ($i = 0; $i < sizeof($value); ++$i) {
                    if ($value[$i]->vaccine_name == 'TT') {
                        echo $value[$i]->balance;
                    }
                } ?></td>

              <td><?php for ($i = 0; $i < sizeof($value); ++$i) {
                    if ($value[$i]->vaccine_name == 'Yellow-Fever') {
                        echo $value[$i]->balance;
                    }
                } ?></td>

                <td><?php for ($i = 0; $i < sizeof($value); ++$i) {
                      if ($value[$i]->vaccine_name == 'Yellow-Fever-Diluent') {
                          echo $value[$i]->balance;
                      }
                  } ?></td>
              <td><?php

                echo date('d M,Y', strtotime($value[0]->timestamp)); ?>

              </td>

            </tr>
          <?php
        }
          ?>


   </tbody>
</table>
  </div>

</div>

<script>

$(document).ready(function(){
  $('#<?php echo $table_id; ?>').DataTable( {
    dom: 'Bfrtip',
    bDestroy: true,
    extend: 'collection',
    text: 'Export',
    buttons: [
        'copy',
        //'excel',
        'csv',
        'pdf',
        'print'
    ]
} );
});

</script>

