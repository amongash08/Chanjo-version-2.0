<script src="<?php echo base_url() ?>assets/js/jquery-2.1.0.js"></script>
<script src="<?php echo base_url() ?>assets/plugins/highcharts/highcharts.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>assets/plugins/highcharts/modules/no-data-to-display.js"></script>
<script src="<?php echo base_url() ?>assets/plugins/highcharts/modules/exporting.js"></script>


<style>
.col-md-6{

  margin-bottom: 2%;
}

.col-md-12{

  margin-bottom: 2%;
}

.title{
  text-align: center;
  font-weight: 300;
  background: whitesmoke;
  margin-bottom: 0px;
  padding:5px;
}

</style>

<link href="<?php echo base_url() ?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<div class="container" >
  <div >
    <div class="col-md-4">

    </div>
    <div class="col-md-1">
      <img src="<?php echo base_url() ?>assets/images/coat_of_arms_small.png" class="pull-left" style="" />

    </div>
    <div class="col-md-7">
      <div style="margin-left:2%;">

        <p style="padding-top:14px;font-size:1.6em;font-weight:600;margin:auto">
          Ministry of Health.
        </p>
        <p style="font-size:1.2em;font-weight:600">
          NATIONAL VACCINES & IMMUNIZATION PROGRAM.
        </p>
        <p style="font-size:12px;font-weight:600">
          <?php echo $title.' As of '.date('Y-m-d H:i:s', strtotime(date('Y-m-d H:i:s'))); ?>
        </p>

      </div>

    </div>
  </div>

<div class="container-fluid row">

  <div class="col-md-12">

<div id="stocks"></div>

  </div>

  <div class="col-md-12">

<div id="mos"></div>

  </div>

  <div class="col-md-12">
    <h3 class="title">Stock Levels (Doses)</h3>
  <div id="stock_levels">

  </div>

</div>

<div class="col-md-6">

<div id="mos_nairobi">

</div>

</div>

<div class="col-md-6">

<div id="mos_eld">

</div>

</div>

<div class="col-md-6">

<div id="mos_garissa">

</div>

</div>

<div class="col-md-6">

<div id="mos_kisumu">

</div>

</div>

<div class="col-md-6">

<div id="mos_kakamega">

</div>

</div>

<div class="col-md-6">

<div id="mos_meru">

</div>

</div>

<div class="col-md-6">

<div id="mos_mombasa">

</div>

</div>

<div class="col-md-6">

<div id="mos_nyeri">

</div>

</div>

<div class="col-md-6">

<div id="mos_naks">

</div>

</div>


</div>



</div>

<script type="text/javascript">

var url="<?php echo base_url(); ?>";
var nairobi="Nairobi Regional Store";
var eld="Eldoret Regional Store";
var garissa="Garissa Regional Store";
var kisumu="Kisumu Regional Store";
var kakamega="Kakamega Regional Store";
var meru="Meru Regional Store";
var mombasa="Mombasa Regional Store";
var nyeri="Nyeri Regional Store";
var naks="Nakuru Regional Store";

ajax_fill_data('dashboard/vaccineBalance/NULL/column',"#stocks");
ajax_fill_data('dashboard/vaccineBalancemos/NULL/NULL/column',"#mos");
ajax_fill_data('dashboard/vaccineBalancemos/2/'+nairobi+'/column',"#mos_nairobi");
ajax_fill_data('dashboard/vaccineBalancemos/2/'+eld+'/column',"#mos_eld");
ajax_fill_data('dashboard/vaccineBalancemos/2/'+garissa+'/column',"#mos_garissa");
ajax_fill_data('dashboard/vaccineBalancemos/2/'+kisumu+'/column',"#mos_kisumu");
ajax_fill_data('dashboard/vaccineBalancemos/2/'+kakamega+'/column',"#mos_kakamega");
ajax_fill_data('dashboard/vaccineBalancemos/2/'+meru+'/column',"#mos_meru");
ajax_fill_data('dashboard/vaccineBalancemos/2/'+mombasa+'/column',"#mos_mombasa");
ajax_fill_data('dashboard/vaccineBalancemos/2/'+nyeri+'/column',"#mos_nyeri");
ajax_fill_data('dashboard/vaccineBalancemos/2/'+naks+'/column',"#mos_naks");
//ajax_fill_data('reports/stock_batch_summary/',"#batch_summary");
ajax_fill_data('reports/stock_levels/2/NULL/NULL/NULL/NULL/region',"#stock_levels");
ajax_fill_data('reports/stock_levels_mos/2/NULL/NULL/NULL/NULL',"#stock_levels_mos");


function ajax_fill_data(function_url,div){
      var function_url =url+function_url;
      var loading_icon=url+"assets/images/loader.gif";
        $.ajax({
          type: "POST",
          url: function_url,
          beforeSend: function() {
          $(div).html("<img style='margin:10% 50% 0 50%;' src="+loading_icon+">");
          },
          success: function(msg) {
          $(div).html(msg);
        }
      });
    }
</script>
