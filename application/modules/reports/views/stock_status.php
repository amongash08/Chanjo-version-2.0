<!--<script src="<?php //echo base_url() ?>assets/js/jquery-2.1.0.js"></script>
<script src="<?php // echo base_url() ?>assets/plugins/highcharts/highcharts.js" type="text/javascript"></script>
<script src="<?php //echo base_url() ?>assets/plugins/highcharts/modules/no-data-to-display.js"></script>
<script src="<?php // echo base_url() ?>assets/plugins/highcharts/modules/exporting.js"></script>-->
<div class="row" style="min-height:300px">
    <div class="block-web">
        <div class="col-lg-12">

            <div class="col-md-6">
                <h5 class="content-header text-info">Stock Available</h5>

                <div id="stocks" name="stocks"></div>
            </div>
            <div class="col-md-6">

                <h5 class="content-header text-info">Months of Stock</h5>

                <div id="mos" name="mos"></div>
            </div>

        </div>
    </div>
</div>

<div class="row" style="min-height:150px">
    <div class="block-web">
        <div class="col-lg-12">
          <h5 class="content-header text-info">National Stock Summary</h5>

          <div id="batch_summary">

          </div>


        </div>
    </div>
</div>

<div class="row" style="min-height:300px">
    <div class="block-web">
        <div class="col-lg-12">
          <h5 class="content-header text-info">Batches expiring in 90 Days</h5>

          <div id="batch_exp">

          </div>


        </div>
    </div>
</div>

<div class="row" style="min-height:300px">
    <div class="block-web">
        <div class="col-lg-12">
          <h5 class="content-header text-info">Stock Levels By Region</h5>

          <div id="stock_levels">

          </div>


        </div>
    </div>
</div>

<div class="row" style="min-height:300px">
    <div class="block-web">
        <div class="col-lg-12">
          <h5 class="content-header text-info">Stock Levels By Region MOS</h5>

          <div id="stock_levels_mos">

          </div>


        </div>
    </div>
</div>

<div class="row" style="min-height:300px">
    <div class="block-web">
        <div class="col-lg-12">
          <h5 class="content-header text-info">Sub-county Stock Balances Doses</h5>

          <div id="subcounty_stock_levels">

          </div>


        </div>
    </div>
</div>

<div class="row" style="min-height:300px">
    <div class="block-web">
        <div class="col-lg-12">
          <h5 class="content-header text-info">Sub-county Stock Balances MOS</h5>

          <div id="subcounty_stock_levels_mos">

          </div>


        </div>
    </div>
</div>

<div class="row" style="min-height:300px">
    <div class="block-web">
        <div class="col-lg-12">
          <h5 class="content-header text-info">Stocks Last Updated</h5>

          <div id="stock_lastupdated">

          </div>


        </div>
    </div>
</div>


<script type="text/javascript">

var url="<?php echo base_url(); ?>";

ajax_fill_data('dashboard/vaccineBalance/NULL/column',"#stocks");
ajax_fill_data('dashboard/vaccineBalancemos/NULL/NULL/column',"#mos");
ajax_fill_data('reports/stock_batch_summary/',"#batch_summary");
ajax_fill_data('reports/stock_levels/2/NULL/NULL/NULL/NULL/region',"#stock_levels");
ajax_fill_data('reports/stock_levels_mos/2/NULL/NULL/NULL/NULL',"#stock_levels_mos");
ajax_fill_data('reports/stock_levels_mos/4/NULL/NULL/NULL/NULL',"#subcounty_stock_levels_mos");
ajax_fill_data('reports/stock_levels/4/NULL/NULL/NULL/NULL',"#subcounty_stock_levels");
ajax_fill_data('reports/rank_stations',"#stock_lastupdated");
ajax_fill_data('reports/batches_expiring/NULL',"#batch_exp");

function ajax_fill_data(function_url,div){
      var function_url =url+function_url;
      var loading_icon=url+"assets/images/loader.gif";
        $.ajax({
          type: "POST",
          url: function_url,
          beforeSend: function() {
          $(div).html("<img style='margin:10% 50% 0 50%;' src="+loading_icon+">");
          },
          success: function(msg) {
          $(div).html(msg);
        }
      });
    }
</script>
