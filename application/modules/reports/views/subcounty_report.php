<script src="<?php echo base_url() ?>assets/js/bootstrap-datepicker.min.1.3.0.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/plug-ins/a5734b29083/integration/bootstrap/3/dataTables.bootstrap.css">
<link rel="stylehseet" type="text/css" href="//cdn.datatables.net/responsive/1.0.1/css/dataTables.responsive.css">
<style>
    .rptheader{
        display: none;
    } 
</style>

<div class="row">
    <div class="col-md-3">
        <div class="row">
            <select class="form-control" id="txtcounty">
                <option value="0">-- select county --</option>
                <?php foreach ($counties as $county): ?>
                    <option value="<?php echo $county; ?>"><?php echo $county ?></option>
                <?php endforeach; ?>
            </select>
        </div>
        <div class="row">
            <select id="subcounty" data-bind="value: thesubcounty" class="form-control" style="margin-top: 2px;"></select>
        </div>

    </div>

    <div class="col-md-3">
        <div class='input-group'>
            <input placeholder="period" type='text' id="txtperiod" class="form-control" />
            <span class="input-group-addon">
                <!--<span class="glyphicon glyphicon-calendar"></span>-->
                <i class="fa fa-calendar"></i>
            </span>
        </div>
    </div>

    <div class="col-md-3">
        <input class="btn btn-info" type="button" id="btngetreport" value="load"/>
        &nbsp;&nbsp;
        <!--<input class="btn btn-info" type="button" id="btnprint" value="print"/>-->
    </div>
</div>
<br/>

<div class="row">
    <center><h3 class="rptheader">SubCounty Immunization services by facility</h3></center>
    <table class="table table-striped table-hover rptheader" data-bind="visible: sctaAntigen().length > 0 ">
        <thead>
            <tr>
                <th>Facility Name</th>
                <th>BCG</th>
                <th>DPT</th>
                <th>PCV</th>
                <th>OPV</th>
                <th>Measles</th>
                <th>IPV</th>
                <th>TT</th>
            </tr>
        </thead>
        <tbody data-bind="foreach: sctaAntigen() ">
            <tr>
                <td data-bind="text: facility_name"></td>
                <td data-bind="text: bcg"></td>
                <td data-bind="text: dpt"></td>
                <td data-bind="text: pcv"></td>
                <td data-bind="text: opv"></td>
                <td data-bind="text: measles"></td>
                <td data-bind="text: ipv"></td>
                <td data-bind="text: tt"></td>

            </tr>
        </tbody>
    </table>
</div>

<br/><br/>

<div class="row">
    <center><h3 class="rptheader">Total Administered</h3></center>
    <div class="col-md-12">
        <table class="table table-striped table-hover rptheader" data-bind="visible: sctaAllAntigen().length > 0">
            <thead>
                <tr>
                    <th>Facility Name</th>
                    <th>BCG</th>
                    <th>DPT1</th>
                    <th>DPT2</th>
                    <th>DPT3</th>
                    <th>IPV</th>
                    <th>MEASLES1</th>
                    <th>MEASLES2</th>
                    <th>MEASLES3</th>
                    <th>OPV1</th>
                    <th>OPV2</th>
                    <th>OPV3</th>
                    <th>PCV1</th>
                    <th>PCV2</th>
                    <th>PCV3</th>
                    <th>TT</th>
                    <th>Yellow Fever</th>
                </tr>
            </thead>
            <tbody data-bind="foreach: sctaAllAntigen() ">
                <tr>
                    <td data-bind="text: facility_name"></td>
                    <td data-bind="text: bcg"></td>
                    <td data-bind="text: dpt1"></td>
                    <td data-bind="text: dpt2"></td>
                    <td data-bind="text: dpt3"></td>
                    <td data-bind="text: ipv"></td>
                    <td data-bind="text: measles1"></td>
                    <td data-bind="text: measles2"></td>
                    <td data-bind="text: measles3"></td>
                    <td data-bind="text: opv1"></td>
                    <td data-bind="text: opv2"></td>
                    <td data-bind="text: opv3"></td>
                    <td data-bind="text: pcv1"></td>
                    <td data-bind="text: pcv2"></td>
                    <td data-bind="text: pcv3"></td>
                    <td data-bind="text: tt"></td>
                    <td data-bind="text: yellowfever"></td>

                </tr>
            </tbody>
        </table>
    </div>
</div>

<br/><br/>

<div class="row">
    <center><h3 class="rptheader">Total Administered Graph</h3></center>
    <div class="col-md-12">
        <table class="table table-striped table-hover rptheader" data-bind="visible: sctaYear().length > 0 ">
            <thead>
                <tr>
                    <th>Month</th>
                    <th>BCG</th>
                    <th>DPT1</th>
                    <th>DPT2</th>
                    <th>DPT3</th>
                    <th>IPV</th>
                    <th>MEASLES1</th>
                    <th>MEASLES2</th>
                    <th>MEASLES3</th>
                    <th>OPV1</th>
                    <th>OPV2</th>
                    <th>OPV3</th>
                    <th>PCV1</th>
                    <th>PCV2</th>
                    <th>PCV3</th>
                    <th>TT</th>
                    <th>Yellow Fever</th>
                </tr>
            </thead>
            <tbody data-bind="foreach: sctaYear() ">
                <tr>
                    <td data-bind="text: periodcode"></td>
                    <td data-bind="text: bcg"></td>
                    <td data-bind="text: dpt1"></td>
                    <td data-bind="text: dpt2"></td>
                    <td data-bind="text: dpt3"></td>
                    <td data-bind="text: ipv"></td>
                    <td data-bind="text: measles1"></td>
                    <td data-bind="text: measles2"></td>
                    <td data-bind="text: measles3"></td>
                    <td data-bind="text: opv1"></td>
                    <td data-bind="text: opv2"></td>
                    <td data-bind="text: opv3"></td>
                    <td data-bind="text: pcv1"></td>
                    <td data-bind="text: pcv2"></td>
                    <td data-bind="text: pcv3"></td>
                    <td data-bind="text: tt"></td>
                    <td data-bind="text: yellowfever"></td>
                </tr>
            </tbody>
        </table>
    </div>
    <br/>
    <div class="col-md-12">
        <div id="tagraph"></div>
    </div>
</div>

<div id="loading-div" class="hide">
    <img src="<?php echo base_url() ?>assets/images/loader.gif" />
</div>


<!-- jQuery DataTables http://datatables.net -->
<script src="//cdn.datatables.net/1.10.2/js/jquery.dataTables.js"></script>

<!-- Bootstrap DataTables http://www.datatables.net/manual/styling/bootstrap -->
<script src="//cdn.datatables.net/plug-ins/a5734b29083/integration/bootstrap/3/dataTables.bootstrap.js"></script>

<!-- Responsive DataTables http://www.datatables.net/extensions/responsive/ -->
<script src="//cdn.datatables.net/responsive/1.0.1/js/dataTables.responsive.js"></script>
<script src="<?php echo base_url(); ?>assets/js/reports/subcountyrpt.js"></script>

<script>
    var baseurl = '<?php echo base_url(); ?>';
    var phpsubcounties='<?php echo json_encode($subcounties); ?>';
    
    $(function () {
        subcountyApp.init();
    })
</script>