<link href="<?php echo base_url() ?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<div class="container-fluid" >
    <div >
        <div class="col-md-5">
            <img src="assets/images/coat_of_arms_small.png" class="pull-left" style="" />
        </div>
        <div class="col-md-7">
            <div style="margin-left:2%;">

                <p style="padding-top:14px;font-size:1.6em;font-weight:600;margin:auto">
                  Ministry of Health.
                </p>
                <p style="font-size:1.2em;font-weight:600">
                  National Vaccines &amp; Immunization Program
                </p>
                <p style="font-size:12px;font-weight:400">
                  <?php echo $title; ?>
                </p>

          </div>

        </div>
  </div>
    <div class="col-md-12">
        <table  class="table table-striped table-bordered" id="" style="margin:auto; page-break-after: always" >
        
            <thead style="background-color: white">
                <tr>
                    <th>Location</th>
                    <th>Population</th>
                    <th>Stock Balance</th>
                    <th>MOS</th>
                    <th>Quantity</th>

                </tr> 
            </thead>
            <tbody>
                <?php foreach ($area as $key => $value) {
    ?>
                <tr>  
                    <td class=""><?php echo $value['name']; ?></td>
                    <td class=""><?php echo $value['population']; ?> </td>
                    <td class=""><?php echo $value['balance']; ?> </td>
                    <td class=""><?php echo $value['mos']; ?> </td>
                    <td class=""><?php echo (int) ($value['quantity'] * $value['y']); ?> </td>
                    
                </tr>
                <?php 
} ?> 
                <tr style=";font-weight:600;">

                    <td class="">Name of Store Manager:</td>
                    <td class="" style="width:20px;"><?php echo $user_object['user_fname'].' '.$user_object['user_lname']?></td>
                    <td class="">Sign</td>
                    <td class=""></td>
                    <td class=""></td>
                   
                </tr>
                <tfoot>
                </tfoot>

            </tbody>

        </table>
    </div>
</div>