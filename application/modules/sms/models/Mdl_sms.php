<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Mdl_sms extends CI_Model
{

    public function get_subscription($subscription_id, $level, $column_name, $column_id)
    {
        // 2 = sms only 3 = sms & email
        $this->db->select('tbl_users.id,tbl_users.phone,tbl_users.email,tbl_users.user_group,tbl_users.user_level,tbl_users.active,
                           tbl_users.subscription,tbl_user_base.national,tbl_user_base.region,tbl_user_base.county,
                           tbl_user_base.subcounty,tbl_user_base.facility');
        $this->db->from('tbl_users');
        $this->db->join('tbl_user_base', 'tbl_user_base.user_id=tbl_users.id');
        $this->db->where('tbl_users.subscription', $subscription_id);
        $this->db->where('tbl_users.user_level', $level);
        $this->db->where('tbl_user_base.' . $column_name, $column_id);
        $this->db->where('tbl_users.active', 1);
        $query = $this->db->get();

        return $query->result();
    }

}
