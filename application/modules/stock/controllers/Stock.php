<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Stock extends Auth_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->module('users');
        $this->load->module('vaccine');
        $this->load->module('region');
        $this->load->module('county');
        $this->load->module('subcounty');
        $this->load->module('facility');
        $this->load->model('mdl_stock_ext');
        $this->load->model('mdl_user_ext');
        $this->load->model('mdl_vaccine');

    }

    public function receive()
    {
        $data['title'] = 'Manage Stock';
        $data['subtitle'] = 'Receive Stocks';
        $data['module'] = 'stock';
        $data['view_file'] = 'stock/receive_index';

        echo Modules::run('templates', $data);
    }

    public function issue()
    {
        $data['title'] = 'Manage Stock';
        $data['subtitle'] = 'Issue Stocks';
        $data['module'] = 'stock';
        $data['view_file'] = 'stock/issue_index';

        echo Modules::run('templates', $data);
    }

    public function count()
    {
        $data['title'] = 'Manage Stock';
        $data['subtitle'] = 'Physical Count';
        $data['module'] = 'stock';
        $data['view_file'] = 'stock/count_index';

        $user_info = $this->users->user_info();
        $data['user_info'] = $user_info;

        if ($this->input->post()) {
            $transaction = null;
            $items = null;
            $transaction_details = null;
            $type = 3;
            $batch = json_decode(stripcslashes($this->input->post('batch')), true);
            array_pop($batch);
            foreach ($batch as $item) {
                $transaction = array(
                    'transaction_voucher' => '',
                    'transaction_date' => $item['count_date'],
                    'to_from' => $user_info->location_id,
                    'type' => $type,
                    'status' => 'count',
                    'location_id' => $user_info->location_id,
                    'user_id' => $user_info->id,
                    'level' => $user_info->level_id,
                );
                $items = array(
                    'vaccine_id' => $item['vaccine_id'],
                    'batch' => strtoupper($item['batch_no']),
                    'expiry_date' => date('Y-m-28', strtotime($item['expiry_date'])),
                    'transaction_quantity' => $item['physical_count'],
                    'current_quantity' => $item['stock_quantity'],
                    'vvm' => (int)$item['vvm'],
                    'comment' => '',
                    'transaction_id' => '',
                );
                $transaction_details = array(
                    'date' => $item['count_date'],
                    'vaccine_id' => $item['vaccine_id'],
                    'batch' => strtoupper($item['batch_no']),
                    'expiry_date' => date('Y-m-28', strtotime($item['expiry_date'])),
                    'vvm' => (int)$item['vvm'],
                    'balance' => $item['physical_count'],
                    'location_id' => $user_info->location_id,
                    'user_id' => $user_info->id,
                    'type' => $type,
                    'transaction_id' => '',
                );
                $balance = $this->_check_transaction_balances($transaction_details);
                $query = $this->_insert_transactions($transaction, $items, $balance);
            }
            if ($query) {
                $this->session->set_flashdata('success_message', 'Physical Count conducted successfully');
                redirect('stock/ledger', 'refresh');
            }
        } else {
            $vaccine_option = array('' => 'Select vaccine');
            $data['vaccine'] = array(
                'name' => 'vaccine',
                'id' => 'vaccine',
                'class' => 'form-control vaccine',
                'options' => $vaccine_option + $this->vaccine->vaccine_dropdown()
            );

            $vvm_option = array('' => 'Select vvm');
            $data['vvm'] = array(
                'name' => 'vvm',
                'id' => 'vvm',
                'class' => 'form-control',
                'options' => $vvm_option + $this->vaccine->vvm_dropdown()
            );
        }

        echo Modules::run('templates', $data);
    }

    public function adjust()
    {
        $data['title'] = 'Manage Stock';
        $data['subtitle'] = 'Adjust Stocks';
        $data['module'] = 'stock';
        $data['view_file'] = 'stock/adjust_index';

        echo Modules::run('templates', $data);
    }

    public function ledger()
    {
        $user_info = $this->users->user_info();
        $data['title'] = 'Manage Stock';
        $data['subtitle'] = 'View Stock Ledger';
        $data['module'] = 'stock';
        $data['level_id'] = $user_info->level_id;

        if ($this->ion_auth->in_group(1)) {

            $data['title'] = 'Stock';
            $data['view_file'] = 'stock/ledger_by_location';
            $data['subtitle'] = 'View Ledger';
            $data['regions'] = $this->mdl_region->get_all();
            $data['vaccines'] = $this->vaccine->vaccine_dropdown();


        } else {

            $data['view_file'] = 'stock/ledger_index';
        }

        echo Modules::run('templates', $data);
    }

    public function view_ledger($id = false)
    {
        $data['title'] = 'Manage Stock';
        $data['subtitle'] = 'View Stock Ledger';
        $data['module'] = 'stock';
        $data['view_file'] = 'stock/view_ledger';
        if ($id && is_numeric($id)) {
            $data['id'] = $id;
            $data['vaccine_name'] = $this->mdl_vaccine->get($id)->vaccine_name;
        }

        echo Modules::run('templates', $data);
    }

    public function adjust_positive()
    {
        $data['title'] = 'Manage Stock';
        $data['subtitle'] = 'Positive Adjustment';
        $data['module'] = 'stock';
        $data['view_file'] = 'stock/adjust_positive';

        $user_info = $this->users->user_info();
        $data['user_info'] = $user_info;

        if ($this->input->post()) {
            $transaction = null;
            $items = null;
            $transaction_details = null;
            $type = 1;
            $transaction = array(
                'transaction_voucher' => '',
                'transaction_date' => $this->input->post('adjust_date'),
                'to_from' => $this->input->post('origin'),
                'type' => $type,
                'status' => 'received',
                'location_id' => $user_info->location_id,
                'user_id' => $user_info->id,
                'level' => $user_info->level_id,
            );
            $batch = json_decode(stripcslashes($this->input->post('batch')), true);
            foreach ($batch as $item) {
                if (!empty($item['vaccine_id']) && !is_null($item['vaccine_id'])) {
                    $items = array(
                        'vaccine_id' => $item['vaccine_id'],
                        'batch' => strtoupper($item['batch_no']),
                        'expiry_date' => date('Y-m-28', strtotime($item['expiry_date'])),
                        'transaction_quantity' => $item['stock_quantity'],
                        'vvm' => $item['vvm'],
                        'comment' => '',
                        'transaction_id' => '',
                    );
                    $transaction_details = array(
                        'date' => $this->input->post('adjust_date'),
                        'vaccine_id' => $item['vaccine_id'],
                        'batch' => strtoupper($item['batch_no']),
                        'expiry_date' => date('Y-m-28', strtotime($item['expiry_date'])),
                        'vvm' => $item['vvm'],
                        'balance' => $item['stock_quantity'],
                        'location_id' => $user_info->location_id,
                        'user_id' => $user_info->id,
                        'type' => $type,
                        'transaction_id' => '',
                    );
                    $balance = $this->_check_transaction_balances($transaction_details);

                    $balance_array[] = $balance;
                    $items_array[] = $items;
                }
            }
            $query = $this->mdl_stock_ext->_issue_items($transaction, $items_array, $balance_array);
            if ($query) {
                $this->session->set_flashdata('success_message', 'Stocks adjusted successfully');
                redirect('stock/ledger', 'refresh');
            }

        } else {
            $vaccine_option = array('' => 'Select vaccine');
            $data['vaccine'] = array(
                'name' => 'vaccine',
                'id' => 'vaccine',
                'class' => 'form-control vaccine',
                'options' => $vaccine_option + $this->vaccine->vaccine_dropdown()
            );

            $vvm_option = array('' => 'Select vvm');
            $data['vvm'] = array(
                'name' => 'vvm',
                'id' => 'vvm',
                'class' => 'form-control',
                'options' => $vvm_option + $this->vaccine->vvm_dropdown()
            );
        }

        echo Modules::run('templates', $data);
    }

    public function adjust_negative()
    {
        $data['title'] = 'Manage Stock';
        $data['subtitle'] = 'Negative Adjustment';
        $data['module'] = 'stock';
        $data['view_file'] = 'stock/adjust_negative';
        $user_info = $this->users->user_info();
        $data['user_info'] = $user_info;

        if ($this->input->post()) {
            $transaction = null;
            $items = null;
            $transaction_details = null;
            $transaction = array(
                'transaction_voucher' => '',
                'transaction_date' => $this->input->post('adjust_date'),
                'to_from' => $user_info->location_id,
                'type' => $this->input->post('reason'),
                'status' => 'adjusted',
                'location_id' => $user_info->location_id,
                'user_id' => $user_info->id,
                'level' => $user_info->level_id,
            );
            $batch = json_decode(stripcslashes($this->input->post('batch')), true);
            foreach ($batch as $item) {
                if (!empty($item['vaccine_id']) && !is_null($item['vaccine_id'])) {
                    $items = array(
                        'vaccine_id' => $item['vaccine_id'],
                        'batch' => strtoupper($item['batch_no']),
                        'expiry_date' => date('Y-m-28', strtotime($item['expiry_date'])),
                        'transaction_quantity' => $item['adjustment'],
                        'current_quantity' => $item['stock_quantity'],
                        'vvm' => $item['vvm'],
                        'comment' => (!$item['info']) ? $item['location'] : $item['info'],
                        'transaction_id' => '',
                    );

                    $transaction_details = array(
                        'date' => $this->input->post('adjust_date'),
                        'vaccine_id' => $item['vaccine_id'],
                        'batch' => strtoupper($item['batch_no']),
                        'expiry_date' => date('Y-m-28', strtotime($item['expiry_date'])),
                        'vvm' => $item['vvm'],
                        'balance' => $item['adjustment'],
                        'location_id' => $user_info->location_id,
                        'user_id' => $user_info->id,
                        'type' => $this->input->post('reason'),
                        'transaction_id' => '',
                    );
                    $balance = $this->_check_transaction_balances($transaction_details);

                    $balance_array[] = $balance;
                    $items_array[] = $items;
                }
            }
            $query = $this->mdl_stock_ext->_issue_items($transaction, $items_array, $balance_array);
            if ($query) {
                $this->session->set_flashdata('success_message', 'Stocks adjusted successfully');
                redirect('stock/ledger', 'refresh');
            }
        } else {
            $redistribution_option = array('' => 'Select Location');
            if ($user_info->level_id == 1) {
                $redistribution = $this->region->region_dropdown();
            } elseif ($user_info->level_id == 2) {
                $redistribution = $this->region->region_dropdown();
            } elseif ($user_info->level_id == 3) {
                $redistribution = $this->subcounty->get_by_county_dropdown($user_info->county_id);
            } elseif ($user_info->level_id == 4) {
                $redistribution = $this->facility->get_by_subcounty_dropdown($user_info->subcounty_id);
            }

            $vaccine_option = array('' => 'Select vaccine');
            $data['vaccine'] = array(
                'name' => 'vaccine',
                'id' => 'vaccine',
                'class' => 'form-control vaccine',
                'options' => $vaccine_option + $this->vaccine->vaccine_dropdown()
            );

            $data['redistribution'] = array(
                'name' => 'redistribution',
                'id' => 'redistribution',
                'class' => 'form-control',
                'options' => $redistribution_option + $redistribution
            );

            $reason_option = array(
                '' => 'Select Reason',
            );

            $data['reason'] = array(
                'name' => 'reason',
                'id' => 'reason',
                'class' => 'form-control',
                'options' => $reason_option + $this->mdl_stock_ext->get_negative_adjustment()
            );
            $data['info'] = array(
                'name' => 'info',
                'id' => 'info',
                'rows' => '1',
                'cols' => '10',
                'class' => 'form-control'
            );

        }

        echo Modules::run('templates', $data);
    }

    public function receive_voucher()
    {
        $data['title'] = 'Manage Stock';
        $data['subtitle'] = 'Receive Stocks';
        $data['module'] = 'stock';
        $data['view_file'] = 'stock/receive_voucher';
        $user_info = $this->users->user_info();
        $data['user_info'] = $user_info;

        if ($this->input->post()) {
            $transaction = null;
            $items = null;
            $transaction_details = null;
            $type = 1;
            $data = array(
                'national' => (int)'1',
                'region' => (int)$user_info->region_id,
                'county' => 0,
                'subcounty' => 0,
                'facility' => 0,
            );

            $to_from = parent::location_id($data);
            $transaction = array(
                'transaction_voucher' => $this->input->post('voucher'),
                'transaction_date' => $this->input->post('receive_date'),
                'to_from' => $to_from,
                'type' => $type,
                'status' => 'received',
                'location_id' => $user_info->location_id,
                'user_id' => $user_info->id,
                'level' => $user_info->level_id,
            );
            $batch = json_decode(stripcslashes($this->input->post('batch')), true);
            foreach ($batch as $item) {
                if (!empty($item['vaccine_id']) && !is_null($item['vaccine_id'])) {
                    $items = array(
                        'vaccine_id' => $item['vaccine_id'],
                        'batch' => strtoupper($item['batch_no']),
                        'expiry_date' => date('Y-m-28', strtotime($item['expiry_date'])),
                        'transaction_quantity' => $item['stock_quantity'],
                        'vvm' => $item['vvm'],
                        'comment' => '',
                        'transaction_id' => '',
                    );
                    $transaction_details = array(
                        'date' => $this->input->post('receive_date'),
                        'vaccine_id' => $item['vaccine_id'],
                        'batch' => strtoupper($item['batch_no']),
                        'expiry_date' => date('Y-m-28', strtotime($item['expiry_date'])),
                        'vvm' => $item['vvm'],
                        'balance' => $item['stock_quantity'],
                        'location_id' => $user_info->location_id,
                        'user_id' => $user_info->id,
                        'type' => $type,
                        'transaction_id' => '',
                    );
                    $balance = $this->_check_transaction_balances($transaction_details);

                    $balance_array[] = $balance;
                    $items_array[] = $items;
                }
            }
            $query = $this->mdl_stock_ext->_issue_items($transaction, $items_array, $balance_array);
            if ($query) {
                $this->session->set_flashdata('success_message', 'Stocks received successfully');
                redirect('stock/ledger', 'refresh');
            }

        } else {
            if ($user_info->level_id == '1') {
                $origin = [$user_info->nation_id => $user_info->nation];
            } elseif ($user_info->level_id == '2') {
                $origin = [$user_info->nation_id => $user_info->nation];
            } elseif ($user_info->level_id == '3') {
                $origin = [$user_info->region_id => $user_info->region];
            } elseif ($user_info->level_id == '4') {
                $origin = [$user_info->region_id => $user_info->region];
            }

            $data['origin'] = array(
                'name' => 'origin',
                'id' => 'origin',
                'class' => 'form-control',
                'options' => $origin
            );
            $vaccine_option = array('' => 'Select vaccine');
            $data['vaccine'] = array(
                'name' => 'vaccine',
                'id' => 'vaccine',
                'class' => 'form-control vaccine',
                'options' => $vaccine_option + $this->vaccine->vaccine_dropdown()
            );

            $vvm_option = array('' => 'Select vvm');
            $data['vvm'] = array(
                'name' => 'vvm',
                'id' => 'vvm',
                'class' => 'form-control',
                'options' => $vvm_option + $this->vaccine->vvm_dropdown()
            );
        }

        echo Modules::run('templates', $data);
    }


    public function issue_single()
    {
        $data['title'] = 'Manage Stock';
        $data['subtitle'] = 'Issue Stocks';
        $data['module'] = 'stock';
        $data['view_file'] = 'stock/issue_single';

        $user_info = $this->users->user_info();
        $data['user_info'] = $user_info;

        if ($this->input->post()) {
            $user_info = $this->users->user_info();
            $transaction = null;
            $items = null;
            $transaction_details = null;
            $type = 2;
            $issued_to = $this->input->post('issued_to');
            if ($user_info->level_id == 2) {
                $sub = $this->subcounty->get_subcounty_county_id($issued_to);
                $data = array(
                    'national' => (int)'1',
                    'region' => (int)$user_info->region_id,
                    'county' => (int)$sub->county_id,
                    'subcounty' => (int)$sub->id,
                    'facility' => (int)$user_info->facility_id,
                );
            } elseif ($user_info->level_id == 3) {
                $data = array(
                    'national' => (int)'1',
                    'region' => (int)$user_info->region_id,
                    'county' => (int)$user_info->county_id,
                    'subcounty' => (int)$issued_to,
                    'facility' => (int)$user_info->facility_id,
                );
            } elseif ($user_info->level_id == 4) {
                $data = array(
                    'national' => (int)'1',
                    'region' => (int)$user_info->region_id,
                    'county' => (int)$user_info->county_id,
                    'subcounty' => (int)$user_info->subcounty_id,
                    'facility' => (int)$issued_to,
                );
            }

            $to_from = $this->mdl_user_ext->get_location($data);
            $transaction = array(
                'transaction_voucher' => $this->input->post('voucher'),
                'transaction_date' => $this->input->post('issue_date'),
                'to_from' => ($user_info->level_id == 1) ? (int)$issued_to : (int)$to_from->id,
                'type' => $type,
                'status' => 'issued',
                'location_id' => $user_info->location_id,
                'user_id' => $user_info->id,
                'level' => $user_info->level_id
            );
            $batch = json_decode(stripcslashes($this->input->post('batch')), true);
            foreach ($batch as $item) {
                $items = array(
                    'vaccine_id' => $item['vaccine_id'],
                    'batch' => strtoupper($item['batch_no']),
                    'expiry_date' => date('Y-m-28', strtotime($item['expiry_date'])),
                    'transaction_quantity' => $item['issue_quantity'],
                    'current_quantity' => $item['quantity'],
                    'vvm' => $item['vvm'],
                    'comment' => '',
                    'transaction_id' => '',
                );
                $transaction_details = array(
                    'date' => $this->input->post('issue_date'),
                    'vaccine_id' => $item['vaccine_id'],
                    'batch' => strtoupper($item['batch_no']),
                    'expiry_date' => date('Y-m-28', strtotime($item['expiry_date'])),
                    'vvm' => $item['vvm'],
                    'balance' => $item['issue_quantity'],
                    'location_id' => $user_info->location_id,
                    'user_id' => $user_info->id,
                    'type' => $type,
                    'transaction_id' => '',
                );
                $balance = $this->_check_transaction_balances($transaction_details);
                $query = $this->_insert_transactions($transaction, $items, $balance);
            }
            if ($query) {
                $this->session->set_flashdata('success_message', 'Stocks issued successfully');
                redirect('stock/ledger', 'refresh');
            }
        } else {
            $location_option = array('' => 'Select Location');
            if ($user_info->level_id == 1) {
                $location = $this->region->region_dropdown();
            } elseif ($user_info->level_id == 2) {
                $location = $this->subcounty->get_by_region_dropdown($user_info->region_id);
            } elseif ($user_info->level_id == 3) {
                $location = $this->subcounty->get_by_county_dropdown($user_info->county_id);
            } elseif ($user_info->level_id == 4) {
                $location = $this->facility->get_by_subcounty_dropdown($user_info->subcounty_id);
            }

            $data['location'] = array(
                'name' => 'location',
                'id' => 'location',
                'class' => 'form-control',
                'options' => $location_option + $location
            );

            $vaccine_option = array('' => 'Select vaccine');
            $data['vaccine'] = array(
                'name' => 'vaccine',
                'id' => 'vaccine',
                'class' => 'form-control vaccine',
                'options' => $vaccine_option + $this->vaccine->vaccine_dropdown()
            );

            $vvm_option = array('' => 'Select vvm');
            $data['vvm'] = array(
                'name' => 'vvm',
                'id' => 'vvm',
                'class' => 'form-control',
                'options' => $vvm_option + $this->vaccine->vvm_dropdown()
            );
        }

        echo Modules::run('templates', $data);
    }

    public function issue_multiple()
    {
        $data['title'] = 'Manage Stock';
        $data['subtitle'] = 'Issue Stocks';
        $data['module'] = 'stock';
        $data['view_file'] = 'stock/issue_multiple';

        $user_info = $this->users->user_info();
        $data['user_info'] = $user_info;
        if ($this->input->post()) {
            $transaction = null;
            $items = null;
            $transaction_details = null;
            $type = 2;

            $batch = json_decode(stripcslashes($this->input->post('batch')), true);
            array_pop($batch);
            foreach ($batch as $item) {

                if ($user_info->level_id == 1) {
                    $data = array(
                        'national' => (int)'1',
                        'region' => (int)$item['location'],
                        'county' => (int)$user_info->county_id,
                        'subcounty' => (int)$user_info->subcounty_id,
                        'facility' => (int)$user_info->facility_id,
                    );

                } elseif ($user_info->level_id == 2) {
                    $sub = $this->subcounty->get_subcounty_county_id($item['location']);
                    $data = array(
                        'national' => (int)'1',
                        'region' => (int)$user_info->region_id,
                        'county' => (int)$sub->county_id,
                        'subcounty' => (int)$sub->id,
                        'facility' => (int)$user_info->facility_id,
                    );
                } elseif ($user_info->level_id == 3) {
                    $data = array(
                        'national' => (int)'1',
                        'region' => (int)$user_info->region_id,
                        'county' => (int)$user_info->county_id,
                        'subcounty' => (int)$item['location'],
                        'facility' => (int)$user_info->facility_id,
                    );
                } elseif ($user_info->level_id == 4) {
                    $data = array(
                        'national' => (int)'1',
                        'region' => (int)$user_info->region_id,
                        'county' => (int)$user_info->county_id,
                        'subcounty' => (int)$user_info->subcounty_id,
                        'facility' => (int)$item['location'],
                    );
                }
                $to_from = $this->mdl_user_ext->get_location($data);
                $transaction = array(
                    'transaction_voucher' => '',
                    'transaction_date' => $item['issue_date'],
                    'to_from' => (int)$to_from->id,
                    'type' => $type,
                    'status' => 'issued',
                    'location_id' => $user_info->location_id,
                    'user_id' => $user_info->id,
                    'level' => $user_info->level_id
                );
                $items = array(
                    'vaccine_id' => $item['vaccine'],
                    'batch' => strtoupper($item['batch_no']),
                    'expiry_date' => date('Y-m-28', strtotime($item['expiry_date'])),
                    'transaction_quantity' => $item['issue_quantity'],
                    'vvm' => $item['vvm'],
                    'comment' => '',
                    'transaction_id' => '',
                );
                $transaction_details = array(
                    'date' => $item['issue_date'],
                    'vaccine_id' => $item['vaccine'],
                    'batch' => strtoupper($item['batch_no']),
                    'expiry_date' => date('Y-m-28', strtotime($item['expiry_date'])),
                    'vvm' => $item['vvm'],
                    'balance' => $item['issue_quantity'],
                    'location_id' => $user_info->location_id,
                    'user_id' => $user_info->id,
                    'type' => $type,
                    'transaction_id' => '',
                );
                $balance = $this->_check_transaction_balances($transaction_details);
                $query = $this->_insert_transactions($transaction, $items, $balance);
            }
            if ($query) {
                $this->session->set_flashdata('success_message', 'Stocks issued successfully');
                redirect('stock/ledger', 'refresh');
            }
        } else {
            $location_option = array('' => 'Select Location');
            $user_info = $this->users->user_info();
            if ($user_info->level_id == 1) {
                $location = $this->region->region_dropdown();
            } elseif ($user_info->level_id == 2) {
                $location = $this->subcounty->get_by_region_dropdown($user_info->region_id);
            } elseif ($user_info->level_id == 3) {
                $location = $this->subcounty->get_by_county_dropdown($user_info->county_id);
            } elseif ($user_info->level_id == 4) {
                $location = $this->facility->get_by_subcounty_dropdown($user_info->subcounty_id);
            }
            $data['location'] = array(
                'name' => 'location',
                'id' => 'location',
                'class' => 'form-control location',
                'options' => $location_option + $location
            );

            $vaccine_option = array('' => 'Select vaccine');
            $data['vaccine'] = array(
                'name' => 'vaccine',
                'id' => 'vaccine',
                'class' => 'form-control vaccine',
                'options' => $vaccine_option + $this->vaccine->vaccine_dropdown()
            );

            $vvm_option = array('' => 'Select vvm');
            $data['vvm'] = array(
                'name' => 'vvm',
                'id' => 'vvm',
                'class' => 'form-control',
                'options' => $vvm_option + $this->vaccine->vvm_dropdown()
            );
        }
        echo Modules::run('templates', $data);
    }

    public function print_count_sheet()
    {
        $data['title'] = 'Print Count Sheet';
        $vaccines = array();
        $user_info = $this->users->user_info();
        foreach ($this->vaccine->vaccine_dropdown() as $id => $name) {
            $query = $this->mdl_stock_ext->get_batches($id, $user_info->location_id);
            $vaccines[$name] = $query;
        }

        $data['vaccines'] = $vaccines;
        $this->load->view('stock/stock/print_count_sheet', $data);
    }

    public function autocomplete_batch()
    {
        if (isset($_POST) && !empty($_POST)) {
            if ($this->input->post('vaccine')) {
                $vaccine = (int)$this->input->post('vaccine');
                $query = $this->mdl_stock_ext->autocomplete_batch($vaccine);

                if (!is_null($query)) {
                    echo json_encode($query);
                }
            }
        }
    }

    public function autocomplete_expiry()
    {
        if (isset($_POST) && !empty($_POST)) {
            if ($this->input->post('batch')) {
                $batch = $this->input->post('batch');
                $query = $this->mdl_stock_ext->autocomplete_expiry($batch);

                if (!is_null($query)) {
                    echo json_encode($query);
                }
            }
        }
    }

    public function batch()
    {
        if (isset($_POST) && !empty($_POST)) {
            if ($this->input->post('vaccine')) {
                $user_info = $this->users->user_info();
                $vaccine = (int)$this->input->post('vaccine');
                $query = $this->mdl_stock_ext->get_batches($vaccine, $user_info->location_id);

                if (!is_null($query)) {
                    echo json_encode($query);
                }
            }
        }
    }

    public function edit_batch()
    {
        if (isset($_POST) && !empty($_POST)) {
            if ($this->input->post('action')) {
                $user_info = $this->users->user_info();
                $batch = $this->input->post('batch_number');
                $expiry_date = $this->input->post('expiry_date');
                $expiry_date = date('Y-m-28', strtotime($expiry_date));
//                $query = $this->mdl_stock_ext->get_batches($vaccine, $user_info->location_id);
//
//                if (!is_null($query)) {
//                    echo json_encode($query);
//                }
            }
        }
    }

    public function batch_detail()
    {
        if (isset($_POST) && !empty($_POST)) {
            if ($this->input->post('batch')) {
                $user_info = $this->users->user_info();
                $batch = $this->input->post('batch');
                $query = $this->mdl_stock_ext->get_batch_details($user_info->location_id, $batch);

                if (!is_null($query)) {
                    echo json_encode($query);
                }
            }
        }
    }

    public function get_stock_balance()
    {
        if (isset($_POST) && !empty($_POST)) {
            if ($this->input->post('vaccine')) {
                $user_info = $this->users->user_info();
                $vaccine = (int)$this->input->post('vaccine');
                $query = $this->mdl_stock_ext->get_stock_balance($vaccine, $user_info->location_id);

                foreach ($query as $key => $value) {
                    echo json_encode($value);
                }
            }
        }
    }

    public function get_level()
    {
        if (isset($_POST) && !empty($_POST)) {
            if ($this->input->post('level')) {
                $level = (int)$this->input->post('level');
                if ($level === 2) {
                    $query = $this->region->region_dropdown();
                } elseif ($level === 3) {
                    $query = $this->county->county_dropdown();
                } elseif ($level === 4) {
                    $query = $this->subcounty->subcounty_dropdown();
                } elseif ($level === 5) {
                    $query = ['' => 'loading...'];
                }

                if (!is_null($query)) {
                    asort($query);
                    echo json_encode($query);
                }

            }
        }
    }

    public function adjust_locations()
    {
        $user_info = $this->users->user_info();
        if ($user_info->level_id == 1) {
            $location = $this->region->region_dropdown();
        } elseif ($user_info->level_id == 2) {
            $location = $this->region->region_dropdown();
        } elseif ($user_info->level_id == 3) {
            $location = $this->county->get_by_region($user_info->region_id);
        } elseif ($user_info->level_id == 4) {
            $location = $this->facility->get_by_subcounty_dropdown($user_info->subcounty_id);
        }

        if (!is_null($location)) {
            echo json_encode($location);
        }

    }

    public function _insert_transactions($transaction, $items, $balance)
    {
        $query = $this->mdl_stock_ext->_insert_transactions($transaction, $items, $balance);
        return $query;
    }

    public function _check_transaction_balances($transaction_details)
    {
        $vaccine_balance = $this->mdl_stock_ext->get_batch_number($transaction_details['vaccine_id'], $transaction_details['location_id'], $transaction_details['batch']);

        if (!empty($vaccine_balance)) {
            foreach ($vaccine_balance as $batch) {
                if ($batch['batch_number'] == $transaction_details['batch']) {
                    if ($transaction_details['type'] == 1) {
                        $balance = ((int)$batch['balance']) + (int)$transaction_details['balance'];
                        $transaction_details['balance'] = $balance;
                    } elseif ($transaction_details['type'] == 2) {
                        $balance = (int)($batch['balance']) - (int)($transaction_details['balance']);
                        $balance = ($balance <= 0) ? 0 : $balance;
                        $transaction_details['balance'] = $balance;
                    } elseif ($transaction_details['type'] == 3) {
                        # do nothing
                    } elseif ($transaction_details['type'] == 4) {
                        $balance = (int)($batch['balance'] + (int)$transaction_details['balance']);
                        $transaction_details['balance'] = $balance;
                    } else {
                        $balance = (int)($batch['balance'] - (int)$transaction_details['balance']);
                        $transaction_details['balance'] = $balance;
                    }
                    unset($transaction_details['type']);
                    return $transaction_details;
                }
            }
        } else {
            if ($transaction_details['type'] == 1) {
                $balance = 0 + $transaction_details['balance'];
                $transaction_details['balance'] = (int)$balance;
                return $transaction_details;
            }
        }
    }


    public function stock_data($vaccine_id, $location_id = 'NULL')
    {
        if ($location_id == 'NULL') {
            $user_info = $this->users->user_info();
            $location_id = $user_info->location_id;
        }
        if ($this->input->post()) {
            if (array_key_exists('action', $this->input->post())) {

                if ($_POST['action'] === 'remove') {
                    $del = null;
                    foreach ($_POST['data'] as $key => $value) {
                        if ($key == 'id') {
                            $del = $value;
                        }
                    }
                    $this->mdl_stock_ext->delete_record($del);
                    $this->recalculate($vaccine_id);
                }
            }
        }

        parent::stock_data($vaccine_id, $location_id);
    }

    public function filter_stock_data($county, $region, $vaccines, $subcounty)
    {

        $location_data = array(
            'national' => 1,
            'region' => (int)$region,
            'county' => (int)$county,
            'subcounty' => (int)$subcounty,
            'facility' => 0,
        );

        $location = $this->user_ext->get_location($location_data);

        $this->stock_data($vaccines, $location->id);

    }


    public function plot_stock_location($location = 'NULL', $graph_type = 'NULL')
    {
        if ($location == 'NULL') {
            $user_info = $this->users->user_info();
            $location = $user_info->location_id;
        }

        $query = $this->mdl_stock_ext->get_my_balances($location);

        $new = json_decode(json_encode($query), true);
        $category_data = [];
        $series_data = [];
        $time_data = [];

        foreach ($new as $key => $value) {
            $category_data[] = $value['vaccine_name'];
            $series_data[] = (int)$value['balance'];
            $time_data[] = $value['timestamp'];
        }

        $data['graph_type'] = $graph_type;
        $data['graph_yaxis_title'] = 'Stock Balance (Doses)';
        $data['graph_id'] = 'Stock';
        $data['legend'] = 'Doses';
        $data['colors'] = "['#008080','#6AF9C4']";
        $data['series_data'] = json_encode($series_data);
        $data['category_data'] = json_encode($category_data);

        if (count($series_data) == 0) {
            echo '<div style="margin:5%;font-size:2em;font-weight:400;"> &#128559; Please receive Stocks. </div>';
            exit;

        } else {
            $data['graph_title'] = 'Balance as of ' . date('d ,M Y', strtotime(max($time_data)));
            $this->load->view('templates/graphs/basic_bar', $data);
        }


    }


    public function plot_stock_mos_location($level = 'NULL', $location = 'NULL', $graph_type = 'NULL')
    {
        $user_info = $this->users->user_info();
        if ($location == 'NULL') {

            $user_info = $this->users->user_info();
            $location = $user_info->location_id;
        }

        if ($level == 'NULL') {
            $level = $user_info->level_id;
        }


        if ($level == 1) {

            //$this->load->model('mdl_national');
            //$query_population = $this->mdl_dashboard->get_population_national();
            $population = 1800000;
        } elseif ($level == 2) {
            $station = json_decode($user_info->json_location)->region;
            $query_population = $this->mdl_region->get_population($station);
            $population = $query_population[0]->population;
        } elseif ($level == 3) {
            $station = json_decode($user_info->json_location)->county;
            $query_population = $this->mdl_county->get_population($station);
            $population = $query_population[0]->population;
        } elseif ($level == 4) {
            $station = json_decode($user_info->json_location)->subcounty;
            $query_population = $this->mdl_subcounty->get_population($station);
            $population = $query_population[0]->population;
        }

        $query = $this->mdl_stock_ext->get_my_balances($location);

        $new = json_decode(json_encode($query), true);
        $category_data = [];
        $series_data = [];
        $time_data = [];

        foreach ($new as $key => $value) {
            $category_data[] = $value['vaccine_name'];
            $time_data[] = $value['timestamp'];
            if ($population != 0) {
                $series = $value['balance'] / (($population * $value['doses_required'] * $value['wastage_factor']) / 12);
                $series_data[] = (float)number_format($series, 2);
            }

        }
        $data['graph_type'] = $graph_type;
        $data['graph_yaxis_title'] = 'Stock Balance (MOS)';
        $data['graph_id'] = rand();
        $data['legend'] = 'MOS';
        $data['colors'] = "['#19C831']";
        $data['series_data'] = json_encode($series_data);
        $data['category_data'] = json_encode($category_data);
        $size = count($series_data);
        if (count($series_data) == 0) {
            echo '<div style="margin:5%;font-size:2em;font-weight:400;"> &#128559; Please receive Stocks. </div>';
            exit;

        } else {

            $data['graph_title'] = 'Balance as of ' . date('d ,M Y', strtotime(max($time_data)));
            $this->load->view('templates/graphs/mos', $data);

        }


    }

    public function stock_last_updated($location = false)
    {
        $user_info = $this->users->user_info();
        // if ($this->input->post()) {
        //     $location = $this->input->post('location');
        //     $period = $this->input->post('interval');
        //
        // } else {
        //     echo '<div style="margin:10%;font-size:3em;font-weight:400;"> No data for the selected filters. </div>';
        //     exit;
        // }

        if (!$location) {
            $location = $user_info->location_id;
        }

        foreach ($this->vaccine->vaccine_dropdown() as $key => $value) {
            $query[$value] = $this->mdl_stock_ext->get_last_update($location, $key);
        }

        $new = json_decode(json_encode($query), true);


        $data['query'] = $new;

        if (count($new) == 0) {
            echo '<div style="margin:5%;font-size:2em;font-weight:400;"> Please Update Stocks. </div>';
            exit;

        } else {
            if ($user_info->level_id == '3') {
                $view = 'county/reports/stock_last_update';
            } else {
                $view = 'stock/stock/stock_last_update';
            }
        }
        $this->load->view($view, $data);
    }

    public function stock_by_month()
    {
        if ($this->input->post()) {
            $location = $this->input->post('location');
            $vaccine_id = $this->input->post('antigen');
            $period = $this->input->post('period');
        } else {
            echo '<div style="margin:10%;font-size:3em;font-weight:400;"> No data for the selected filters. </div>';
            exit;
        }

        $stock = $this->mdl_stock_ext->get_stock_by_month($location, $period, $vaccine_id);
        $category_data = [];
        $series_data = [];
        if ($stock) {
            $new = json_decode(json_encode($stock), true);

            foreach ($new as $key => $value) {
                $category_data[] = $value['type'];
                $series_data[] = (int)$value['quantity'];
            }

            $data['graph_type'] = 'column';
            $data['graph_title'] = 'Stock Analysis.';
            $data['graph_yaxis_title'] = 'Quantity';
            $data['graph_id'] = 'Stock';
            $data['legend'] = 'Doses';
            $data['source'] = 'CHANJO eLMIS';
            $data['series_data'] = json_encode($series_data);
            $data['category_data'] = json_encode($category_data);
        }
        if (count($series_data) == 0) {
            echo '<div style="margin:10%;font-size:3em;font-weight:400;"> No data for the selected filters. </div>';
            exit;

        } else {
            $this->load->view('stock/stock/stock_by_date', $data);
        }
        $this->load->view('stock/stock/stock_by_date', $data);

    }

    public function stock_level($stock = false)
    {
        $user_info = $this->users->user_info();

        if ($this->input->post()) {
            $vaccine_id = $this->input->post('antigen');
            if ($user_info->level_id == '3') {
                $subcounties = $this->subcounty->get_by_county_dropdown($user_info->county_id);
                foreach ($subcounties as $key => $value) {
                    $location_data = array(
                        'national' => (int)'1',
                        'region' => (int)$user_info->region_id,
                        'county' => (int)$user_info->county_id,
                        'subcounty' => (int)$key,
                        'facility' => 0,
                    );
                    $location = $this->mdl_user_ext->get_location($location_data);
                    if ($location) {
                        $id = $location->id;
                        $id_array[] = $id;
                    }
                }
                $id_array = (string)json_encode($id_array);
                $ids = str_replace(["[", "]", "'"], '', $id_array);
                $stock = $this->mdl_stock_ext->get_county_stock_level($ids, $vaccine_id);
            }
        }
        $category_data = [];
        $series_data = [];
        if ($stock) {
            $new = json_decode(json_encode($stock), true);

            foreach ($new as $key => $value) {
                $category_data[] = $this->users->location_name($value['location_id']);
                $series_data[] = (int)$value['quantity'];
            }

            $data['graph_type'] = 'column';
            $data['graph_title'] = 'Stock Analysis.';
            $data['graph_yaxis_title'] = 'Quantity';
            $data['graph_id'] = 'Stock';
            $data['legend'] = 'Doses';
            $data['source'] = 'CHANJO eLMIS';
            $data['series_data'] = json_encode($series_data);
            $data['category_data'] = json_encode($category_data);
        }
        if (count($series_data) == 0) {
            echo '<div style="margin:10%;font-size:3em;font-weight:400;"> No data for the selected filters. </div>';
            exit;

        } else {
            $this->load->view('templates/graphs/basic_bar', $data);
        }
        $this->load->view('templates/graphs/basic_bar', $data);

    }

    public function recalculate($vaccine_id)
    {
        parent::recalculate($vaccine_id);
    }

    public function get_stock_region()
    {
        header('Content-Type: application/json');
        $user_info = $this->users->user_info();
        $stock = $this->mdl_stock_ext->get_stock_by_region();

        if (count($stock) > 0) {

            echo json_encode($stock);

        } else {

            http_response_code(401);
            return;
        }

    }

    public function get_issued($id)
    {
        $data['title'] = 'Manage Stock';
        $data['subtitle'] = 'Issued Items';
        $data['module'] = 'stock';
        $data['view_file'] = 'stock/issued_items';

        $order = false;
        if ($id) {
            $ids = explode('_', $id);
            if ((int)$ids[0]) {
                $transaction_id = $ids[0];
            }
            if ($ids[1] !== 'None') {
                $request_id = $ids[1];
                $order = true;
            } else {
                $request_id = 0;
            }
        }

        $query = $this->mdl_stock_ext->get_issued($transaction_id, $request_id, $order);
        if ($query) {
            $data['items'] = $query;
        }

        echo Modules::run('templates', $data);
    }


}
