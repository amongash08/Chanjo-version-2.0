<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Supply_chain extends MX_Controller
{

    function __construct()
    {
        parent::__construct();
        if (!$this->ion_auth->logged_in()) {
            //redirect them to the login page
            redirect('users');
        }
        $this->load->module('users');
        $this->load->module('vaccine');
        $this->load->module('region');
        $this->load->module('county');
        $this->load->module('subcounty');
        $this->load->module('facility');
        $this->load->model('mdl_stock');
        $this->load->model('mdl_stock_ext');
        $this->load->model('mdl_supply_chain_ext');
        $this->load->model('subcounty/mdl_subcounty');

    }

    public function index()
    {
        $data['title'] = 'Stock';
        $data['subtitle'] = 'Distribution Plan';
        $data['module'] = 'stock';
        $data['view_file'] = 'supply_chain/index';

        echo Modules::run('templates', $data);

    }

    public function new_distribution()
    {
        $data['title'] = 'Stock';
        $data['subtitle'] = 'New Distribution Plan';
        $data['module'] = 'stock';
        $data['view_file'] = 'supply_chain/distribution';

        $vaccine_option = array('' => '- Select Antigen -');
        $data['vaccine'] = array(
            'name' => 'vaccine',
            'id' => 'vaccine',
            'class' => 'form-control vaccine',
            'options' => $vaccine_option + $this->vaccine->vaccine_dropdown()
        );
        echo Modules::run('templates', $data);

    }

    public function stock_status()
    {
        $data['title'] = 'Manage Stock';
        $data['subtitle'] = 'Stock Status';
        $data['module'] = 'stock';
        $data['view_file'] = 'supply_chain/stock_status';

        $vaccine_option = array('' => '- Select Antigen -');
        $data['vaccine'] = array(
            'name' => 'vaccine',
            'id' => 'vaccine',
            'class' => 'form-control vaccine',
            'options' => $vaccine_option + $this->vaccine->vaccine_dropdown()
        );
        echo Modules::run('templates', $data);

    }

    public function distribution_data()
    {
        $user_info = $this->users->user_info();
        $query = $this->mdl_supply_chain_ext->get_distributions($user_info->location_id);
        $array = [];
        if ($query) {
            foreach ($query as $key => $value) {
                $obj['DT_RowId'] = 'row_' . $value->id;
                $obj['date'] = date('d M Y', strtotime($value->created_at));
                $obj['antigen'] = $value->vaccine_name;
                $obj['location_count'] = $value->location_count;
                $obj['quantity'] = $value->quantity;
                $array[] = $obj;
            }
        }

        $output['data'] = $array;
        echo json_encode($output);

    }

    public function distribution()
    {
        if (!$this->input->post()) {
            echo '<div style="margin:5%;font-size:2em;font-weight:400;"> &#128559; Please fill in the form to generate a distribution plan. </div>';
            exit;
        } else {
            $user_info = $this->users->user_info();
            $vaccine = (int)$this->input->post('vaccine');
            $distribution_quantity = (int)$this->input->post('distribution_quantity');
            if ($user_info->level_id == '1') {
                $query = $this->region->region_dropdown();
            } elseif ($user_info->level_id == '2') {
                $query = $this->subcounty->get_location_by_region($user_info->region_id);
            }
            $diff_array = [];
            $location_array = [];
            foreach ($query as $key => $value) {
                $period_stock = $this->mdl_stock_ext->calculate_period_stock($key, $user_info->level_id, $vaccine);
                $vaccine_balance = $this->mdl_stock_ext->get_stock_balance($vaccine, $key);
                $vaccine_presentation = $this->mdl_stock_ext->get_vaccine_presentation($vaccine);
                $dose_volume = $this->mdl_stock_ext->get_vaccine_volume($vaccine);
                $max_stock = (int)($period_stock * 1.25);
                if ($max_stock != 0) {
                    $max_stock = ($vaccine_presentation - ($max_stock % $vaccine_presentation)) % $vaccine_presentation;
                }
                $difference = (int)$vaccine_balance['stock_balance'] - (int)$max_stock;
                $location_array[$key] = [
                    'location' => $value,
                    'stock_balance' => (int)$vaccine_balance['stock_balance'],
                    'max_stock' => (int)$max_stock
                ];
                $diff_array[] = $difference;
            }


            $sum = array_sum($diff_array);
            foreach ($location_array as $key => $value) {
                if ((int)$sum != 0) {
                    $quantity = (($value['stock_balance'] - $value['max_stock']) / (int)$sum) * $distribution_quantity;
                } else {
                    $quantity = 0;
                }
                $location_quantity[$key] = ['quantity' => $quantity];
            }
            $data['vaccine_id'] = $vaccine;
            $data['distribution_quantity'] = $distribution_quantity;
            $data['result'] = array_replace_recursive($location_array, $location_quantity);
            $this->load->view('stock/supply_chain/distribution_table', $data);
        }

    }

    public function save_distribution()
    {
        if ($this->input->post()) {
            $user_info = $this->users->user_info();
            $collection = json_decode(stripcslashes($this->input->post('collection')), true);
            $vaccine = $this->input->post('vaccine');
            $distribution_quantity = $this->input->post('distribution_quantity');

            $transaction = [
                'location_id' => $user_info->location_id,
                'vaccine_id' => $vaccine,
                'quantity' => $distribution_quantity,
                'level' => $user_info->level_id,
                'user_id' => $user_info->id,
            ];

            $this->mdl_supply_chain_ext->_save_distribution($transaction, $collection);
        }
    }

}