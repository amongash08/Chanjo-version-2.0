<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mdl_stock_ext extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function get_stock_balance($vaccine, $location)
    {
        $sql = 'SELECT DISTINCT SUM(balance) AS stock_balance
                FROM (SELECT MAX(transaction_id) AS transaction_id, vaccine_id, batch
                FROM batch_balances
                GROUP BY vaccine_id , location_id , batch) AS b
                INNER JOIN batch_balances t ON t.transaction_id = b.transaction_id
                AND t.vaccine_id = b.vaccine_id AND t.batch = b.batch
                WHERE b.vaccine_id = "' . $vaccine . '" AND location_id= "' . $location . '" AND balance > 0';
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function get_batch_number($vaccine, $location, $batch)
    {
        $sql = "SELECT DISTINCT  b.batch AS batch_number, balance, expiry_date
                FROM (SELECT MAX(transaction_id) AS transaction_id, vaccine_id, batch
                FROM batch_balances GROUP BY vaccine_id , location_id , batch) AS b
                INNER JOIN batch_balances t ON t.transaction_id = b.transaction_id
                AND t.vaccine_id = b.vaccine_id AND t.batch = b.batch
                WHERE b.vaccine_id = '" . $vaccine . "'  AND location_id = '" . $location . "' AND b.batch = '" . $batch . "'
                GROUP BY b.batch, balance, expiry_date HAVING (balance) > 0 ORDER BY expiry_date ASC";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function get_batch_details($location, $batch)
    {
        $sql = "SELECT DISTINCT  b.batch AS batch_number, balance, expiry_date, vvm
                FROM (SELECT MAX(transaction_id) AS transaction_id, vaccine_id, batch
                FROM batch_balances GROUP BY vaccine_id , location_id , batch) AS b
                INNER JOIN batch_balances t ON t.transaction_id = b.transaction_id
                AND t.vaccine_id = b.vaccine_id AND t.batch = b.batch
                WHERE location_id = '" . $location . "' AND b.batch = '" . $batch . "'
                GROUP BY b.batch, balance, expiry_date, vvm HAVING (balance) > 0 ORDER BY expiry_date ASC";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function get_batches($vaccine, $location)
    {
        $sql = "SELECT DISTINCT b.batch AS batch_number, balance, expiry_date, doses_required
                FROM (SELECT MAX(transaction_id) AS transaction_id, vaccine_id, batch
                FROM batch_balances GROUP BY vaccine_id , location_id , batch) AS b
                INNER JOIN batch_balances t ON t.transaction_id = b.transaction_id
                JOIN vaccines v ON v.id = b.vaccine_id
                AND t.vaccine_id = b.vaccine_id AND t.batch = b.batch
                WHERE b.vaccine_id = " . $vaccine . "  AND location_id = '" . $location . "'
                GROUP BY b.batch, balance, expiry_date HAVING (balance) > 0 ORDER BY expiry_date ASC";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function autocomplete_batch($id)
    {
        $query = $this->db->distinct()
            ->select('batch')
            ->from('batch_balances')
            ->where('vaccine_id', $id)
            ->get();
        return $query->result();
    }

    public function autocomplete_expiry($batch)
    {
        $query = $this->db->distinct()
            ->select('expiry_date')
            ->from('batch_balances')
            ->where('batch', $batch)
            ->get();
        return $query->row();
    }

    public function get_doses_required($vaccine)
    {
        $query = $this->db->select('doses_required')
            ->where('id', $vaccine)
            ->get('vaccines');
        return (int)$query->row()->doses_required;
    }

    public function get_vaccine_presentation($vaccine)
    {
        $query = $this->db->select('vaccine_presentation')
            ->where('id', $vaccine)
            ->get('vaccines');
        return (int)$query->row()->vaccine_presentation;
    }

    public function get_vaccine_volume($vaccine)
    {
        $query = $this->db->select('vaccine_pck_vol as vol')
            ->where('id', $vaccine)
            ->get('vaccines');
        return (float)$query->row()->vol;
    }

    public function get_population($location, $level)
    {
        $population = $this->db->select('total_population, under_one_population, women_population')
            ->where('id', $location);
        if ($level == 1) {
            $query = $this->db->select('
                        sum(total_population) as total_population,
                        sum(under_one_population) as under_one_population,
                        sum(women_population) as women_population')
                ->get('regions');
        } elseif ($level == 2) {
            $query = $population->get('regions');
        } elseif ($level == 3) {
            $query = $population->get('counties');
        } elseif ($level == 4) {
            $query = $population->get('subcounties');
        } elseif ($level == 5) {
            $query = $population->get('facilities');
        }
        return $query->row();
    }

    public function calculate_period_stock($location, $level, $vaccine)
    {
        $select = $this->db->select('total_population, under_one_population, women_population');

        if ($level == 2) {
            $population = $select
                ->from('regions')
                ->join('locations', 'regions.id = JSON_UNQUOTE(JSON_EXTRACT(locations.json_location, "$.region"))', 'left')
                ->where('locations.id', $location)
                ->get();
        } elseif ($level == 3) {
            $population = $select->get('counties');
        } elseif ($level == 4) {
            $population = $select->get('subcounties');
        } elseif ($level == 5) {
            $population = $select->get('facilities');
        }

        $vaccines = $this->db->select('doses_required, wastage_factor')
            ->from('vaccines')
            ->join('wastage_factor', 'wastage_factor.vaccine_id = vaccines.id', 'left')
            ->where('wastage_factor.vaccine_id', $vaccine)
            ->get();

        $x = ($population->num_rows() > 0) ? (int)$population->row()->under_one_population : 0;
        $y = ($vaccines->num_rows() > 0) ? (int)$vaccines->row()->doses_required : 0;
        $z = ($vaccines->num_rows() > 0) ? (float)$vaccines->row()->wastage_factor : 0;

        return $x * $y * $z;

    }

    public function _issue_items($transaction, $items_array, $balance_array)
    {
        $this->db->trans_begin();
        $this->db->insert('transactions', $transaction);
        $transaction_id = $this->db->insert_id();

        foreach ($items_array as $items) {
            foreach ($items as $key => $value) {
                if ($key == 'transaction_id') {
                    $items['transaction_id'] = $transaction_id;
                }

            }
            $this->db->insert('transaction_items', $items);

        }

        foreach ($balance_array as $balance) {

            foreach ($balance as $key => $value) {

                if ($key == 'vvm') {
                    if ($balance['vvm'] == 3 || $balance['vvm'] == 4) {
                        $balance = null;
                    }
                }
            }
        }

        foreach ($balance_array as $balance) {
            if (!is_null($balance)) {
                foreach ($balance as $key => $value) {
                    if ($key == 'transaction_id') {
                        $balance['transaction_id'] = $transaction_id;
                    }
                    if ($key == 'type') {
                        unset($balance['type']);
                    }
                }

                $this->db->insert('batch_balances', $balance);
                $totals = $this->get_stock_balance($balance['vaccine_id'], $balance['location_id']);

                foreach ($totals as $total) {
                    foreach ($total as $k => $v) {
                        $balance['balance'] = $v;
                        unset($balance['user_id']);
                        unset($balance['date']);
                        unset($balance['batch']);
                        unset($balance['vvm']);
                        unset($balance['expiry_date']);
                    }
                }

                $this->db->insert('balances', $balance);
            }
        }

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return FALSE;
        } else {
            $this->db->trans_commit();
            return TRUE;
        }
    }

    public function _insert_transactions($transaction, $items, $balance)
    {
        $this->db->trans_begin();
        $this->db->insert('transactions', $transaction);
        $transaction_id = $this->db->insert_id();

        foreach ($items as $key => $value) {
            if ($key == 'transaction_id') {
                $items['transaction_id'] = $transaction_id;
            }
        }
        foreach ($balance as $key => $value) {
            if ($key == 'transaction_id') {
                $balance['transaction_id'] = $transaction_id;
                unset($balance['type']);
            }
        }

        $this->db->insert('transaction_items', $items);

        foreach ($balance as $key => $value) {
            if ($key == 'vvm') {
                if ($balance['vvm'] == 3 || $balance['vvm'] == 4) {
                    $balance = null;
                }
            }
        }

        if (!is_null($balance) && !empty($balance)) {
            $this->db->insert('batch_balances', $balance);
            $totals = $this->get_stock_balance($balance['vaccine_id'], $balance['location_id']);

            foreach ($totals as $total) {
                foreach ($total as $key => $value) {
                    $balance['balance'] = (int)$value;
                    unset($balance['user_id']);
                    unset($balance['date']);
                    unset($balance['batch']);
                    unset($balance['vvm']);
                    unset($balance['expiry_date']);
                }
            }


            $this->db->insert('balances', $balance);
        }

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return FALSE;
        } else {
            $this->db->trans_commit();
            return TRUE;
        }
    }

    public function get_ledger($location_id, $vaccine_id)
    {

        $this->db->distinct()
            ->select('transactions.id as order,transactions.timestamp as timestamp,transactions.transaction_date as date,
                        transactions.to_from as to_from,transactions.location_id as location_id, transaction_type.type as type,
                        transaction_items.id as row_id, transaction_items.vaccine_id as vaccine_id,transaction_items.batch as batch,transaction_items.expiry_date as expiry,
                        transaction_items.transaction_quantity as quantity')
            ->join('transaction_items', 'transaction_items.transaction_id = transactions.id', 'right')
            ->join('vaccines', 'vaccines.id = transaction_items.vaccine_id', 'inner')
            ->join('transaction_type', 'transaction_type.id = transactions.type', 'inner')
            ->join('balances', 'balances.transaction_id = transaction_items.transaction_id AND balances.vaccine_id = transaction_items.vaccine_id', 'left')
            ->from('transactions')
            ->where('transactions.location_id', $location_id)
            ->where('transaction_items.vaccine_id', $vaccine_id)
            ->order_by('transactions.transaction_date', 'desc')
            ->order_by('transactions.id', 'desc');
        $query = $this->db->get();
        return $query->result();
    }

    public function get_balance_by_batch($location_id, $vaccine_id, $batch, $trid)
    {
        $this->db->select('vaccine_id,batch,balance');
        $this->db->from('batch_balances');
        $this->db->where('location_id', $location_id);
        $this->db->where('vaccine_id', $vaccine_id);
        $this->db->where('batch', $batch);
        $this->db->where('transaction_id = ', $trid);
        $this->db->order_by('transaction_id', 'desc');
        $this->db->limit('1');
        $query = $this->db->get();
        return $query->result();
    }

    public function previous_balance_by_batch($location_id, $vaccine_id, $batch, $trid)
    {
        $this->db->select('vaccine_id,batch,balance');
        $this->db->from('batch_balances');
        $this->db->where('location_id', $location_id);
        $this->db->where('vaccine_id', $vaccine_id);
        $this->db->where('batch', $batch);
        $this->db->where('transaction_id < ', $trid);
        $this->db->order_by('transaction_id', 'desc');
        $this->db->limit('1');
        $query = $this->db->get();
        return $query->result();
    }

    public function get_balance_by_id($location_id, $vaccine_id, $trid)
    {
        $this->db->select('vaccine_id,balance');
        $this->db->from('balances');
        $this->db->where('location_id', $location_id);
        $this->db->where('vaccine_id', $vaccine_id);
        $this->db->where('transaction_id = ', $trid);
        $this->db->order_by('transaction_id', 'desc');
        $this->db->limit('1');
        $query = $this->db->get();
        return $query->result();
    }

    public function get_my_balances($location_id)
    {

        $sql = 'SELECT v.id, v.vaccine_name, sum(balance) as balance, timestamp, doses_required ,wastage_factor,vaccine_pck_vol
                FROM (SELECT MAX(transaction_id) AS transaction_id, vaccine_id
                FROM batch_balances GROUP BY vaccine_id , batch, location_id) AS b
                INNER JOIN batch_balances bb ON bb.transaction_id = b.transaction_id
                INNER JOIN vaccines v ON bb.vaccine_id = v.id AND bb.vaccine_id = b.vaccine_id
                WHERE location_id = "' . $location_id . '"  AND balance > 0
                GROUP BY v.id , doses_required , wastage_factor';
        $query = $this->db->query($sql);
        return $query->result_array();


    }

    public function delete_record($id)
    {
        $tables = ['transactions', 'transaction_items', 'balances', 'batch_balances'];
        foreach ($tables as $key => $value) {
            if ($value == 'transactions') {
                $this->db->where('id', $id);
                $this->db->delete($value);
            } else {
                $this->db->where('transaction_id', $id);
                $this->db->delete($value);
            }
        }
    }


    public function first_record($user_id)
    {
        $this->db->select('id');
        $this->db->from('transactionTable' . $user_id . '');
        $this->db->where('type', 1);
        $this->db->order_by('id');
        $this->db->limit('1');
        $query = $this->db->get();
        return $query->result();
    }

    public function retrieve_table($user_id)
    {
        $this->db->select('*');
        $this->db->from('transactionTable' . $user_id . '');
        $query = $this->db->get();
        return $query->result();
    }

    public function temp_transactions($user_id, $location, $vaccine_id)
    {
        $this->db->query('DROP TABLE IF EXISTS transactionTable' . $user_id . '');
        $sql = 'CREATE TEMPORARY TABLE transactionTable' . $user_id . ' AS ';
        $sql .= 'SELECT  transactions.id as id, type, transaction_date, to_from, location_id, ';
        $sql .= 'user_id, vaccine_id, batch, vvm, expiry_date, transaction_quantity ';
        $sql .= 'FROM transactions INNER JOIN ';
        $sql .= 'transaction_items ON transactions.id = transaction_items.transaction_id ';
        $sql .= 'WHERE location_id = "' . $location . '" AND vaccine_id =' . $vaccine_id . '';
        $query = $this->db->query($sql);
        return $query;
    }


    public function temp_batch_balance($user_id)
    {
        $this->db->query('DROP TABLE IF EXISTS batchBalanceTable' . $user_id . '');
        $sql = 'CREATE TEMPORARY TABLE batchBalanceTable' . $user_id . '(';
        $sql .= '  `vaccine_id` int(11) NOT NULL,
                  `batch` varchar(50) NOT NULL,
                  `expiry_date` date NOT NULL,
                  `vvm` varchar(10) NOT NULL,
                  `balance` int(11) NOT NULL,
                  `user_id` varchar(30) NOT NULL,
                  `location_id` varchar(50) NOT NULL,
                  `transaction_id` int(11) NOT NULL ,
                  `date` date NOT NULL,
                  `timestamp` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP  )';
        $query = $this->db->query($sql);
        return $query;
    }

    public function another_temp_batch_balance($user_id)
    {
        $this->db->query('DROP TABLE IF EXISTS batchBalance' . $user_id . '');
        $sql = 'CREATE TEMPORARY TABLE batchBalance' . $user_id . '(';
        $sql .= '  `vaccine_id` int(11) NOT NULL,
                  `batch` varchar(50) NOT NULL,
                  `expiry_date` date NOT NULL,
                  `vvm` varchar(10) NOT NULL,
                  `balance` int(11) NOT NULL,
                  `user_id` varchar(30) NOT NULL,
                  `location_id` varchar(50) NOT NULL,
                  `transaction_id` int(11) NOT NULL ,
                  `date` date NOT NULL,
                  `timestamp` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
                  )';
        $query = $this->db->query($sql);
        return $query;
    }

    public function temp_balance($user_id)
    {
        $this->db->query('DROP TABLE IF EXISTS balanceTable' . $user_id . '');
        $sql = 'CREATE TEMPORARY TABLE balanceTable' . $user_id . '(';
        $sql .= '`vaccine_id` int(11) NOT NULL,
                `balance` int(11) NOT NULL,
                `location_id` varchar(50) NOT NULL,
                `transaction_id` int(11) NOT NULL ,
                `timestamp` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
                 )';
        $query = $this->db->query($sql);
        return $query;
    }

    public function get_temp_batch($user_id, $location, $vaccine_id, $batch)
    {
        $sql = 'SELECT DISTINCT b.batch AS batch_number, balance, expiry_date FROM
                (SELECT MAX(transaction_id) AS transaction_id, vaccine_id, batch FROM
                batchBalanceTable' . $user_id . ' GROUP BY vaccine_id , location_id , batch) AS b
                INNER JOIN  batchBalance' . $user_id . ' t ON t.transaction_id = b.transaction_id
                AND t.vaccine_id = b.vaccine_id AND t.batch = b.batch
                WHERE b.vaccine_id = ' . $vaccine_id . ' AND location_id = "' . $location . '" AND b.batch = "' . $batch . '"
                GROUP BY b.batch, balance, expiry_date HAVING (balance) > 0 ORDER BY expiry_date ASC';
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function _insert_temp_balance($user_id, $balance)
    {

        if (!is_null($balance)) {
            $this->db->insert('batchBalanceTable' . $user_id . '', $balance);
            $this->db->insert('batchBalance' . $user_id . '', $balance);
            $totals = $this->get_temp_balance($user_id, $balance['vaccine_id'], $balance['location_id']);
            foreach ($totals as $total) {
                foreach ($total as $key => $value) {
                    $balance['balance'] = $value;
                    unset($balance['user_id']);
                    unset($balance['date']);
                    unset($balance['batch']);
                    unset($balance['vvm']);
                    unset($balance['expiry_date']);
                }
            }

            if ($balance['balance'] == null) {
                $balance['balance'] = 0;
            }

            $this->db->insert('balanceTable' . $user_id . '', $balance);
            return $this->db->affected_rows();
        }

    }

    public function set_new_batch_balance($user_id)
    {
        $sql = 'UPDATE batch_balances dest, batchBalanceTable' . $user_id . ' src SET
                    dest.batch = src.batch,
                    dest.balance = src.balance,
                    dest.timestamp = src.timestamp
                    WHERE dest.transaction_id = src.transaction_id AND dest.vaccine_id = src.vaccine_id AND dest.batch = src.batch
                    ';
        $this->db->query($sql);
        return $this->db->affected_rows();
    }

    public function insert_new_batch_balance($user_id)
    {
        $sql = 'INSERT INTO  batch_balances(
                vaccine_id,
                batch,
                expiry_date,
                vvm,
                balance,
                user_id,
                location_id,
                transaction_id,
                date,
                timestamp
                )
                SELECT
                vaccine_id,
                batch,
                expiry_date,
                vvm,
                balance,
                user_id,
                location_id,
                transaction_id,
                date,
                timestamp FROM batchBalanceTable' . $user_id . '
                ';
        $this->db->query($sql);
        return $this->db->affected_rows();
    }

    public function set_new_balance($user_id)
    {
        $sql = 'UPDATE balances dest, balanceTable' . $user_id . ' src SET
                    dest.balance = src.balance,
                    dest.timestamp = src.timestamp
                    WHERE dest.transaction_id = src.transaction_id AND dest.vaccine_id = src.vaccine_id
                    ';
        $this->db->query($sql);
        return $this->db->affected_rows();
    }

    public function insert_new_balance($user_id)
    {
        $sql = 'INSERT INTO balances(
                vaccine_id,
                balance,
                location_id,
                transaction_id,
                timestamp
                )
                SELECT
                vaccine_id,
                balance,
                location_id,
                transaction_id,
                timestamp FROM balanceTable' . $user_id . '
                ';
        $this->db->query($sql);
        return $this->db->affected_rows();
    }

    public function get_temp_batch_balance($user_id)
    {
        $this->db->select('*');
        $this->db->from('batchBalanceTable' . $user_id . '');
        $query = $this->db->get();
        return $query->result();
    }

    public function get_another_temp_batch_balance($user_id)
    {
        $this->db->select('*');
        $this->db->from('batchBalance' . $user_id . '');
        $query = $this->db->get();
        return $query->result();
    }

    public function get_temp_balance($user_id, $selected_vaccine, $location)
    {
        $sql = 'SELECT DISTINCT SUM(balance) AS stock_balance
                FROM (SELECT MAX(transaction_id) AS transaction_id, vaccine_id, batch
                FROM batchBalanceTable' . $user_id . '
                GROUP BY vaccine_id , location_id , batch) AS b
                INNER JOIN batchBalance' . $user_id . ' t ON t.transaction_id = b.transaction_id
                AND t.vaccine_id = b.vaccine_id AND t.batch = b.batch
                WHERE b.vaccine_id = ' . $selected_vaccine . ' AND location_id= "' . $location . '" AND balance >= 0';
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function get_negative_adjustment()
    {
        $query = $this->db->select('*')
            ->from('transaction_type')
            ->where('id >', '4')
            ->get();
        $options = array();
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $options[$row->id] = $row->type;
            }
            return $options;
        }
        return $options;
    }

    public function get_stock_by_month($location, $period, $vaccine_id)
    {
        $sql = "SELECT location_id, vaccine_name, vaccine_id, SUM(transaction_quantity) AS quantity, tp.type,
                MONTHNAME(transaction_date) AS month, DATE_FORMAT(transaction_date,'%Y-%m') as period
                FROM transactions t
                INNER JOIN transaction_items ti ON ti.transaction_id = t.id
                LEFT JOIN transaction_type tp ON t.type = tp.id
                LEFT JOIN vaccines v ON ti.vaccine_id = v.id
                WHERE location_id = '" . $location . "' AND DATE_FORMAT(transaction_date, '%Y-%m') = '" . $period . "' AND vaccine_id = '" . $vaccine_id . "'
                GROUP BY location_id , vaccine_name , vaccine_id , t.type, MONTHNAME(transaction_date) , DATE_FORMAT(transaction_date,'%Y-%m')";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result();
        }
        return false;
    }

    public function get_last_update($location, $vaccine_id)
    {
        $sql = "SELECT v.id, v.vaccine_name, balance, timestamp
                FROM (SELECT MAX(transaction_id) AS transaction_id, vaccine_id
                FROM balances GROUP BY vaccine_id , location_id) AS b
                INNER JOIN balances bb ON bb.transaction_id = b.transaction_id
                INNER JOIN vaccines v ON bb.vaccine_id = v.id AND bb.vaccine_id = b.vaccine_id
                WHERE location_id = '" . $location . "' AND v.id = '" . $vaccine_id . "'
                GROUP BY v.id , balance, timestamp";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->row();
        }
        return false;
    }

    public function get_county_stock_level($locations, $vaccine_id)
    {
        $sql = "SELECT v.id, v.vaccine_name, balance, timestamp
                FROM (SELECT MAX(transaction_id) AS transaction_id, vaccine_id
                FROM balances GROUP BY vaccine_id , location_id) AS b
                INNER JOIN balances bb ON bb.transaction_id = b.transaction_id
                INNER JOIN vaccines v ON bb.vaccine_id = v.id AND bb.vaccine_id = b.vaccine_id
                WHERE location_id IN (" . $locations . ") AND v.id = '" . $vaccine_id . "'
                GROUP BY v.id , balance, timestamp";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->row();
        }
        return false;
    }

    public function get_stock_by_type($location_id, $type)
    {

        $this->db->distinct();
        $this->db->select('transactions.id as order,transactions.timestamp as timestamp,transactions.transaction_date as date,
                        transactions.to_from as to_from,transactions.location_id as location_id, transaction_type.type as type
                        ,transaction_items.vaccine_id as vaccine_id,transaction_items.batch as batch,transaction_items.expiry_date as expiry,
                        transaction_items.transaction_quantity as quantity,vaccines.alias,vaccines.vaccine_presentation,vaccines.product_type,vaccines.vvm_type,vaccines.manufacturer,vaccines.origin,locations.json_location,transaction_voucher');
        $this->db->join('transaction_items', 'transaction_items.transaction_id = transactions.id', 'right');
        $this->db->join('vaccines', 'vaccines.id = transaction_items.vaccine_id', 'inner');
        $this->db->join('transaction_type', 'transaction_type.id = transactions.type', 'inner');
        $this->db->join('balances', 'balances.transaction_id = transaction_items.transaction_id AND balances.vaccine_id = transaction_items.vaccine_id', 'left');
        $this->db->join('locations', 'locations.id = transactions.to_from', 'inner');
        $this->db->from('transactions');
        $this->db->where('transactions.location_id', $location_id);
        $this->db->where('transaction_type.type', $type);
        //$this->db->where('transactions.to_from !=', $location_id);
        $this->db->where('YEAR(transactions.transaction_date) >=', 2017);
        $this->db->order_by('transactions.transaction_date', 'desc');
        $this->db->order_by('transactions.id', 'desc');
        $query = $this->db->get();
        return $query->result();
    }

    public function get_stock_by_type_region($location_id, $type)
    {

        $this->db->distinct();
        $this->db->select('transactions.id as order,transactions.timestamp as timestamp,transactions.transaction_date as date,
                        transactions.to_from as to_from,transactions.location_id as location_id, transaction_type.type as type
                        ,transaction_items.vaccine_id as vaccine_id,transaction_items.batch as batch,transaction_items.expiry_date as expiry,
                        transaction_items.transaction_quantity as quantity,vaccines.alias,vaccines.vaccine_presentation,vaccines.product_type,vaccines.vvm_type,vaccines.manufacturer,vaccines.origin,locations.json_location,transaction_voucher');
        $this->db->join('transaction_items', 'transaction_items.transaction_id = transactions.id', 'right');
        $this->db->join('vaccines', 'vaccines.id = transaction_items.vaccine_id', 'inner');
        $this->db->join('transaction_type', 'transaction_type.id = transactions.type', 'inner');
        $this->db->join('balances', 'balances.transaction_id = transaction_items.transaction_id AND balances.vaccine_id = transaction_items.vaccine_id', 'left');
        $this->db->join('locations', 'locations.id = transactions.to_from', 'inner');
        $this->db->from('transactions');
        $this->db->where('transactions.location_id', $location_id);
        $this->db->where('transaction_type.type', $type);
        $this->db->where('transactions.to_from !=', $location_id);
        $this->db->where('YEAR(transactions.transaction_date) >=', 2017);
        $this->db->where('JSON_EXTRACT(`json_location`, "$.subcounty") !=', 0);
        $this->db->order_by('transactions.transaction_date', 'desc');
        $this->db->order_by('transactions.id', 'desc');
        $query = $this->db->get();
        return $query->result();
    }

    public function get_stock_by_region()
    {
        $sql = "SELECT `region_balances`.`Region Name` as region_name, `region_balances`.`ROTA`,`region_balances`.`BCG`,
                        `region_balances`.`BCG Diluent` as bcg_dil,`region_balances`.`TT`, `region_balances`.`OPV`,
                        `region_balances`.`IPV`,`region_balances`.`YF`, `region_balances`.`YF Diluent` as yf_dil,
                        `region_balances`.`PCV`, `region_balances`.`DPT`, `region_balances`.`MR`,`region_balances`.`MR Diluent` as mr_dil
                         FROM `chanjo`.`region_balances`;";
        $query = $this->db->query($sql)->result();
        return $query;
    }

    public function get_issued($transaction_id, $request_id =false, $order = false)
    {
        $query = $this->db->select('vaccine_name, batch, expiry_date as expiry, transaction_quantity as quantity,vvm')
            ->from('transactions')
            ->join('transaction_items', 'transaction_items.transaction_id = transactions.id')
            ->join('vaccines', 'vaccines.id = transaction_items.vaccine_id', 'inner');
        if ($order) {
            $query->where('request_id', $request_id);
        } else {
            $query->where('transactions.id', $transaction_id);
        }
        $query = $this->db->get();
        return $query->result();
    }


}
