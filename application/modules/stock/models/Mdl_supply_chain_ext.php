<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mdl_supply_chain_ext extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function get_distributions($location_id)
    {
        $query = $this->db->select('d.id, d.location_id, vaccine_id, vaccine_name, d.quantity,  sum(case when  dl.location_id and dl.quantity != 0  then 1 else 0 end) as location_count, user_id, created_at')
            ->from('distributions d')
            ->join('distribution_locations dl', 'd.id = dl.distribution_id')
            ->join('vaccines v', 'v.id = d.vaccine_id')
            ->where('d.location_id', $location_id)
            ->group_by('d.id, d.location_id')
            ->get();

        if ($query->num_rows() > 0) {
            return $query->result();
        }
        return false;

    }

    public function _save_distribution($transaction, $collection)
    {
        $this->db->trans_begin();
        $this->db->insert('distributions', $transaction);
        $distribution_id = $this->db->insert_id();

        foreach ($collection as $items) {
            foreach ($items as $key => $value) {
                $items['distribution_id'] = $distribution_id;
            }
            $this->db->insert('distribution_locations', $items);
        }


        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return FALSE;
        } else {
            $this->db->trans_commit();
            return TRUE;
        }
    }

}
