<?php echo form_open('stock/adjust_negative', array('class' => "form-horizontal", 'id' => "negative")); ?>
<?php
if ($user_info->level_id == '1') {
    $location = $user_info->nation;
} elseif ($user_info->level_id == '2') {
    $location = $user_info->region;
} elseif ($user_info->level_id == '3') {
    $location = $user_info->county;
} elseif ($user_info->level_id == '4') {
    $location = $user_info->subcounty;
} ?>
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Transaction Details</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                class="fa fa-minus"></i>
                    </button>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-lg-3">
                            <div class="form-group-sm">
                                <label>Location</label>
                                <br>
                                <input type="text" name="location" value="<?php echo $location; ?>"
                                       class="form-control" id="location" disabled>

                            </div>
                        </div>

                        <div class="col-lg-3">
                            <div class="form-group-sm">
                                <label>Reason for adjustment</label>
                                <br>
                                <?php echo form_dropdown($reason); ?>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group-sm">
                                <label>Date of adjustment</label>
                                <br>
                                <input type="text" class="form-control adjust_date" id="adjust_date"
                                       name="adjust_date" placeholder="YYYY-MM-DD">
                            </div>
                        </div>
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- ./box-body -->
        </div>
        <!-- /.box -->
    </div>
    <!-- /.col -->
</div>
<!-- /.row -->

<div class="row">
    <div class="col-sm-12">
        <div class="box box-solid">
            <div class="box-header with-border">
                <h3 class="box-title">Vaccine Details</h3>
            </div>
            <div class="box-body">
                <div class="table-responsive" id="table">
                    <table class="table table-bordered table-hover table-striped">
                        <thead>
                        <tr align="center" class="form-group-sm">
                            <th style="width:13%;">Vaccine /<br>Diluents</th>
                            <th style="width:15%;">Batch <br>Number</th>
                            <th style="width:13%;">Stock <br>Quantity</th>
                            <th style="width:13%;">Quantity</th>
                            <th style="width:14%;">More Info</th>
                            <th style="width:8%;">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr row="0" class="form-group-sm">
                            <td>
                                <?php echo form_dropdown($vaccine); ?>
                            </td>
                            <td>
                                <input type="text" class="form-control batch_no" id="batch_no" name="batch_no" disabled>
                            </td>
                            <td hidden>
                                <input type="text" class="form-control expiry_date" id="expiry_date"
                                       name="expiry_date"
                                       disabled>
                            </td>
                            <td>
                                <input type="text" class="form-control stock_quantity" id="stock_quantity"
                                       name="stock_quantity" disabled>
                            </td>
                            <td>
                                <input type="number" class="form-control adjustment" id="adjustment"
                                       name="adjustment">
                            </td>
                            <td>
                                <?php echo form_textarea($info); ?>
                            </td>
                            <td hidden>
                                <?php echo form_dropdown($redistribution, '', '', 'disabled'); ?>
                            </td>
                            <td hidden>
                                <input type="number" class="form-control vvm" id="vvm"
                                       name="vvm" disabled>
                            </td>
                            <td class="small">
                                <a class="add btn-xs" style="cursor: pointer; cursor: hand;"><span
                                            class="label label-success"><i
                                                class="fa fa-plus-square"></i> <b>ADD</b></span></a><br>
                                <a class="remove btn-xs" style="cursor: pointer; cursor: hand;"><span
                                            class="label label-danger"><i
                                                class="fa fa-minus-square"></i> <b>REMOVE</b></span></a>
                            </td>
                        </tr>
                        <tr id="template" class="form-group-sm" hidden>
                            <td>
                                <?php echo form_dropdown($vaccine, '', '', 'disabled'); ?>
                            </td>
                            <td>
                                <input type="text" class="form-control batch_no" id="batch_no" name="batch_no" disabled>
                            </td>
                            <td hidden>
                                <input type="text" class="form-control expiry_date" id="expiry_date"
                                       name="expiry_date"
                                       disabled>
                            </td>
                            <td>
                                <input type="text" class="form-control stock_quantity" id="stock_quantity"
                                       name="stock_quantity" disabled>
                            </td>
                            <td>
                                <input type="number" class="form-control adjustment" id="adjustment"
                                       name="adjustment" disabled>
                            </td>
                            <td>
                                <?php echo form_textarea($info, '', 'disabled'); ?>
                            </td>
                            <td hidden>
                                <?php echo form_dropdown($redistribution, '', '', 'disabled'); ?>
                            </td>
                            <td hidden>
                                <input type="number" class="form-control vvm" id="vvm"
                                       name="vvm" disabled>
                            </td>
                            <td class="small">
                                <a class="add btn-xs" style="cursor: pointer; cursor: hand;"><span
                                            class="label label-success"><i
                                                class="fa fa-plus-square"></i> <b>ADD</b></span></a><br>
                                <a class="remove btn-xs" style="cursor: pointer; cursor: hand;"><span
                                            class="label label-danger"><i
                                                class="fa fa-minus-square"></i> <b>REMOVE</b></span></a>
                            </td>
                        </tr>

                        </tbody>
                    </table>
                </div>
            </div>
            <div class="box-footer">
                <button type="submit" id="validate" class="btn bg-navy margin">
                    Submit
                </button>
            </div>
        </div>
    </div>

</div>
<?php echo form_close(); ?>
<script>
    $(document).ready(function () {

        $('#negative')
            .formValidation({
                framework: 'bootstrap',
                err: {
                    container: 'tooltip'
                },
                row: {
                    selector: 'td'
                },
                // This option will not ignore invisible fields which belong to inactive panels
                excluded: ':hidden, :not(:visible)',
                fields: {
                    adjust_date: {
                        row: '.form-group-sm',
                        validators: {
                            notEmpty: {
                                message: '*required'
                            },
                            date: {
                                format: 'YYYY-MM-DD',
                                message: '*date is not valid',
                                max: moment().format('YYYY-MM-DD'),
                                min: moment('2016-06-01').format('YYYY-MM-DD')
                            }
                        }
                    },
                    vaccine: {
                        validators: {
                            notEmpty: {
                                message: '*required'
                            }
                        }
                    },
                    batch_no: {
                        validators: {
                            notEmpty: {
                                message: '*required'
                            }
                        }
                    },
                    stock_quantity: {
                        validators: {
                            notEmpty: {
                                message: '*required'
                            },
                            numeric: {
                                message: '*not a number'
                            },
                            greaterThan: {
                                value: 0,
                                message: '*not a valid number'
                            }
                        }
                    },
                    adjustment: {
                        validators: {
                            notEmpty: {
                                message: '*required'
                            },
                            numeric: {
                                message: '*not a number'
                            },
                            greaterThan: {
                                value: 0,
                                message: '*not a valid number'
                            },
                            lessThan: {
                                value: 'stock_quantity',
                                message: '*exceeds stock quantity'
                            }
                        }
                    },
                    location: {
                        validators: {
                            notEmpty: {
                                message: '*required'
                            }
                        }
                    },
                    reason: {
                        validators: {
                            notEmpty: {
                                message: '*required'
                            }
                        }
                    }
                }
            }).on('click', '#table .add', function () {
            var thisRow = $(this).closest('tr');
            return validateRow(thisRow);

        }).on('click', '#table .remove', function () {
            if ($('#table tbody tr').length === 2) return;
            $(this).parents("tr").fadeOut('slow', function () {
                $(this).remove();
            });

        }).on('change', '#vaccine', function () {
            var row = $(this);
            var vaccine = row.val();
            load_batches(vaccine, row);

        }).on('change', '#batch_no', function () {
            var row = $(this);
            var batch = row.val();
            load_batch_details(batch, row);

        }).on('change', '#reason', function () {
            var row = $(this);
            var reason = row.val();
            load_locations(reason);

        }).on('success.form.fv', function (e) {
            // Prevent form submission
            e.preventDefault();
            swal({
                    title: "Confirm Submission",
                    text: "Are you sure you want to submit the entered details?",
                    type: "info",
                    showCancelButton: true,
                    confirmButtonColor: "#54CEF7",
                    confirmButtonText: "Submit",
                    closeOnConfirm: false,
                    showLoaderOnConfirm: true,
                },
                function (isConfirm) {
                    if (isConfirm) {
                        submit();
                    } else {
                        return false;
                    }
                });

        });

        function submit() {

            var vaccine_count = 0;
            $.each($("#vaccine"), function (i, v) {
                vaccine_count++;
            });

            var vaccines = retrieveFormValues_Array('vaccine');
            var batch_no = retrieveFormValues_Array('batch_no');
            var expiry_date = retrieveFormValues_Array('expiry_date');
            var stock_quantity = retrieveFormValues_Array('stock_quantity');
            var adjustment = retrieveFormValues_Array('adjustment');
            var reason = retrieveFormValues('reason');
            var location = retrieveFormValues('location');
            var info = retrieveFormValues_Array('info');
            var vvm = retrieveFormValues_Array('vvm');
            var adjust_date = retrieveFormValues('adjust_date');

            var dat = new Array();

            for (var i = 0; i < vaccine_count; i++) {
                var data = new Array();
                var get_vaccine = vaccines[i];
                var get_batch = batch_no[i];
                var get_expiry = expiry_date[i];
                var get_stock_quantity = stock_quantity[i];
                var get_adjustment = adjustment[i];
                var get_location = location[i];
                var get_info = info[i];
                var get_vvm = vvm[i];


                data = {
                    "vaccine_id": get_vaccine,
                    "batch_no": get_batch,
                    "expiry_date": get_expiry,
                    "stock_quantity": get_stock_quantity,
                    "adjustment": get_adjustment,
                    "info": get_info,
                    "location": get_location,
                    "vvm": get_vvm
                };
                dat.push(data);
            }

            batch = JSON.stringify(dat);
            $.ajax({
                url: $('#negative').attr('action'),
                type: "POST",
                data: {
                    "adjust_date": adjust_date,
                    "reason": reason,
                    "batch": batch
                },
                beforeSend: function () {
                    $('#validate').fadeOut(300, function () {
                        $(this).remove();
                    });
                },

                success: function (data, textStatus, jqXHR) {

                    window.location.replace('<?php echo site_url('stock/ledger');?>');
                },

                error: function (jqXHR, textStatus, errorThrown) {
                    //if fails
                }
            });
        }

        $('#adjust_date')
            .datepicker({
                dateFormat: "yy-mm-dd",
                maxDate: 0,
                beforeShow: function (textbox, instance) {
                    var txtBoxOffset = $(this).offset();
                    var top = txtBoxOffset.top;
                    var left = txtBoxOffset.left;
                    var textBoxWidth = $(this).outerWidth();
                    setTimeout(function () {
                        instance.dpDiv.css({
                            top: top - 100, //you can adjust this value accordingly
                            left: left + textBoxWidth//show at the end of textBox
                        });
                    }, 0);

                },
                onSelect: function (date, inst) {
                    /* Revalidate the field when choosing it from the datepicker */
                    $('#negative').formValidation('revalidateField', 'adjust_date');
                }
            });

        function load_locations(reason) {

            var dropdown_start = '<select class="form-control" id="location" name="location">';
            var dropdown_option = new Array();
            var default_option = "<option value=''>" + "Select Location" + "</option> ";
            var dropdown_end = '</select>';
            dropdown_option.push(default_option);
            el = $('#table tbody').find('tr');
            if (reason == '4.4') {
                var request = $.ajax({
                    url: '<?php echo site_url('stock/adjust_locations'); ?>',
                    type: 'POST',
                });

                request.done(function (data) {
                    data = JSON.parse(data);

                    $.each(data, function (key, value) {
                        option = "<option value='" + key + "'>" + value + "</option> ";
                        dropdown_option.push(option);
                    });


                    $.each(el, function (index) {
                        row = el.eq(index);
                        row.find("#info").replaceWith(dropdown_start + dropdown_option.join("") + dropdown_end),
                            fv = $('#negative').data('formValidation');
                        fv.addField(row.find("#location"));
                    });

                });
                request.fail(function (jqXHR, textStatus) {

                });
            } else {

                $.each(el, function (index) {
                    row = el.eq(index);
                    info = '<textarea name="info" cols="10" rows="1" id="info" class="form-control" ></textarea>';
                    row.find("#location").replaceWith(info);
                    fv = $('#negative').data('formValidation');
                    fv.removeField(row.find("#location"));

                });

            }
        }

        function load_batches(vaccine, row) {

            var dropdown_start = '<select class="form-control" id="batch_no" name="batch_no">';
            var dropdown_option = new Array();
            var default_option = "<option value=''>" + "Select batch" + "</option> ";
            var dropdown_end = '</select>';
            dropdown_option.push(default_option);
            if (vaccine.length > 0) {
                var request = $.ajax({
                    url: '<?php echo site_url('stock/batch'); ?>',
                    type: 'post',
                    data: {
                        "vaccine": vaccine
                    },

                });

                request.done(function (data) {
                    data = JSON.parse(data);

                    $.each(data, function (key, value) {
                        option = "<option value='" + value.batch_number + "'>" + value.batch_number + "</option> ";
                        dropdown_option.push(option);
                    });


                    row.closest("tr").find("#batch_no").replaceWith(dropdown_start + dropdown_option.join("") + dropdown_end),
                        fv = $('#negative').data('formValidation');
                    fv.addField(row.closest("tr").find("#batch_no"));
                });
                request.fail(function (jqXHR, textStatus) {

                });
            } else {
                dropdown = dropdown_start + dropdown_end;
                row.closest("tr").find("#batch_no")
                    .replaceWith(dropdown),
                    fv = $('#negative').data('formValidation');
                fv.addField(row.closest("tr").find("#batch_no").attr('disabled', ''));
                row.closest("tr").find("#expiry_date").val('');
                row.closest("tr").find("#stock_quantity").val('');
            }
        }

        function load_batch_details(batch, row) {

            if (batch.length > 0) {
                var request = $.ajax({
                    url: '<?php echo site_url('stock/batch_detail'); ?>',
                    type: 'post',
                    data: {
                        "batch": batch
                    },

                });

                request.done(function (data) {
                    data = JSON.parse(data);
                    $.each(data, function (key, value) {
                        row.closest("tr").find("#expiry_date").val(value.expiry_date);
                        row.closest("tr").find("#stock_quantity").val(value.balance);
                        row.closest("tr").find("#vvm").val(value.vvm);
                        $('#negative').formValidation('revalidateField', 'expiry_date');
                        $('#negative').formValidation('revalidateField', 'stock_quantity');
                    });

                });
                request.fail(function (jqXHR, textStatus) {

                });
            } else {
                row.closest("tr").find("#expiry_date").val('');
                row.closest("tr").find("#stock_quantity").val('');
                row.closest("tr").find("#vvm").val('');
            }
        }

        function validateRow(thisRow) {
            var fv = $('#negative').data('formValidation'), // FormValidation instance
                // The current row
                $row = thisRow;
            // Validate the container
            fv.validateContainer($row);


            var isValidStep = fv.isValidContainer($row);
            if (isValidStep === false || isValidStep === null) {
                // Do not add row
                return false;
            }

            addRow($row);
        }

        function addRow(thisRow) {

            var template = $('#template');
            var cloned_object = template.clone().removeAttr('hidden').removeAttr('id');

            var count_row = thisRow.attr("row");
            var row_index = parseInt(count_row) + 1;
            var new_row = cloned_object.attr("row", row_index);


            new_row.insertAfter(thisRow).find('input').val('');

            var adjust_date = new_row.find('input[name="adjust_date"]').removeAttr('disabled');
            var vaccine = new_row.find('select[name="vaccine"]').removeAttr('disabled');
            var batch_no = new_row.find('input[name="batch_no"]');
            var stock_quantity = new_row.find('input[name="stock_quantity"]');
            var adjustment = new_row.find('input[name="adjustment"]').removeAttr('disabled');
            var reason = new_row.find('select[name="reason"]').removeAttr('disabled');
            var info = new_row.find('[name="info"]').removeAttr('disabled');

            $('#negative').formValidation('addField', adjust_date);
            adjust_date.removeClass('hasDatepicker')
                .attr('id', 'adjust_date' + row_index)
                .datepicker({
                    dateFormat: "yy-mm-dd",
                    maxDate: 0,
                    beforeShow: function (textbox, instance) {
                        var txtBoxOffset = $(this).offset();
                        var top = txtBoxOffset.top;
                        var left = txtBoxOffset.left;
                        var textBoxWidth = $(this).outerWidth();
                        setTimeout(function () {
                            instance.dpDiv.css({
                                top: top - 100, //you can adjust this value accordingly
                                left: left + textBoxWidth//show at the end of textBox
                            });
                        }, 0);

                    },
                    onSelect: function (date, inst) {
                        /* Revalidate the field when choosing it from the datepicker */
                        $('#negative').formValidation('revalidateField', 'adjust_date');
                    }
                });

            $('#negative').formValidation('addField', vaccine);
            $('#negative').formValidation('addField', batch_no);
            $('#negative').formValidation('addField', stock_quantity);
            $('#negative').formValidation('addField', adjustment);
            $('#negative').formValidation('addField', reason);
            $('#negative').formValidation('addField', info);

            new_row.slideDown();
        }

        function retrieveFormValues(name) {
            var dump;
            $.each($("input[name=" + name + "], select[name=" + name + "], textarea[name=" + name + "]"), function (i, v) {
                var theTag = v.tagName;
                var theElement = $(v);
                var theValue = theElement.val();
                dump = theValue;
            });
            return dump;
        }

        function retrieveFormValues_Array(name) {
            var dump = new Array();
            var counter = 0;
            $.each($("input[name=" + name + "], select[name=" + name + "], textarea[name=" + name + "]"), function (i, v) {
                var theTag = v.tagName;
                var theElement = $(v);
                var theValue = theElement.val();
                dump[counter] = theValue;

                counter++;
            });
            return dump;
        }


    });
</script>
