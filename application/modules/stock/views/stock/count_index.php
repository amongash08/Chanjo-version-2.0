<div class="row">
    <?php echo form_open('stock/count', array('class' => "form-horizontal", 'id' => "count")); ?>
    <div class="col-sm-12">
        <div class="box box-solid">
            <div class="box-header with-border">
                <h3 class="box-title">Vaccine Details</h3>
            </div>

            <div class="box-body">
                <div class="row">
                    <div class="col-sm-3">
                        <?php $records = 0;
                        $interval = 0;
                        $date_next_count = 5;
                        if ($records == 0) {
                            echo '<div class="count" style="color:blue">' . 0 . ' Days <i class="fa fa-clock-o icon-margin"></i></div>
           <h4>To Next Count</h4><span style="color:red">Please Conduct Count.</span>';
                        }
                        if ($interval > 31) {
                            echo '<div class="count" style="color:red">' . $interval . ' Days <i class="fa fa-clock-o icon-margin"></i></div>
           <h4>Overdue for Stock Count</h4><span >You are overdue for a stock count.Download Count Sheet below</span>';
                        }
                        if ($interval <= 31 && $records != 0) {
                            echo '<div class="count" style="color:green">' . $interval . ' Days <i class="fa fa-clock-o icon-margin"></i></div>
           <h4>To Next Stock Count</h4><span >You are due for a stock count on ' . $date_next_count .
                                '</span>';
                        }

                        ?>
                    </div>
                    <div class="col-sm-3 pull-right">
                        <a href="<?php echo site_url('stock/print_count_sheet'); ?>" target="_blank"
                           class="btn btn-default margin pull-right"><i class="fa fa-print"></i> Print Count Sheet</a>
                    </div>
                </div>
                <hr>
                <div class="table-responsive" id="table">
                    <table class="table table-bordered table-hover table-striped">
                        <thead>
                        <tr align="center" class="form-group-sm">
                            <th style="width:12%;">Date of <br>Count</th>
                            <th style="width:13%;">Vaccine /<br>Diluents</th>
                            <th style="width:15%;">Batch <br>Number</th>
                            <th style="width:12%;">Expiry <br>Date</th>
                            <th style="width:13%;">Stock <br>Quantity</th>
                            <th style="width:14%;">Physical <br>Count</th>
                            <th style="width:13%;">VVM <br>Status</th>
                            <th style="width:8%;">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr row="0" class="form-group-sm">
                            <td>
                                <input type="text" class="form-control count_date" id="count_date" name="count_date" placeholder="YYYY-MM-DD">
                            </td>
                            <td>
                                <?php echo form_dropdown($vaccine); ?>
                            </td>
                            <td>
                                <input type="text" class="form-control batch_no" id="batch_no" name="batch_no" disabled>
                            </td>
                            <td>
                                <input type="text" class="form-control expiry_date" id="expiry_date" name="expiry_date"
                                       disabled>
                            </td>
                            <td>
                                <input type="text" class="form-control stock_quantity" id="stock_quantity"
                                       name="stock_quantity" disabled>
                            </td>
                            <td>
                                <input type="number" class="form-control physical_count" id="physical_count"
                                       name="physical_count">
                            </td>
                            <td>
                                <?php echo form_dropdown($vvm); ?>
                            </td>
                            <td class="small">
                                <a class="add btn-xs" style="cursor: pointer; cursor: hand;"><span
                                            class="label label-success"><i
                                                class="fa fa-plus-square"></i> <b>ADD</b></span></a><br>
                                <a class="remove btn-xs" style="cursor: pointer; cursor: hand;"><span
                                            class="label label-danger"><i
                                                class="fa fa-minus-square"></i> <b>REMOVE</b></span></a>
                            </td>
                        </tr>
                        <tr id="template" class="form-group-sm" hidden>
                            <td>
                                <input type="text" class="form-control count_date" id="count_date" name="count_date"
                                       placeholder="YYYY-MM-DD" disabled>
                            </td>
                            <td>
                                <?php echo form_dropdown($vaccine, '', '', 'disabled'); ?>
                            </td>
                            <td>
                                <input type="text" class="form-control batch_no" id="batch_no" name="batch_no" disabled>
                            </td>
                            <td>
                                <input type="text" class="form-control expiry_date" id="expiry_date" name="expiry_date"
                                       disabled>
                            </td>
                            <td>
                                <input type="text" class="form-control stock_quantity" id="stock_quantity"
                                       name="stock_quantity" disabled>
                            </td>
                            <td>
                                <input type="number" class="form-control physical_count" id="physical_count"
                                       name="physical_count" disabled>
                            </td>
                            <td>
                                <?php echo form_dropdown($vvm, '', '', 'disabled'); ?>
                            </td>
                            <td class="small">
                                <a class="add btn-xs" style="cursor: pointer; cursor: hand;"><span
                                            class="label label-success"><i
                                                class="fa fa-plus-square"></i> <b>ADD</b></span></a><br>
                                <a class="remove btn-xs" style="cursor: pointer; cursor: hand;"><span
                                            class="label label-danger"><i
                                                class="fa fa-minus-square"></i> <b>REMOVE</b></span></a>
                            </td>
                        </tr>

                        </tbody>
                    </table>
                </div>
            </div>
            <div class="box-footer">
                <button type="submit" id="validate" class="btn bg-navy margin">
                    Submit
                </button>
            </div>
        </div>
    </div>
    <?php echo form_close(); ?>
</div>

<script>
    $(document).ready(function () {

        $('#count')
            .formValidation({
                framework: 'bootstrap',
                err: {
                    container: 'tooltip'
                },
                row: {
                    selector: 'td'
                },
                // This option will not ignore invisible fields which belong to inactive panels
                excluded: ':hidden, :not(:visible)',
                fields: {
                    count_date: {
                        validators: {
                            notEmpty: {
                                message: '*required'
                            },
                            date: {
                                format: 'YYYY-MM-DD',
                                message: '*date is not valid',
                                max: moment().format('YYYY-MM-DD'),
                                min: moment('2016-06-01').format('YYYY-MM-DD')
                            }
                        }
                    },
                    vaccine: {
                        validators: {
                            notEmpty: {
                                message: '*required'
                            }
                        }
                    },
                    batch_no: {
                        validators: {
                            notEmpty: {
                                message: '*required'
                            }
                        }
                    },
                    physical_count: {
                        validators: {
                            notEmpty: {
                                message: '*required'
                            },
                            numeric: {
                                message: '*not a number'
                            },
                            greaterThan: {
                                value: 0,
                                message: '*not a valid number'
                            }
                        }
                    },
                    vvm: {
                        validators: {
                            notEmpty: {
                                message: '*required'
                            }
                        }
                    }
                }
            }).on('click', '#table .add', function () {
            var thisRow = $(this).closest('tr');
            return validateRow(thisRow);

        }).on('click', '#table .remove', function () {
            if ($('#table tbody tr').length === 2) return;
            $(this).parents("tr").fadeOut('slow', function () {
                $(this).remove();
            });

        }).on('change', '#vaccine', function () {
            var row = $(this);
            var vaccine = row.val();
            load_batches(vaccine, row);

        }).on('change', '#batch_no', function () {
            var row = $(this);
            var batch = row.val();
            load_batch_details(batch, row);

        }).on('success.form.fv', function (e) {
            // Prevent form submission
            e.preventDefault();
            swal({
                    title: "Confirm Submission",
                    text: "Are you sure you want to submit the entered details?",
                    type: "info",
                    showCancelButton: true,
                    confirmButtonColor: "#54CEF7",
                    confirmButtonText: "Submit",
                    closeOnConfirm: false,
                    showLoaderOnConfirm: true,
                },
                function (isConfirm) {
                    if (isConfirm) {
                        submit();
                    }
                });


        });

        function submit() {
            var vaccine_count = 0;
            $.each($(".vaccine"), function (i, v) {
                vaccine_count++;
            });

            var vaccines = retrieveFormValues_Array('vaccine');
            var batch_no = retrieveFormValues_Array('batch_no');
            var expiry_date = retrieveFormValues_Array('expiry_date');
            var count_date = retrieveFormValues_Array('count_date');
            var stock_quantity = retrieveFormValues_Array('stock_quantity');
            var physical_count = retrieveFormValues_Array('physical_count');
            var vvm = retrieveFormValues_Array('vvm');


            var dat = new Array();

            for (var i = 0; i < vaccine_count; i++) {
                var get_vaccine = vaccines[i];
                var get_batch = batch_no[i];
                var get_expiry = expiry_date[i];
                var get_date = count_date[i];
                var get_quantity = stock_quantity[i];
                var get_count = physical_count[i];
                var get_vvm = vvm[i];


                data = {
                    "vaccine_id": get_vaccine,
                    "batch_no": get_batch,
                    "count_date": get_date,
                    "stock_quantity": get_quantity,
                    "physical_count": get_count,
                    "expiry_date": get_expiry,
                    "vvm": get_vvm
                };
                dat.push(data);
            }
            batch = JSON.stringify(dat);
            $.ajax({
                url: $('#count').attr('action'),
                type: "POST",
                data: {
                    "batch": batch
                },
                beforeSend: function () {
                    $('#validate').fadeOut(300, function () {
                        $(this).remove();
                    });
                },

                success: function (data, textStatus, jqXHR) {
                    window.location.replace('<?php echo site_url('stock/ledger'); ?>');
                },

                error: function (jqXHR, textStatus, errorThrown) {
                    //if fails
                }
            });
        }

        $('#count_date')
            .datepicker({
                dateFormat: "yy-mm-dd",
                maxDate: 0,
                beforeShow: function (textbox, instance) {
                    var txtBoxOffset = $(this).offset();
                    var top = txtBoxOffset.top;
                    var left = txtBoxOffset.left;
                    var textBoxWidth = $(this).outerWidth();
                    setTimeout(function () {
                        instance.dpDiv.css({
                            top: top - 100, //you can adjust this value accordingly
                            left: left + textBoxWidth//show at the end of textBox
                        });
                    }, 0);

                },
                onSelect: function (date, inst) {
                    /* Revalidate the field when choosing it from the datepicker */
                    $('#count').formValidation('revalidateField', 'count_date');
                }
            });

        function load_batches(vaccine, row) {

            var dropdown_start = '<select class="form-control" id="batch_no" name="batch_no">';
            var dropdown_option = new Array();
            var default_option = "<option value=''>" + "Select batch" + "</option> ";
            var dropdown_end = '</select>';
            dropdown_option.push(default_option);
            if (vaccine.length > 0) {
                var request = $.ajax({
                    url: '<?php echo site_url('stock/batch'); ?>',
                    type: 'post',
                    data: {
                        "vaccine": vaccine
                    },

                });

                request.done(function (data) {
                    data = JSON.parse(data);

                    $.each(data, function (key, value) {
                        option = "<option value='" + value.batch_number + "'>" + value.batch_number + "</option> ";
                        dropdown_option.push(option);
                    });


                    row.closest("tr").find("#batch_no").replaceWith(dropdown_start + dropdown_option.join("") + dropdown_end),
                        fv = $('#count').data('formValidation');
                    fv.addField(row.closest("tr").find("#batch_no"));
                });
                request.fail(function (jqXHR, textStatus) {

                });
            } else {
                dropdown = dropdown_start + dropdown_end;
                row.closest("tr").find("#batch_no")
                    .replaceWith(dropdown),
                    fv = $('#count').data('formValidation');
                fv.addField(row.closest("tr").find("#batch_no").attr('disabled', ''));
                row.closest("tr").find("#expiry_date").val('');
                row.closest("tr").find("#stock_quantity").val('');
            }
        }

        function load_batch_details(batch, row) {

            if (batch.length > 0) {
                var request = $.ajax({
                    url: '<?php echo site_url('stock/batch_detail'); ?>',
                    type: 'post',
                    data: {
                        "batch": batch
                    },

                });

                request.done(function (data) {
                    data = JSON.parse(data);
                    $.each(data, function (key, value) {
                        row.closest("tr").find("#expiry_date").val(value.expiry_date);
                        row.closest("tr").find("#stock_quantity").val(value.balance);
                    });
                });
                request.fail(function (jqXHR, textStatus) {

                });
            } else {
                row.closest("tr").find("#expiry_date").val('');
                row.closest("tr").find("#stock_quantity").val('');
            }
        }

        function validateRow(thisRow) {
            var fv = $('#count').data('formValidation'), // FormValidation instance
                // The current row
                $row = thisRow;
            // Validate the container
            fv.validateContainer($row);


            var isValidStep = fv.isValidContainer($row);
            if (isValidStep === false || isValidStep === null) {
                // Do not add row
                return false;
            }

            addRow($row);
        }

        function addRow(thisRow) {

            var template = $('#template');
            var cloned_object = template.clone().removeAttr('hidden').removeAttr('id');

            var count_row = thisRow.attr("row");
            var row_index = parseInt(count_row) + 1;
            var new_row = cloned_object.attr("row", row_index);


            new_row.insertAfter(thisRow).find('input').val('');

            var count_date = new_row.find('input[name="count_date"]').removeAttr('disabled');
            var vaccine = new_row.find('select[name="vaccine"]').removeAttr('disabled');
            var batch_no = new_row.find('input[name="batch_no"]');
            var expiry_date = new_row.find('input[name="expiry_date"]');
            var stock_quantity = new_row.find('input[name="stock_quantity"]');
            var physical_count = new_row.find('input[name="physical_count"]').removeAttr('disabled');
            var vvm = new_row.find('select[name="vvm"]').removeAttr('disabled');

            $('#count').formValidation('addField', count_date);
            count_date.removeClass('hasDatepicker')
                .attr('id', 'count_date' + row_index)
                .datepicker({
                    dateFormat: "yy-mm-dd",
                    maxDate: 0,
                    beforeShow: function (textbox, instance) {
                        var txtBoxOffset = $(this).offset();
                        var top = txtBoxOffset.top;
                        var left = txtBoxOffset.left;
                        var textBoxWidth = $(this).outerWidth();
                        setTimeout(function () {
                            instance.dpDiv.css({
                                top: top - 100, //you can adjust this value accordingly
                                left: left + textBoxWidth//show at the end of textBox
                            });
                        }, 0);

                    },
                    onSelect: function (date, inst) {
                        /* Revalidate the field when choosing it from the datepicker */
                        $('#count').formValidation('revalidateField', 'count_date');
                    }
                });

            $('#count').formValidation('addField', vaccine);
            $('#count').formValidation('addField', batch_no);
            $('#count').formValidation('addField', expiry_date);
            $('#count').formValidation('addField', stock_quantity);
            $('#count').formValidation('addField', physical_count);
            $('#count').formValidation('addField', vvm);

            new_row.slideDown();
        }

        function retrieveFormValues(name) {
            var dump;
            $.each($("input[name=" + name + "], select[name=" + name + "]"), function (i, v) {
                var theTag = v.tagName;
                var theElement = $(v);
                var theValue = theElement.val();
                dump = theValue;
            });
            return dump;
        }

        function retrieveFormValues_Array(name) {
            var dump = new Array();
            var counter = 0;
            $.each($("input[name=" + name + "], select[name=" + name + "]"), function (i, v) {
                var theTag = v.tagName;
                var theElement = $(v);
                var theValue = theElement.val();
                dump[counter] = theValue;

                counter++;
            });
            return dump;
        }


    });
</script>
