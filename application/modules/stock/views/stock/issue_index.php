<div class="row">
    <div class="box-body">
        <div class="col-sm-12">
            <div class="btn-group">
                <button class="btn btn-primary" type="button">Issue Stocks</button>
                <button data-toggle="dropdown" class="btn btn-primary dropdown-toggle" type="button">
                    <span class="caret"></span>
                    <span class="sr-only">Toggle Dropdown</span>
                </button>
                <ul role="menu" class="dropdown-menu">
                    <li><a href="<?php echo site_url('stock/issue_single'); ?>">Single Location</a></li>
                    <li><a href="<?php echo site_url('stock/issue_multiple'); ?>">Multiple Locations</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <div class="panel default blue_title">
            <div class="panel-body">
                <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#tab1"><b>Orders to me</b></a>
                    </li>

                    <li><a data-toggle="tab" href="#tab2"><b>Order History</b></a>
                    </li>
                </ul>
                <div class="tab-content">

                    <!--Listing Placed Orders-->


                    <div id="tab1" class="tab-pane fade in active">
                        <table class="table table-striped table-hover" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th style="width:120px;">Order Number</th>
                                <th style="">From</th>
                                <th>Date Created</th>
                                <th>Status</th>
                                <th class="center">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>

                    </div>
                    <div id="tab2" class="tab-pane fade">
                        <table class="table table-striped table-hover" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th style="width:120px;">Order Number</th>
                                <th style="">From</th>
                                <th>Date Created</th>
                                <th>Status</th>
                                <th class="center">Action</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        // swal("System Update", "Issuing directly to lower levels will not be allowed after 30th of December 2017. Kindly have your lower levels update their system and place orders from there")
    });
</script>