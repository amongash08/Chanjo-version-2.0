<?php echo form_open('stock/issue_multiple', array('class' => "form-horizontal", 'id' => "multiple")); ?>
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Transaction Details</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                class="fa fa-minus"></i>
                    </button>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-lg-3">
                            <div class="form-group-sm">
                                <label>Vaccine/Diluent</label>
                                <br>
                                <?php echo form_dropdown($vaccine); ?>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group-sm">
                                <label>Stock Quantity</label>
                                <br>
                                <input type="text" class="form-control" name="current_quantity" id="current_quantity"
                                       disabled>
                            </div>
                        </div>

                        <div class="col-lg-3">
                            <div class="form-group-sm">
                                <label>Allocated Quantity</label>
                                <br>
                                <input type="text" class="form-control" name="allocated_quantity"
                                       id="allocated_quantity"
                                       disabled>
                            </div>
                        </div>

                        <div class="col-lg-3">
                            <div class="form-group-sm">
                                <label>Remaining Quantity</label>
                                <br>
                                <input type="text" class="form-control" name="remaining_quantity"
                                       id="remaining_quantity"
                                       disabled>
                            </div>
                        </div>
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- ./box-body -->
        </div>
        <!-- /.box -->
    </div>
    <!-- /.col -->
</div>
<!-- /.row -->
<div class="row">
    <!-- /.box-header -->
    <div class="box-body">
        <div class="box-group" id="">
            <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
            <div class="panel box box-primary">
                <div class="box-header with-border">
                    <h4 class="box-title">
                        Vaccine Details
                    </h4>

                </div>

                <div class="box-body">
                    <div class="table-responsive" id="table">
                        <table class="table table-bordered table-hover table-striped">
                            <thead>
                            <tr align="center" class="form-group-sm">
                                <th style="width:13%;">Date Issued</th>
                                <th style="width:13%;">Location</th>
                                <th style="width:15%;">Batch Number</th>
                                <th style="width:12%;">Expiry Date</th>
                                <th style="width:13%;">Stock Quantity</th>
                                <th style="width:13%;">Quantity Issued</th>
                                <th style="width:13%;">VVM Status</th>
                                <th style="width:8%;">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr row="0" class="form-group-sm">
                                <td>
                                    <input type="text" class="form-control issue_date" id="issue_date"
                                           name="issue_date" placeholder="YYYY-MM-DD">
                                </td>
                                <td>
                                    <?php echo form_dropdown($location); ?>
                                </td>
                                <td>
                                    <input type="text" class="form-control batch_no" id="batch_no" name="batch_no"
                                           disabled>
                                </td>
                                <td>
                                    <input type="text" class="form-control expiry_date" id="expiry_date"
                                           name="expiry_date"
                                           disabled>
                                </td>
                                <td>
                                    <input type="text" class="form-control stock_quantity" id="stock_quantity"
                                           name="stock_quantity" disabled>
                                </td>
                                <td>
                                    <input type="text" class="form-control issue_quantity" id="issue_quantity"
                                           name="issue_quantity">
                                </td>
                                <td>
                                    <?php echo form_dropdown($vvm); ?>
                                </td>
                                <td class="small">
                                    <a class="add btn-xs" style="cursor: pointer; cursor: hand;"><span
                                                class="label label-success"><i
                                                    class="fa fa-plus-square"></i> <b>ADD</b></span></a><br>
                                    <a class="remove btn-xs" style="cursor: pointer; cursor: hand;"><span
                                                class="label label-danger"><i
                                                    class="fa fa-minus-square"></i> <b>REMOVE</b></span></a>
                                </td>
                            </tr>
                            <tr id="template" class="form-group-sm" hidden>
                                <td>
                                    <input type="text" class="form-control issue_date" id="issue_date"
                                           name="issue_date" placeholder="YYYY-MM-DD"
                                           disabled>
                                </td>
                                <td>
                                    <?php echo form_dropdown($location, '', '', 'disabled'); ?>
                                </td>
                                <td>
                                    <input type="text" class="form-control batch_no" id="batch_no" name="batch_no"
                                           disabled>
                                </td>
                                <td>
                                    <input type="text" class="form-control expiry_date" id="expiry_date"
                                           name="expiry_date"
                                           disabled>
                                </td>
                                <td>
                                    <input type="text" class="form-control stock_quantity" id="stock_quantity"
                                           name="stock_quantity" disabled>
                                </td>
                                <td>
                                    <input type="text" class="form-control issue_quantity" id="issue_quantity"
                                           name="issue_quantity" disabled>
                                </td>

                                <td>
                                    <?php echo form_dropdown($vvm, '', '', 'disabled'); ?>
                                </td>
                                <td class="small">
                                    <a class="add btn-xs" style="cursor: pointer; cursor: hand;"><span
                                                class="label label-success"><i
                                                    class="fa fa-plus-square"></i> <b>ADD</b></span></a><br>
                                    <a class="remove btn-xs" style="cursor: pointer; cursor: hand;"><span
                                                class="label label-danger"><i
                                                    class="fa fa-minus-square"></i> <b>REMOVE</b></span></a>
                                </td>
                            </tr>

                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="box-footer">
                    <button type="submit" id="validate" class="btn bg-navy margin">
                        Submit
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
<?php echo form_close(); ?>
<script>
    $(document).ready(function () {

        $('#multiple')
            .formValidation({
                framework: 'bootstrap',
                err: {
                    container: 'tooltip'
                },
                row: {
                    selector: 'td'
                },
                // This option will not ignore invisible fields which belong to inactive panels
                excluded: ':hidden, :not(:visible)',
                fields: {
                    vaccine: {
                        row: '.form-group-sm',
                        validators: {
                            notEmpty: {
                                message: '*required'
                            }
                        }
                    },
                    location: {
                        validators: {
                            notEmpty: {
                                message: '*required'
                            }
                        }
                    },
                    issue_date: {
                        validators: {
                            notEmpty: {
                                message: '*required'
                            },
                            date: {
                                format: 'YYYY-MM-DD',
                                message: '*date is not valid',
                                max: moment().format('YYYY-MM-DD'),
                                min: moment('2016-01-01').format('YYYY-MM-DD')
                            }
                        }
                    },
                    batch_no: {
                        validators: {
                            notEmpty: {
                                message: '*required'
                            }
                        }
                    },
                    expiry_date: {
                        validators: {
                            notEmpty: {
                                message: '*required'
                            },
                            date: {
                                format: 'YYYY-MM-DD',
                                message: '*date is not valid',
                                min: moment().format('YYYY-MM-DD'),
                            }
                        }
                    },
                    stock_quantity: {
                        validators: {
                            notEmpty: {
                                message: '*required'
                            },
                            numeric: {
                                message: '*not a number'
                            },
                            greaterThan: {
                                value: 0,
                                message: '*not a valid number'
                            }
                        }
                    },
                    issue_quantity: {
                        validators: {
                            notEmpty: {
                                message: '*required'
                            },
                            numeric: {
                                message: '*not a number'
                            },
                            between: {
                                min: 0,
                                max: 'stock_quantity',
                                message: '*not a valid number'
                            }
                        }
                    },
                    vvm: {
                        validators: {
                            notEmpty: {
                                message: '*required'
                            }
                        }
                    }
                }
            }).on('click', '#table .add', function () {
            var thisRow = $(this).closest('tr');
            return validateRow(thisRow);

        }).on('click', '#table .remove', function () {
            if ($('#table tbody tr').length === 2) return;
            $(this).parents("tr").fadeOut('slow', function () {
                $(this).remove();
            });

        }).on('change', '#vaccine', function () {
            var vaccine = $(this).val();
            load_balance(vaccine);

        }).on('change', '#location', function () {
            var row = $(this);
            var vaccine = $('#vaccine').val();
            load_batches(vaccine, row);

        }).on('change', '#batch_no', function () {
            var row = $(this);
            var batch = row.val();
            load_batch_details(batch, row);

        }).on('change', '#issue_quantity', function () {
            var totals = 0;
            var table = $("#multiple");
            table.each(function () {
                $(this).find('#issue_quantity').each(function () {
                    totals += parseInt($(this).val());
                });
            });
            $('#allocated_quantity').each(function () {
                if (totals > 0 && !isNaN(totals)) {
                    $(this).val(totals);
                } else {
                    $(this).val('');
                }
            });
            $("#remaining_quantity").each(function () {
                var total = $("#current_quantity").val();
                var allocated = $("#allocated_quantity").val();
                var remainder = total - allocated;
                if (remainder > 0 && !isNaN(remainder)) {
                    $(this).val(remainder);
                }
                if (remainder < 0) {
                    $(this).val('');
                } else {
                    $(this).val(remainder);
                }
            });

        }).on('success.form.fv', function (e) {
            // Prevent form submission
            e.preventDefault();
            swal({
                    title: "Confirm Submission",
                    text: "Are you sure you want to submit the entered details?",
                    type: "info",
                    showCancelButton: true,
                    confirmButtonColor: "#54CEF7",
                    confirmButtonText: "Submit",
                    closeOnConfirm: false,
                    showLoaderOnConfirm: true,
                },
                function (isConfirm) {
                    if (isConfirm) {
                        submit();
                    } else {
                        return false;
                    }
                });

        });

        function submit() {

            var location_count = 0;
            $.each($(".location"), function (i, v) {
                location_count++;
            });

            var vaccine = retrieveFormValues('vaccine');
            var issue_date = retrieveFormValues_Array('issue_date');
            var location = retrieveFormValues_Array('location');
            var batch_no = retrieveFormValues_Array('batch_no');
            var expiry_date = retrieveFormValues_Array('expiry_date');
            var vvm = retrieveFormValues_Array('vvm');
            var issue_quantity = retrieveFormValues_Array('issue_quantity');
            var dat = new Array();
            for (var i = 0; i < location_count; i++) {
                var data = new Array();
                var get_issue_date = issue_date[i];
                var get_location = location[i];
                var get_batch = batch_no[i];
                var get_expiry = expiry_date[i];
                var get_issue_quantity = issue_quantity[i];
                var get_vvm = vvm[i];
                data = {
                    "vaccine": vaccine,
                    "issue_date": get_issue_date,
                    "location": get_location,
                    "batch_no": get_batch,
                    "expiry_date": get_expiry,
                    "issue_quantity": get_issue_quantity,
                    "vvm": get_vvm
                };
                dat.push(data);
            }


            batch = JSON.stringify(dat);
            $.ajax({
                url: $('#multiple').attr('action'),
                type: "POST",
                data: {
                    "batch": batch
                },
                beforeSend: function () {
                    $('#validate').fadeOut(300, function () {
                        $(this).remove();
                    });
                },

                success: function (data, textStatus, jqXHR) {
                    window.location.replace('<?php echo site_url('stock/ledger');?>');
                },

                error: function (jqXHR, textStatus, errorThrown) {
                    //if fails
                }
            });
        }

        $('#issue_date')
            .datepicker({
                dateFormat: "yy-mm-dd",
                maxDate: 0,
                beforeShow: function (textbox, instance) {
                    var txtBoxOffset = $(this).offset();
                    var top = txtBoxOffset.top;
                    var left = txtBoxOffset.left;
                    var textBoxWidth = $(this).outerWidth();
                    setTimeout(function () {
                        instance.dpDiv.css({
                            top: top - 100, //you can adjust this value accordingly
                            left: left + textBoxWidth//show at the end of textBox
                        });
                    }, 0);

                },
                onSelect: function (date, inst) {
                    /* Revalidate the field when choosing it from the datepicker */
                    $('#multiple').formValidation('revalidateField', 'issue_date');
                }
            });

        function load_balance(vaccine) {
            if (vaccine.length > 0) {
                var request = $.ajax({
                    url: '<?php echo site_url('stock/get_stock_balance'); ?>',
                    type: 'post',
                    data: {
                        "vaccine": vaccine
                    },
                });
                request.done(function (data) {
                    data = JSON.parse(data);
                    $('#current_quantity').val(data.stock_balance);
                });
                request.fail(function (jqXHR, textStatus) {

                });
            }
        }

        function load_batches(vaccine, row) {

            var dropdown_start = '<select class="form-control" id="batch_no" name="batch_no">';
            var dropdown_option = new Array();
            var default_option = "<option value=''>" + "Select batch" + "</option> ";
            var dropdown_end = '</select>';
            dropdown_option.push(default_option);
            if (vaccine.length > 0) {
                var request = $.ajax({
                    url: '<?php echo site_url('stock/batch'); ?>',
                    type: 'post',
                    data: {
                        "vaccine": vaccine
                    },

                });

                request.done(function (data) {
                    data = JSON.parse(data);

                    $.each(data, function (key, value) {
                        option = "<option value='" + value.batch_number + "'>" + value.batch_number + "</option> ";
                        dropdown_option.push(option);
                    });


                    row.closest("tr").find("#batch_no").replaceWith(dropdown_start + dropdown_option.join("") + dropdown_end),
                        fv = $('#multiple').data('formValidation');
                    fv.addField(row.closest("tr").find("#batch_no"));
                });
                request.fail(function (jqXHR, textStatus) {

                });
            } else {
                dropdown = dropdown_start + dropdown_end;
                row.closest("tr").find("#batch_no")
                    .replaceWith(dropdown),
                    fv = $('#multiple').data('formValidation');
                fv.addField(row.closest("tr").find("#batch_no").attr('disabled', ''));
                row.closest("tr").find("#expiry_date").val('');
                row.closest("tr").find("#stock_quantity").val('');
            }
        }


        function load_batch_details(batch, row) {

            if (batch.length > 0) {
                var request = $.ajax({
                    url: '<?php echo site_url('stock/batch_detail'); ?>',
                    type: 'post',
                    data: {
                        "batch": batch
                    },

                });

                request.done(function (data) {
                    data = JSON.parse(data);
                    $.each(data, function (key, value) {
                        row.closest("tr").find("#expiry_date").val(value.expiry_date);
                        row.closest("tr").find("#stock_quantity").val(value.balance);
                        $('#multiple').formValidation('revalidateField', 'expiry_date');
                        $('#multiple').formValidation('revalidateField', 'stock_quantity');
                    });
                });
                request.fail(function (jqXHR, textStatus) {

                });
            } else {
                row.closest("tr").find("#expiry_date").val('');
                row.closest("tr").find("#stock_quantity").val('');
            }
        }

        function validateRow(thisRow) {
            var fv = $('#multiple').data('formValidation'), // FormValidation instance
                // The current row
                $row = thisRow;
            // Validate the container
            fv.validateContainer($row);
            fv.validateField('vaccine');


            var isValidStep = fv.isValidContainer($row);
            if (isValidStep === false || isValidStep === null) {
                // Do not add row
                return false;
            }

            addRow($row);
        }

        function addRow(thisRow) {

            var template = $('#template');
            var cloned_object = template.clone().removeAttr('hidden').removeAttr('id');

            var multiple_row = thisRow.attr("row");
            var row_index = parseInt(multiple_row) + 1;
            var new_row = cloned_object.attr("row", row_index);


            new_row.insertBefore(template);

            var issue_date = new_row.find('input[name="issue_date"]').removeAttr('disabled');
            var location = new_row.find('select[name="location"]').removeAttr('disabled');
            var batch_no = new_row.find('input[name="batch_no"]');
            var expiry_date = new_row.find('input[name="expiry_date"]');
            var stock_quantity = new_row.find('input[name="stock_quantity"]');
            var issue_quantity = new_row.find('input[name="issue_quantity"]').removeAttr('disabled');
            var vvm = new_row.find('select[name="vvm"]').removeAttr('disabled');

            $('#multiple').formValidation('addField', issue_date);
            issue_date.removeClass('hasDatepicker')
                .attr('id', 'issue_date' + row_index)
                .datepicker({
                    dateFormat: "yy-mm-dd",
                    maxDate: 0,
                    beforeShow: function (textbox, instance) {
                        var txtBoxOffset = $(this).offset();
                        var top = txtBoxOffset.top;
                        var left = txtBoxOffset.left;
                        var textBoxWidth = $(this).outerWidth();
                        setTimeout(function () {
                            instance.dpDiv.css({
                                top: top - 100, //you can adjust this value accordingly
                                left: left + textBoxWidth//show at the end of textBox
                            });
                        }, 0);

                    },
                    onSelect: function (date, inst) {
                        /* Revalidate the field when choosing it from the datepicker */
                        $('#multiple').formValidation('revalidateField', 'issue_date');
                    }
                });

            $('#multiple').formValidation('addField', location);
            $('#multiple').formValidation('addField', batch_no);
            $('#multiple').formValidation('addField', expiry_date);
            $('#multiple').formValidation('addField', stock_quantity);
            $('#multiple').formValidation('addField', issue_quantity);
            $('#multiple').formValidation('addField', vvm);

            new_row.slideDown();
        }


        function retrieveFormValues(name) {
            var dump;
            $.each($("input[name=" + name + "], select[name=" + name + "]"), function (i, v) {
                var theTag = v.tagName;
                var theElement = $(v);
                var theValue = theElement.val();
                dump = theValue;
            });
            return dump;
        }

        function retrieveFormValues_Array(name) {
            var dump = new Array();
            var counter = 0;
            $.each($("input[name=" + name + "], select[name=" + name + "]"), function (i, v) {
                var theTag = v.tagName;
                var theElement = $(v);
                var theValue = theElement.val();
                dump[counter] = theValue;

                counter++;
            });
            return dump;
        }

    });
</script>
