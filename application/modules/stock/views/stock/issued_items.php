<!-- Main content -->
<section class="invoice">
    <!-- Table row -->
    <div class="row">
        <div class="col-xs-12 table-responsive">
            <table class="table table-striped table-condensed table-hover">
                <thead>
                <tr>
                    <th style="width:13%;">Vaccine /<br>Diluents</th>
                    <th style="width:13%;">Batch <br>Number</th>
                    <th style="width:13%;">Expiry <br>Date</th>
                    <th style="width:12%;">Amount <br>Issued</th>
                    <th style="width:13%;">VVM <br>Status</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($items as $key => $value): ?>
                    <tr>
                        <td><?php echo $value->vaccine_name; ?></td>
                        <td><?php echo $value->batch; ?></td>
                        <td><?php echo $value->expiry; ?></td>
                        <td><?php echo $value->quantity; ?></td>
                        <td>Stage <?php echo $value->vvm; ?></td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
    <hr>

</section>
<!-- /.content -->