<!-- DataTables -->
<link href="<?php echo base_url() ?>assets/plugins/datatables/datatables.css" rel="stylesheet"/>
<script src="<?php echo base_url() ?>assets/plugins/datatables/datatables.js"></script>
<script src="<?php echo base_url() ?>assets/plugins/tabledit/jquery.tabledit.js"></script>
<div class="container-fluid">
  <div class="row">

    <form id="filterLocations">

      <div class="row form-group has-feedback">

        <div class="col-xs-3">
            <select id="vaccines" name="vaccines" class=" form-control custom-select">
                <option value="0"> Select Vaccine </option>

                <?php
                foreach ($vaccines as $key => $value) {
                    $name = $value;
                    $id = $key;
                    echo "<option value='$id'>$name</option>";
                }

                ?>

            </select>

        </div>
          <div class="col-xs-3">
              <select id="regions" name="regions" class=" form-control custom-select">
                  <option value="0"> Select Region </option>

                  <?php
                  foreach ($regions as $key => $value) {
                      $name = $value->region_name;
                      $id = $value ->id;
                      echo "<option value='$id'>$name</option>";
                  }

                  ?>

              </select>

          </div>

          <div class="col-xs-4">
              <select id="subcounties" name="subcounties" class=" form-control custom-select">
                  <option value="0"> Select Sub County </option>

              </select>
              <input type="hidden" id="county" name="county" value="0" />

          </div>

          <button type="submit" class="btn btn-primary  btn-flat" id="filterByLocation"><i class="fa fa-filter"></i>  Filter</button>

      </div>
    </form>

  </div>

  <div class="row">

    <div class="" id="legerContainer" style="min-height:350px;">

      <table id="ledger" data-order='[[ 0, "desc" ]]' class="table table-striped table-hover dataTable"
             cellspacing="0" width="100%">
          <thead style="background-color: white">
          <tr>
              <th></th>
              <th>Date</th>
              <th>Type</th>
              <th>Station</th>
              <th>Quantity</th>
              <th>Batch</th>
              <th>Expiry</th>
              <th>Stock Balance</th>
          </tr>
          </thead>

      </table>

    </div>

  </div>

</div>

<script type="text/javascript">

    $( document ).ready(function() {

      $('#regions').on('change', function(){
        $("#county").val(0);

        var drop_down='';
        var region_id = $("#regions").val();

        var subcounty_select = "<?php echo base_url(); ?>subcounty/getscbyregionjson/"+region_id;

        $.getJSON( subcounty_select ,function( json ) {
          $("#subcounties").html('<option value="0" selected="selected">Select Sub Counties</option>');
            $.each(json, function( key, val ) {
              // console.log(val['id']);
              drop_down +="<option value='"+val['id']+"' data-county='"+val['county_id']+"'>"+val['subcounty_name']+"</option>";
          });

            $("#subcounties").append(drop_down);


          });
      });

    $('#subcounties').on('change', function(){

      if($("#subcounties").val() == 0){

       $("#county").val(0);

      }else  {

        $("#county").val($("option:selected", this).attr('data-county'));

      }

      var county = $("#county").val()

      });

      $( "#filterByLocation" ).click(function() {

         event.preventDefault(); // avoid to execute the actual submit of the form.

        var formData ='#filterLocations';
        var msg ='Successfully';
        var div = '#legerContainer';
        var county = $("#county").val();
        var region = $("#regions").val();
        var vaccines = $("#vaccines").val();
        var subcounty = $("#subcounties").val();

        var url="<?php echo base_url(); ?>stock/filter_stock_data/"+county+'/'+region+'/'+vaccines+'/'+subcounty; // the script where you handle the form input.


        if ( $.fn.DataTable.isDataTable('#ledger') ) {
            $('#ledger').DataTable().destroy();
          }


        editor = new $.fn.dataTable.Editor({
            table: "#ledger"
        });
        $.fn.dataTable.ext.buttons.reload = {
            text: 'Recalculate',
            action: function (e, dt, node, config) {
                $.ajax({
                    url: "<?php echo base_url('stock/calculate/' . $id) ?>",
                    type: "GET",
                    success: function (data, textStatus, jqXHR) {
                        dt = $('#ledger').DataTable();
                        dt.ajax.reload();
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                    }
                });
            }
        };


        var table = $('#ledger').DataTable({
            dom: "Bfrtip",
            scrollY: 300,
            paging: false,
            searching: false,
            ajax: {
                url: url ,
                type: 'GET'
            },
            columns: [{
                data: null,
                defaultContent: '',
                className: 'select-checkbox',
                orderable: false
            }, {
                data: "date"
            }, {
                data: "type"
            }, {
                data: "to_from"
            }, {
                data: "quantity"
            }, {
                data: "batch"
            }, {
                data: "expiry"
            }, {
                data: "balance",
                render: $.fn.dataTable.render.number(',', '.', 0)
            }],
            order: [
                [1, "desc"]
            ],
            select: {
                style: 'multi',
                selector: 'td:first-child'
            },
            buttons: [{
                extend: 'remove',
                editor: editor,
                action: function (e, dt, button, config) {
                    var selected = dt.row({selected: true}).data();
                    swal({
                            title: "Are you sure?",
                            text: "You will not be able to undo this action!",
                            type: "warning",
                            showCancelButton: true,
                            confirmButtonText: "Yes",
                            cancelButtonText: "Cancel",
                            closeOnConfirm: false,
                            closeOnCancel: true,
                            showLoaderOnConfirm: true
                        },
                        function (isConfirm) {
                            if (isConfirm) {
                                table.row('.selected').remove().draw(false);
                                remove(selected);
                            }
                        });
                }
            }, {
                extend: "reload"
            }, {
                extend: 'collection',
                text: 'Export',
                buttons: [
                    'copyHtml5',
                    'excelHtml5',
                    'csvHtml5'
                ]
            }],
            fnRowCallback: function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                if (aData['type'] == "Issue") {
                    $('td', nRow).css('color', 'Blue');
                } else if (aData['type'] == "Receive") {
                    $('td', nRow).css('color', 'Red');
                } else if (aData['type'] == "Physical Count") {
                    $('td', nRow).css('color', 'black');
                } else if (aData['type'] == "Adjustment") {
                    $('td', nRow).css('color', 'black');
                }
            }
        });


      });

  });

    </script>
