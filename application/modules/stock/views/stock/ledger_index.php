<?php if ($level_id!=4){?>
<div class="row" style="margin-bottom: 1%;">
    <div class="col-md-4">
        <button type="button" class="btn btn-block bg-olive btn-flat margin btn-lg recipient">Recipient Data</button>
    </div>
    <div class="col-md-4">
        <button type="button" class="btn btn-block bg-olive btn-flat margin btn-lg arrivals">Arrivals</button>
    </div>
    <div class="col-md-4">
        <button type="button" class="btn btn-block bg-olive btn-flat margin btn-lg issue">Issue Data</button>
    </div>
</div>
<?php }?>

<div class="row">

    <div class="col-md-12">
        <div class="box">
            <div class="box-body">
                <div class="col-lg-12">
                    <div class="table-responsive">

                        <table class="table table table-bordered table-hover table-striped">
                            <thead>

                            <tr>
                                <th>Vaccine/Diluents</th>
                                <th>Vaccine Formulation</th>
                                <th>Mode Of Administration</th>
                                <th>Action</th>
                            </tr>
                            </thead>

                            <tbody>

                            <tr>
                                <td>BCG</td>
                                <td>Liquid</td>

                                <td>Intradermal Injection</td>
                                <td align="center"><a class="btn btn-success btn-xs"
                                                      href="<?php echo site_url('stock/view_ledger'); ?>/2">
                                        view vaccine ledger <i class="fa  fa-book"></i> </a></td>

                            </tr>
                            <tr>
                                <td>BCG-Diluent</td>
                                <td>Liquid</td>

                                <td>Intradermal Injection</td>
                                <td align="center"><a class="btn btn-success btn-xs"
                                                      href="<?php echo site_url('stock/view_ledger'); ?>/5">
                                        view vaccine ledger <i class="fa  fa-book"></i> </a></td>

                            </tr>
                            <tr>
                                <td>DPT-Hib-HepB</td>
                                <td>Liquid</td>

                                <td>Intramuscular Injection</td>
                                <td align="center"><a class="btn btn-success btn-xs"
                                                      href="<?php echo site_url('stock/view_ledger'); ?>/10">
                                        view vaccine ledger <i class="fa  fa-book"></i> </a></td>

                            </tr>
                            <tr>
                                <td>IPV</td>
                                <td>Liquid</td>

                                <td>Intramuscular Injection</td>
                                <td align="center"><a class="btn btn-success btn-xs"
                                                      href="<?php echo site_url('stock/view_ledger'); ?>/6">
                                        view vaccine ledger <i class="fa  fa-book"></i> </a></td>

                            </tr>
                            <tr>
                                <td>Measles</td>
                                <td>Powder</td>

                                <td>Intramuscular Injection</td>
                                <td align="center"><a class="btn btn-success btn-xs"
                                                      href="<?php echo site_url('stock/view_ledger'); ?>/7">
                                        view vaccine ledger <i class="fa  fa-book"></i> </a></td>

                            </tr>
                            <tr>
                                <td>Measles Rubella</td>
                                <td>Powder</td>

                                <td>Intramuscular Injection</td>
                                <td align="center"><a class="btn btn-success btn-xs"
                                                      href="<?php echo site_url('stock/view_ledger'); ?>/13">
                                        view vaccine ledger <i class="fa  fa-book"></i> </a></td>

                            </tr>
                            <tr>
                                <td>Measles Rubella Diluent</td>
                                <td>Liquid</td>

                                <td>Intradermal Injection</td>
                                <td align="center"><a class="btn btn-success btn-xs"
                                                      href="<?php echo site_url('stock/view_ledger'); ?>/14">
                                        view vaccine ledger <i class="fa  fa-book"></i> </a></td>

                            </tr>
                            <tr>
                                <td>Measles-Diluent</td>
                                <td>Liquid</td>

                                <td>Intradermal Injection</td>
                                <td align="center"><a class="btn btn-success btn-xs"
                                                      href="<?php echo site_url('stock/view_ledger'); ?>/11">
                                        view vaccine ledger <i class="fa  fa-book"></i> </a></td>

                            </tr>
                            <tr>
                                <td>OPV</td>
                                <td>Liquid</td>

                                <td>Oral Administration</td>
                                <td align="center"><a class="btn btn-success btn-xs"
                                                      href="<?php echo site_url('stock/view_ledger'); ?>/4">
                                        view vaccine ledger <i class="fa  fa-book"></i> </a></td>

                            </tr>
                            <tr>
                                <td>OPV Droppers</td>
                                <td></td>

                                <td>Oral Administration</td>
                                <td align="center"><a class="btn btn-success btn-xs"
                                                      href="<?php echo site_url('stock/view_ledger'); ?>/15">
                                        view vaccine ledger <i class="fa  fa-book"></i> </a></td>

                            </tr>
                            <tr>
                                <td>PCV</td>
                                <td>Liquid</td>

                                <td>Intramuscular Injection</td>
                                <td align="center"><a class="btn btn-success btn-xs"
                                                      href="<?php echo site_url('stock/view_ledger'); ?>/9">
                                        view vaccine ledger <i class="fa  fa-book"></i> </a></td>

                            </tr>
                            <tr>
                                <td>ROTA</td>
                                <td>Liquid</td>

                                <td>Oral Administration</td>
                                <td align="center"><a class="btn btn-success btn-xs"
                                                      href="<?php echo site_url('stock/view_ledger'); ?>/1">
                                        view vaccine ledger <i class="fa  fa-book"></i> </a></td>

                            </tr>
                            <tr>
                                <td>TT</td>
                                <td>Liquid</td>

                                <td>Intramuscular Injection</td>
                                <td align="center"><a class="btn btn-success btn-xs"
                                                      href="<?php echo site_url('stock/view_ledger'); ?>/3">
                                        view vaccine ledger <i class="fa  fa-book"></i> </a></td>

                            </tr>
                            <tr>
                                <td>Yellow- Fever</td>
                                <td>Powder</td>

                                <td>Intramuscular Injection</td>
                                <td align="center"><a class="btn btn-success btn-xs"
                                                      href="<?php echo site_url('stock/view_ledger'); ?>/8">
                                        view vaccine ledger <i class="fa  fa-book"></i> </a></td>

                            </tr>
                            <tr>
                                <td>Yellow-Fever-Diluent</td>
                                <td>Liquid</td>

                                <td>Intramuscular Injection</td>
                                <td align="center"><a class="btn btn-success btn-xs"
                                                      href="<?php echo site_url('stock/view_ledger'); ?>/12">
                                        view vaccine ledger <i class="fa  fa-book"></i> </a></td>

                            </tr>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $( document ).ready(function() {

        $('.recipient').click(function(){

            var url="<?php echo base_url(); ?>reports/create_recipient_csv";
            processData(url);


        }) ;
        $('.arrivals').click(function(){

            var url="<?php echo base_url(); ?>reports/create_arrivals_csv";
            processData(url);


        }) ;
        $('.issue').click(function(){

            var url="<?php echo base_url(); ?>reports/create_issues_csv";
            processData(url);


        }) ;

        function processData(url){
            $.ajax({
                type: "POST",
                url: url,
                success: function(data,status, jqXHR)
                {
                    window.location = url;
                    swal("Your report Downloaded!", 'Your report Downloaded!', "success");//show response from the php script.
//
                   console.log(jqXHR);


                },error: function (jqXHR, status, err,data) {
//                    console.log(jqXHR);
                    toastr.error(jqXHR.responseText)
                }
            });

        }



    });

</script>