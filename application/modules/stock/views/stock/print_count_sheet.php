<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo $title; ?></title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/ionicons/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/AdminLTE.min.css">
</head>
<body onload="window.print();">

<!-- Main content -->
<section class="invoice">
    <!-- title row -->
    <div class="row">
        <div class="col-sm-12">

            <img class="pull-left" width="90" height=""
                 src="<?php echo base_url() ?>assets/images/logo.png">
            <div class="col-sm-7">
                <div class="margin">
                    <p style="padding-top:14px;font-size:1.6em;font-weight:600;margin:auto">
                        Ministry of Health.
                    </p>
                    <p style="font-size:1.2em;font-weight:600">
                        National Vaccines &amp; Immunization Programme.
                    </p>
                </div>

            </div>
        </div>
        <!-- /.col -->
    </div>
    <hr>

    <!-- Table row -->
    <div class="row">
        <div class="col-xs-12 table-responsive">
            <table class="table table-striped table-condensed table-bordered">
                <thead>
                <tr>
                    <th>Vaccine Name</th>
                    <th>Batch</th>
                    <th>Expiry</th>
                    <th>Stock Balance</th>
                    <th>Current Stock Count</th>
                    <th>Date</th>
                    <th>Remarks</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($vaccines as $key => $vaccine) :
                    if (!empty($vaccine)):
                        foreach ($vaccine as $value):?>
                            <tr>
                                <td width="20%"><?php echo $key; ?></td>

                                <td width="10%"><?php echo $value['batch_number']; ?></td>
                                <td width="10%"><?php echo date('d M Y', strtotime($value['expiry_date'])); ?></td>
                                <td width="10%"><?php echo ($value['balance'] < 0) ? ' ' : $value['balance']; ?></td>
                                <td width="10%"></td>
                                <td width="20%"></td>
                                <td width="20%"></td>

                            </tr>
                        <?php endforeach;
                    else: ?>
                        <tr>
                            <td width="20%"><?php echo $key; ?></td>
                            <td width="10%"></td>
                            <td width="10%"></td>
                            <td width="10%"></td>
                            <td width="10%"></td>
                            <td width="20%"></td>
                            <td width="20%"></td>
                        </tr>
                    <?php endif;
                endforeach; ?>

                </tbody>
                <tfoot>
                <tr>
                    <th>Name of Store Manager</th>
                    <th></th>
                    <th>Signature</th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                </tr>
                </tfoot>
            </table>
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</body>
</html>