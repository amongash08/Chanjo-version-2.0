<div class="box">
    <div class="row">
        <div class="box-body">
            <div class="col-sm-12">
                <div class="btn-group">
                    <button class="btn btn-primary" type="button">Receive Stocks</button>
                    <button data-toggle="dropdown" class="btn btn-primary dropdown-toggle" type="button">
                        <span class="caret"></span>
                        <span class="sr-only">Toggle Dropdown</span>
                    </button>
                    <ul role="menu" class="dropdown-menu">
                        <li><a href="<?php echo site_url('stock/receive_voucher'); ?>">With voucher</a></li>
<!--                        <li><a href="--><?php //echo site_url('stock/adjust_negative'); ?><!--">Negative</a></li>-->
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
