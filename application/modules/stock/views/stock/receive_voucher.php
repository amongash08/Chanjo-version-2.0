<?php echo form_open('stock/receive_voucher', array('class' => "form-horizontal", 'id' => "receive")); ?>
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Transaction Details</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                class="fa fa-minus"></i>
                    </button>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-lg-3">
                            <div class="form-group-sm">
                                <label>Origin</label>
                                <?php echo form_dropdown($origin, '', '', 'disabled'); ?>

                            </div>
                        </div>

                        <div class="col-lg-3">
                            <div class="form-group-sm">
                                <label>Voucher #</label>
                                <br>
                                <input type="text" name="voucher" class="form-control" id="voucher">

                            </div>
                        </div>

                        <div class="col-lg-3">
                            <div class="form-group-sm">
                                <label>Date received</label>
                                <br>
                                <input type="text" class="form-control receive_date" id="receive_date"
                                       name="receive_date" placeholder="YYYY-MM-DD">
                            </div>
                        </div>
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- ./box-body -->
        </div>
        <!-- /.box -->
    </div>
    <!-- /.col -->
</div>
<!-- /.row -->
<div class="row">
    <!-- /.box-header -->
    <div class="box-body">
        <div class="box-group" id="">
            <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
            <div class="panel box box-primary">
                <div class="box-header with-border">
                    <h4 class="box-title">
                        Vaccine Details
                    </h4>

                </div>

                <div class="box-body">
                    <div class="table-responsive" id="table">
                        <table class="table table-bordered table-hover table-striped">
                            <thead>
                            <tr align="center" class="form-group-sm">
                                <th style="width:13%;">Vaccine /Diluents</th>
                                <th style="width:15%;">Batch Number</th>
                                <th style="width:12%;">Expiry Date</th>
                                <th style="width:13%;">Quantity</th>
                                <th style="width:13%;">VVM Status</th>
                                <th style="width:8%;">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr row="0" class="form-group-sm">
                                <td>
                                    <?php echo form_dropdown($vaccine); ?>
                                </td>
                                <td>
                                    <input type="text" class="form-control batch_no" id="batch_no" name="batch_no"
                                           disabled>
                                </td>
                                <td>
                                    <input type="text" class="form-control expiry_date" id="expiry_date"
                                           name="expiry_date" placeholder="YYYY-MM-DD" disabled>
                                </td>
                                <td>
                                    <input type="text" class="form-control stock_quantity" id="stock_quantity"
                                           name="stock_quantity" disabled>
                                </td>
                                <td>
                                    <?php echo form_dropdown($vvm, '', '', 'disabled'); ?>
                                </td>
                                <td class="small">
                                    <a class="add btn-xs" style="cursor: pointer; cursor: hand;"><span
                                                class="label label-success"><i
                                                    class="fa fa-plus-square"></i> <b>ADD</b></span></a><br>
                                    <a class="remove btn-xs" style="cursor: pointer; cursor: hand;"><span
                                                class="label label-danger"><i
                                                    class="fa fa-minus-square"></i> <b>REMOVE</b></span></a>
                                </td>
                            </tr>
                            <tr id="template" class="form-group-sm" hidden>
                                <td>
                                    <?php echo form_dropdown($vaccine, '', '', 'disabled'); ?>
                                </td>
                                <td>
                                    <input type="text" class="form-control batch_no" id="batch_no" name="batch_no"
                                           disabled>
                                </td>
                                <td>
                                    <input type="text" class="form-control expiry_date" id="expiry_date"
                                           name="expiry_date" placeholder="YYYY-MM-DD"
                                           disabled>
                                </td>
                                <td>
                                    <input type="text" class="form-control stock_quantity" id="stock_quantity"
                                           name="stock_quantity" disabled>
                                </td>

                                <td>
                                    <?php echo form_dropdown($vvm, '', '', 'disabled'); ?>
                                </td>
                                <td class="small">
                                    <a class="add btn-xs" style="cursor: pointer; cursor: hand;"><span
                                                class="label label-success"><i
                                                    class="fa fa-plus-square"></i> <b>ADD</b></span></a><br>
                                    <a class="remove btn-xs" style="cursor: pointer; cursor: hand;"><span
                                                class="label label-danger"><i
                                                    class="fa fa-minus-square"></i> <b>REMOVE</b></span></a>
                                </td>
                            </tr>

                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="box-footer">
                    <button type="submit" id="validate" class="btn bg-navy margin">
                        Submit
                    </button>
                </div>
            </div>
        </div>

    </div>
</div>
<?php echo form_close(); ?>
<script>
    $(document).ready(function () {
        var batches = [];

        $('#receive')
            .formValidation({
                framework: 'bootstrap',
                err: {
                    container: 'tooltip'
                },
                row: {
                    selector: 'td',
                },
                // This option will not ignore invisible fields which belong to inactive panels
                excluded: ':hidden, :not(:visible)',
                fields: {
                    origin: {
                        row: '.form-group-sm',
                        validators: {
                            notEmpty: {
                                message: '*required'
                            }
                        }
                    },
                    receive_date: {
                        row: '.form-group-sm',
                        validators: {
                            notEmpty: {
                                message: '*required'
                            },
                            date: {
                                format: 'YYYY-MM-DD',
                                message: '*date is not valid',
                                max: moment().format('YYYY-MM-DD'),
                                min: moment('2016-06-01').format('YYYY-MM-DD')
                            }
                        }
                    },
                    vaccine: {
                        validators: {
                            notEmpty: {
                                message: '*required'
                            }
                        }
                    },
                    batch_no: {
                        validators: {
                            notEmpty: {
                                message: '*required'
                            }
                        }
                    },
                    expiry_date: {
                        validators: {
                            notEmpty: {
                                message: '*required'
                            },
                            date: {
                                format: 'YYYY-MM-DD',
                                message: '*date is not valid',
                                min: moment().format('YYYY-MM-DD')
                            }
                        }
                    },
                    stock_quantity: {
                        validators: {
                            notEmpty: {
                                message: '*required'
                            },
                            numeric: {
                                message: '*not a number'
                            },
                            greaterThan: {
                                value: 0,
                                message: '*not a valid number'
                            }
                        }
                    },
                    vvm: {
                        validators: {
                            notEmpty: {
                                message: '*required'
                            }
                        }
                    }
                }
            }).on('click', '#table .add', function () {
            var thisRow = $(this).closest('tr');
            return validateRow(thisRow);

        }).on('click', '#table .remove', function () {
            if ($('#table tbody tr').length === 2) return;
            $(this).parents("tr").fadeOut('slow', function () {
                $(this).remove();
            });

        }).on('change', ' .remove', function () {
            if ($('#table tbody tr').length === 2) return;
            $(this).parents("tr").fadeOut('slow', function () {
                $(this).remove();
            });

        }).on('change', '#vaccine', function () {
            var row = $(this);
            var vaccine = row.val();
            load_batches(vaccine, row);

        }).on('change', '#batch_no', function () {
            var row = $(this);
            var batch = row.val();
            load_batch_details(batch, row);

        }).on('success.form.fv', function (e) {
            // Prevent form submission
            e.preventDefault();
            swal({
                    title: "Confirm Submission",
                    text: "Are you sure you want to submit the entered details?",
                    type: "info",
                    showCancelButton: true,
                    confirmButtonColor: "#54CEF7",
                    confirmButtonText: "Submit",
                    closeOnConfirm: false,
                    showLoaderOnConfirm: true,
                },
                function (isConfirm) {
                    if (isConfirm) {
                        submit();
                    } else {
                        return false;
                    }
                });
        });


        function submit() {

            var vaccine_count = 0;
            $.each($(".vaccine"), function (i, v) {
                vaccine_count++;
            });

            var vaccines = retrieveFormValues_Array('vaccine');
            var voucher = retrieveFormValues('voucher');
            var batch_no = retrieveFormValues_Array('batch_no');
            var expiry_date = retrieveFormValues_Array('expiry_date');
            var stock_quantity = retrieveFormValues_Array('stock_quantity');
            var vvm = retrieveFormValues_Array('vvm');
            var receive_date = retrieveFormValues('receive_date');
            var origin = retrieveFormValues('origin');

            var dat = new Array();

            for (var i = 0; i < vaccine_count; i++) {
                var data = new Array();
                var get_vaccine = vaccines[i];
                var get_batch = batch_no[i];
                var get_expiry = expiry_date[i];
                var get_stock_quantity = stock_quantity[i];
                var get_vvm = vvm[i];


                data = {
                    "vaccine_id": get_vaccine,
                    "batch_no": get_batch,
                    "expiry_date": get_expiry,
                    "stock_quantity": get_stock_quantity,
                    "vvm": get_vvm
                };
                dat.push(data);
            }

            batch = JSON.stringify(dat);
            $.ajax({
                url: $('#receive').attr('action'),
                type: "POST",
                data: {
                    "receive_date": receive_date,
                    "origin": origin,
                    "voucher": voucher,
                    "batch": batch
                },
                beforeSend: function () {
                    $('#validate').fadeOut(300, function () {
                        $(this).remove();
                    });
                },

                success: function (data, textStatus, jqXHR) {
                    window.location.replace('<?php echo site_url('stock/ledger');?>');
                },

                error: function (jqXHR, textStatus, errorThrown) {
                    //if fails
                }
            });
        }


        $('#receive_date')
            .datepicker({
                dateFormat: "yy-mm-dd",
                maxDate: 0,
                beforeShow: function (textbox, instance) {
                    var txtBoxOffset = $(this).offset();
                    var top = txtBoxOffset.top;
                    var left = txtBoxOffset.left;
                    var textBoxWidth = $(this).outerWidth();
                    setTimeout(function () {
                        instance.dpDiv.css({
                            top: top - 100, //you can adjust this value accordingly
                            left: left + textBoxWidth//show at the end of textBox
                        });
                    }, 0);

                },
                onSelect: function (date, inst) {
                    /* Revalidate the field when choosing it from the datepicker */
                    $('#receive').formValidation('revalidateField', 'receive_date');
                }
            });

        $('#expiry_date')
            .datepicker({
                dateFormat: "yy-mm-dd",
                minDate: 0,
                changeMonth: true,
                changeYear: true,
                beforeShow: function (textbox, instance) {
                    var txtBoxOffset = $(this).offset();
                    var top = txtBoxOffset.top;
                    var left = txtBoxOffset.left;
                    var textBoxWidth = $(this).outerWidth();
                    setTimeout(function () {
                        instance.dpDiv.css({
                            top: top - 100, //you can adjust this value accordingly
                            left: left + textBoxWidth//show at the end of textBox
                        });
                    }, 0);

                },
                onSelect: function (date, inst) {
                    /* Revalidate the field when choosing it from the datepicker */
                    $('#receive').formValidation('revalidateField', 'expiry_date');
                }
            });


        function load_batches(vaccine, row) {
            if (vaccine.length > 0) {
                var request = $.ajax({
                    url: '<?php echo site_url('stock/autocomplete_batch'); ?>',
                    type: 'post',
                    data: {
                        "vaccine": vaccine
                    },

                });

                request.done(function (data) {
                    row.closest('tr').find('input[name="batch_no"]').removeAttr('disabled');
                    row.closest('tr').find('input[name="expiry_date"]').removeAttr('disabled');
                    row.closest('tr').find('input[name="stock_quantity"]').removeAttr('disabled');
                    row.closest('tr').find('select[name="vvm"]').removeAttr('disabled');
                    data = JSON.parse(data);
                    if (batches.length > 0) {
                        batches.length = 0;
                    }
                    $.each(data, function (key, value) {
                        batches.push(value.batch);
                    });
                    row.closest('tr').find('input[name="batch_no"]').autocomplete({
                        source: batches
                    });
                });
                request.fail(function (jqXHR, textStatus) {

                });
            }
        }

        function load_batch_details(batch, row) {

            if (batch.length > 0) {
                var request = $.ajax({
                    url: '<?php echo site_url('stock/autocomplete_expiry'); ?>',
                    type: 'post',
                    data: {
                        "batch": batch
                    },

                });

                request.done(function (data) {
                    if (data.length > 0) {
                        data = JSON.parse(data);
                        row.closest('tr').find('.expiry_date').val(data.expiry_date);
                        $('#receive').formValidation('revalidateField', 'expiry_date');
                    }
                });
                request.fail(function (jqXHR, textStatus) {

                });
            } else {
                row.closest('tr').find('.expiry_date').val('');
            }
        }

        function validateRow(thisRow) {
            var fv = $('#receive').data('formValidation'), // FormValidation instance
                // The current row
                $row = thisRow;
            // Validate the container
            fv.validateContainer($row);


            var isValidStep = fv.isValidContainer($row);
            if (isValidStep === false || isValidStep === null) {
                // Do not add row
                return false;
            }

            addRow($row);
        }

        function addRow(thisRow) {

            var template = $('#template');
            var cloned_object = template.clone().removeAttr('hidden').removeAttr('id');

            var count_row = thisRow.attr("row");
            var row_index = parseInt(count_row) + 1;
            var new_row = cloned_object.attr("row", row_index);


            new_row.insertAfter(thisRow).find('input').val('');

            var vaccine = new_row.find('select[name="vaccine"]').removeAttr('disabled');
            var batch_no = new_row.find('input[name="batch_no"]');
            var expiry_date = new_row.find('input[name="expiry_date"]');
            var stock_quantity = new_row.find('input[name="stock_quantity"]');
            var vvm = new_row.find('select[name="vvm"]');

            $('#receive').formValidation('addField', expiry_date);
            expiry_date.removeClass('hasDatepicker')
                .attr('id', 'expiry_date' + row_index)
                .datepicker({
                    dateFormat: "yy-mm-dd",
                    minDate: 0,
                    changeMonth: true,
                    changeYear: true,
                    beforeShow: function (textbox, instance) {
                        var txtBoxOffset = $(this).offset();
                        var top = txtBoxOffset.top;
                        var left = txtBoxOffset.left;
                        var textBoxWidth = $(this).outerWidth();
                        setTimeout(function () {
                            instance.dpDiv.css({
                                top: top - 100, //you can adjust this value accordingly
                                left: left + textBoxWidth//show at the end of textBox
                            });
                        }, 0);

                    },
                    onSelect: function (date, inst) {
                        /* Revalidate the field when choosing it from the datepicker */
                        $('#receive').formValidation('revalidateField', 'expiry_date');
                    }
                });

            $('#receive').formValidation('addField', vaccine);
            $('#receive').formValidation('addField', batch_no);
            $('#receive').formValidation('addField', stock_quantity);
            $('#receive').formValidation('addField', vvm);

            new_row.slideDown();
        }

        function retrieveFormValues(name) {
            var dump;
            $.each($("input[name=" + name + "], select[name=" + name + "]"), function (i, v) {
                var theTag = v.tagName;
                var theElement = $(v);
                var theValue = theElement.val();
                dump = theValue;
            });
            return dump;
        }

        function retrieveFormValues_Array(name) {
            var dump = new Array();
            var counter = 0;
            $.each($("input[name=" + name + "], select[name=" + name + "]"), function (i, v) {
                var theTag = v.tagName;
                var theElement = $(v);
                var theValue = theElement.val();
                dump[counter] = theValue;

                counter++;
            });
            return dump;
        }

    });
</script>
