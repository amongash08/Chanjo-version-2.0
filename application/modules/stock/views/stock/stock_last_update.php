<style>


</style>
<div class="">
    <div style="min-height: 400px;" id="reports_display">
        <table  class="table table-bordered table-hover table-striped" id="" >
            <thead style="background-color: white">
            <tr>
                <th>Antigen</th>
                <th>Balance (Doses)</th>
                <th>Last Updated</th>

            </tr>
            </thead>

            <tbody>

            <?php

            foreach ($query as $key => $value) {

                $maxdate = date('Y-m-d');
                $mindate = new DateTime(date('Y-m-d'));
                $interval = new DateInterval('P1M');
                $mindate = $mindate->sub($interval)->format('Y-m-d');

                $timestamp=date('Y-m-d',strtotime($value['timestamp']));
                //echo $timestamp.'----'.$mindate;

                if ($timestamp <= $mindate){

                    $color='red';

                }
                else{ $color='black';}



                ?>
                <tr style="color:<?php echo $color; ?>">

                    <td ><?php echo $value['vaccine_name']; ?></td>
                    <td ><?php echo $value['balance']; ?></td>
                    <td style="font-weight:600;" ><?php echo date('d , M Y',strtotime($value['timestamp'])); ?></td>




                </tr>
                <?php
            }
            ?>


            </tbody>
        </table>
    </div>

</div>

<script>

    $(document).ready(function(){

        $("td").each(function() {
            var text = $(this).html();
            if(text=='' || text==' '){

                $(this).html( "<span class='label label-danger'><i class='fa fa-times' aria-hidden='true'></i></span>" );

            }
            //console.log(text)
        });

    });

</script>
