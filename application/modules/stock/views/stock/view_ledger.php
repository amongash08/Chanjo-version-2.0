<!-- DataTables -->
<link href="<?php echo base_url() ?>assets/plugins/datatables/datatables.css" rel="stylesheet"/>
<script src="<?php echo base_url() ?>assets/plugins/datatables/datatables.js"></script>
<script src="<?php echo base_url() ?>assets/plugins/tabledit/jquery.tabledit.js"></script>


<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title"><?php echo $vaccine_name; ?></h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                class="fa fa-minus"></i>
                    </button>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="row">
                    <div class="col-md-4 pull-left">
                        <div class="col-sm-3">
                            <i class="fa fa-cubes fa-5x"> </i>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="pull-right">
                            <button type="button" class="btn btn-navy" id="batches">Batch Summary
                            </button>

                        </div>
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- ./box-body -->
        </div>
        <!-- /.box -->
    </div>
    <!-- /.col -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-body">
                <div class="table-responsive">
                    <table id="ledger" data-order='[[ 0, "desc" ]]' class="table table-striped table-hover dataTable"
                           cellspacing="0" width="100%">
                        <thead style="background-color: white">
                        <tr>
                            <th></th>
                            <th>Date</th>
                            <th>Type</th>
                            <th>Station</th>
                            <th>Quantity</th>
                            <th>Batch</th>
                            <th>Expiry</th>
                            <th>Stock Balance</th>
                        </tr>
                        </thead>

                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="batch" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h3>Batch Summary</h3>
            </div>
            <div class="modal-body">
                <img id="loader" src="<?php echo base_url('assets/images/ring.gif') ?>" alt="loading"
                     style="margin:10% 50% 10% ;">
                <div class="table-responsive" id="batch-content">
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>

<div class="modal fade" id="batch" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h3>Issued Antigens</h3>
            </div>
            <div class="modal-body">
                <img id="loader" src="<?php echo base_url('assets/images/ring.gif') ?>" alt="loading"
                     style="margin:10% 50% 10% ;">
                <div class="table-responsive" id="issued_items">
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>
<script>

    $('#batches').click(function () {
        $('#loader').show();
        $('#batch').modal();


        $.post("<?php echo base_url('stock/batch')?>", {vaccine: '<?php echo $id; ?>'})
            .done(function (data) {
                $('#loader').hide();
                data = JSON.parse(data);

                var col = [];
                for (var i = 0; i < data.length; i++) {
                    for (var key in data[i]) {
                        if (col.indexOf(key) === -1) {
                            col.push(key);
                        }
                    }
                }

                // CREATE DYNAMIC TABLE.
                var table = document.createElement("table");
                table.setAttribute('class', 'table table-bordered table-hover table-striped');
                table.setAttribute('id', 'batch_summary');

                // CREATE HTML TABLE HEADER ROW USING THE EXTRACTED HEADERS ABOVE.

                var tr = table.insertRow(-1);                   // TABLE ROW.

                var header = new Array();
                header.push(["Batch Number", "Quantity", "Expiry Date"]);
                for (var i = 0; i < 3; i++) {
                    var headerCell = document.createElement("TH");
                    headerCell.innerHTML = header[0][i];
                    tr.appendChild(headerCell);
                }


                // ADD JSON DATA TO THE TABLE AS ROWS.
                for (var i = 0; i < data.length; i++) {

                    tr = table.insertRow(-1);

                    for (var j = 0; j < 3; j++) {
                        var tabCell = tr.insertCell(-1);
                        tabCell.innerHTML = data[i][col[j]];
                    }
                }

                // FINALLY ADD THE NEWLY CREATED TABLE WITH JSON DATA TO A CONTAINER.
                var divContainer = document.getElementById("batch-content");
                divContainer.innerHTML = "";
                divContainer.appendChild(table);
                //$('#batch_summary').Tabledit({
                //    url: '<?php //echo base_url('stock/edit_batch')?>//',
                //    deleteButton: false,
                //    restoreButton: false,
                //    columns: {
                //        identifier: [0, 'batch_number'],
                //        editable: [[0, 'batch_number'], [2, 'expiry_date']]
                //    }
                //
                //});
            });

    });

    $('#batch').on("hidden.bs.modal", function () {
        $('#batch-content').empty();
    });


    $(document).ready(function () {

        editor = new $.fn.dataTable.Editor({
            table: "#ledger"
        });
        $.fn.dataTable.ext.buttons.reload = {
            text: 'Recalculate',
            action: function (e, dt, node, config) {
                swal({
                        title: "Are you sure?",
                        text: "This process will change your current balances.",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonText: "Yes",
                        cancelButtonText: "Cancel",
                        closeOnConfirm: false,
                        closeOnCancel: true,
                        showLoaderOnConfirm: true
                    },
                    function (isConfirm) {
                        if (isConfirm) {
                            $.ajax({
                                url: "<?php echo base_url('stock/recalculate/' . $id) ?>",
                                type: "GET",
                                success: function (data, textStatus, jqXHR) {
                                    dt = $('#ledger').DataTable();
                                    dt.ajax.reload();
                                    swal.close();
                                },
                                error: function (jqXHR, textStatus, errorThrown) {
                                }
                            });
                        }
                    });
            }
        };


        var table = $('#ledger').DataTable({
            dom: "Bfrtip",
            scrollY: 300,
            paging: false,
            ajax: {
                url: '<?php echo site_url('stock/stock_data/' . $id) ?>',
                type: 'GET'
            },
            columns: [{
                data: null,
                defaultContent: '',
                className: 'select-checkbox',
                orderable: false
            }, {
                data: "date"
            }, {
                data: "type"
            }, {
                data: "to_from",
                render: function (data, type, row, meta) {

                    if (type === 'display') {
                        data = '<a target="_blank" href="<?php echo site_url('stock/get_issued/') ?>' + row.reference_id + '">' + data + '</a>';
                    }

                    return data;
                }
            }, {
                data: "quantity"
            }, {
                data: "batch"
            }, {
                data: "expiry"
            }, {
                data: "balance",
                render: $.fn.dataTable.render.number(',', '.', 0)
            }],
            order: [
                [1, "desc"]
            ],
            select: {
                style: 'multi',
                selector: 'td:first-child'
            },
            buttons: [{
                extend: 'remove',
                editor: editor,
                action: function (e, dt, button, config) {
                    var selected = dt.row({selected: true}).data();
                    swal({
                            title: "Are you sure?",
                            text: "You will not be able to undo this action!",
                            type: "warning",
                            showCancelButton: true,
                            confirmButtonText: "Yes",
                            cancelButtonText: "Cancel",
                            closeOnConfirm: false,
                            closeOnCancel: true,
                            showLoaderOnConfirm: true
                        },
                        function (isConfirm) {
                            if (isConfirm) {
                                table.row('.selected').remove().draw(false);
                                remove(selected);
                            }
                        });
                }
            }, {
                extend: "reload"
            }, {
                extend: 'collection',
                text: 'Export',
                buttons: [
                    'copyHtml5',
                    'excelHtml5',
                    'csvHtml5'
                ]
            }],
            fnRowCallback: function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                if (aData['type'] == "Issue") {
                    $('td', nRow).css('color', 'Blue');
                } else if (aData['type'] == "Receive") {
                    $('td', nRow).css('color', 'Red');
                } else if (aData['type'] == "Physical Count") {
                    $('td', nRow).css('color', 'black');
                } else if (aData['type'] == "Adjustment") {
                    $('td', nRow).css('color', 'black');
                }
            }
        });

        function remove(data) {
            $.ajax({
                url: '<?php echo site_url('stock/stock_data/' . $id) ?>',
                type: 'POST',
                data: {data, 'action': 'remove'},
                success: function (data) {
                    table.ajax.reload();
                    swal.close();
                }
            });
        }

    });

</script>