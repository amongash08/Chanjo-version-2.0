<!-- DataTables -->
<link href="<?php echo base_url() ?>assets/plugins/datatables/datatables.css" rel="stylesheet"/>
<script src="<?php echo base_url() ?>assets/plugins/datatables/datatables.js"></script>
<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">
            Distribution Details
        </h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <div class="row">
            <form method="post" id="plan" class="form-horizontal">
                <div class="form-group-sm">
                    <div class="col-sm-3 col-xs-3">
                        <?php echo form_dropdown($vaccine); ?>
                    </div>
                </div>

                <div class="form-group-sm">
                    <div class="col-sm-3 col-xs-3">
                        <input type="text" class="form-control stock_quantity" id="stock_quantity"
                               name="stock_quantity" disabled placeholder="Stock Quantity">
                    </div>
                </div>

                <div class="form-group-sm">
                    <div class="col-sm-3 col-xs-3">
                        <input type="text" class="form-control distribution_quantity" id="distribution_quantity"
                               name="distribution_quantity" placeholder="Enter Distribution Quantity">
                    </div>
                </div>
                <div class="form-group-sm">
                    <div class="col-xs-1">
                        <button type="submit" class="btn btn-sm bg-purple" id="validate">Submit</button>
                    </div>
                    <div class="col-xs-offset-0 col-xs-1">
                        <button type="button" class="btn btn-sm bg-blue-active disabled" id="save">Save</button>
                    </div>
                </div>
            </form>
        </div>

    </div>
</div>
<div class="box">
    <div class="box-body">
        <div id="distribution_data"></div>
    </div>
</div>

<script>
    $(document).ready(function () {

        $('#plan')
            .formValidation({
                framework: 'bootstrap',
                excluded: ':hidden, :not(:visible)',
                err: {
                    container: 'tooltip'
                },
                fields: {
                    vaccine: {
                        row: '.form-group-sm',
                        validators: {
                            notEmpty: {
                                message: '*required'
                            }
                        }
                    },
                    stock_quantity: {
                        row: '.form-group-sm',
                        validators: {
                            notEmpty: {
                                message: '*required'
                            },
                            numeric: {
                                message: '*not a number'
                            }
                        }
                    },
                    distribution_quantity: {
                        row: '.form-group-sm',
                        validators: {
                            notEmpty: {
                                message: '*required'
                            },
                            numeric: {
                                message: '*not a number'
                            },
                            greaterThan: {
                                value: 0,
                                message: '*not a valid number'
                            },
                            lessThan: {
                                value: 'stock_quantity',
                                message: '*exceeds stock quantity'
                            }
                        }
                    }
                }

            }).on('change', '#vaccine', function () {
            var vaccine = $(this).val();
            load_balance(vaccine);
        }).on('success.form.fv', function (e) {
            e.preventDefault();
            var vaccine = retrieveFormValues('vaccine');
            var stock_quantity = retrieveFormValues('stock_quantity');
            var distribution_quantity = retrieveFormValues('distribution_quantity');
            data = {
                "vaccine": vaccine,
                "stock_quantity": stock_quantity,
                "distribution_quantity": distribution_quantity,
            };
            ajax_fill_data('stock/supply_chain/distribution', "#distribution_data", data);


        });

        ajax_fill_data('stock/supply_chain/distribution', "#distribution_data");

        function load_balance(vaccine) {
            if (vaccine.length > 0) {
                var request = $.ajax({
                    url: '<?php echo site_url('stock/get_stock_balance'); ?>',
                    type: 'post',
                    data: {
                        "vaccine": vaccine
                    },
                });
                request.done(function (data) {
                    data = JSON.parse(data);
                    stock_quantity = $('#plan').find("#stock_quantity");
                    if (data === 0) {
                        stock_quantity.val('');
                        toastr.error('You do not have any stocks for the selected antigen!');
                    } else {
                        stock_quantity.val(data);
                    }
                    $('#plan').formValidation('revalidateField', stock_quantity);
                    $('#plan').formValidation('revalidateField', 'distribution_quantity');

                });
                request.fail(function (jqXHR, textStatus) {

                });
            }
        }

        function ajax_fill_data(function_url, div, data) {
            var url = "<?php echo base_url(); ?>";
            var function_url = url + function_url;
            var loading_icon = url + "assets/images/ring.gif";

            $.ajax({
                url: function_url,
                type: "POST",
                data: data,
                beforeSend: function () {
                    $(div).html("<img style='margin:10% 50% 10% ;' src=" + loading_icon + ">");
                },
                success: function (msg) {
                    $(div).html(msg);
                }
            });
        }

        function retrieveFormValues(name) {
            var dump;
            $.each($("input[name=" + name + "], select[name=" + name + "]"), function (i, v) {
                var theTag = v.tagName;
                var theElement = $(v);
                var theValue = theElement.val();
                dump = theValue;
            });
            return dump;
        }


    });
</script>
