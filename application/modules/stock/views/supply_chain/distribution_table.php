<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<!-- DataTables -->
<link href="<?php echo base_url() ?>assets/plugins/datatables/datatables.css" rel="stylesheet"/>
<script src="<?php echo base_url() ?>assets/plugins/datatables/datatables.js"></script>
<script src="<?php echo base_url() ?>assets/js/dataTables.cellEdit.js"></script>
<div class="row">
    <div class="col-lg-12" style="margin-top: 10px;">
        <div class="table-responsive">
            <form id="distribution_form">
                <table id="table" class="table table-bordered table-hover table-striped">
                    <thead>
                    <tr>
                        <th hidden></th>
                        <th>Location</th>
                        <th>Stock Quantity</th>
                        <th>Max Stock</th>
                        <th>Distribution Quantity</th>
                    </tr>
                    </thead>
                    <tbody>

                    <?php foreach ($result as $key => $value): ?>
                        <tr>
                            <td hidden class="form-group-sm" id="id"> <?php echo $key; ?></td>
                            <td class="form-group-sm" id="location"><?php echo $value['location']; ?> </td>
                            <td class="form-group-sm" id="stock_balance"><?php echo $value['stock_balance']; ?> </td>
                            <td class="form-group-sm" id="max_stock"><?php echo $value['max_stock']; ?> </td>
                            <td class="form-group-sm" id="quantity"><?php echo $value['quantity']; ?></td>
                        </tr>
                    <?php endforeach; ?>

                    </tbody>
                    <tfoot>
                    <tr>
                        <th hidden></th>
                        <th>Location</th>
                        <th>Stock Quantity</th>
                        <th>Max Stock</th>
                        <th>Distribution Quantity</th>
                    </tr>
                    </tfoot>
                </table>
            </form>
        </div>

    </div>
</div>
<script type="text/javascript">
    var table;
    $(document).ready(function () {
        $('#save').removeClass('disabled');
        $('#save').on('click', function (e) {
            e.preventDefault();
            swal({
                    title: "Confirm Submission",
                    text: "Are you sure you want to proceed with this action?",
                    type: "info",
                    showCancelButton: true,
                    confirmButtonColor: "#54CEF7",
                    confirmButtonText: "Submit",
                    closeOnConfirm: false,
                    showLoaderOnConfirm: true,
                },
                function (isConfirm) {
                    if (isConfirm) {
                        submit();
                    }
                });
        });


        function submit() {
            var data = table.rows().data();
            var dat = new Array();
            data.each(function (obj) {
                row = {
                    'location_id': obj[0],
                    'balance': obj[2],
                    'max_stock': obj[3],
                    'quantity': obj[4],
                };
                dat.push(row);
            });
            collection = JSON.stringify(dat);
            $.ajax({
                url: '<?php echo base_url('stock/supply_chain/save_distribution'); ?>',
                type: "POST",
                data: {
                    "collection": collection,
                    "vaccine": <?php echo $vaccine_id; ?>,
                    "distribution_quantity": <?php echo $distribution_quantity; ?>,
                },
                success: function (data, textStatus, jqXHR) {
                    swal("Saved!", "This distribution plan has been saved successfully.", "success");
                },

                error: function (jqXHR, textStatus, errorThrown) {
                    swal("Error!", jqXHR, "error");
                }
            });
        }

        table = $('#table').DataTable({
            dom: 'Bfrtip',
            buttons: [{
                extend: 'collection',
                text: 'Export',
                buttons: [
                    'copyHtml5',
                    'excelHtml5',
                    'csvHtml5'
                ]
            }]
        });

        table.MakeCellsEditable({
            "onUpdate": myCallbackFunction,
            "columns": [4],
            "inputCss": 'form-control',
            "inputTypes": [
                {
                    "column": 4,
                    "type": "number"
                }
            ]
        });

        function myCallbackFunction(updatedCell, updatedRow, oldValue) {

        }

    });
</script>
