<!-- DataTables -->
<link href="<?php echo base_url() ?>assets/plugins/datatables/datatables.css" rel="stylesheet"/>
<script src="<?php echo base_url() ?>assets/plugins/datatables/datatables.js"></script>
<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">
            Distribution Plans
        </h3>

        <a href="<?php echo base_url('stock/supply_chain/new_distribution'); ?>"
           class="btn btn-sm bg-olive btn-flat margin pull-right">
            New Distribution Plan
        </a>


    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <div class="table-responsive">
            <table id="distribution" class="table table-bordered table-striped" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th></th>
                    <th>Date Created</th>
                    <th>Antigen</th>
                    <th>No of locations</th>
                    <th>Quantity Allocated(Total)</th>
                </tr>
                </thead>


            </table>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {

        var table = $('#distribution').DataTable({
            dom: "Bfrtip",
            serverSide: false,
            scrollY: 250,
            scrollX: false,
            paging: false,
            ajax: {
                url: "/stock/supply_chain/distribution_data",
                type: "POST"
            },
            columns: [
                {
                    data: null,
                    defaultContent: '',
                    className: 'select-checkbox',
                    orderable: false
                },
                {data: 'date'},
                {data: 'antigen'},
                {data: 'location_count'},
                {
                    data: 'quantity',
                    render: $.fn.dataTable.render.number(',', '.', 0)
                }
            ],
            order: [
                [1, "desc"]
            ],
            select: {
                style: 'multi',
                selector: 'td:first-child'
            },
            buttons: [{
                extend: 'collection',
                text: 'Export',
                buttons: [
                    'copyHtml5',
                    'excelHtml5',
                    'csvHtml5'
                ]
            }],
        });

    });

</script>
