<div class="row">
    <?php echo form_open('stock/supply_chain/stock_count', array('class' => "form-horizontal", 'id' => "stock_count")); ?>
    <div class="col-sm-12">
        <div class="box box-solid">
            <div class="box-header with-border">
                <h3 class="box-title">Vaccine Details</h3>
            </div>

            <div class="box-body">
                <div class="table-responsive" id="table">
                    <table class="table table-bordered table-hover table-striped">
                        <thead>
                        <tr align="center" class="form-group-sm">
                            <th style="width:12%;">Date</th>
                            <th style="width:13%;">Vaccine /<br>Diluents</th>
                            <th style="width:13%;">Stock <br>Quantity</th>
                            <th style="width:8%;">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr row="0" class="form-group-sm">
                            <td>
                                <input type="text" class="form-control date" id="date" name="date"
                                       placeholder="YYYY-MM-DD">
                            </td>
                            <td>
                                <?php echo form_dropdown($vaccine); ?>
                            </td>
                            <td>
                                <input type="number" class="form-control stock_count" id="stock_count"
                                       name="stock_count">
                            </td>
                            <td class="small">
                                <a class="add btn-xs" style="cursor: pointer; cursor: hand;"><span
                                            class="label label-success"><i
                                                class="fa fa-plus-square"></i> <b>ADD</b></span></a><br>
                                <a class="remove btn-xs" style="cursor: pointer; cursor: hand;"><span
                                            class="label label-danger"><i
                                                class="fa fa-minus-square"></i> <b>REMOVE</b></span></a>
                            </td>
                        </tr>
                        <tr id="template" class="form-group-sm" hidden>
                            <td>
                                <input type="text" class="form-control date" id="date" name="date"
                                       placeholder="YYYY-MM-DD" disabled>
                            </td>
                            <td>
                                <?php echo form_dropdown($vaccine, '', '', 'disabled'); ?>
                            </td>
                            <td>
                                <input type="number" class="form-control stock_count" id="stock_count"
                                       name="stock_count" disabled>
                            </td>
                            <td class="small">
                                <a class="add btn-xs" style="cursor: pointer; cursor: hand;"><span
                                            class="label label-success"><i
                                                class="fa fa-plus-square"></i> <b>ADD</b></span></a><br>
                                <a class="remove btn-xs" style="cursor: pointer; cursor: hand;"><span
                                            class="label label-danger"><i
                                                class="fa fa-minus-square"></i> <b>REMOVE</b></span></a>
                            </td>
                        </tr>

                        </tbody>
                    </table>
                </div>
            </div>
            <div class="box-footer">
                <button type="submit" id="validate" class="btn bg-navy margin">
                    Submit
                </button>
            </div>
        </div>
    </div>
    <?php echo form_close(); ?>
</div>

<script>
    $(document).ready(function () {

        $('#stock_status')
            .formValidation({
                framework: 'bootstrap',
                err: {
                    container: 'tooltip'
                },
                row: {
                    selector: 'td'
                },
                // This option will not ignore invisible fields which belong to inactive panels
                excluded: ':hidden, :not(:visible)',
                fields: {
                    date: {
                        validators: {
                            notEmpty: {
                                message: '*required'
                            },
                            date: {
                                format: 'YYYY-MM-DD',
                                message: '*date is not valid',
                                max: moment().format('YYYY-MM-DD'),
                                min: moment('2016-06-01').format('YYYY-MM-DD')
                            }
                        }
                    },
                    vaccine: {
                        validators: {
                            notEmpty: {
                                message: '*required'
                            }
                        }
                    },
                    stock_count: {
                        validators: {
                            notEmpty: {
                                message: '*required'
                            },
                            numeric: {
                                message: '*not a number'
                            },
                            greaterThan: {
                                value: -1,
                                message: '*not a valid number'
                            }
                        }
                    }
                }
            }).on('click', '#table .add', function () {
            var thisRow = $(this).closest('tr');
            return validateRow(thisRow);

        }).on('click', '#table .remove', function () {
            if ($('#table tbody tr').length === 2) return;
            $(this).parents("tr").fadeOut('slow', function () {
                $(this).remove();
            });

        }).on('success.form.fv', function (e) {
            // Prevent form submission
            e.preventDefault();
            swal({
                    title: "Confirm Submission",
                    text: "Are you sure you want to submit the entered details?",
                    type: "info",
                    showCancelButton: true,
                    confirmButtonColor: "#54CEF7",
                    confirmButtonText: "Submit",
                    closeOnConfirm: false,
                    showLoaderOnConfirm: true,
                },
                function (isConfirm) {
                    if (isConfirm) {
                        submit();
                    }
                });


        });

        function submit() {
            var vaccine_count = 0;
            $.each($(".vaccine"), function (i, v) {
                vaccine_count++;
            });

            var vaccines = retrieveFormValues_Array('vaccine');
            var date = retrieveFormValues_Array('date');
            var stock_count = retrieveFormValues_Array('stock_count');

            var dat = new Array();

            for (var i = 0; i < vaccine_count; i++) {
                var get_vaccine = vaccines[i];
                var get_quantity = stock_count[i];
                var get_date = date[i];


                data = {
                    "vaccine_id": get_vaccine,
                    "date": get_date,
                    "stock_count": get_quantity
                };
                dat.push(data);
            }
            dat = JSON.stringify(dat);
            $.ajax({
                url: $('#stock_status').attr('action'),
                type: "POST",
                data: {
                    "data": dat
                },
                beforeSend: function () {
                    $('#validate').fadeOut(300, function () {
                        $(this).remove();
                    });
                },

                success: function (data, textStatus, jqXHR) {
                    window.location.replace('<?php echo site_url('stock/ledger'); ?>');
                },

                error: function (jqXHR, textStatus, errorThrown) {
                    //if fails
                }
            });
        }

        $('#date')
            .datepicker({
                dateFormat: "yy-mm-dd",
                maxDate: 0,
                beforeShow: function (textbox, instance) {
                    var txtBoxOffset = $(this).offset();
                    var top = txtBoxOffset.top;
                    var left = txtBoxOffset.left;
                    var textBoxWidth = $(this).outerWidth();
                    setTimeout(function () {
                        instance.dpDiv.css({
                            top: top - 100, //you can adjust this value accordingly
                            left: left + textBoxWidth//show at the end of textBox
                        });
                    }, 0);

                },
                onSelect: function (date, inst) {
                    /* Revalidate the field when choosing it from the datepicker */
                    $('#stock_status').formValidation('revalidateField', 'date');
                }
            });

        function validateRow(thisRow) {
            var fv = $('#stock_status').data('formValidation'), // FormValidation instance
                // The current row
                $row = thisRow;
            // Validate the container
            fv.validateContainer($row);


            var isValidStep = fv.isValidContainer($row);
            if (isValidStep === false || isValidStep === null) {
                // Do not add row
                return false;
            }

            addRow($row);
        }

        function addRow(thisRow) {

            var template = $('#template');
            var cloned_object = template.clone().removeAttr('hidden').removeAttr('id');

            var count_row = thisRow.attr("row");
            var row_index = parseInt(count_row) + 1;
            var new_row = cloned_object.attr("row", row_index);


            new_row.insertAfter(thisRow).find('input').val('');

            var date = new_row.find('input[name="date"]').removeAttr('disabled');
            var vaccine = new_row.find('select[name="vaccine"]').removeAttr('disabled');
            var stock_count = new_row.find('input[name="stock_count"]').removeAttr('disabled');
            $('#stock_status').formValidation('addField', date);
            date.removeClass('hasDatepicker')
                .attr('id', 'date' + row_index)
                .datepicker({
                    dateFormat: "yy-mm-dd",
                    maxDate: 0,
                    beforeShow: function (textbox, instance) {
                        var txtBoxOffset = $(this).offset();
                        var top = txtBoxOffset.top;
                        var left = txtBoxOffset.left;
                        var textBoxWidth = $(this).outerWidth();
                        setTimeout(function () {
                            instance.dpDiv.css({
                                top: top - 100, //you can adjust this value accordingly
                                left: left + textBoxWidth//show at the end of textBox
                            });
                        }, 0);

                    },
                    onSelect: function (date, inst) {
                        /* Revalidate the field when choosing it from the datepicker */
                        $('#stock_status').formValidation('revalidateField', 'date');
                    }
                });

            $('#stock_status').formValidation('addField', vaccine);
            $('#stock_status').formValidation('addField', stock_count);
            new_row.slideDown();
        }

        function retrieveFormValues_Array(name) {
            var dump = new Array();
            var counter = 0;
            $.each($("input[name=" + name + "], select[name=" + name + "]"), function (i, v) {
                var theTag = v.tagName;
                var theElement = $(v);
                var theValue = theElement.val();
                dump[counter] = theValue;

                counter++;
            });
            return dump;
        }


    });
</script>
