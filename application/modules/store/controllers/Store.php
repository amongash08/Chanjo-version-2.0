<?php

class Store extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        if (!$this->ion_auth->logged_in()) {
            //redirect them to the login page
            redirect('users');
        }
        $this->load->module('users');
        $this->load->module('region');
        $this->load->module('county');
        $this->load->module('subcounty');
        $this->load->module('facility');
        $this->load->module('levels');
        $this->load->module('fridges');
        $this->load->module('vaccine');
        $this->load->model('mdl_store');
        $this->load->model('mdl_level');
        $this->load->model('mdl_region');
        $this->load->model('mdl_fridges');
        $this->load->model('mdl_vaccine');
        $this->load->library('form_validation');


    }

    public function index()
    {
        $user_info = $this->users->user_info();
        $level=$user_info->level_id;
        $location = $user_info->location_id;

        if ($level == 1){
            $data['module'] = 'national';
            $data['view_file'] = 'home/store';
            $stores=$this->mdl_store->get_stores_in_national();
            $data['stores']=$stores;

        }

        if ($level == 2){
            $data['module'] = 'region';
            $data['view_file'] = 'home/store';
            $station=json_decode($user_info->json_location)->region;
            //$data['counties'] = $this->mdl_county->get_by_region($station);
            $data['subcounties'] = $this->mdl_subcounty->get_by_region($station);
            $stores=$this->mdl_store->get_stores_region($station);
            $data['stores']=$stores;
            //$data['counties'] = $this->mdl_county->get_by_region($station);

        }

        if ($level == 3){
            $data['module'] = 'county';
            $data['view_file'] = 'home/store';
            $station=json_decode($user_info->json_location)->county;
            $data['subcounties'] = $this->mdl_subcounty->get_by_county($station);
            $stores=$this->mdl_store->get_stores_county($station);
            $data['stores']=$stores;
            $data['subcounties'] = $this->mdl_subcounty->get_by_county($station);
        }

        if ($level == 4){
            $data['module'] = 'subcounty';
            $data['view_file'] = 'home/store';
            $station=json_decode($user_info->json_location)->subcounty;
            $stores=$this->mdl_store->get_stores_subcounty($station);
            $data['stores']=$stores;
            $data['facilities'] = $this->mdl_facility->get_by_subcounty($station);
        }

        $data['title'] = 'Store';
        $data['page_title'] = 'Store';
        $data['subtitle'] = 'Store';

        $mystore=$this->mdl_store->get_store_by_location($location);
        //echo '<pre>',print_r($stores),'</pre>';exit;

        if(count($mystore)>0){

            $store_id=$mystore[0]->id;
            $data['myfridges'] = $this->mdl_fridges->get_by_store($store_id);

            $data['mystore'] = $mystore[0];

        }else{

            $data['module'] = '';
            $data['view_file'] = 'errors/html/error_404';
            $data['message'] = 'We could not find your store (s).';

        }
//        $t =$this->mdl_fridges->get_by_store($store_id)[2];
//        $x=json_decode($t->power_source);
//
//        foreach ($x as $value){
//            echo '<pre>',print_r($value),'</pre>';
//        }
//
//
//        exit;

        $data['levels'] = $this->mdl_level->get_all();
        $data['regions'] = $this->mdl_region->get_all();
        $data['fridges'] = $this->mdl_fridges->get_all();
        $data['vaccines'] = $this->mdl_vaccine->get_all();


        //echo '<pre>',print_r($this->mdl_county->get_all()),'</pre>';exit;

        echo Modules::run('templates', $data);

    }

    public function add()
    {

        $this->form_validation->set_rules('catchment_pop', 'Catchment Population', 'required');
        $this->form_validation->set_rules('live_birth_pop', 'Live birth population', 'required');
        $this->form_validation->set_rules('pop_pregnant_women', 'Pregnant Women population', 'required');
        $this->form_validation->set_rules('pop_surviving_infants', 'Surviving Infants population', 'required');
        $this->form_validation->set_rules('pop_adolescent_girls', 'Cohort Girls population', 'required');
        $this->form_validation->set_rules('cold_boxes', 'cold boxes', 'required');
        $this->form_validation->set_rules('name', 'Store Name', 'required');
        $this->form_validation->set_rules('vaccine_carriers', 'vaccine carriers', 'required');
        $this->form_validation->set_rules('ice_packs', 'ice packs', 'required');
        $this->form_validation->set_rules('time', 'time', 'required');
        $this->form_validation->set_rules('distance', 'distance', 'required');
        $this->form_validation->set_rules('nearesttown', 'Nearest town', 'required');
        $this->form_validation->set_rules('electricity', 'electricity', 'required');
        $this->form_validation->set_rules('roof', 'roof', 'required');
        $this->form_validation->set_rules('manager_name', 'manager_name', 'required');
        $this->form_validation->set_rules('manager_phone', 'manager_phone', 'required');
        $this->form_validation->set_rules('road_access', 'Access By Road', 'required');
        $this->form_validation->set_rules('road_quality', 'Quality Of Road', 'required');
        $this->form_validation->set_rules('special_access', 'Special Access Needs', 'required');
        $this->form_validation->set_rules('wastage[]', 'Wastage Factor(s)', 'required');



        if ($this->form_validation->run() == FALSE)
        {
            echo validation_errors();
            http_response_code(401);
            return;
        }


        $location_data = array(
            'national' => 1,
            'region' => (int)$this->input->post('regions'),
            'county' => (int)$this->input->post('counties'),
            'subcounty' => (int)$this->input->post('subcounties'),
            'facility' =>0,
        );
        //echo '<pre>',print_r($location_data),'</pre>';exit;


        $location = $this->user_ext->get_location($location_data);

        //check if location exists.
        $checkstore=$this->mdl_store->get_store_by_location($location->id);



        if($checkstore){

            echo $this->input->post('name').' store already Exists';
            http_response_code(401);
            return;

        }

        $data = array(
            'name' => $this->input->post('name'),
            'location_id' => $location->id,
            'catchment_population' => $this->input->post('catchment_pop'),
            'live_birth_population' => $this->input->post('live_birth_pop'),
            'pop_pregnant_women' => $this->input->post('pop_pregnant_women'),
            'pop_surviving_infants' => $this->input->post('pop_surviving_infants'),
            'pop_adolescent_girls' => $this->input->post('pop_adolescent_girls'),
            'cold_boxes' => $this->input->post('cold_boxes'),
            'vaccine_carriers' => $this->input->post('vaccine_carriers'),
            'ice_packs' => $this->input->post('ice_packs'),
            'time_to_store' => $this->input->post('time'),
            'distance_to_store' => $this->input->post('distance'),
            'electricity_status' => $this->input->post('electricity'),
            'roof_type' => $this->input->post('roof'),
            'manager_name' => $this->input->post('manager_name'),
            'manager_phone' => $this->input->post('manager_phone'),
            'nearest_town' => $this->input->post('nearesttown'),
            'road_access' => $this->input->post('road_access'),
            'road_quality' => $this->input->post('road_quality'),
            'special_access' => $this->input->post('special_access')

        );


        $store_id = $this->mdl_store->insert($data);

        $wastage = $this->input->post('wastage');
        $vaccine_ids = $this->input->post('vaccine_id');

        foreach ($wastage as $key => $value){

            $data = array(
                'vaccine_id' => $vaccine_ids[$key],
                'wastage' => $value,
                'store_id' => $store_id
            );

            $this->mdl_store->insert_wastage($data);

        }

    }

    public function edit()
    {

        $this->form_validation->set_rules('edit_catchment_pop', 'Catchment Population', 'required');
        $this->form_validation->set_rules('edit_live_birth_pop', 'Live birth population', 'required');
        $this->form_validation->set_rules('edit_pop_pregnant_women', 'Pregnant Women population', 'required');
        $this->form_validation->set_rules('edit_pop_surviving_infants', 'Surviving Infants population', 'required');
        $this->form_validation->set_rules('edit_pop_adolescent_girls', 'Cohort Girls population', 'required');
        $this->form_validation->set_rules('edit_cold_boxes', 'cold boxes', 'required');
        $this->form_validation->set_rules('edit_nearesttown', 'Nearest town', 'required');
        $this->form_validation->set_rules('edit_wastage[]', 'Wastage Factor(s)', 'required');
        $this->form_validation->set_rules('edit_cold_boxes', 'cold boxes', 'required');
        $this->form_validation->set_rules('edit_vaccine_carriers', 'vaccine carriers', 'required');
        $this->form_validation->set_rules('edit_ice_packs', 'ice packs', 'required');
        $this->form_validation->set_rules('edit_time', 'time', 'required');
        $this->form_validation->set_rules('edit_distance', 'distance', 'required');
        $this->form_validation->set_rules('edit_electricity', 'electricity', 'required');
        $this->form_validation->set_rules('edit_roof', 'roof', 'required');
        $this->form_validation->set_rules('edit_manager_name', 'manager_name', 'required');
        $this->form_validation->set_rules('edit_manager_phone', 'manager_phone', 'required');
        $this->form_validation->set_rules('edit_road_access', 'Access By Road', 'required');
        $this->form_validation->set_rules('edit_road_quality', 'Quality Of Road', 'required');
        $this->form_validation->set_rules('edit_special_access', 'Special Access Needs', 'required');



        if ($this->form_validation->run() == FALSE)
        {
            echo validation_errors();
            http_response_code(401);
            return;
        }


       //echo '<pre>',print_r($location_data),'</pre>';exit;


        $data = array(
            'id' => $this->input->post('store_'),
            'catchment_population' => $this->input->post('edit_catchment_pop'),
            'live_birth_population' => $this->input->post('edit_live_birth_pop'),
            'pop_pregnant_women' => $this->input->post('edit_pop_pregnant_women'),
            'pop_surviving_infants' => $this->input->post('edit_pop_surviving_infants'),
            'pop_adolescent_girls' => $this->input->post('edit_pop_adolescent_girls'),
            'cold_boxes' => $this->input->post('edit_cold_boxes'),
            'vaccine_carriers' => $this->input->post('edit_vaccine_carriers'),
            'ice_packs' => $this->input->post('edit_ice_packs'),
            'time_to_store' => $this->input->post('edit_time'),
            'distance_to_store' => $this->input->post('edit_distance'),
            'electricity_status' => $this->input->post('edit_electricity'),
            'roof_type' => $this->input->post('edit_roof'),
            'manager_name' => $this->input->post('edit_manager_name'),
            'manager_phone' => $this->input->post('edit_manager_phone'),
            'nearest_town' => $this->input->post('edit_nearesttown'),
            'road_access' => $this->input->post('edit_road_access'),
            'road_quality' => $this->input->post('edit_road_quality'),
            'special_access' => $this->input->post('edit_special_access')


        );
        // echo '<pre>',print_r($data),'</pre>';exit;

        $this->mdl_store->update_store($data);

        $store_id = $this->input->post('store_');

        $transaction_ids=$this->input->post('tr_id');
        $vaccine_ids = $this->input->post('edit_vaccine_id');
        $wastage = $this->input->post('edit_wastage');

        if (in_array("NULL", $transaction_ids))
          {

            //do fresh insert

            foreach ($wastage as $key => $value){

                $data = array(
                    'vaccine_id' => $vaccine_ids[$key],
                    'wastage' => $value,
                    'store_id' => $store_id
                );

                $this->mdl_store->insert_wastage($data);

            }


          }

          foreach ($wastage as $key => $value){

              $data = array(
                  'vaccine_id' => $vaccine_ids[$key],
                  'wastage' => $value,
                  'store_id' => $store_id,
                  'id' => $transaction_ids[$key]
              );


              $this->mdl_store->update_wastage($data);

          }


    }

    public function editmystore()
    {

        $this->form_validation->set_rules('edit_mycatchment_pop', 'Catchment Population', 'required');
        $this->form_validation->set_rules('edit_mylive_birth_pop', 'Live birth population', 'required');
        $this->form_validation->set_rules('edit_mycold_boxes', 'cold boxes', 'required');
        $this->form_validation->set_rules('edit_myvaccine_carriers', 'vaccine carriers', 'required');
        $this->form_validation->set_rules('edit_myice_packs', 'ice packs', 'required');
        $this->form_validation->set_rules('edit_mytime', 'time', 'required');
        $this->form_validation->set_rules('edit_mydistance', 'distance', 'required');
        $this->form_validation->set_rules('edit_myelectricity', 'electricity', 'required');
        $this->form_validation->set_rules('edit_myroof', 'roof', 'required');
        $this->form_validation->set_rules('edit_mymg_name', 'manager_name', 'required');
        $this->form_validation->set_rules('edit_mymg_phone', 'manager_phone', 'required');
        $this->form_validation->set_rules('edit_myrota', 'rota', 'required');
        $this->form_validation->set_rules('edit_mybcg', 'bcg', 'required');
        $this->form_validation->set_rules('edit_mybcg_diluent', 'bcg_diluent', 'required');
        $this->form_validation->set_rules('edit_myopv', 'opv', 'required');
        $this->form_validation->set_rules('edit_mytt', 'tt', 'required');
        $this->form_validation->set_rules('edit_myipv', 'ipv', 'required');
        $this->form_validation->set_rules('edit_mymeasles', 'measles', 'required');
        $this->form_validation->set_rules('edit_mymeasles_diluent', 'measles_diluent', 'required');
        $this->form_validation->set_rules('edit_myyf', 'yf', 'required');
        $this->form_validation->set_rules('edit_mypcv', 'pcv', 'required');
        $this->form_validation->set_rules('edit_mydpt', 'dpt', 'required');


        if ($this->form_validation->run() == FALSE)
        {
            echo validation_errors();
            http_response_code(401);
            return;
        }


        //echo '<pre>',print_r($location_data),'</pre>';exit;


        $data = array(
            'id' => $this->input->post('mystore_'),
            'catchment_population' => $this->input->post('edit_mycatchment_pop'),
            'live_birth_population' => $this->input->post('edit_mylive_birth_pop'),
            'cold_boxes' => $this->input->post('edit_mycold_boxes'),
            'vaccine_carriers' => $this->input->post('edit_myvaccine_carriers'),
            'ice_packs' => $this->input->post('edit_myice_packs'),
            'time_to_store' => $this->input->post('edit_mytime'),
            'distance_to_store' => $this->input->post('edit_mydistance'),
            'electricity_status' => $this->input->post('edit_myelectricity'),
            'roof_type' => $this->input->post('edit_myroof'),
            'manager_name' => $this->input->post('edit_mymg_name'),
            'manager_phone' => $this->input->post('edit_mymg_phone'),
            'wastage_rota' => $this->input->post('edit_myrota'),
            'wastage_bcg' => $this->input->post('edit_mybcg'),
            'wastage_bcg_diluent' => $this->input->post('edit_mybcg_diluent'),
            'wastage_opv' => $this->input->post('edit_myopv'),
            'wastage_tt' => $this->input->post('edit_mytt'),
            'wastage_ipv' => $this->input->post('edit_myipv'),
            'wastage_measles' => $this->input->post('edit_mymeasles'),
            'wastage_measles_diluent' => $this->input->post('edit_mymeasles_diluent'),
            'wastage_yf' => $this->input->post('edit_myyf'),
            'wastage_pcv' => $this->input->post('edit_mypcv'),
            'wastage_dpt' => $this->input->post('edit_mydpt')

        );
        //echo '<pre>',print_r($data),'</pre>';exit;

        $this->mdl_store->update_store($data);

    }

    public function generate()
    {
        $user_info = $this->users->user_info();
        $station=json_decode($user_info->json_location)->subcounty;
        $facilities = $this->mdl_facility->get_by_subcounty($station);
        //echo '<pre>',print_r($user_info),'</pre>';exit;



        foreach ($facilities as $key => $value){

            $location_data = array(
                'national' => 1,
                'region' => (int)json_decode($user_info->json_location)->region,
                'county' => (int)json_decode($user_info->json_location)->county,
                'subcounty' => (int)json_decode($user_info->json_location)->subcounty,
                'facility' => (int)$value->id,
            );

            $location = $this->user_ext->get_location($location_data);
            //echo '<pre>',print_r($location),'</pre>';exit;

            $data = array(
                'name' => $value->facility_name,
                'location_id' => $location->id,
                'catchment_population' => $value->catchment_population,
                'live_birth_population' => $value->live_birth_population,
                'cold_boxes' => $value->cold_boxes,
                'vaccine_carriers' => $value->vaccine_carriers,
                'ice_packs' => $value->ice_packs

            );
           // echo '<pre>',print_r($data),'</pre>';exit;

            $this->mdl_store->insert($data);
            //echo '<pre>',print_r($value->id),'</pre>';


        }


    }

    public function getWastageStore($store_id)
    {
      header('Content-Type: application/json');

      $wastage_factors = $this->mdl_vaccine->get_wastage_by_store($store_id);

      echo json_encode($wastage_factors);


    }

    public function getVaccinesJson()
    {

      header('Content-Type: application/json');

      $vaccines = $this->mdl_vaccine->get_all();

      echo json_encode($vaccines);


    }



}
