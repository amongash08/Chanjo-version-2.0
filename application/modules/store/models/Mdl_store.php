<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Mdl_Store extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function get_table()
    {
        $table = 'tbl_stores';

        return $table;
    }

    public function get_by_id($id)
    {
        $this->db->select('');
        $this->db->from('store');
        $this->db->where('id', $id);
        $query = $this->db->get();

        return $query->result();
    }



    function insert($data)
    {
        $this->db->insert('store',$data);
        $insert_id = $this->db->insert_id();
        return  $insert_id;
    }

    public function get_store_by_location($location)
    {
        $this->db->select('*');
        $this->db->from('store');
        $this->db->where('location_id', $location);
        $query = $this->db->get();
        return $query->result();

    }

    public function get_stores_in_national()
    {
        $query = $this->db->select('store.id as id,locations.id as location_id,name,location_id,
        catchment_population,live_birth_population,pop_pregnant_women,pop_surviving_infants,pop_adolescent_girls,cold_boxes,vaccine_carriers,ice_packs,time_to_store,distance_to_store,
        electricity_status,roof_type,manager_name,manager_phone,nearest_town,special_access,road_access,road_quality')
            ->from('store')
            ->join('locations', 'store.location_id = locations.id')
            ->where('JSON_EXTRACT(`json_location`, "$.national") =', 1)
            ->where('JSON_EXTRACT(`json_location`, "$.region") !=', 0)
            ->where('JSON_EXTRACT(`json_location`, "$.county") =', 0)
            ->where('JSON_EXTRACT(`json_location`, "$.subcounty") =', 0)
            ->where('JSON_EXTRACT(`json_location`, "$.facility") =', 0)
            ->get();
        return $query->result();


    }

    public function get_stores_region($station)
    {
        $query = $this->db->select('store.id as id,locations.id as location_id,name,location_id,
        catchment_population,live_birth_population,pop_pregnant_women,pop_surviving_infants,pop_adolescent_girls,cold_boxes,vaccine_carriers,ice_packs,time_to_store,distance_to_store,
        electricity_status,roof_type,manager_name,manager_phone,nearest_town,special_access,road_access,road_quality')
            ->from('store')
            ->join('locations', 'store.location_id = locations.id')
            ->where('JSON_EXTRACT(`json_location`, "$.national") =', 1)
            ->where('JSON_EXTRACT(`json_location`, "$.region") =', $station)
            ->where('JSON_EXTRACT(`json_location`, "$.subcounty") !=', 0)
            ->where('JSON_EXTRACT(`json_location`, "$.county") !=', 0)
            ->where('JSON_EXTRACT(`json_location`, "$.facility") =', 0)
            ->get();
        return $query->result();

    }

    public function get_stores_county($station)
    {
        $query = $this->db->select('')
            ->from('store')
            ->join('locations', 'store.location_id = locations.id')
            ->where('JSON_EXTRACT(`json_location`, "$.county") =', $station)
            ->where('JSON_EXTRACT(`json_location`, "$.subcounty") !=', 0)
            ->get();
        return $query->result();

    }

    public function get_stores_subcounty($station)
    {
        $query = $this->db->select('store.id as id,locations.id as location_id,name,location_id,
        catchment_population,live_birth_population,pop_pregnant_women,pop_surviving_infants,pop_adolescent_girls,cold_boxes,vaccine_carriers,ice_packs,time_to_store,distance_to_store,
        electricity_status,roof_type,manager_name,manager_phone,nearest_town,special_access,road_access,road_quality')
            ->from('store')
            ->join('locations', 'store.location_id = locations.id')
            ->where('JSON_EXTRACT(`json_location`, "$.subcounty") =', $station)
            ->where('JSON_EXTRACT(`json_location`, "$.facility") !=', 0)
            ->get();
        return $query->result();

    }

    function update_store($data)
    {
        $this->db->set('catchment_population', $data['catchment_population']);
        $this->db->set('live_birth_population', $data['live_birth_population']);
        $this->db->set('pop_pregnant_women', $data['pop_pregnant_women']);
        $this->db->set('pop_surviving_infants', $data['pop_surviving_infants']);
        $this->db->set('pop_adolescent_girls', $data['pop_adolescent_girls']);
        $this->db->set('cold_boxes', $data['cold_boxes']);
        $this->db->set('vaccine_carriers', $data['vaccine_carriers']);
        $this->db->set('ice_packs', $data['ice_packs']);
        $this->db->set('time_to_store', $data['time_to_store']);
        $this->db->set('distance_to_store', $data['distance_to_store']);
        $this->db->set('electricity_status', $data['electricity_status']);
        $this->db->set('roof_type', $data['roof_type']);
        $this->db->set('manager_name', $data['manager_name']);
        $this->db->set('manager_phone', $data['manager_phone']);
        $this->db->set('nearest_town', $data['nearest_town']);
        $this->db->set('road_access', $data['road_access']);
        $this->db->set('road_quality', $data['road_quality']);
        $this->db->set('special_access', $data['special_access']);
        $this->db->where('id', $data['id']);
        $this->db->update('store');


    }

    public function get_store_capacity_by_station($station,$type)
    {
        $query = $this->db->select('f.id as fridge_id,s.id as store_id,s.name,s.location_id,sum(f.`Vaccine storage volume (L)`) as volume')
            ->from('store_fridges sf')
            ->join('store s', 's.id=sf.store_id')
            ->join('fridges f', 'f.id=sf.fridge_id')
            ->where('sf.fridge_status =', 'Functional')
            ->where('sf.delete_status =', 1)
            ->where('Freezer_capacity =', $type)
            ->where('s.location_id =', $station)
            ->group_by('sf.store_id')
            ->get();
        return $query->result();

    }

    function insert_wastage($data)
    {

        $this->db->insert('wastage_factor',$data);
    }

    function update_wastage($data)
    {
        $this->db->set('wastage', $data['wastage']);
        $this->db->set('vaccine_id', $data['vaccine_id']);
        $this->db->set('store_id', $data['store_id']);
        $this->db->where('id', $data['id']);
        $this->db->update('wastage_factor');


    }




}
