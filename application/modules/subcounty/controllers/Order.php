<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Order extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->module('users');
        $this->load->model('order/mdl_order');
    }

    public function index()
    {
        $user_info = $this->users->user_info();
        $my_orders = $this->mdl_order->get_where(array('location_id' => $user_info->location_id));
        $data = ['my_orders' => $my_orders];
        $this->load->view('subcounty/order/order', $data);

    }


}