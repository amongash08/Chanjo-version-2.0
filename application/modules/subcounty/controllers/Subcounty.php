<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Subcounty extends MX_Controller
{
    function __construct()
    {
        parent::__construct();
        if (!$this->ion_auth->logged_in()) {
            //redirect them to the login page
            redirect('users');
        }
        $this->load->module('users');
        $this->load->model('mdl_subcounty');
        $this->load->model('mdl_subcounty_ext');

    }

    public function index()
    {
        $data['title'] = 'View Subcounties';
        $data['page_title'] = 'Subcounty';
        $data['subtitle'] = 'View Subcounties';
        $data['module'] = 'subcounty';
        $data['view_file'] = 'subcounty/index';
        $data['sidebar'] = 'subcounty_sidebar';

        echo Modules::run('templates', $data);
    }

    function edit($id = null)
    {
        $data['title'] = "Edit Subcounty";
        $data['subtitle'] = 'Edit Subcounty';
        $data['module'] = 'subcounty';
        $data['view_file'] = 'subcounty/edit';
        $data['id'] = (is_numeric($id)) ? $id : null;

        if (!$this->ion_auth->logged_in() || (!$this->ion_auth->is_admin())) {
            redirect('users', 'refresh');
        }

        $selected_subcounty = $this->mdl_subcounty->get($id);

        //validate form input
        $this->form_validation->set_rules('women_population', 'Women Population', 'required');
        $this->form_validation->set_rules('under_one_population', 'Under One Population', 'required');
        $this->form_validation->set_rules('total_population', 'Total Population', 'required');

        if (isset($_POST) && !empty($_POST)) {
            // do we have a valid request?
            if ($id !== $this->input->post('id')) {
                $this->session->set_flashdata('error_message', 'An error has occurred.');
                redirect('subcounty', 'refresh');
            }

            if ($this->form_validation->run() === TRUE) {

                $data = array(
                    'women_population' => $this->input->post('women_population'),
                    'under_one_population' => $this->input->post('under_one_population'),
                    'total_population' => $this->input->post('total_population'),
                );


                //check to see if we are updating the user
                if ($this->mdl_subcounty->update($selected_subcounty->id, $data, TRUE)) {

                    //redirect them back to the admin page if admin, or to the base url if non admin
                    $this->session->set_flashdata('success_message', 'Subcounty updated successfully.');
                    if ($this->ion_auth->is_admin()) {
                        redirect('subcounty', 'refresh');
                    } else {
                        redirect('/', 'refresh');
                    }

                } else {
                    //redirect them back to the admin page if admin, or to the base url if non admin
                    $this->session->set_flashdata('error_message', 'An error has occurred.');
                    if ($this->ion_auth->is_admin()) {
                        redirect('subcounty', 'refresh');
                    } else {
                        redirect('/', 'refresh');
                    }

                }

            }
        }

        //pass the subcounty to the view
        $data['selected_subcounty'] = $selected_subcounty;

        if (!is_null($selected_subcounty)) {
            $data['subcounty_name'] = array(
                'name' => 'subcounty_name',
                'id' => 'subcounty_name',
                'type' => 'text',
                'class' => 'form-control',
                'disabled' => '',
                'value' => $this->form_validation->set_value('county_name', $selected_subcounty->subcounty_name),
            );
            $data['women_population'] = array(
                'name' => 'women_population',
                'id' => 'women_population',
                'type' => 'text',
                'class' => 'form-control',
                'value' => $this->form_validation->set_value('women_population', $selected_subcounty->women_population),
            );
            $data['under_one_population'] = array(
                'name' => 'under_one_population',
                'id' => 'under_one_population',
                'type' => 'text',
                'class' => 'form-control',
                'value' => $this->form_validation->set_value('under_one_population', $selected_subcounty->under_one_population),
            );
            $data['total_population'] = array(
                'name' => 'total_population',
                'id' => 'total_population',
                'type' => 'text',
                'class' => 'form-control',
                'value' => $this->form_validation->set_value('total_population', $selected_subcounty->total_population),
            );
        }

        echo Modules::run('templates', $data);
    }


    public function subcounty_data()
    {
        $user_info = $this->users->user_info();
        //retrieve subcounty data array
        $subcounties = $this->mdl_subcounty_ext->get_all_by_field('region_id', $user_info->region_id);
        $output['data'] = $subcounties;
        echo json_encode($output);
    }

    public function subcounty_dropdown()
    {
        $subcounties = $this->mdl_subcounty->subcounty_location();
        asort($subcounties);
        return $subcounties;
    }

    /** Get subcounty json location_id by region */
    public function get_location_by_region($region_id = false)
    {
        if (isset($_POST) && !empty($_POST)) {
            if ($this->input->post('id') && is_numeric($this->input->post('id'))) {
                $id = $this->input->post('id');
                $data = $this->mdl_subcounty->subcounty_location_region($id);

                if (!is_null($data)) {
                    return $data;
                }
            } else {
                $data = $this->mdl_subcounty->subcounty_location_region($region_id);
                if (!is_null($data)) {
                    return $data;
                }
            }
        }
    }

    /** Get subcounty by county */
    public function get_by_county($county_id = false)
    {
        if (isset($_POST) && !empty($_POST)) {
            if ($this->input->post('id') && is_numeric($this->input->post('id'))) {
                $id = $this->input->post('id');
                $data = $this->mdl_subcounty->get_many_by('county_id', $id);

                if (!is_null($data)) {
                    echo json_encode($data);
                }
            }
        } else {
            $id = $county_id;
            $data = $this->mdl_subcounty->get_many_by('county_id', $id);

            if (!is_null($data)) {
                return $data;
            }
        }
    }

    public function get_by_county_dropdown($county_id = false)
    {
        if ($county_id && is_numeric($county_id)) {
            $id = $county_id;
            $data = $this->mdl_subcounty_ext->get_by_field_dropdown('county_id', $id);

            if (!is_null($data)) {
                return $data;
            }
        }
    }

    public function get_by_region_dropdown($region_id = false)
    {
        if ($region_id && is_numeric($region_id)) {
            $id = $region_id;
            $data = $this->mdl_subcounty_ext->get_by_field_dropdown('region_id', $id);

            if (!is_null($data)) {
              return $data;
            }
        }
    }

    public function get_subcounty_county_id($subcounty_id = false)
    {
        if ($subcounty_id && is_numeric($subcounty_id)) {
            $id = $subcounty_id;
            $data = $this->mdl_subcounty_ext->get_subcounty_county_id($id);

            if (!is_null($data)) {
                return $data;
            }
        }
    }

    public function getsubcountiesjson($county_id)
    {
        echo json_encode($this->mdl_subcounty_ext->get_by_county($county_id), true);
    }

    public function getscbyregionjson($region)
    {
        echo json_encode($this->mdl_subcounty_ext->get_by_r($region), true);
    }
}
