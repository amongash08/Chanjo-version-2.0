<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mdl_subcounty extends MY_Model
{


    public $_table = 'subcounties';

    public function __construct()
    {
        parent::__construct();
        $this->load->model('users/mdl_user_ext');
    }

    public function get_table()
    {
        $table = 'subcounties';

        return $table;
    }

    public function get_all()
    {
        $table = $this->get_table();
        $this->db->order_by('subcounty_name', 'asc');
        $query = $this->db->get($table);

        return $query->result();
    }

    public function get_population($station)
    {
        $this->db->select('under_one_population as population');
        $this->db->from('subcounties');
        $this->db->where('id', $station);
        $this->db->limit('1');
        $query = $this->db->get();
        return $query->result();
    }

    public function get_by_county($station)
    {
        $this->db->select('id,subcounty_name,county_id,region_id');
        $this->db->from('subcounties');
        $this->db->where('county_id', $station);
        $query = $this->db->get();
        return $query->result();
    }

    public function get_by_region($region)
    {
        $this->db->select('id,subcounty_name,county_id,region_id');
        $this->db->from('subcounties');
        $this->db->where('region_id', $region);
        $query = $this->db->get();
        return $query->result();
    }

    public function subcounty_location()
    {
        $query = $this->db->select('
                    locations.id,
                    subcounty_name as subcounty
                    ')
            ->from('locations')
            ->join('subcounties', 'subcounties.id = JSON_UNQUOTE(JSON_EXTRACT(locations.json_location, "$.subcounty"))', 'left')
            ->where('CAST(JSON_EXTRACT(locations.json_location, "$.subcounty") AS UNSIGNED) !=0')
            ->where('CAST(JSON_EXTRACT(locations.json_location, "$.facility") AS UNSIGNED) =0')
            ->get();
        $location = array();
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $location[$row->id] = $row->subcounty;
            }
            return $location;
        }
        return false;
    }

    public function subcounty_location_region($region_id)
    {
        $query = $this->db->select('
                    locations.id,
                    subcounty_name as subcounty
                    ')
            ->from('locations')
            ->join('subcounties', 'subcounties.id = JSON_UNQUOTE(JSON_EXTRACT(locations.json_location, "$.subcounty"))', 'left')
            ->where('CAST(JSON_EXTRACT(locations.json_location, "$.region") AS UNSIGNED) =' .$region_id)
            ->where('CAST(JSON_EXTRACT(locations.json_location, "$.subcounty") AS UNSIGNED) !=0')
            ->where('CAST(JSON_EXTRACT(locations.json_location, "$.facility") AS UNSIGNED) =0')
            ->get();
        $location = array();
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $location[$row->id] = $row->subcounty;
            }
            return $location;
        }
        return false;
    }

    public function get_by_id($id)
    {
        $this->db->select('');
        $this->db->from('subcounties');
        $this->db->where('id', $id);
        $this->db->limit('1');
        $query = $this->db->get();
        return $query->result();
    }

}