<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mdl_subcounty_ext extends CI_Model
{

    public $_table = 'subcounties';

    public function __construct()
    {
        parent::__construct();
    }

    public function get_all_by_field($field, $id)
    {
        $query = $this->db->select('*')
            ->where($field, $id)
            ->order_by('subcounty_name', 'asc')
            ->get($this->_table);
        if ($query->num_rows() > 0) {
            return $query->result();
        }
        return false;
    }

    public function get_by_field_dropdown($field, $id)
    {
        $query = $this->db->select('id, subcounty_name,county_id')
            ->where($field, $id)
            ->order_by('subcounty_name', 'asc')
            ->get($this->_table);
        $options = array();
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $options[$row->id] = $row->subcounty_name;
            }
            return $options;
        }
        return false;
    }

    public function get_by_county($county_id)
    {
        $query = $this->db->select('id,subcounty_name')
            ->order_by('subcounty_name', 'asc')
            ->where('county_id', $county_id)
            ->get($this->_table);
        return $query->result();
    }

    public function get_subcounty_county_id($subcounty_id)
    {
        $query = $this->db->select('id,county_id')
            ->where('id', $subcounty_id)
            ->get($this->_table);
        return $query->row();
    }

    public function get_by_r($id)
    {
        $query = $this->db->select('id, subcounty_name,county_id')
            ->where('region_id', $id)
            ->get($this->_table);

        return $query->result();;
    }


}
