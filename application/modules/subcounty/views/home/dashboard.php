<div class="row">

    <section class="col-lg-6">

        <!-- stock levels box -->
        <div class="box box-solid ">
            <div class="box-header with-border">
                <i class="fa fa-calendar"></i>

                <h3 class="box-title">
                    Stock Levels In Doses
                </h3>

            </div>

            <div class="box-body">

                <div class="chart">
                    <div id="doses" ></div>
                </div>


            </div>


        </div>
        <!-- /.box -->

    </section>

    <section class="col-lg-6">

        <!-- stock levels box -->
        <div class="box box-solid ">
            <div class="box-header with-border">
                <i class="fa fa-calendar"></i>

                <h3 class="box-title">
                    Stock Levels In MOS
                </h3>

            </div>

            <div class="box-body">

                <div class="chart">
                    <div id="mos" ></div>
                </div>


            </div>


        </div>
        <!-- /.box -->

    </section>




</div>


<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title">Latest Orders</h3>

        <div class="box-tools pull-right">
            <button data-widget="collapse" class="btn btn-box-tool" type="button"><i class="fa fa-minus"></i>
            </button>
            <button data-widget="remove" class="btn btn-box-tool" type="button"><i class="fa fa-times"></i>
            </button>
        </div>
    </div>
    <div id="orders"></div>

</div>
<!-- /.box -->


<div class="box box-solid ">
    <div class="box-header with-border">
        <i class="fa fa-clock-o"></i>

        <h3 class="box-title">
            Cold Chain
        </h3>


    </div>
    <div class="box-body">

        <div class="row">

        </div>

        <div class="chart">
            <div id="coldchain" ></div>
        </div>


    </div>


</div>

<script>

    var url = "<?php echo base_url(); ?>";


    ajax_fill_data('subcounty/order', "#orders");
    ajax_fill_data('stock/plot_stock_location/NULL/column',"#doses");
    ajax_fill_data('stock/plot_stock_mos_location/NULL/NULL/column',"#mos");
    ajax_fill_data('reports/plot_fridge_capacity',"#coldchain");

    function ajax_fill_data(function_url, div, data) {
        var function_url = url + function_url;
        var loading_icon = url + "assets/images/ring.gif";
        $.ajax({
            type: "POST",
            url: function_url,
            data: data,
            beforeSend: function () {
                $(div).html("<img style='margin:10% 50% 10% ;' src=" + loading_icon + ">");
            },
            success: function (msg) {
                $(div).html(msg);
            }
        });
    }

</script>
