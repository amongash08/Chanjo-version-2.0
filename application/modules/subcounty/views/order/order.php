<head>
    <META HTTP-EQUIV="Access-Control-Allow-Origin" CONTENT="<?php echo base_url('subcounty/home'); ?>">
</head>

    <div class="box-body" style="height: 300px">
        <div class="table-responsive">
            <table class="table no-margin">
                <thead>
                <tr>
                    <th>Order Number</th>
                    <th>Date</th>
                    <th>Collection Date</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?php if ($my_orders):
                    foreach ($my_orders as $key => $value):
                        if ($value->status != 'received'):
                            if ($key == 4) break; ?>
                            <tr>
                            <td>
                                <?php echo $value->id; ?>
                            </td>
                            <td><?php echo date('Y-m-d', strtotime($value->pickup_date)); ?></td>
                            <td><?php echo date('Y-m-d', strtotime($value->timestamp)); ?></td>
                            <?php if ($value->status == 'pending' || $value->status == 'packing'): ?>
                            <td><span class="label label-danger">Pending</span></td>
                            <td><a href="<?php echo site_url('order/order_sheet/' . $value->id); ?>"
                                   class="btn btn-primary state-change btn-xs">View</a></td>
                        <?php elseif ($value->status == 'issued'): ?>
                            <td><span class="label label-success">Issued</span></td>
                            <td><a href="<?php echo site_url('order/receive/' . $value->id); ?>"
                                   class="btn btn-primary state-change btn-xs">Receive</a></td>
                            </tr>
                        <?php endif;
                        endif;
                    endforeach;
                endif; ?>
                </tbody>
            </table>
        </div>
        <!-- /.table-responsive -->
    </div>
    <!-- /.box-body -->
    <div class="box-footer clearfix">
        <a class="btn btn-sm btn-primary btn-flat pull-left" href="<?php echo site_url('order/create'); ?>">Place New Order</a>
    </div>
    <!-- /.box-footer -->
