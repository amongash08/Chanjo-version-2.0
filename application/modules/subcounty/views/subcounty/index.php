<!-- DataTables -->
<link href="<?php echo base_url() ?>assets/plugins/datatables/datatables.css" rel="stylesheet"/>
<script src="<?php echo base_url() ?>assets/plugins/datatables/datatables.js"></script>
<div class="box">
    <div class="box-header">
        <h3 class="box-data">Below is a list of subcounties</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <table id="subcounties" class="table table-bordered table-striped" cellspacing="0" width="100%">
            <thead>
            <tr>
                <th>Subcounty Name</th>
                <th>Population</th>
                <th>Population One</th>
                <th>Women Population</th>
                <th align="center">Action</th>
            </tr>
            </thead>
            <tbody>

            </tbody>
            <tfoot>
            <tr>
                <th>Subcounty Name</th>
                <th>Population</th>
                <th>Population One</th>
                <th>Women Population</th>
                <th align="center">Action</th>
            </tr>
            </tfoot>
        </table>
    </div>
    <!-- /.box-body -->

</div>
<!-- /.box -->
<script type="text/javascript">
    $(document).ready(function () {
        $("#subcounties").DataTable({
            serverSide: false,
            ajax: {
                url: "/subcounty/subcounty_data",
                type: "POST"
            },
            columns: [
                {data: "subcounty_name"},
                {data: "women_population"},
                {data: "under_one_population"},
                {data: "total_population"},
                {
                    data: "id",
                    render: function (data, type, full) {
                        return '<a href="<?php echo base_url();?>subcounty/edit/' + data + '" class="btn btn-primary btn-xs">Edit</a>';
                    }

                }
            ]
        });

    });

</script>

