<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* Name:  default Theme Configuration File
*
* Version: 0.1
*
* Author: Amos Kamari
*
* Created:  22.06.2017
*
*/


/*
 * -----------------------------------------------------------------------------
 * Define default views 
 * -----------------------------------------------------------------------------
 */

$config['default_header'] = 'header';
$config['default_content'] = 'content';
$config['default_footer'] = 'footer';
$config['sidebar_default'] = 'sidebar';
$config['sidebar_epi'] = 'epi_sidebar';
$config['sidebar_admin'] = 'epi_admin_sidebar';

$config['auth_header'] = 'login_header';
$config['auth_content'] = 'login_content';
$config['auth_footer'] = 'login_footer';
    
/* End of file default.php */
/* Location: ./application/modules/templates/config/default.php */