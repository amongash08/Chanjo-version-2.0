<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Templates extends Auth_Controller
{
    function __construct()
    {
        parent::__construct();
    }

    function index($data)
    {

        $data['logged_in'] = $this->ion_auth->logged_in();
        $data['is_admin'] = $this->ion_auth->is_admin();
        $data['user'] = $this->ion_auth->user()->row();
        $user_groups = $this->ion_auth->get_users_groups()->row();
        $user_info = $this->user_info();

        if (!isset($data['template'])) {
            $data['template'] = $this->config->item('main_template');
        }

        //get defaults for template
        $this->load->config($data['template']);

        // if no alternative header, sidebar content or footer specified, apply template defaults
        if (!isset($data['header'])) {
            $data['header'] = $this->config->item('default_header');
        }

        if (!isset($data['content'])) {
            $data['content'] = $this->config->item('default_content');
        }


        if (!isset($data['footer'])) {
            $data['footer'] = $this->config->item('default_footer');
        }


        if (!$this->ion_auth->logged_in()) {
            $data['auth_header'] = $this->config->item('auth_header');
            $data['auth_content'] = $this->config->item('auth_content');
            $data['auth_footer'] = $this->config->item('auth_footer');

            $this->load->view('templates/' . $data['auth_header'], $data);
            $this->load->view('templates/' . $data['auth_content'], $data);
            $this->load->view('templates/' . $data['auth_footer'], $data);

        } else {

            $data['location_breadcrumb'] = [];
            if ($user_info->level_id == '1') {
                $data['location_breadcrumb'] = $user_info->nation;
            } elseif ($user_info->level_id == '2') {
                $data['location_breadcrumb'] = $user_info->nation . ' / ' . $user_info->region;
            } elseif ($user_info->level_id == '3') {
                $data['location_breadcrumb'] = $user_info->region . ' / ' . $user_info->county;
            } elseif ($user_info->level_id == '4') {
                $data['location_breadcrumb'] = $user_info->county . ' / ' . $user_info->subcounty;
            }

            if (!isset($data['sidebar'])) {
                if ($user_groups->id == 1) {
                    $data['sidebar'] = $this->config->item('sidebar_default');
                } elseif ($user_groups->id == 2) {
                    if ($user_info->level_id == 4) {
                        $data['sidebar'] = 'subcounty_sidebar';
                    } elseif ($user_info->level_id == 3) {
                        $data['sidebar'] = 'epi_admin_county_sidebar';
                    } elseif ($user_info->level_id == 2) {
                        $data['sidebar'] = 'region_sidebar';
                    } elseif ($user_info->level_id == 1) {
                        $data['sidebar'] = 'nation_sidebar';
                    } else {
                        $data['sidebar'] = $this->config->item('sidebar_admin');
                    }

                } elseif ($user_groups->id == 3) {
                    if ($user_info->level_id == 4) {
                        $data['sidebar'] = 'subcounty_sidebar';
                    } elseif ($user_info->level_id == 3) {
                        $data['sidebar'] = 'county_sidebar';
                    } elseif ($user_info->level_id == 2) {
                        $data['sidebar'] = 'region_sidebar';
                    } elseif ($user_info->level_id == 1) {
                        $data['sidebar'] = 'nation_sidebar';
                    } else {
                        $data['sidebar'] = $this->config->item('sidebar_epi');
                    }

                }
            }

            $this->load->view('templates/' . $data['header'], $data);
            $this->load->view('templates/' . $data['sidebar'], $data);
            $this->load->view('templates/' . $data['content'], $data);
            $this->load->view('templates/' . $data['footer'], $data);

        }

    }


}


/* End of file Templates.php */
/* Location: ./application/modules/templates/controllers/Templates.php */
