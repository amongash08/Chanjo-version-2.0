<!-- =============================================== -->

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?php echo $title = (isset($title)) ? $title.' |' : ''; ?>  <?php echo $subtitle = (isset($subtitle)) ? $subtitle : ''; ?>
            <small><strong>
                    <?php echo $location_breadcrumb; ?></strong></small>
        </h1>

    </section>

    <!-- Main content -->
    <section class="content">

        <?php $this->load->view($module . '/' . $view_file); ?>

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->