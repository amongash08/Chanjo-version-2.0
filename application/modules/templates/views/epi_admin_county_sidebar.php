<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">

            <li>
                <a href="<?php echo site_url('county/home'); ?>">
                    <i class="fa fa-home"></i> <span>Home</span>
                    <span class="pull-right-container">

            </span>
                </a>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-bar-chart"></i>
                    <span>Reports</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo site_url('county/reports/supply_chain'); ?>"><i class="fa fa-circle-o"></i>
                            Supply Chain</a></li>
                </ul>
            </li>

            <li class="treeview">
                <a href="#">
                    <i class="fa fa-thermometer-three-quarters"></i>
                    <span>Cold Chain</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo site_url('store'); ?>"><i class="fa fa-truck"></i> Inventory </a></li>

                    <li class="treeview menu-open">
                        <a href="#">
                            <i class="fa fa-gears"></i> <span>Spare Parts</span>
                            <span class="pull-right-container">
                              <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu" style="display: block;">
                            <li><a href="<?php echo site_url('equipment'); ?>"><i class="fa fa-wrench"></i> Spare Parts</a>
                            </li>
                            <li>
                                <a href="<?php echo site_url('jobcard'); ?>"><i class="fa fa-list-alt"></i> Job
                                    Cards</a>
                            </li>
                        </ul>
                    </li>

                </ul>
            </li>

            <li class="treeview">
                <a href="#">
                    <i class="fa fa-cogs"></i>
                    <span>Configurations</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo site_url('users'); ?>"><i class="fa fa-users"></i> User Management</a></li>
                    <li><a href="<?php echo site_url('users/migrate_user'); ?>"><i class="fa fa-users"></i> User Migration</a></li>
                    <li><a href="<?php echo site_url('users/profile'); ?>"><i class="fa fa-user"></i> My Profile</a></li>

                </ul>
            </li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>