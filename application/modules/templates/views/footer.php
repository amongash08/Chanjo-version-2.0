<footer class="main-footer">

</footer>


</div>

<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
    $.widget.bridge('uibutton', $.ui.button);
</script>

<!-- SlimScroll -->
<script src="<?php echo base_url(); ?>assets/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url(); ?>assets/plugins/fastclick/fastclick.js"></script>
<!-- FormValidation plugin and the class supports validating Bootstrap form -->
<script src="<?php echo base_url(); ?>assets/plugins/formvalidation/dist/js/formValidation.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/formvalidation/dist/js/framework/bootstrap.min.js"></script>
<!-- Select2 -->
<script src="<?php echo base_url(); ?>assets/plugins/select2/select2.full.min.js"></script>
<!-- moment -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<!-- Toastr -->
<script src="<?php echo base_url(); ?>assets/plugins/toastr/build/toastr.min.js"></script>
<!-- SweetAlert -->
<script src="<?php echo base_url(); ?>assets/plugins/sweetalert/dist/sweetalert.min.js"></script>
<!-- HighCharts -->
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/maps/modules/map.js"></script>

<script src="https://code.highcharts.com/maps/modules/exporting.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url(); ?>assets/js/adminlte.js"></script>
<!--<script src="--><?php //echo base_url(); ?><!--nodejs/node_modules/socket.io-client/dist/socket.io.js"></script>-->
<!--<script src="--><?php //echo base_url();?><!--nodejs/main.js" type="text/javascript"></script>-->
<script>

    $(document).ready(function () {
        $('.sidebar-menu').tree();
        toastr.options = {
            "closeButton": false,
            "debug": false,
            "newestOnTop": false,
            "progressBar": false,
            "positionClass": "toast-top-right",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        }
        <?php if(!empty($this->session->flashdata('success_message'))): ?>
        toastr.success('<?php echo $this->session->flashdata('success_message'); ?>');
        <?php endif; ?>

        <?php if(!empty($this->session->flashdata('error_message'))): ?>
        toastr.error('<?php echo $this->session->flashdata('error_message'); ?>');
        <?php endif; ?>
    });
</script>
</body>
</html>

  