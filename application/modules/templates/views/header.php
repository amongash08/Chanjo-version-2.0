<!DOCTYPE html>
<html>
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name='viewport' content='initial-scale=1,maximum-scale=1,user-scalable=no'/>

    <link rel="icon" type="image/png" sizes="16x16"
          href="<?php echo base_url() ?>assets/images/favicons/favicon-16x16.png">
    <link rel="manifest" href="<?php echo base_url() ?>assets/images/favicons/manifest.json">

    <meta name="theme-color" content="#ffffff">
    <title><?php echo $title; ?></title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bootstrap/css/bootstrap.min.css">

    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/ionicons/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/AdminLTE.min.css">
    <!-- Select2 -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/select2/select2.min.css">
    <!-- DataTables -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/lte-datatables/dataTables.bootstrap.css">
    <!-- FormValidation CSS file -->
    <link rel="stylesheet"
          href="<?php echo base_url(); ?>assets/plugins/formvalidation/dist/css/formValidation.min.css">
    <!-- daterange picker -->
    <!--    <link rel="stylesheet" href="-->
    <?php //echo base_url(); ?><!--assets/plugins/daterangepicker/daterangepicker.css">-->
    <!-- bootstrap datepicker -->
    <!--    <link rel="stylesheet" href="-->
    <?php //echo base_url(); ?><!--assets/plugins/datepicker/datepicker3.css">-->
    <!-- Toastr -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/toastr/build/toastr.css">
    <!-- SweetAlert -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/sweetalert/dist/sweetalert.css">
    <!-- jQuery UI CSS -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/jquery-ui-1.12.1/jquery-ui.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/skins/_all-skins.min.css">

    <link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/wow/css/libs/animate.css">


    <!-- jQuery 3.1.1 -->
    <script src="<?php echo base_url() ?>assets/plugins/jQuery/jquery-3.1.1.js"></script>
    <!-- jQuery UI 1.21.1 -->
    <script src="<?php echo base_url() ?>assets/plugins/jquery-ui-1.12.1/jquery-ui.js"></script>
    <!-- Bootstrap 3.3.6 -->
    <script src="<?php echo base_url() ?>assets/bootstrap/js/bootstrap.min.js"></script>

    <script src="<?php echo base_url() ?>assets/plugins/wow/dist/wow.js"></script>

    <script src="<?php echo base_url() ?>assets/plugins/wow/dist/wow.js"></script>


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>

    <![endif]-->
</head>
<body class="skin-blue-light sidebar-mini fixed">
<!-- Site wrapper -->
<div class="wrapper">

    <header class="main-header">
        <!-- Logo -->
        <a href="<?php echo base_url(); ?>" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini"><b>NVIP</b></span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg"><b>NVIP</b>&nbsp;Chanjo  <img width="45" height="40"
                                                                src="<?php echo base_url() ?>assets/images/coat_of_arms.png"></span>

        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>

            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <!-- Messages: style can be found in dropdown.less-->
                    <li class="dropdown messages-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-envelope-o"></i>
                            <!-- Message count <span class="label label-success"> </span> -->
                        </a>
                        <ul class="dropdown-menu">
                            <li class="header">You have no new messages</li>
                            <li>
                                <!-- inner menu: contains the actual data -->
                                <ul class="menu">
                                    <li><!-- start message -->
                                        <!-- <a href="#">
                                          <h4>
                                            Support Team
                                            <small><i class="fa fa-clock-o"></i> 5 mins</small>
                                          </h4>
                                          <p>Why not buy a new awesome theme?</p>
                                        </a>
                                      </li> -->
                                        <!-- end message -->
                                </ul>
                            </li>
                            <li class="footer"><a href="#">See All Messages</a></li>
                        </ul>
                    </li>
                    <!-- Notifications: style can be found in dropdown.less -->
                    <li class="dropdown notifications-menu">

                    </li>
                    <!-- Tasks: style can be found in dropdown.less -->
                    <li class="dropdown tasks-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-flag-o"></i>
                            <!-- <span class="label label-danger">9</span> -->
                        </a>
                        <ul class="dropdown-menu">
                            <li class="header">You have no new tasks</li>
                            <!-- <li> -->
                            <!-- inner menu: contains the actual data -->
                            <ul class="menu">
                                <!--                  <li>-->
                                <!-- Task item -->
                                <!-- <a href="#">
                                  <h3>
                                    Design some buttons
                                    <small class="pull-right">20%</small>
                                  </h3>
                                  <div class="progress xs">
                                    <div class="progress-bar progress-bar-aqua" style="width: 20%" role="progressbar"
                                         aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                                      <span class="sr-only">20% Complete</span>
                                    </div>
                                  </div>
                                </a> -->
                                <!--                  </li>-->
                                <!-- end task item -->
                            </ul>
                            </li>
                            <li class="footer">
                                <a href="#">View all tasks</a>
                            </li>
                        </ul>
                    </li>
                    <!-- User Account: style can be found in dropdown.less -->
                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" style="text-transform: capitalize;">
                            <span class="hidden-xs" id="full-name"><i class="fa fa-user-circle-o"
                                                                      style="padding-right: 1rem;"></i> <?php echo $user->first_name; ?></span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- User image -->
                            <!--  <li class="user-header">
                               <img src="" class="img-circle" alt="User Image">

                               <p>
                                 Alexander Pierce - Web Developer
                                 <small>Member since Nov. 2012</small>
                               </p>
                             </li> -->
                            <!-- Menu Body -->
                            <!-- <li class="user-body">
                              <div class="row">
                                <div class="col-xs-4 text-center">
                                  <a href="#">Followers</a>
                                </div>
                                <div class="col-xs-4 text-center">
                                  <a href="#">Sales</a>
                                </div>
                                <div class="col-xs-4 text-center">
                                  <a href="#">Friends</a>
                                </div>
                              </div> -->
                            <!-- /.row -->
                            </li>
                            <!-- Menu Footer-->
                            <li class="user-footer">
                                <!--                                <div class="pull-left">-->
                                <!--                                    <a href="-->
                                <?php //echo site_url('users/profile'); ?><!--" class="btn btn-default btn-flat"><i class="fa fa-user"></i>  Profile</a>-->
                                <!--                                </div>-->
                                <div class="pull-right">
                                    <a href="<?php echo site_url('users/logout'); ?>"
                                       class="btn btn-default btn-flat logout"> <i class="fa fa-power-off"></i> Sign out</a>
                                </div>
                            </li>
                        </ul>
                    </li>
                </ul>

            </div>
        </nav>
    </header>
    <!-- =============================================== -->
    <script type="text/javascript">
        $(document).ready(function () {

            var errorSleepTime = 2000;

            //function long_polling() {
            //    $.ajax({
            //        url: "http://192.168.99.100:8080/mini_notif_server",
            //        data: {key: "<?php //echo $this->session->userdata('current_key'); ?>//"},
            //        dataType: "json",
            //        type: "GET",
            //        success: function (response) {
            //            console.log(response);
            //            console.log(response.notification);
            //            $("#notifications-menu").append(response.notification);
            //            long_polling();
            //            // resets the value in case things get normal after a brief issue
            //            errorSleepTime = 2000;
            //        }, error: function () {
            //            errorSleepTime *= 2;
            //            console.log("errorSleepTime is: " + errorSleepTime);
            //            setTimeout(function () {
            //                long_polling();
            //            }, errorSleepTime);
            //        }
            //    });
            //}
            //
            //long_polling();

            $('.logout').click(function () {

                localStorage.clear();

            });
        });
    </script>

    <!-- amcharts Resources -->
    <script src="https://www.amcharts.com/lib/3/amcharts.js"></script>
    <script src="https://www.amcharts.com/lib/3/serial.js"></script>
    <script src="https://www.amcharts.com/lib/3/plugins/export/export.min.js"></script>
    <link rel="stylesheet" href="https://www.amcharts.com/lib/3/plugins/export/export.css" type="text/css" media="all"/>

