<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">

            <li>
                <a href="<?php echo site_url('national/home'); ?>">
                    <i class="fa fa-home"></i> <span>Home</span>
                    <span class="pull-right-container">

            </span>
                </a>
            </li>

            <li class="treeview">
                <a href="#">
                    <i class="fa fa-cubes"></i>
                    <span>Manage Stock</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo site_url('stock/count'); ?>"><i class="fa fa-calculator"></i> Physical Count</a>
                    </li>
                    <li class="treeview">
                        <a href="#">
                            <i class="fa fa-cubes"></i>
                            <span>Adjustments</span>
                            <span class="pull-right-container">
                              <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="<?php echo site_url('stock/adjust_positive'); ?>"><i class="fa fa-plus"></i>
                                    Positive
                                    Adjustment</a>
                            </li>
                            <li><a href="<?php echo site_url('stock/adjust_negative'); ?>"><i class="fa fa-minus"></i>
                                    Negative Adjustment </a></li>
                        </ul>
                    </li>

                    <li><a href="<?php echo site_url('stock/receive_voucher'); ?>"><i class="fa fa-truck"></i> Receive
                            Stock</a>
                    </li>
                    <li><a href="<?php echo site_url('stock/issue'); ?>"><i class="fa fa-hand-o-right"></i> Issue Stock
                        </a></li>
                    <li><a href="<?php echo site_url('order'); ?>"><i class="fa fa-shopping-bag"></i> Vaccine
                            Request</a></li>
                    <li><a href="<?php echo site_url('stock/ledger'); ?>"><i class="fa fa-file-text"></i> Vaccine
                            Ledgers</a></li>
                    <li><a href="<?php echo site_url('stock/supply_chain'); ?>"><i class="fa fa-bus"></i>Distribute
                            Vaccines
                        </a></li>

                </ul>
            </li>

            <li class="treeview">
                <a href="#">
                    <i class="fa fa-thermometer-three-quarters"></i>
                    <span>Cold Chain</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo site_url('store'); ?>"><i class="fa fa-truck"></i> Inventory </a></li>

                    <li class="treeview menu-open">
                        <a href="#">
                            <i class="fa fa-gears"></i> <span>Spare Parts</span>
                            <span class="pull-right-container">
                              <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu" style="display: block;">
                            <li><a href="<?php echo site_url('equipment'); ?>"><i class="fa fa-wrench"></i> Spare Parts</a></li>
                            <li >
                                <a href="<?php echo site_url('jobcard'); ?>"><i class="fa fa-list-alt"></i> Job Cards
<!--                                    <span class="pull-right-container">-->
<!--                              <i class="fa fa-angle-left pull-right"></i>-->
<!--                            </span>-->
                                </a>

                            </li>
                        </ul>
                    </li>


                    <li><a href="#"><i class="fa fa-thermometer-full"></i> Remote Temperature
                        </a></li>
                    <li><a href="#"><i class="fa fa-briefcase"></i> Equipment Management</a>
                    </li>
                    <li><a href="#"><i class="fa fa-line-chart"></i> Gap Analysis </a></li>
                    <li><a href="#"><i class="fa fa-money"></i> Planning & Budgeting</a></li>

                </ul>
            </li>

            <li class="treeview">
                <a href="#">
                    <i class="fa fa-bar-chart"></i>
                    <span>Reports</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo site_url('national/reports/supply_chain'); ?>"><i
                                    class="fa fa-circle-o"></i> Supply Chain</a></li>
                    <!-- <li><a href="<?php //echo site_url('national/reports/cold_chain'); ?>"><i class="fa fa-archive"></i>
                            Cold Chain</a></li>
                    <li><a href="<?php //echo site_url('national/reports/program_management'); ?>"><i
                                    class="fa fa-briefcase"></i> Program Management</a></li>
                    <li><a href="<?php //echo site_url('national/reports/financial_management'); ?>"><i
                                    class="fa fa-money"></i> Financial Management</a></li>
                    <li><a href="<?php //echo site_url('national/reports/general_admin'); ?>"><i class="fa fa-cog"></i>
                            General Administration</a></li> -->
                </ul>
            </li>

            <li class="treeview">
                <a href="#">
                    <i class="fa fa-cogs"></i>
                    <span>Configurations</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo site_url('users'); ?>"><i class="fa fa-users"></i> User Management</a></li>
                    <li><a href="<?php echo site_url('national/home/ledger'); ?>"><i class="fa fa-book"></i>View Stocks</a>
                    </li>
                    <li><a href="#"><i class="fa fa-medkit"></i>Vaccines</a></li>
                    <li><a href="<?php echo site_url('Notification'); ?>"><i class="fa fa-phone"></i>Communication</a>
                    </li>
                    <li><a href="<?php echo site_url('users/profile'); ?>"><i class="fa fa-user-circle-o"></i>
                            Profile</a></li>


                </ul>
            </li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
