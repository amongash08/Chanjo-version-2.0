  <!-- Left side column. contains the sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li>
                    <a href="<?php echo site_url('users'); ?>">
                        <i class="fa fa-home"></i> <span>Home</span>
                        <span class="pull-right-container">

            </span>
                    </a>
                </li>
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-bar-chart"></i>
                        <span>Reports</span>
                        <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
            </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="#"><i class="fa fa-circle-o"></i> Supply Chain</a></li>
                        <li><a href="#"><i class="fa fa-archive"></i> Cold Chain</a></li>
                        <li><a href="#"><i class="fa fa-briefcase"></i> Program Management</a></li>
                        <li><a href="#"><i class="fa fa-money"></i> Financial Management</a></li>
                    </ul>
                </li>

                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-cogs"></i>
                        <span>Configurations</span>
                        <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="<?php echo site_url('stock/ledger'); ?>"><i class="fa fa-user"></i> Stock </a></li>
                        <li><a href="<?php echo site_url('users'); ?>"><i class="fa fa-users"></i> User Management</a></li>
                        <li><a href="<?php echo site_url('facility'); ?>"><i class="fa fa-user"></i> My Facilities</a></li>
                        <li><a href="<?php echo site_url('region'); ?>"><i class="fa fa-circle-o"></i> Regions</a></li>
                        <li><a href="<?php echo site_url('county'); ?>"><i class="fa fa-circle-o"></i> Counties</a></li>
                        <li><a href="<?php echo site_url('subcounty'); ?>"><i class="fa fa-circle-o"></i> Subcounties</a></li>
                        <li><a href="<?php echo site_url('users/profile'); ?>"><i class="fa fa-user"></i> My Profile</a></li>

                    </ul>
                </li>

      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>
