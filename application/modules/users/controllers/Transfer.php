<?php if (!defined('BASEPATH')) exit("No direct script access allowed");

class Transfer extends Auth_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->module('users');
        $this->load->module('vaccine');
        $this->load->model('mdl_transfer');
        $this->load->model('mdl_user_ext');
        $this->load->model('region/mdl_region', 'region');
        $this->load->model('county/mdl_county', 'county');
        $this->load->model('subcounty/mdl_subcounty', 'subcounty');
        $this->load->model('facility/mdl_facility', 'facility');

    }

    public function transfer_user($email = false)
    {
        if ($this->mdl_transfer->check_email($email) === false) {
            $res = $this->mdl_transfer->get_nvip_user($email);
            if ($res) {
                foreach ($res as $key => $value) {
                    if ($value->session_id != '0') {
                        $data = array(
                            'username' => $value->username,
                            'password' => $value->password,
                            'email' => $value->email,
                            'first_name' => $value->f_name,
                            'last_name' => $value->l_name,
                            'phone' => $value->phone,
                            'active' => $value->active,
                            'created_on' => now(),
                            'last_login' => $value->last_activity,
                            'ip_address' => '',
                        );
                        $location_data = array(
                            'national' => (int)'1',
                            'region' => (int)$value->region,
                            'county' => (int)$value->county,
                            'subcounty' => (int)$value->subcounty,
                            'facility' => (int)$value->facility,
                        );
                        $location = $this->mdl_user_ext->get_location($location_data);

                        $registration = $this->db->insert('users', $data);
                        $insert_id = $this->db->insert_id();
                        if ($registration) {
                            $location_data = array(
                                'user_id' => $insert_id,
                                'level_id' => $value->user_level,
                                'location_id' => ($location) ? (int)$location->id : (int)$location,
                            );

                            $group_data = array(
                                'user_id' => $insert_id,
                                'group_id' => $value->user_group,
                            );

                            $this->db->insert('users_groups', $group_data);
                            $this->mdl_user_ext->insert_location($location_data);
                        }

                    }
                }
                return $data = ['current_id' => $insert_id, 'previous_id' => $value->id];
            }
        } else {
            return false;
        }
    }

    public function update_user_id($data = array())
    {
        if (!empty($data)) {
            $tables = ['transactions', 'store', 'requests', 'packing', 'batch_balances'];
            $stmt = 'UPDATE $table SET user_id = $current_id WHERE user_id = $previous_id';
            foreach ($tables as $name) {
                $stmt = str_replace('$table', $name, $stmt);
                $stmt = str_replace('$current_id', $data['current_id'], $stmt);
                $stmt = str_replace('$previous_id', $data['previous_id'], $stmt);
                $this->db->query($stmt);
            }
        }

    }

    public function get_location_id($name)
    {
        $res = $this->mdl_transfer->get_location_id($name);
        if ($res) {
            return $res->id;
        }
        return false;
    }

    public function cleanup_locations($location_id = false)
    {
        if (is_numeric($location_id)) {
            $location = $this->users->location_name($location_id);
        } else {
            $user_info = $this->users->user_info();
            if ($user_info->level_id == 1) {
                $location = $user_info->nation;
            } elseif ($user_info->level_id == 2) {
                $location = $user_info->region;
            } elseif ($user_info->level_id == 3) {
                $location = $user_info->county;
            } elseif ($user_info->level_id == 4) {
                $location = $user_info->subcounty;
            }
        }
        $query = $this->db->distinct()
            ->select('to_from')
            ->from('nvip.tbl_transaction')
            ->where('station', $location)
            ->get();
        $data = [];
        foreach ($query->result() as $key => $value) {
            if (!empty($value->to_from)) {
                $data[$value->to_from] = $this->get_location_id($value->to_from);
            }

        }
        foreach ($data as $key => $value) {
            if ($value) {
                $sql = 'UPDATE <TABLE_NAME> SET <FIELD_NAME> = ' . $value . ' WHERE <FIELD_NAME> = "' . $key . '" AND location_id =  "' . $location . '" ;';
                $sql_array[] = $sql;
            }

        }

        $statement = [];
        foreach ($sql_array as $key => $value) {
            $table_names = array('transactions');
            $table_name = array('<TABLE_NAME>');
            $field_name = array('<FIELD_NAME>');
            foreach ($table_names as $table => $name) {
                $field_names = array('to_from');
                foreach ($field_names as $field => $val) {
                    $another_formatted_sql = str_replace($field_name, $val, $value);
                    $formatted_sql = str_replace($table_name, $name, $another_formatted_sql);
                    $statement[] = $formatted_sql;
                }

            }
        }
        $characters = ['"', '[', ']', ','];
        $statement = str_replace($characters, '', json_encode($statement, JSON_PRETTY_PRINT));
        $strip_statement = str_replace("\\", '"', $statement);
        if (!write_file('docs/sql/sql_update-' . $location . '.sql', $strip_statement)) {
            show_404();
        } else {
            if (file_exists('docs/sql/sql_update-' . $location . '.sql')) {
                $transaction_table = 'UPDATE transactions SET location_id = ' . $this->get_location_id($location) . ' WHERE location_id = "' . $location . '" ; ';
                $batch_table = 'UPDATE batch_balances SET location_id = ' . $this->get_location_id($location) . ' WHERE location_id = "' . $location . '" ; ';
                $balance_table = 'UPDATE balances SET location_id = ' . $this->get_location_id($location) . ' WHERE location_id = "' . $location . '" ; ';
                $alter = [$transaction_table, $batch_table, $balance_table];
                $statement1 = str_replace($characters, '', json_encode($alter, JSON_PRETTY_PRINT));
                $strip_statement1 = str_replace("\\", '"', $statement1);
                write_file('docs/sql/sql_update-' . $location . '.sql', $strip_statement1, 'a');

                $sql = file_get_contents('docs/sql/sql_update-' . $location . '.sql');
                $sql_s = explode(';', $sql);
                array_pop($sql_s);

                foreach ($sql_s as $sql_statement) {
                    $sql_statement = $sql_statement . ";";
                    $this->db->query($sql_statement);
                }
                delete_files('docs/sql/sql_update-' . $location . '.sql');
                return true;
            } else {
                show_404();
            }

        }
    }

    public function move_transactions($location_id = false, $user_id = false)
    {

        if (is_numeric($location_id)) {
            $location = $this->users->location_name($location_id);
        } else {
            $user_info = $this->users->user_info();
            if ($user_info->level_id == 1) {
                $location = $user_info->nation;
            } elseif ($user_info->level_id == 2) {
                $location = $user_info->region;
            } elseif ($user_info->level_id == 3) {
                $location = $user_info->county;
            } elseif ($user_info->level_id == 4) {
                $location = $user_info->subcounty;
            }
            $location_id = $user_info->location_id;
        }

        $con = [
            'host' => $this->db->hostname,
            'port' => 3306,
            'database' => $this->db->database,
            'user' => $this->db->username,
            'password' => $this->db->password,
        ];
        $con = json_encode($con);

        $migrate = system("python " . APPPATH . "../scripts/migrate.py '$con' '$location' '$location_id'");
        $this->cleanup_locations($location_id);
        foreach ($this->vaccine->vaccine_dropdown() as $key => $value) {
            $this->recalculate($key);
        }
    }

    public function get_users()
    {
        $user_info = $this->users->user_info();
        if ($user_info->level_id == 1) {
            $location = $user_info->nation_id;
        } elseif ($user_info->level_id == 2) {
            $location = $user_info->region_id;
        } elseif ($user_info->level_id == 3) {
            $location = $user_info->county_id;
        } elseif ($user_info->level_id == 4) {
            $location = $user_info->subcounty_id;
        }

        $users = $this->mdl_transfer->get_my_users($user_info->level_id, $location);
        $array = array();
        foreach ($users as $key => $value) {

            $location_data = array(
                'national' => (int)'1',
                'region' => (int)$value->region,
                'county' => (int)$value->county,
                'subcounty' => (int)$value->subcounty,
                'facility' => (int)$value->facility,
            );
            $location = $this->users->location_id($location_data);
            $location_name = $this->users->location_name($location);
            $data[] = array(
                'email' => $value->email,
                'full_name' => $value->f_name . '&nbsp;' . $value->l_name,
                'phone' => $value->phone,
                'location_name' => $location_name,
                'migrate_id' => $location
            );

        }
        $array['data'] = $data;
        echo json_encode($array);
    }

    public function migrate()
    {
        if (!$this->ion_auth->in_group(1) && !$this->ion_auth->in_group(2)) {
            if ($this->input->post()) {
                $error = 'You are not authorized to do this. Please contact your supervisor';
                header("HTTP/1.1 500 Internal Server Error");
                echo $error;
            } else {
                show_404();
            }
        } else {
            if ($this->input->post()) {
                $location_id = $this->input->post('migrate_id');
                $email = $this->input->post('email');
            } else {
                $location_id = false;
                $email = null;
            }

            if (is_numeric($location_id) && valid_email($email)) {
                if (!empty($this->transfer_user($email))) {
                    $this->move_transactions($location_id);
                    $this->cleanup_locations($location_id);
                    $this->update_user_id($this->transfer_user($email));
                    $this->deactivate_user($email);
                    $this->send_migration_email($email);
                } else {
                    $this->move_transactions($location_id);
                    $this->cleanup_locations($location_id);
                    $this->send_migration_email($email);
                }

            }
        }
    }

    public function deactivate_user($email)
    {
        $this->mdl_transfer->deactivate_user($email);
    }

    public function send_migration_email($email)
    {
        date_default_timezone_set('Africa/Nairobi');
        $this->email->from('info@epikenya.org', 'Chanjo eLMIS');
        $this->email->to($email);
        $this->email->subject('System Update');
        $data = [];
        $body = $this->load->view('users/email/migrate', $data, TRUE);
        $this->email->message($body);
        if (!$this->email->send()) {
            echo "Please contact your system administrator";
        } else {
            echo "User successfully migrated. An email has been automatically sent to inform the user.";
        }
    }
}