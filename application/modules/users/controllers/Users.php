<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends Auth_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('users/mdl_user', 'user');
//        $this->load->model('notification/mdl_notification', 'notification');

    }

    //redirect if needed, otherwise display the user list
    function index()
    {
        if (!$this->ion_auth->logged_in()) {
            redirect('users/login');
        }

        $data['title'] = "Configurations";
        $data['subtitle'] = 'Users';
        $data['module'] = 'users';
        $data['view_file'] = 'index';
        $user_info = $this->user_info();
        if ($this->ion_auth->in_group(3)) {

            if ($user_info->level_id == 1) {
                redirect('national/home');
            } elseif ($user_info->level_id == 2) {
                redirect('region/home');
            } elseif ($user_info->level_id == 3) {
                redirect('county/home');
            } elseif ($user_info->level_id == 4) {
                redirect('subcounty/home');
            }
        } else if ($this->ion_auth->in_group(2)) {
            $data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
            echo Modules::run('templates', $data);

        } else {
            //set the flash data error message if there is one
            $data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
            echo Modules::run('templates', $data);

        }
    }

    //log the user in
    function login()
    {
        $data['title'] = "Login";
        $data['page_title'] = 'Login';
        $data['module'] = 'users';
        $data['view_file'] = 'login';

        if ($this->input->post()) {

            //validate form input
            $this->form_validation->set_rules('email', 'Email', 'trim|required');
            $this->form_validation->set_rules('password', 'Password', 'trim|required');
            if ($this->form_validation->run() == true) {
                //check to see if the user is logging in
                $remember = false;
                $email = $this->input->post('email');
                $password = $this->input->post('password');
                if ($this->user_ext->login($email, $this->hash_it($password))) {
                    $res = $this->user_ext->login($email, $this->hash_it($password));
                    $this->ion_auth->set_session($res);

                    $this->transfer->move_transactions();
                    $this->transfer->cleanup_locations();
                    $this->transfer->deactivate_user($email);
                    redirect('users/update_password');

                    //if the login is successful, update password
                } elseif ($this->ion_auth->login($email, $password, $remember)) {
                    //if the login is successful
                    $user_info = $this->user_info();
//                    $user = $this->notification->get_user_with_email($user_info->email);


//                    if (!$user) {
//                        $name = $user_info->first_name . ' ' . $user_info->last_name;
//                        $slug = explode("@", $name);
//                        $user_data = array(
//                            "name" => $name,
//                            "email" => $email,
//                            "slug"=> $slug
//                        );

//                        $this->notification->insert_user($user_data);
//                        $user = $this->notification->get_user_with_email($user_info->email);
//                    }
//                    $rand = substr(md5(microtime()), rand(0, 26), 5);

//                    $this->notification->update_current_key_for_user($user[0]["_id"], $rand);
//
//                    $session_data = array("_id" => (string)$user[0]["_id"], "current_key" => $rand);
//                    $this->session->set_userdata($session_data);


                    $this->session->set_flashdata('success_message', $this->ion_auth->messages());
                    if (!$this->ion_auth->is_admin()) {

                        if ($user_info->level_id == 1) {
                            redirect('national/home');
                        } elseif ($user_info->level_id == 2) {
                            redirect('region/home');
                        } elseif ($user_info->level_id == 3) {
                            redirect('county/home');
                        } elseif ($user_info->level_id == 4) {
                            redirect('subcounty/home');
                        }

                    } else {
                        redirect('users');
                    }

                } else {
                    //if the login was un-successful
                    //redirect them back to the login page
                    $this->session->set_flashdata('message', '<div id="message" class="alert alert-danger text-center">' . $this->ion_auth->errors() . '</div>');
                    //use redirects instead of loading views for compatibility with MY_Controller libraries
                    redirect('users/login', 'refresh');
                }
            }
        } else {

            //the user is not logging in so display the login page
            //set the flash data error message if there is one
            $data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
            $data['email'] = array(
                'name' => 'email',
                'id' => 'email',
                'type' => 'email',
                'class' => 'form-control',
                'placeholder' => 'Email',
                'value' => $this->form_validation->set_value('email'),
            );
            $data['password'] = array(
                'name' => 'password',
                'id' => 'password',
                'type' => 'password',
                'class' => 'form-control',
                'placeholder' => 'Password',
            );

            echo Modules::run('templates', $data);
        }
    }


    function update_password()
    {
        $data['title'] = 'Update Password';
        $data['page_title'] = 'Update Password';
        $data['module'] = 'users';
        $data['view_file'] = 'update_password';
        $data['header'] = 'login_header';
        $data['sidebar'] = 'empty_sidebar';
        $data['content'] = 'login_content';
        $data['footer'] = 'login_footer';

        $user = $this->session->userdata();

        $this->form_validation->set_rules('new', $this->lang->line('change_password_validation_new_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[new_confirm]');
        $this->form_validation->set_rules('new_confirm', $this->lang->line('change_password_validation_new_password_confirm_label'), 'required');

        if ($this->form_validation->run() == true) {
            if ($this->input->post()) {
                $pass = $this->db->select('password')
                    ->where('email', $user['identity'])
                    ->limit(1)
                    ->order_by('id', 'desc')
                    ->get('users');
                if ($pass->row()) {
                    $change = $this->ion_auth->change_password($user['identity'], $pass->row()->password, $this->input->post('new'), true);

                    if ($change) {
                        //  if the password was successfully changed
                        $this->session->set_flashdata('success_message', $this->ion_auth->messages());
                        redirect('users', 'refresh');
                    } else {
                        $this->session->set_flashdata('message', '<div id="message" class="alert alert-danger text-center">' . $this->ion_auth->errors() . '</div>');
                        redirect(current_url());
                    }
                }
            }
        }
        //display the form
        //set the flash data error message if there is one
        $data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

        $data['min_password_length'] = $this->config->item('min_password_length', 'ion_auth');

        $data['new_password'] = array(
            'name' => 'new',
            'id' => 'new',
            'type' => 'password',
            'pattern' => '^.{' . $data['min_password_length'] . '}.*$',
            'class' => 'form-control',
            'placeholder' => 'New Password',
        );
        $data['new_password_confirm'] = array(
            'name' => 'new_confirm',
            'id' => 'new_confirm',
            'type' => 'password',
            'pattern' => '^.{' . $data['min_password_length'] . '}.*$',
            'class' => 'form-control',
            'placeholder' => 'Confirm New Password',
        );


        echo Modules::run('templates', $data);
    }


    private function hash_it($password)
    {
        $safe_password = $this->salt($password);
        $safe_password = hash('sha512', $safe_password);
        return $safe_password;
    }


    private function salt($password)
    {
        $pass = $password .= '3456789abhijklmnopqrstuvRSTUVWYZ@#$%^&*';
        return $pass;
    }

    //log the user out
    function logout()
    {
        if ($this->ion_auth->logged_in()) {
            $this->session->unset_userdata(array('id', 'user_id'));
            $this->ion_auth->logout();
            //redirect them to the login page
            $this->session->set_flashdata('message', $this->ion_auth->messages());
            header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
            header("Expires: Wed, 4 Jul 2012 05:00:00 GMT"); // Date in the past
            redirect('users/login', 'refresh');
        } else {
            redirect('users', 'refresh');
        }
    }

    //change password
    function change_password()
    {
        $data['title'] = "Change Password";
        $data['page_title'] = 'Change Password';
        $data['module'] = 'users';
        $data['view_file'] = 'change_password';

        $this->form_validation->set_rules('old', $this->lang->line('change_password_validation_old_password_label'), 'required');
        $this->form_validation->set_rules('new', $this->lang->line('change_password_validation_new_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[new_confirm]');
        $this->form_validation->set_rules('new_confirm', $this->lang->line('change_password_validation_new_password_confirm_label'), 'required');

        if (!$this->ion_auth->logged_in()) {
            redirect('users/login', 'refresh');
        }

        $user = $this->ion_auth->user()->row();

        if ($this->form_validation->run() == false) {
            //display the form
            //set the flash data error message if there is one
            $data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

            $data['min_password_length'] = $this->config->item('min_password_length', 'ion_auth');
            $data['old_password'] = array(
                'name' => 'old',
                'id' => 'old',
                'type' => 'password',
            );
            $data['new_password'] = array(
                'name' => 'new',
                'id' => 'new',
                'type' => 'password',
                'pattern' => '^.{' . $data['min_password_length'] . '}.*$',
            );
            $data['new_password_confirm'] = array(
                'name' => 'new_confirm',
                'id' => 'new_confirm',
                'type' => 'password',
                'pattern' => '^.{' . $data['min_password_length'] . '}.*$',
            );
            $data['user_id'] = array(
                'name' => 'user_id',
                'id' => 'user_id',
                'type' => 'hidden',
                'value' => $user->id,
            );

            echo Modules::run('templates', $data);
        } else {
            $identity = $this->session->userdata('identity');

            $change = $this->ion_auth->change_password($identity, $this->input->post('old'), $this->input->post('new'));

            if ($change) {
                //if the password was successfully changed
                $this->session->set_flashdata('message', $this->ion_auth->messages());
                $this->logout();
            } else {
                $this->session->set_flashdata('message', $this->ion_auth->errors());
                redirect('users/change_password', 'refresh');
            }
        }
    }

    //forgot password
    function forgot_password()
    {
        if ($this->ion_auth->logged_in()) {
            redirect('users');
        }
        $data['title'] = "Forgotten Password";
        $data['page_title'] = 'Forgotten Password';
        $data['module'] = 'users';
        $data['view_file'] = 'forgot_password';

        //setting validation rules by checking wheather email is username or email
        if ($this->config->item('identity', 'ion_auth') == 'username') {
            $this->form_validation->set_rules('email', $this->lang->line('forgot_password_username_identity_label'), 'required');
        } else {
            $this->form_validation->set_rules('email', $this->lang->line('forgot_password_validation_email_label'), 'required|valid_email');
        }


        if ($this->form_validation->run() == false) {
            //setup the input
            $data['email'] = array('name' => 'email',
                'id' => 'email',
                'class' => 'form-control',
                'type' => 'email',
                'placeholder' => 'Email Address',
            );

            if ($this->config->item('identity', 'ion_auth') == 'username') {
                $data['identity_label'] = $this->lang->line('forgot_password_username_identity_label');
            } else {
                $data['identity_label'] = $this->lang->line('forgot_password_email_identity_label');
            }

            //set any errors and display the form
            $data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

            echo Modules::run('templates', $data);

        } else {
            // get email from username or email
            if ($this->config->item('identity', 'ion_auth') == 'username') {
                $email = $this->ion_auth->where('username', strtolower($this->input->post('email')))->users()->row();
            } else {
                $email = $this->ion_auth->where('email', strtolower($this->input->post('email')))->users()->row();
            }
            if (empty($email)) {

                if ($this->config->item('email', 'ion_auth') == 'username') {
                    $this->ion_auth->set_message('forgot_password_username_not_found');
                } else {
                    $this->ion_auth->set_message('forgot_password_email_not_found');
                }

                $this->session->set_flashdata('message', $this->ion_auth->messages());
                redirect("users/forgot_password", 'refresh');
            }

            //run the forgotten password method to email an activation code to the user
            $forgotten = $this->ion_auth->forgotten_password($email->{$this->config->item('identity', 'ion_auth')});

            if ($forgotten) {
                //if there were no errors
                $this->session->set_flashdata('message', $this->ion_auth->messages());
                redirect("users/login", 'refresh'); //we should display a confirmation page here instead of the login page
            } else {
                $this->session->set_flashdata('message', $this->ion_auth->errors());
                redirect("users/forgot_password", 'refresh');
            }
        }
    }

    //reset password - final step for forgotten password
    public function reset_password($code = NULL)
    {
        $data['title'] = "Reset Password";
        $data['page_title'] = 'Reset Password';
        $data['module'] = 'users';
        $data['view_file'] = 'reset_password';

        if (!$code) {
            show_404();
        }

        $user = $this->ion_auth->forgotten_password_check($code);

        if ($user) {
            //if the code is valid then display the password reset form

            $this->form_validation->set_rules('new', $this->lang->line('reset_password_validation_new_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[new_confirm]');
            $this->form_validation->set_rules('new_confirm', $this->lang->line('reset_password_validation_new_password_confirm_label'), 'required');

            if ($this->form_validation->run() == false) {
                //display the form

                //set the flash data error message if there is one
                $data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

                $data['min_password_length'] = $this->config->item('min_password_length', 'ion_auth');
                $data['new_password'] = array(
                    'name' => 'new',
                    'id' => 'new',
                    'type' => 'password',
                    'class' => 'form-control',
                    'pattern' => '^.{' . $data['min_password_length'] . '}.*$',
                );
                $data['new_password_confirm'] = array(
                    'name' => 'new_confirm',
                    'id' => 'new_confirm',
                    'type' => 'password',
                    'class' => 'form-control',
                    'pattern' => '^.{' . $data['min_password_length'] . '}.*$',
                );
                $data['user_id'] = array(
                    'name' => 'user_id',
                    'id' => 'user_id',
                    'type' => 'hidden',
                    'class' => 'form-control',
                    'value' => $user->id,
                );
                $data['code'] = $code;

                //render
                echo Modules::run('templates', $data);
            } else {
                // do we have a valid request?
                if ($user->id != $this->input->post('user_id')) {

                    //something fishy might be up
                    $this->ion_auth->clear_forgotten_password_code($code);

                } else {
                    // finally change the password
                    $identity = $user->{$this->config->item('identity', 'ion_auth')};

                    $change = $this->ion_auth->reset_password($identity, $this->input->post('new'));

                    if ($change) {
                        //if the password was successfully changed
                        $this->session->set_flashdata('message', $this->ion_auth->messages());
                        $this->logout();
                    } else {
                        $this->session->set_flashdata('message', $this->ion_auth->errors());
                        redirect('users/reset_password/' . $code, 'refresh');
                    }
                }
            }
        } else {
            //if the code is invalid then send them back to the forgot password page
            $this->session->set_flashdata('message', $this->ion_auth->errors());
            redirect("users/forgot_password", 'refresh');
        }
    }


    //activate the user
    function activate($id, $code = false)
    {
        if ($code !== false) {
            $activation = $this->ion_auth->activate($id, $code);
        } else if ($this->ion_auth->in_group(1) || $this->ion_auth->in_group(2)) {
            $activation = $this->ion_auth->activate($id);
        }

        if ($activation) {
            //redirect them to the users page
            $this->session->set_flashdata('success_message', $this->ion_auth->messages());
            redirect("users", 'refresh');
        } else {
            //redirect them to the forgot password page
            $this->session->set_flashdata('message', $this->ion_auth->errors());
            redirect("users/forgot_password", 'refresh');
        }
    }

    //deactivate the user
    function deactivate($id = NULL)
    {
        $data['title'] = 'Deactivate User';
        $data['page_title'] = 'Deactivate User';
        $data['module'] = 'users';
        $data['view_file'] = 'deactivate_user';


        if (!$this->ion_auth->in_group(1) && !$this->ion_auth->in_group(2)) {
            //redirect them to the home page because they must be an administrator to view this
            return show_error('You must be an administrator to view this page.');
        }

        if (!$id) {
            redirect('users');
        }

        $id = (int)$id;

        $this->load->library('form_validation');
        $this->form_validation->set_rules('confirm', $this->lang->line('deactivate_validation_confirm_label'), 'required');
        $this->form_validation->set_rules('id', $this->lang->line('deactivate_validation_user_id_label'), 'required|alpha_numeric');

        if ($this->form_validation->run() == FALSE) {

            $data['selected_user'] = $this->ion_auth->user($id)->row();
            $data['id'] = $id;
            echo Modules::run('templates', $data);
        } else {
            // do we really want to deactivate?
            if ($this->input->post('confirm') == 'yes') {
                // do we have a valid request?
                if ($id != $this->input->post('id')) {
                    show_error('Incorrect Submission');
                }

                // do we have the right userlevel?
                if ($this->ion_auth->logged_in() && ($this->ion_auth->in_group(1) || $this->ion_auth->in_group(2))) {
                    $this->ion_auth->deactivate($id);
                }
            }
            $this->session->set_flashdata('success_message', 'Account Deactivated');
            //redirect them back to the users page
            redirect('users', 'refresh');
        }
    }

    //create a new user
    function create_user()
    {
        $data['title'] = "Create User";
        $data['subtitle'] = 'Create User';
        $data['module'] = 'users';
        $data['view_file'] = 'create_user';

        if (!$this->ion_auth->in_group(1) && !$this->ion_auth->in_group(2)) {
            redirect('users', 'refresh');
        }

        $tables = $this->config->item('tables', 'ion_auth');
        $groups = $this->ion_auth->groups()->result_array();
        $group_ids = [];
        foreach ($groups as $group) {
            if ((int)$group['id'] !== 1) {
                $group_ids[$group['id']] = $group['description'];
            }
        }

        $levels = $this->user_ext->levels()->result_array();
        $level_ids = [];
        foreach ($levels as $level) {
            $level_ids[$level['id']] = $level['description'];

        }

        //validate form input
        $this->form_validation->set_rules('first_name', $this->lang->line('create_user_validation_fname_label'), 'required');
        $this->form_validation->set_rules('last_name', $this->lang->line('create_user_validation_lname_label'), 'required');
        $this->form_validation->set_rules('email', $this->lang->line('create_user_validation_email_label'), 'required|valid_email|is_unique[' . $tables['users'] . '.email]');
        $this->form_validation->set_rules('phone', $this->lang->line('create_user_validation_phone_label'), 'required');
        $this->form_validation->set_rules('password', $this->lang->line('create_user_validation_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[password_confirm]');
        $this->form_validation->set_rules('password_confirm', $this->lang->line('create_user_validation_password_confirm_label'), 'required');

        if ($this->form_validation->run() == true) {
            $username = strtolower($this->clean($this->input->post('first_name'))) . '.' . strtolower($this->clean($this->input->post('last_name')));
            $email = strtolower($this->input->post('email'));
            $password = $this->input->post('password');

            $additional_data = array(
                'first_name' => $this->input->post('first_name'),
                'last_name' => $this->input->post('last_name'),
                'phone' => $this->input->post('phone'),
            );


        }
        if ($this->form_validation->run() == true) {
            //check to see if we are creating the user
            $registration = $this->ion_auth->register($username, $password, $email, $additional_data);
            $location_data = array(
                'national' => 1,
                'region' => (int)$this->input->post('region'),
                'county' => (int)$this->input->post('county'),
                'subcounty' => (int)$this->input->post('subcounty'),
                'facility' => 0,
            );
            $location = $this->user_ext->get_location($location_data);

            if ($registration && $location) {
                $location_array = array(
                    'user_id' => $registration['id'],
                    'level_id' => $this->input->post('level'),
                    'location_id' => ($location) ? (int)$location->id : (int)$location,
                );
                $this->user_ext->insert_location($location_array);
            }

            //redirect them back to the admin page
            $this->session->set_flashdata('message', $this->ion_auth->messages());
            redirect("users", 'refresh');
        } else {
            //display the create user form

            $data['first_name'] = array(
                'name' => 'first_name',
                'id' => 'first_name',
                'type' => 'text',
                'class' => 'form-control',
                'value' => $this->form_validation->set_value('first_name'),
            );
            $data['last_name'] = array(
                'name' => 'last_name',
                'id' => 'last_name',
                'type' => 'text',
                'class' => 'form-control',
                'value' => $this->form_validation->set_value('last_name'),
            );
            $data['email'] = array(
                'name' => 'email',
                'id' => 'email',
                'type' => 'email',
                'class' => 'form-control',
                'value' => $this->form_validation->set_value('email'),
            );
            $data['phone'] = array(
                'name' => 'phone',
                'id' => 'phone',
                'type' => 'tel',
                'class' => 'form-control',
                'value' => $this->form_validation->set_value('phone'),
            );
            $data['password'] = array(
                'name' => 'password',
                'id' => 'password',
                'type' => 'password',
                'class' => 'form-control',
                'value' => $this->form_validation->set_value('password'),
            );
            $data['password_confirm'] = array(
                'name' => 'password_confirm',
                'id' => 'password_confirm',
                'type' => 'password',
                'class' => 'form-control',
                'value' => $this->form_validation->set_value('password_confirm'),
            );

            $group_option = ['' => 'Select User group'];
            $data['group'] = array(
                'name' => 'group',
                'id' => 'group',
                'class' => 'form-control',
                'options' => $group_option + $group_ids
            );

            $level_option = ['' => 'Select User level'];
            $data['level'] = array(
                'name' => 'level',
                'id' => 'level',
                'class' => 'form-control',
                'options' => $level_option + $level_ids
            );

            $national_option = ['1' => 'National'];
            $data['national'] = array(
                'name' => 'national',
                'id' => 'national',
                'class' => 'form-control',
                'options' => $national_option
            );

            $region_option = ['' => 'Select Region'];
            $regions = $this->region->dropdown('region_name');
            $data['region'] = array(
                'name' => 'region',
                'id' => 'region',
                'class' => 'form-control',
                'options' => $region_option + $regions
            );

            echo Modules::run('templates', $data);
        }
    }

    public function profile()
    {
        $data['title'] = "Profile";
        $data['subtitle'] = 'Users';
        $data['module'] = 'users';
        $data['view_file'] = 'profile';
        if (!$this->ion_auth->logged_in()) {
            redirect('users', 'refresh');
        }
        $user = $this->user_info();
        $selected_user = $this->ion_auth->user($user->id)->row();

        if (isset($_POST) && !empty($_POST)) {

            //update the password if it was posted
            if ($this->input->post('password')) {
                $this->form_validation->set_rules('password', $this->lang->line('edit_user_validation_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[password_confirm]');
                $this->form_validation->set_rules('password_confirm', $this->lang->line('edit_user_validation_password_confirm_label'), 'required');
            }

            if ($this->form_validation->run() === TRUE) {
                $data = array(
                    'first_name' => $this->input->post('first_name'),
                    'last_name' => $this->input->post('last_name'),
                    'phone' => $this->input->post('phone'),
                );

                //update the password if it was posted
                if ($this->input->post('password')) {
                    $data['password'] = $this->input->post('password');
                }

                //check to see if we are updating the user
                if ($this->ion_auth->update($selected_user->id, $data)) {
                    //redirect them back to the admin page if admin, or to the base url if non admin
                    $this->session->set_flashdata('success_message', $this->ion_auth->messages());
                    redirect('users/profile', 'refresh');


                } else {
                    //redirect them back to the admin page if admin, or to the base url if non admin
                    $this->session->set_flashdata('success_message', $this->ion_auth->errors());
                    redirect('users/profile', 'refresh');

                }
            }
        }


        //set the flash data error message if there is one
        $data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));

        $data['first_name'] = array(
            'name' => 'first_name',
            'id' => 'first_name',
            'type' => 'text',
            'class' => 'form-control',
            'value' => $this->form_validation->set_value('first_name', $selected_user->first_name),
        );
        $data['last_name'] = array(
            'name' => 'last_name',
            'id' => 'last_name',
            'type' => 'text',
            'class' => 'form-control',
            'value' => $this->form_validation->set_value('last_name', $selected_user->last_name),
        );
        $data['phone'] = array(
            'name' => 'phone',
            'id' => 'phone',
            'type' => 'tel',
            'class' => 'form-control',
            'value' => $this->form_validation->set_value('phone', $selected_user->phone),
        );
        $data['password'] = array(
            'name' => 'password',
            'id' => 'password',
            'type' => 'password',
            'class' => 'form-control',
        );
        $data['password_confirm'] = array(
            'name' => 'password_confirm',
            'id' => 'password_confirm',
            'type' => 'password',
            'class' => 'form-control',
        );


        echo Modules::run('templates', $data);

    }

    //edit a user
    function edit_user($id)
    {
        $data['title'] = "Edit User";
        $data['subtitle'] = 'Edit User';
        $data['module'] = 'users';
        $data['view_file'] = 'edit_user';

        if (!$this->ion_auth->logged_in() || (!$this->ion_auth->in_group(1) && !$this->ion_auth->in_group(2) && !($this->ion_auth->user()->row()->id == $id))) {
            redirect('users', 'refresh');
        }

        $selected_user = $this->user_info($id);
        if (!$selected_user) {
            redirect('users', 'refresh');
        }
        $groups = $this->ion_auth->groups()->result_array();
        $currentGroups = $this->ion_auth->get_users_groups($id)->result();
        $group_ids = [];
        $group_id = null;
        foreach ($groups as $group) {
            if ((int)$group['id'] !== 1) {
                $group_ids[$group['id']] = $group['description'];
            }
        }
        foreach ($currentGroups as $group) {
            if ((int)$group->id !== 1) {
                $group_id = $group->id;
            }
        }

        $levels = $this->user_ext->levels()->result_array();
        $level_ids = [];
        $level_id = $this->user_info($id)->level_id;
        foreach ($levels as $level) {
            $level_ids[$level['id']] = $level['description'];

        }

        //validate form input
        $this->form_validation->set_rules('first_name', $this->lang->line('edit_user_validation_fname_label'), 'required');
        $this->form_validation->set_rules('last_name', $this->lang->line('edit_user_validation_lname_label'), 'required');
        $this->form_validation->set_rules('phone', $this->lang->line('edit_user_validation_phone_label'), 'required');

        if (isset($_POST) && !empty($_POST)) {

            // do we have a valid request?
            if ($id != $this->input->post('id')) {
                $this->session->set_flashdata('error_message', 'Incorrect Submission');

                redirect('users/edit_user/' . $id, 'refresh');
            }


            //update the password if it was posted
            if ($this->input->post('password')) {
                $this->form_validation->set_rules('password', $this->lang->line('edit_user_validation_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[password_confirm]');
                $this->form_validation->set_rules('password_confirm', $this->lang->line('edit_user_validation_password_confirm_label'), 'required');
            }

            if ($this->form_validation->run() === TRUE) {
                $data = array(
                    'first_name' => $this->input->post('first_name'),
                    'last_name' => $this->input->post('last_name'),
                    'phone' => $this->input->post('phone'),
                );

                $location_data = array(
                    'national' => 1,
                    'region' => (int)$this->input->post('region'),
                    'county' => (int)$this->input->post('county'),
                    'subcounty' => (int)$this->input->post('subcounty'),
                    'facility' => 0,
                );
                $location = $this->user_ext->get_location($location_data);
                $location_array = array(
                    'level_id' => $this->input->post('level'),
                    'location_id' => ($location) ? (int)$location->id : (int)$location,
                );
                $this->user_ext->update_location($id, $location_array);

                //update the password if it was posted
                if ($this->input->post('password')) {
                    $data['password'] = $this->input->post('password');
                }


                // Only allow updating groups if user is admin
                if ($this->ion_auth->in_group(1) || $this->ion_auth->in_group(2)) {
                    //Update the groups user belongs to
                    $groupData = $this->input->post('group');

                    if (isset($groupData) && !empty($groupData)) {

                        $this->ion_auth->remove_from_group('', $id);

                        $this->ion_auth->add_to_group($groupData, $id);

                    }
                }

                //check to see if we are updating the user
                if ($this->ion_auth->update($selected_user->id, $data) && $location) {
                    //redirect them back to the admin page if admin, or to the base url if non admin
                    $this->session->set_flashdata('success_message', $this->ion_auth->messages());
                    if ($this->ion_auth->in_group(1) || $this->ion_auth->in_group(2)) {
                        redirect('users', 'refresh');
                    } else {
                        redirect('/', 'refresh');
                    }

                } else {
                    //redirect them back to the admin page if admin, or to the base url if non admin
                    $this->session->set_flashdata('error_message', $this->ion_auth->errors());
                    if ($this->ion_auth->in_group(1) || $this->ion_auth->in_group(2)) {
                        redirect('users', 'refresh');
                    } else {
                        redirect('/', 'refresh');
                    }

                }

            }
        }


        //set the flash data error message if there is one
        $data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('error_message')));

        //pass the user to the view
        $data['selected_user'] = $selected_user;
        $data['groups'] = $groups;
        $data['currentGroups'] = $currentGroups;
        $data['first_name'] = array(
            'name' => 'first_name',
            'id' => 'first_name',
            'type' => 'text',
            'class' => 'form-control',
            'value' => $this->form_validation->set_value('first_name', $selected_user->first_name),
        );
        $data['last_name'] = array(
            'name' => 'last_name',
            'id' => 'last_name',
            'type' => 'text',
            'class' => 'form-control',
            'value' => $this->form_validation->set_value('last_name', $selected_user->last_name),
        );
        $data['phone'] = array(
            'name' => 'phone',
            'id' => 'phone',
            'type' => 'tel',
            'class' => 'form-control',
            'value' => $this->form_validation->set_value('phone', $selected_user->phone),
        );
        $data['password'] = array(
            'name' => 'password',
            'id' => 'password',
            'type' => 'password',
            'class' => 'form-control',
        );
        $data['password_confirm'] = array(
            'name' => 'password_confirm',
            'id' => 'password_confirm',
            'type' => 'password',
            'class' => 'form-control',
        );
        $group_option = ['' => 'Select User group'];
        $data['group'] = array(
            'name' => 'group',
            'id' => 'group',
            'class' => 'form-control',
            'options' => $group_option + $group_ids,
            'selected' => $group_id
        );

        $level_option = ['' => 'Select User level'];
        $data['level'] = array(
            'name' => 'level',
            'id' => 'level',
            'class' => 'form-control',
            'options' => $level_option + $level_ids,
            'selected' => $level_id
        );

        $national_option = ['1' => 'National'];
        $data['national'] = array(
            'name' => 'national',
            'id' => 'national',
            'class' => 'form-control',
            'options' => $national_option
        );

        $region_option = ['' => 'Select Region'];
        $regions = $this->region->dropdown('region_name');

        $data['region'] = array(
            'name' => 'region',
            'id' => 'region',
            'class' => 'form-control',
            'options' => $region_option + $regions,
            'selected' => ((int)$selected_user->level_id >= 2) ? $selected_user->region_id : ''
        );

        $county_option = ['' => 'Select County'];
        $counties = $this->get_county_by_region_dropdown($selected_user->region_id);
        $data['county'] = array(
            'name' => 'county',
            'id' => 'county',
            'class' => 'form-control',
            'options' => $county_option + ((is_array($counties)) ? $counties : array($counties)),
            'selected' => ((int)$selected_user->level_id >= 3) ? $selected_user->county_id : ''
        );

        $subcounty_option = ['' => 'Select Subcounty'];
        $subcounties = $this->get_subcounty_by_county_dropdown($selected_user->county_id);
        $data['subcounty'] = array(
            'name' => 'subcounty',
            'id' => 'subcounty',
            'class' => 'form-control',
            'options' => $subcounty_option + ((is_array($subcounties)) ? $subcounties : array($subcounties)),
            'selected' => ((int)$selected_user->level_id >= 4) ? $selected_user->subcounty_id : ''
        );


        echo Modules::run('templates', $data);
    }

    // create a new group
    function create_group()
    {
        $data['title'] = $this->lang->line('create_group_title');
        $data['subtitle'] = 'Create Group';
        $data['module'] = 'users';
        $data['view_file'] = 'create_group';


        if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin()) {
            redirect('users', 'refresh');
        }

        //validate form input
        $this->form_validation->set_rules('group_name', $this->lang->line('create_group_validation_name_label'), 'required|alpha_dash');

        if ($this->form_validation->run() == TRUE) {
            $new_group_id = $this->ion_auth->create_group($this->input->post('group_name'), $this->input->post('description'));
            if ($new_group_id) {
                // check to see if we are creating the group
                // redirect them back to the admin page
                $this->session->set_flashdata('success_message', $this->ion_auth->messages());
                redirect("users", 'refresh');
            }
        } else {
            //display the create group form
            //set the flash data error message if there is one
            $data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('error_message')));

            $data['group_name'] = array(
                'name' => 'group_name',
                'id' => 'group_name',
                'type' => 'text',
                'class' => 'form-control',
                'value' => $this->form_validation->set_value('group_name'),
            );
            $data['description'] = array(
                'name' => 'description',
                'id' => 'description',
                'type' => 'text',
                'class' => 'form-control',
                'value' => $this->form_validation->set_value('description'),
            );

            echo Modules::run('templates', $data);
        }
    }

    //edit a group
    function edit_group($id)
    {
        $data['title'] = 'Edit Group';
        $data['subtitle'] = 'Edit Group';
        $data['module'] = 'users';
        $data['view_file'] = 'edit_group';

        // bail if no group id given
        if (!$id || empty($id)) {
            redirect('users', 'refresh');
        }

        $data['title'] = $this->lang->line('edit_group_title');

        if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin()) {
            redirect('users', 'refresh');
        }

        $group = $this->ion_auth->group($id)->row();

        //validate form input
        $this->form_validation->set_rules('group_name', $this->lang->line('edit_group_validation_name_label'), 'required|alpha_dash');

        if (isset($_POST) && !empty($_POST)) {
            if ($this->form_validation->run() === TRUE) {
                $group_update = $this->ion_auth->update_group($id, $_POST['group_name'], $_POST['group_description']);

                if ($group_update) {
                    $this->session->set_flashdata('success_message', $this->lang->line('edit_group_saved'));
                } else {
                    $this->session->set_flashdata('error_message', $this->ion_auth->errors());
                }
                redirect("users", 'refresh');
            }
        }

        //set the flash data error message if there is one
        $data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('error_message')));

        //pass the user to the view
        $data['group'] = $group;

        $data['group_name'] = array(
            'name' => 'group_name',
            'id' => 'group_name',
            'type' => 'text',
            'class' => 'form-control',
            'value' => $this->form_validation->set_value('group_name', $group->name),
        );
        $data['group_description'] = array(
            'name' => 'group_description',
            'id' => 'group_description',
            'type' => 'text',
            'class' => 'form-control',
            'value' => $this->form_validation->set_value('group_description', $group->description),
        );

        echo Modules::run('templates', $data);
    }

    public function delete($user_id = NULL)
    {
        if (is_null($user_id)) {
            $this->session->set_flashdata('error_message', 'There\'s no user to delete');
        } else {
            $this->ion_auth->delete_user($user_id);
            $this->user_ext->remove_location($user_id);
            $this->session->set_flashdata('success_message', $this->ion_auth->messages());
        }
        redirect('users', 'refresh');
    }

    public function user_data()
    {
        $user = $this->user_info();
        $output['data'] = [];
        if ($this->ion_auth->in_group(1) || $this->ion_auth->in_group(2)) {
            //list the users
            $data = [
                'national' => (int)$user->nation_id,
                'region' => (int)$user->region_id,
                'county' => (int)$user->county_id,
                'subcounty' => (int)$user->subcounty_id,
                'facility' => (int)$user->facility_id,
            ];
            $users = $this->user->users($user->level_id, $data, true)->result();
            foreach ($users as $k => $user) {
                $users[$k]->groups = $this->ion_auth->get_users_groups($user->id)->result();
                $users[$k]->location = $this->location_name($user->location_id);
            }
            $output['data'] = $users;

        }

        echo json_encode($output);
    }

    function migrate()
    {
        $data['title'] = "Users";
        $data['subtitle'] = 'Migrate User';
        $data['module'] = 'users';
        $data['view_file'] = 'migrate_user';

        if ($this->ion_auth->in_group(1) || $this->ion_auth->in_group(2)) {
            echo Modules::run('templates', $data);
        }
    }

    public function update_location_name()
    {
        parent::update_location_name();
    }

    protected function generate_idx()
    {
        $name = 'NVIP';
        $national = [
            'national' => 1,
            'region' => 0,
            'county' => 0,
            'subcounty' => 0,
            'facility' => 0
        ];
        $nation[] = [$name, $national];
        $regions = $this->transformRegions($this->region->get_all());
        $counties = $this->transformCounties($this->county->get_all());
        $subcounties = $this->transformSubcounties($this->subcounty->get_all());
        $facilities = $this->transformFacilities($this->facility->get_all());
        $data = [$nation, $regions, $counties, $subcounties, $facilities];

        $this->location->destroy_table();
        $this->location->create_table();
        foreach ($data as $key => $value) {
            foreach ($value as $row) {
                $this->location->_insert($row);
            }

        }

    }

    private function transformRegions($data)
    {
        $regions = [];
        foreach ($data as $key => $value) {
            $name = $value->region_name;
            $region = [
                'national' => (int)'1',
                'region' => (int)$value->id,
                'county' => 0,
                'subcounty' => 0,
                'facility' => 0,
            ];
            $regions[] = [$name, $region];
        }
        return $regions;

    }

    private function transformCounties($data)
    {
        $counties = [];
        foreach ($data as $key => $value) {
            $name = $value->county_name;
            $county = [
                'national' => (int)'1',
                'region' => (int)$value->region_id,
                'county' => (int)$value->id,
                'subcounty' => 0,
                'facility' => 0,
            ];
            $counties[] = [$name, $county];
        }
        return $counties;

    }

    private function transformSubcounties($data)
    {
        $subcounties = [];
        foreach ($data as $key => $value) {
            $name = $value->subcounty_name;
            $subcounty = [
                'national' => (int)'1',
                'region' => (int)$value->region_id,
                'county' => (int)$value->county_id,
                'subcounty' => (int)$value->id,
                'facility' => 0,
            ];
            $subcounties[] = [$name, $subcounty];
        }
        return $subcounties;

    }

    private function transformFacilities($data)
    {
        $facilities = [];
        foreach ($data as $key => $value) {
            $name = $value->facility_name;
            $facility = [
                'national' => (int)'1',
                'region' => (int)$value->region_id,
                'county' => (int)$value->county_id,
                'subcounty' => (int)$value->subcounty_id,
                'facility' => (int)$value->id,
            ];
            $facilities[] = [$name, $facility];
        }
        return $facilities;

    }

    private function clean($string)
    {
        return
            preg_replace(
                array('#[\\s-]+#', '#[^A-Za-z0-9\. -]+#'),
                array('', ''),

                urldecode($string)

            );
    }

    public function add_location_fields()
    {
        parent::add_location_fields(); // TODO: Change the autogenerated stub
    }


}
