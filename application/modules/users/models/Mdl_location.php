<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mdl_location extends CI_Model
{
    public $locations = 'locations';

    public function __construct()
    {
        parent::__construct();
    }

    public function _insert($data)
    {
        $this->db->insert($this->locations, $data);
        $insert_id = $this->db->insert_id();
        return  $insert_id;
    }


    /** Create a table to save our newly generated json indexes
     * The locations field should be of TYPE: JSON */
    public function create_table()
    {

        $this->load->dbforge();
        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'INT',
                'constraint' => 10,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ),
            'location_name' => array(
                'type' => 'VARCHAR(100)',
                'null' => FALSE,
            ),
            'json_location' => array(
                'type' => 'JSON',
                'null' => FALSE,
            )
        ));
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table($this->locations);
    }

    public function destroy_table()
    {
        $this->load->dbforge();
        $this->dbforge->drop_table($this->locations, TRUE);
    }
}
