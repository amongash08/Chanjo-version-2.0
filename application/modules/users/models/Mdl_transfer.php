<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mdl_transfer extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    function get_location_id($name)
    {
        $query = $this->db->select('
                    locations.id,
                    nation.name as nation,
                    region_name as region,
                    county_name as county,
                    subcounty_name as subcounty,
                    json_location')
            ->from('locations')
            ->join('nation', 'nation.id = JSON_UNQUOTE(JSON_EXTRACT(locations.json_location, "$.national"))')
            ->join('regions', 'regions.id = JSON_UNQUOTE(JSON_EXTRACT(locations.json_location, "$.region"))', 'left')
            ->join('counties', 'counties.id = JSON_UNQUOTE(JSON_EXTRACT(locations.json_location, "$.county"))', 'left')
            ->join('subcounties', 'subcounties.id = JSON_UNQUOTE(JSON_EXTRACT(locations.json_location, "$.subcounty"))', 'left')
            ->join('facilities', 'facilities.id = JSON_UNQUOTE(JSON_EXTRACT(locations.json_location, "$.facility"))', 'left')
            ->where('nation.name', $name)
            ->or_where('region_name', $name)
            ->or_where('county_name', $name)
            ->or_where('subcounty_name', $name)
            ->or_where('facility_name', $name)
            ->get();
        if ($query->num_rows() > 0) {
            return $query->row();
        }
        return FAlSE;
    }

    function get_nvip_user($email)
    {
        $query = $this->db->select()
            ->from('nvip.tbl_users')
            ->join('nvip.tbl_user_base', 'tbl_user_base.user_id = tbl_users.id')
            ->where('migrated', '0')
            ->where('email', $email)
            ->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        }
        return FAlSE;
    }

    function get_users()
    {
        $query = $this->db->select()
            ->from('nvip.tbl_users')
            ->join('nvip.tbl_user_base', 'tbl_user_base.user_id = tbl_users.id')
            ->where('migrated', '0')
            ->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        }
        return FAlSE;
    }

    function get_my_users($level, $id)
    {
        $query = $this->db->select()
            ->from('nvip.tbl_users')
            ->join('nvip.tbl_user_base', 'tbl_user_base.user_id = tbl_users.id')
            ->where('migrated', '0')
            ->where('session_id !=', '0')
            ->where('active', '1');
        if ($level == 1) {
            $result = $query->get();
        } elseif ($level == 2) {
            $level++;
            $result = $query->where('region', $id)
                ->get();
        } elseif ($level == 3) {
            $level++;
            $result = $query->where('county', $id)
                ->where('user_level', $level)
                ->get();
        } elseif ($level == 4) {
            $level++;
            $result = $query->where('subcounty', $id)
                ->where('user_level', $level)
                ->get();
        }
        if ($result->num_rows() > 0) {
            return $result->result();
        }
        return FAlSE;
    }

    function get_transactions($location)
    {
        $query = $this->db->select()
            ->from('nvip.tbl_transaction')
            ->where('station', $location)
            ->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        }
        return FAlSE;
    }

    function check_inserted_transactions($location)
    {
        $count = $this->db->select()
            ->from('transactions')
            ->where('location_id', $location)
            ->get();
        if ($count->num_rows() > 0) {
            return TRUE;
        }
        return FAlSE;
    }

    function get_balances($transaction_id)
    {
        $query = $this->db->select('`id`, `balance`, `station` as location_id, `timestamp`, `transaction_id`, `vaccine_id`')
            ->from('nvip.tbl_balances')
            ->where('transaction_id', $transaction_id)
            ->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return FAlSE;
    }

    function get_batch_balances($transaction_id)
    {
        $query = $this->db->select('`balance`, `batch`, `date`, `expiry_date`, `id`, `station` as location_id, `timestamp`, `transaction_id`, `user_id`, `vaccine_id`, `vvm`')
            ->from('nvip.tbl_batch_balances')
            ->where('transaction_id', $transaction_id)
            ->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return FAlSE;
    }


    function get_transaction_items($id)
    {
        $query = $this->db->select()
            ->from('nvip.tbl_transaction_items')
            ->where('transaction_id', $id)
            ->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return FAlSE;
    }

    public function deactivate_user($email)
    {
        $sql = 'UPDATE nvip.tbl_users SET migrated = 1, active = 0 WHERE email = "'.$email.'"';
        $this->db->query($sql);
    }

    public function check_email($email)
    {
        $query = $this->db->select('*')
            ->from('users')
            ->where('email', $email)
            ->limit(1)
            ->get();
        if ($query->num_rows() == 1) {
            return true;
        } else {
            return false;
        }
    }
}