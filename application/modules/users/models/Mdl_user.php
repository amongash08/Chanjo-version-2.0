<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mdl_user extends MY_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('users/mdl_user_ext');
    }

    public function users($level = false, $location = false, $admin = false)
    {
        $query = $this->db->select('first_name, last_name, email, phone, active, user_base.user_id, users.id, location_id, level_id')
            ->from('users')
            ->join('user_base', 'user_base.user_id = users.id')
            ->join('users_groups', 'users_groups.user_id = users.id')
            ->join('locations', 'locations.id = user_base.location_id', 'left');
        if ($admin) {
            if ($level == 1) {
                return $query->order_by('group_id', 'asc')
                    ->get();
            } elseif ($level == 2) {
                return $query->where('level_id', 3)
                    ->where('JSON_EXTRACT(`json_location`, "$.national") =', $location['national'])
                    ->where('JSON_EXTRACT(`json_location`, "$.region") =', $location['region'])
                    ->order_by('group_id', 'asc')
                    ->get();
            } elseif ($level == 3) {
                return $query->where('level_id', 4)
                    ->where('JSON_EXTRACT(`json_location`, "$.national") =', $location['national'])
                    ->where('JSON_EXTRACT(`json_location`, "$.region") =', $location['region'])
                    ->where('JSON_EXTRACT(`json_location`, "$.county") =', $location['county'])
                    ->order_by('group_id', 'asc')
                    ->get();
            } elseif ($level == 4) {
                return $query->where('level_id', 5)
                    ->where('JSON_EXTRACT(`json_location`, "$.national") =', $location['national'])
                    ->where('JSON_EXTRACT(`json_location`, "$.region") =', $location['region'])
                    ->where('JSON_EXTRACT(`json_location`, "$.county") =', $location['county'])
                    ->where('JSON_EXTRACT(`json_location`, "$.subcounty") =', $location['subcounty'])
                    ->order_by('group_id', 'asc')
                    ->get();
            }

        } else {
            return $query->order_by('group_id', 'asc')
                ->get();
        }

    }

}