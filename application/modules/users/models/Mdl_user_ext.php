<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mdl_user_ext extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    // Read data using username and password
    public function login($email, $password)
    {
        $condition = 'email =' . "'" . $email . "' AND " . 'password =' . "'" . $password . "'";
        $query = $this->db->select('*')
            ->from('users')
            ->where($condition)
            ->limit(1)
            ->get();
        if ($query->num_rows() == 1) {
            return $query->row();
        } else {
            return false;
        }
    }

    public function levels()
    {
        $query = $this->db->select()
            ->order_by('id', 'asc')
            ->get('levels');
        return $query;
    }

    public function insert_location($data)
    {
        $this->db->insert('user_base', $data);
    }

    public function update_location($id, $data)
    {
        $this->db->where('user_id', $id)->update('user_base', $data);

    }

    public function get_location($data)
    {
        $query = $this->db->select('id')
            ->where('JSON_EXTRACT(`json_location`, "$.national") =', $data['national'])
            ->where('JSON_EXTRACT(`json_location`, "$.region") =', $data['region'])
            ->where('JSON_EXTRACT(`json_location`, "$.county") =', $data['county'])
            ->where('JSON_EXTRACT(`json_location`, "$.subcounty") =', $data['subcounty'])
            ->where('JSON_EXTRACT(`json_location`, "$.facility") =', $data['facility'])
            ->get('locations');
        if ($query->num_rows() > 0) {
            return $query->row();
        }
        return FAlSE;

    }

    public function remove_location($id)
    {
        $this->db->trans_begin();
        $this->db->delete('user_base', array('user_id' => $id));
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return FALSE;
        }
        $this->db->trans_commit();
        return TRUE;
    }

    public function get_user_info($id)
    {
        if ($id) {
            $query = $this->db->select('
                    users.id as id,
                    email,
                    first_name,
                    last_name,
                    active,
                    phone,
                    level_id,
                    location_id,
                    location_name,
                    nation.name as nation,
                    region_name as region,
                    county_name as county,
                    subcounty_name as subcounty,
                    facility_name as facility,
                    JSON_UNQUOTE(JSON_EXTRACT(locations.json_location, "$.national")) as nation_id,
                    JSON_UNQUOTE(JSON_EXTRACT(locations.json_location, "$.region")) as region_id,
                    JSON_UNQUOTE(JSON_EXTRACT(locations.json_location, "$.county")) as county_id,
                    JSON_UNQUOTE(JSON_EXTRACT(locations.json_location, "$.subcounty")) as subcounty_id,
                    JSON_UNQUOTE(JSON_EXTRACT(locations.json_location, "$.facility")) as facility_id,
                    json_location')
                ->from('users')
                ->join('user_base', 'user_base.user_id = users.id')
                ->join('users_groups', 'users_groups.user_id = users.id')
                ->join('locations', 'locations.id = user_base.location_id')
                ->join('nation', 'nation.id = JSON_UNQUOTE(JSON_EXTRACT(locations.json_location, "$.national"))')
                ->join('regions', 'regions.id = JSON_UNQUOTE(JSON_EXTRACT(locations.json_location, "$.region"))', 'left')
                ->join('counties', 'counties.id = JSON_UNQUOTE(JSON_EXTRACT(locations.json_location, "$.county"))', 'left')
                ->join('subcounties', 'subcounties.id = JSON_UNQUOTE(JSON_EXTRACT(locations.json_location, "$.subcounty"))', 'left')
                ->join('facilities', 'facilities.id = JSON_UNQUOTE(JSON_EXTRACT(locations.json_location, "$.facility"))', 'left')
                ->where('users.id', $id)
                ->get();
            if ($query->num_rows() > 0) {
                return $query->row();
            }
            return FAlSE;
        }
    }

    function get_location_name($id)
    {
        $query = $this->db->select('
                    nation.name as nation,
                    region_name as region,
                    county_name as county,
                    subcounty_name as subcounty,
                    facility_name as facility
                   ')
            ->from('locations')
            ->join('nation', 'nation.id = JSON_UNQUOTE(JSON_EXTRACT(locations.json_location, "$.national"))')
            ->join('regions', 'regions.id = JSON_UNQUOTE(JSON_EXTRACT(locations.json_location, "$.region"))', 'left')
            ->join('counties', 'counties.id = JSON_UNQUOTE(JSON_EXTRACT(locations.json_location, "$.county"))', 'left')
            ->join('subcounties', 'subcounties.id = JSON_UNQUOTE(JSON_EXTRACT(locations.json_location, "$.subcounty"))', 'left')
            ->join('facilities', 'facilities.id = JSON_UNQUOTE(JSON_EXTRACT(locations.json_location, "$.facility"))', 'left')
            ->where('locations.id', $id)
            ->get();
        if ($query->num_rows() > 0) {
            return $query->row();
        }
        return FAlSE;
    }


}