<link href="<?php echo base_url() ?>assets/plugins/jquery.steps/jquery.steps.css" rel="stylesheet" type="text/css"/>
<div class="login-box-custom">

    <!-- /.login-logo -->
    <div class="login-box-body">
        <style type="text/css">
            body {
                height: auto;
            }

            .login-box-custom {
                background-color: #ffffff;
                border: 2px solid #cccccc;
                border-radius: 5px;
                margin: 90px auto 0;
                width: 50%;
            }
        </style>
        <div id="smartwizard">
            <h1>Guidelines</h1>
            <div id="step-1" class="">
                This process is aimed at updating all your records from the previous version of the Chanjo eLMIS.
                <br>
                This process is mandatory for all users and will take some time. Please be patient as it completes
                this process. To proceed click on the <b>Next</b> button.
            </div>
            <h1>Update Vaccine Records</h1>

            <div id="step-2" class="">

            </div>
            <h1>Update User Password</h1>
            <div id="step-3" class="">
                Step Content
            </div>

        </div>
        <!-- /.login-box-body -->
    </div>
    <!-- /.login-box -->
    <script type="text/javascript">
        $(document).ready(function () {
            var url = '<?php echo base_url();?>';
            $('#smartwizard').steps();

        });
    </script>
    <script type="text/javascript"
            src="<?php echo base_url() ?>assets/plugins/jquery.steps/jquery.steps.js"></script>
    <!--    <script type="text/javascript"-->
    <!--            src="-->
    <?php //echo base_url() ?><!--assets/plugins/smartwizard/dist/js/jquery.smartWizard.min.js"></script>-->