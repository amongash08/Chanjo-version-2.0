<link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/intl-tel-input/dist/css/intlTelInput.css"/>
<script src="<?php echo base_url() ?>assets/plugins/intl-tel-input/dist/js/intlTelInput.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap-wizard/1.2/jquery.bootstrap.wizard.min.js"></script>
<style>
    .nav-tabs > li, .nav-pills > li {
        float: none;
        display: inline-block;
        *display: inline; /* ie7 fix */
        zoom: 1; /* hasLayout ie7 trigger */
    }

    .nav-tabs, .nav-pills {
        text-align: center;
    }
</style>
<!-- Horizontal Form -->
<div class="box">
    <div class="box-header with-border">
        <h3>User Details</h3>
    </div>
    <!-- /.box-header -->
    <!-- form start -->
    <?php echo form_open("users/create_user", array('class' => "form-horizontal", 'id' => "user")); ?>
    <ul class="nav nav-pills">
        <li class="active"><a href="#basic-tab" data-toggle="tab">Basic information</a></li>
        <li><a href="#system-tab" data-toggle="tab">System Information</a></li>
        <li><a href="#location-tab" data-toggle="tab">User Location</a></li>
    </ul>
    <div class="box-body" style="height: 300px">
        <div class="tab-content">
            <!-- First tab -->
            <div class="tab-pane active col-sm-offset-2" id="basic-tab">
                <div class="form-group">
                    <?php echo lang('create_user_fname_label', 'first_name', array('class' => 'col-sm-2 control-label')); ?>
                    <div class="col-sm-5">
                        <?php echo form_input($first_name); ?>
                    </div>
                </div>
                <div class="form-group">
                    <?php echo lang('create_user_lname_label', 'last_name', array('class' => 'col-sm-2 control-label')); ?>

                    <div class="col-sm-5">
                        <?php echo form_input($last_name); ?>
                    </div>
                </div>
                <div class="form-group">
                    <?php echo lang('create_user_email_label', 'email', array('class' => 'col-sm-2 control-label')); ?>

                    <div class="col-sm-5">
                        <?php echo form_input($email); ?>
                    </div>
                </div>
                <div class="form-group">
                    <?php echo lang('create_user_phone_label', 'phone', array('class' => 'col-sm-2 control-label')); ?>

                    <div class="col-sm-5">
                        <?php echo form_input($phone); ?>
                    </div>
                </div>
            </div>

            <!-- Second tab -->
            <div class="tab-pane col-sm-offset-2" id="system-tab">
                <div class="form-group">
                    <?php echo lang('create_user_password_label', 'password', array('class' => 'col-sm-2 control-label')); ?>

                    <div class="col-sm-5">
                        <?php echo form_input($password); ?>
                    </div>
                </div>
                <div class="form-group">
                    <?php echo lang('create_user_password_confirm_label', 'password_confirm', array('class' => 'col-sm-2 control-label')); ?>

                    <div class="col-sm-5">
                        <?php echo form_input($password_confirm); ?>
                    </div>
                </div>
            </div>
            <!-- Third tab -->
            <div class="tab-pane col-sm-offset-2" id="location-tab">
                <div class="form-group">
                    <label class="col-sm-2 control-label">User Group</label>

                    <div class="col-sm-5">
                        <?php echo form_dropdown($group); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">User Level</label>
                    <div class="col-sm-5">
                        <?php echo form_dropdown($level); ?>
                    </div>
                </div>
                <div class="form-group hidden national">
                    <label class="col-sm-2 control-label">National</label>
                    <div class="col-sm-5">
                        <?php echo form_dropdown($national); ?>
                    </div>
                </div>
                <div class="form-group hidden region">
                    <label class="col-sm-2 control-label">Region</label>
                    <div class="col-sm-5">
                        <?php echo form_dropdown($region); ?>
                    </div>
                </div>
                <div class="form-group hidden county">
                    <label class="col-sm-2 control-label">County</label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" id="county" name="county" disabled>
                    </div>
                </div>
                <div class="form-group hidden subcounty">
                    <label class="col-sm-2 control-label">Subcounty</label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" id="subcounty" name="subcounty" disabled>
                    </div>
                </div>
            </div>
        </div>


    </div>
    <!-- /.box-body -->
    <div class="box-footer">
        <!-- Previous/Next buttons -->
        <ul class="pager wizard">
            <li class="previous"><a href="javascript: void(0);">Previous</a></li>
            <li class="next"><a href="javascript: void(0);">Next</a></li>
        </ul>
    </div>
    <!-- /.box-footer -->
    <?php echo form_close(); ?>
</div>
<!-- /.box -->
<script>
    $(document).ready(function () {
        var $level = $('#level');
        var $national = $('.national');
        var $region = $('.region');
        var $county = $('.county');
        var $subcounty = $('.subcounty');


        $('#user')
            .find('[name="phone"]')
            .intlTelInput({
                utilsScript: '<?php echo base_url()?>assets/plugins/intl-tel-input/dist/js/utils.js',
                autoPlaceholder: true,
                preferredCountries: ['ke']
            });

        $('#user')
            .formValidation({
                framework: 'bootstrap',
                err: {
                    container: 'tooltip'
                },
                excluded: ':hidden',
                fields: {
                    first_name: {
                        validators: {
                            notEmpty: {
                                message: 'This field is required'
                            }
                        }
                    },
                    last_name: {
                        validators: {
                            notEmpty: {
                                message: 'This field is required'
                            }
                        }
                    },
                    email: {
                        validators: {
                            notEmpty: {
                                message: 'The email address is required'
                            },
                            emailAddress: {
                                message: 'The email address is not valid'
                            }
                        }
                    },
                    phone: {
                        validators: {
                            callback: {
                                message: 'The phone number is not valid',
                                callback: function (value, validator, $field) {
                                    return value === ' ' || $field.intlTelInput('isValidNumber');
                                }
                            }
                        }
                    },
                    password: {
                        validators: {
                            notEmpty: {
                                message: 'The password is required'
                            },
                            identical: {
                                field: 'password_confirm',
                                message: 'The passwords are not the same'
                            },
                            stringLength: {
                                min: 8,
                                message: 'The Password field must be at least 8 characters in length'
                            }
                        }
                    },
                    password_confirm: {
                        validators: {
                            notEmpty: {
                                message: 'The password is required'
                            },
                            identical: {
                                field: 'password',
                                message: 'The passwords are not the same'
                            },
                            stringLength: {
                                min: 8,
                                message: 'The Confirm Password field must be at least 8 characters in length'
                            }
                        }
                    },
                    level: {
                        validators: {
                            notEmpty: {
                                message: 'The level is required'
                            }
                        }
                    },
                    national: {
                        validators: {
                            notEmpty: {
                                message: 'Mandatory'
                            }
                        }
                    },
                    region: {
                        validators: {
                            notEmpty: {
                                message: 'The region is required'
                            }
                        }
                    },
                    county: {
                        validators: {
                            notEmpty: {
                                message: 'The county is required'
                            }
                        }
                    },
                    subcounty: {
                        validators: {
                            notEmpty: {
                                message: 'The subcounty is required'
                            }
                        }
                    }
                }
            })
            .bootstrapWizard({
                tabClass: 'nav nav-pills',
                onTabClick: function (tab, navigation, index) {
                    return validateTab(index);
                },
                onNext: function (tab, navigation, index) {
                    var numTabs = $('#user').find('.tab-pane').length,
                        isValidTab = validateTab(index - 1);
                    if (!isValidTab) {
                        return false;
                    }

                    if (index === numTabs) {
                        // We are at the last tab

                        // Uncomment the following line to submit the form using the defaultSubmit() method
                        swal({
                                title: "Confirm Submission",
                                text: "Are you sure you want to submit the entered details?",
                                type: "info",
                                showCancelButton: true,
                                confirmButtonColor: "#54CEF7",
                                confirmButtonText: "Submit",
                                closeOnConfirm: false,
                                showLoaderOnConfirm: true,
                            },
                            function (isConfirm) {
                                if (isConfirm) {
                                    $('#user').formValidation('defaultSubmit');
                                }
                            });
                    }

                    return true;
                },
                onPrevious: function (tab, navigation, index) {
                    return validateTab(index + 1);
                },
                onTabShow: function (tab, navigation, index) {
                    // Update the label of Next button when we are at the last tab
                    var numTabs = $('#user').find('.tab-pane').length;
                    $('#user')
                        .find('.next')
                        .removeClass('disabled')    // Enable the Next button
                        .find('a')
                        .html(index === numTabs - 1 ? 'Submit' : 'Next');


                }
            }).on('click', '.country-list', function () {
            $('#user').formValidation('revalidateField', 'phone');
        }).on('change', '#level', function () {
            fv = $('#user').data('formValidation');
            if ($level.val().length > 0) {
                if ($level.val() == 1) {
                    $national.removeClass('hidden').fadeIn();
                    if ($region.is(':visible')) {
                        $region.addClass('hidden').find('#region').attr('disabled');
                        fv.removeField($('#region'));
                    }
                    if ($county.is(':visible')) {
                        $county.addClass('hidden').find('#county').attr('disabled');
                        fv.removeField($('#county'));
                    }
                    if ($subcounty.is(':visible')) {
                        $subcounty.addClass('hidden').find('#subcounty').attr('disabled');
                        fv.removeField($('#subcounty'));
                    }
                } else if ($level.val() == 2) {
                    $national.removeClass('hidden').fadeIn();
                    $region.removeClass('hidden').fadeIn();
                    if ($county.is(':visible')) {
                        $county.addClass('hidden').find('#county').attr('disabled');
                        fv.removeField($('#county'));
                    }
                    if ($subcounty.is(':visible')) {
                        $subcounty.addClass('hidden').find('#subcounty').attr('disabled');
                        fv.removeField($('#subcounty'));
                    }
                } else if ($level.val() == 3) {
                    $national.removeClass('hidden').fadeIn();
                    $region.removeClass('hidden').fadeIn();
                    $county.removeClass('hidden').fadeIn();

                    if ($subcounty.is(':visible')) {
                        $subcounty.addClass('hidden').find('#subcounty').attr('disabled');
                        fv.removeField($('#subcounty'));
                    }
                } else if ($level.val() == 4) {
                    $national.removeClass('hidden').fadeIn();
                    $region.removeClass('hidden').fadeIn();
                    $county.removeClass('hidden').fadeIn();
                    $subcounty.removeClass('hidden').fadeIn();
                }
            }
        }).on('change', '#region', function () {

            var dropdown_option = new Array();
            var dropdown_end = '</select>';
            if ($level.val() == 3 || $level.val() == 4) {
                dropdown_start = '<select class="form-control" id="county" name="county">';
                default_option = "<option value=''>" + "Select County" + "</option> ";
                dropdown_option.push(default_option);
                var request = $.ajax({
                    url: '<?php echo site_url('county/get_by_region'); ?>',
                    type: 'post',
                    data: {
                        "id": $('#region').val()
                    },

                });

                request.done(function (data) {
                    data = JSON.parse(data);

                    $.each(data, function (key, value) {
                        option = "<option value='" + value.id + "'>" + value.county_name + "</option> ";
                        dropdown_option.push(option);
                    });


                    $('#county').replaceWith(dropdown_start + dropdown_option.join("") + dropdown_end);
                    fv.addField($('#county'));
                });
                request.fail(function (jqXHR, textStatus) {

                });
            }
        }).on('change', '#county', function () {

            var dropdown_option = new Array();
            var dropdown_end = '</select>';
            if ($level.val() == 3 || $level.val() == 4) {
                dropdown_start = '<select class="form-control" id="subcounty" name="subcounty">';
                default_option = "<option value=''>" + "Select Subcounty" + "</option> ";
                dropdown_option.push(default_option);
                var request = $.ajax({
                    url: '<?php echo site_url('subcounty/get_by_county'); ?>',
                    type: 'post',
                    data: {
                        "id": $('#county').val()
                    },

                });

                request.done(function (data) {
                    data = JSON.parse(data);

                    $.each(data, function (key, value) {
                        option = "<option value='" + value.id + "'>" + value.subcounty_name + "</option> ";
                        dropdown_option.push(option);
                    });


                    $('#subcounty').replaceWith(dropdown_start + dropdown_option.join("") + dropdown_end);
                    fv.addField($('#subcounty'));
                });
                request.fail(function (jqXHR, textStatus) {

                });
            }
        });

        function validateTab(index) {
            var fv = $('#user').data('formValidation'), // FormValidation instance
                // The current tab
                $tab = $('#user').find('.tab-pane').eq(index);
            // Validate the container
            fv.validateContainer($tab);

            var isValidStep = fv.isValidContainer($tab);
            if (isValidStep === false || isValidStep === null) {
                // Do not jump to the target tab
                return false;
            }

            return true;
        }
    });
</script>