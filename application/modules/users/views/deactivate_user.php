<div class="box">
    <div class="box-header">
        <h3 class="box-data"><?php echo lang('deactivate_heading'); ?></h3>
        <p><?php echo sprintf(lang('deactivate_subheading'), $selected_user->username); ?></p>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <?php echo form_open("users/deactivate/" . $selected_user->id, array('class' => 'form-horizontal')); ?>

        <div class="col-sm-4">
            <div class="radio">
                <label for="confirm">
                    <input type="radio" name="confirm" value="yes" checked="checked"/>
                    Yes
                </label>

            </div>
            <div class="radio">
                <label for="confirm">
                    <input type="radio" name="confirm" value="no"/>
                    No
                </label>

            </div>
        </div>

        <?php echo form_hidden(array('id' => $selected_user->id)); ?>
    </div>
    <!-- /.box-body -->

    <div class="box-footer">
        <?php echo form_submit('submit', lang('deactivate_submit_btn'), array('class' => 'btn bg-navy margin')); ?>
    </div>
    <?php echo form_close(); ?>
</div>
<!-- /.box -->
