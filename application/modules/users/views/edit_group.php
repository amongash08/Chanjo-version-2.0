<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title"><?php echo lang('edit_group_heading'); ?></h3>
    </div>
    <div id="infoMessage" class="col-sm-offset-4"><?php echo $message; ?></div>
    <div class="box-body col-sm-offset-2">
        <?php echo form_open(uri_string(), array('class' => 'form-horizontal')); ?>

        <div class="form-group">
            <?php echo lang('edit_group_name_label', 'group_name', array('class' => 'col-sm-2 control-label')); ?>
            <div class="col-sm-5">
                <?php echo form_input($group_name); ?>
            </div>
        </div>
        <div class="form-group">
            <?php echo lang('edit_group_desc_label', 'description', array('class' => 'col-sm-2 control-label')); ?>
            <div class="col-sm-5">
                <?php echo form_input($group_description); ?>
            </div>
        </div>
    </div>
    <!-- /.box-body -->
    <div class="box-footer">
        <!-- buttons -->
        <?php echo form_submit('submit', lang('edit_group_submit_btn'), array('class' => 'btn bg-navy margin')); ?>
    </div>
    <!-- /.box-footer -->
    <?php echo form_close(); ?>
</div>
<!-- /.box -->
