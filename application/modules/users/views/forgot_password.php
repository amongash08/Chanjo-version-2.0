<!-- Automatic element centering -->
<div class="lockscreen-wrapper">
    <div class="lockscreen-logo">
        <a href="/users"><b>NVIP</b> Chanjo</a>
    </div>

    <!-- START ITEM -->
    <div class="lockscreen-item">
        <!-- image -->
        <div class="lockscreen-image">
            <img src="../assets/images/lock.png" alt="Lock Image">
        </div>
        <!-- /image -->

        <!-- (contains the form) -->
        <?php echo form_open('users/forgot_password', 'class="lockscreen-credentials"'); ?>
        <div class="input-group">
            <?php echo form_input($email); ?>
            <div class="input-group-btn">
                <button type="submit" class="btn"><i class="fa fa-arrow-right text-muted"></i></button>
            </div>
        </div>
        <?php echo form_close(); ?>
        <!-- /.form -->

    </div>
    <!-- /.lockscreen-item -->

    <div class="text-center"><?php echo $message; ?></div>

    <div class="help-block text-center">
        Enter your Email address to receive a new password
    </div>
    <div class="text-center">
        <a href="/users/login">Or go back to Login Screen</a>
    </div>

</div>
<!-- /.center -->