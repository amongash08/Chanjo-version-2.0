<div class="box">
    <div class="box-header">
        <h3 class="box-data"><?php echo lang('index_subheading'); ?></h3>
    </div>
    <div id="infoMessage" class="col-sm-offset-4"><?php echo $message; ?></div>
    <!-- /.box-header -->
    <div class="box-body">
        <table id="users" class="table table-bordered table-striped" cellspacing="0" width="100%">
            <thead>
            <tr>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Email</th>
                <th>Phone</th>
                <th>Location</th>
                <th>Groups</th>
                <th>Status</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            </tbody>
            <tfoot>
            <tr>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Email</th>
                <th>Phone</th>
                <th>Location</th>
                <th>Groups</th>
                <th>Status</th>
                <th>Action</th>
            </tr>
            </tfoot>
        </table>
    </div>
    <!-- /.box-body -->

    <div class="box-footer">
        <a href="<?php echo base_url(); ?>users/create_user" class="btn bg-navy margin">Add User</a>
        <a href="<?php echo base_url(); ?>users/migrate" class="btn bg-navy margin">Migrate Users</a>
<!--        <a href="--><?php //echo base_url(); ?><!--users/create_group" class="btn bg-navy margin">Add Group</a>-->
    </div>
</div>
<!-- /.box -->
<script type="text/javascript">
    $(document).ready(function () {
        $("#users").DataTable({
            serverSide: false,
            ajax: {
                url: "/users/user_data",
                type: "POST"
            },
            columns: [
                {data: "first_name"},
                {data: "last_name"},
                {data: "email"},
                {data: "phone"},
                {data: "location"},
                {
                    data: "groups",
                    render: function (data, type, full) {
                        return $.map(data, function (d, i) {
//                            return ' <a href="<?php //echo base_url();?>//users/edit_group/' + d.id + '"><b>' + d.name + '</b></a>';
                            return '<b><i>' + d.name + '</i></b>';
                        });
                    }
                },
                {
                    data: "active",
                    render: function (data, type, full) {
                        if (data === '1') {
                            return '<a href="<?php echo base_url();?>users/deactivate/' + full.user_id + '">Deactivate</a>';
                        } else if (data === '0') {
                            return '<a href="<?php echo base_url();?>users/activate/' + full.user_id + '">Activate</a>';
                        }
                    }

                },
                {
                    data: "user_id",
                    render: function (data, type, full) {
                        return '<a href="<?php echo base_url();?>users/edit_user/' + data + '" class="btn btn-primary btn-xs">Edit</a>'
                            + '&nbsp;<a href="<?php echo base_url();?>users/delete/' + data + '" class="btn btn-danger btn-xs">Delete</a>';
                    }

                }
            ]
        });

    });

</script>
<!-- DataTables -->
<link href="<?php echo base_url() ?>assets/plugins/datatables/datatables.css" rel="stylesheet"/>
<script src="<?php echo base_url() ?>assets/plugins/datatables/datatables.min.js"></script>
