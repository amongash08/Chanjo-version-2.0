<div class="login-box-custom">

    <!-- /.login-logo -->
    <div class="login-box-body">
        <div class="alert alert-info text-center"><h2>SIGN IN</h2></div>
        <style type="text/css">
            .alert {
                padding: 0px !important;
            }

            .login-box-custom {
                background-color: #ffffff;
                border: 2px solid #cccccc;
                border-radius: 5px;
                margin: 90px auto 0;
                width: 320px;
            }
        </style>

        <?php echo $message; ?>
        <?php echo form_open("users/login"); ?>
        <div class="form-group has-feedback">
            <?php echo form_input($email); ?>
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
        </div>
        <div class="form-group has-feedback">
            <?php echo form_input($password); ?>
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
        </div>
        <div class="row">
            <div class="col-xs-8">
                <!-- <div class="checkbox icheck">
                   <label>
                     <input type="checkbox"> Remember Me
                   </label>
                 </div>  -->
            </div>
            <!-- /.col -->
            <div class="col-xs-4">
                <?php echo form_submit('submit', lang('login_submit_btn'), 'class="btn btn-primary btn-block btn-flat"'); ?>
            </div>
            <!-- /.col -->
        </div>
        <?php echo form_close(); ?>

        <a href="forgot_password" class="text-red"><?php echo lang('login_forgot_password'); ?></a><br>
        <hr>
        <div class="text-center">
            <img width="25" height="25" src="<?php echo base_url() ?>assets/images/coat_of_arms.png">
            <span class="text-aqua">
            <b>NVIP Chanjo</b>
            </span>
            &copy; <?php echo date('Y'); ?>
        </div>

    </div>
    <!-- /.login-box-body -->
</div>
<!-- /.login-box -->
