<div class="box">
    <div class="box-header">
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <table id="users" class="table table-bordered table-striped" cellspacing="0" width="100%">
            <thead>
            <tr>
                <th>Full Name</th>
                <th>Email</th>
                <th>Phone</th>
                <th>Location</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            </tbody>
            <tfoot>
            <tr>
                <th>Full Name</th>
                <th>Email</th>
                <th>Phone</th>
                <th>Location</th>
                <th>Action</th>
            </tr>
            </tfoot>
        </table>
    </div>
    <!-- /.box-body -->
</div>
<!-- /.box -->
<script type="text/javascript">
    $(document).ready(function () {
        $("#users").DataTable({
            serverSide: false,
            ajax: {
                url: "/users/transfer/get_users",
                type: "POST"
            },
            columns: [
                {data: "full_name"},
                {data: "email"},
                {data: "phone"},
                {data: "location_name"},
                {
                    data: "migrate_id",
                    render: function (data, type, full) {
                        return '<a href="#" id="migrate"  data-id="' + data + '" data-email="' + full.email + '" class="btn btn-primary btn-xs"><i class="fa fa-fighter-jet"></i>&nbsp;Migrate</a>';
                    }
                }
            ]
        });

    });

    $(document).on('click', '#migrate', function (e) {
        var email = $(this).data('email');
        var id = $(this).data('id');
        var data_object = {'migrate_id': id, 'email': email};
        swal({
                title: "Confirm Submission",
                text: "Are you sure you want to migrate this user to an updated system?",
                type: "info",
                showCancelButton: true,
                confirmButtonColor: "#54CEF7",
                confirmButtonText: "Submit",
                closeOnConfirm: false,
                showLoaderOnConfirm: true,
            },
            function (isConfirm) {
                if (isConfirm) {
                    submit(data_object);
                }
            });
    });


    function submit(data) {
        $.ajax({
            url: 'transfer/migrate',
            type: "POST",
            data: data,
            beforeSend: function () {
                $('#validate').fadeOut(300, function () {
                    $(this).remove();
                });
            },
            success: function (data, textStatus, jqXHR) {
                swal("Success!", jqXHR.responseText, "success");
            },
            error: function (jqXHR, textStatus, errorThrown) {
                swal("Warning!", jqXHR.responseText, "warning");
            }
        });


    }

</script>

<!-- DataTables -->
<link href="<?php echo base_url() ?>assets/plugins/datatables/datatables.css" rel="stylesheet"/>
<script src="<?php echo base_url() ?>assets/plugins/datatables/datatables.min.js"></script>