<div class="row">
    <div class="col-md-3">

        <!-- About Me Box -->
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">About Me</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <strong><i class="fa fa-book margin-r-5"></i> User Level</strong>

                <p class="text-muted">
                    User Level here
                </p>

                <hr>

                <strong><i class="fa fa-map-marker margin-r-5"></i> Location</strong>

                <p class="text-muted">Region , County</p>


                <hr>

            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
    <!-- /.col -->
    <div class="col-md-9">
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#settings" data-toggle="tab" aria-expanded="true">Edit Profile</a></li>
            </ul>
            <div class="tab-content">

                <div class="tab-pane active" id="settings">
                    <?php echo form_open('users/profile', array('class' => "form-horizontal")); ?>
                        <div class="form-group">
                            <label for="inputName" class="col-sm-2 control-label">First Name</label>

                            <div class="col-sm-10">
                                <?php echo form_input($first_name); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputName" class="col-sm-2 control-label">Last Name</label>

                            <div class="col-sm-10">
                                <?php echo form_input($last_name); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputName" class="col-sm-2 control-label">Phone</label>

                            <div class="col-sm-10">
                                <?php echo form_input($phone); ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="inputName" class="col-sm-2 control-label">Password (if changing password)</label>

                            <div class="col-sm-10">
                                <?php echo form_input($password); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputName" class="col-sm-2 control-label">Confirm Password (if changing password)</label>

                            <div class="col-sm-10">
                                <?php echo form_input($password_confirm); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-danger">Submit</button>
                            </div>
                        </div>
                    <?php echo form_close(); ?>
                </div>
                <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
        </div>
        <!-- /.nav-tabs-custom -->
    </div>
    <!-- /.col -->
</div>