<div class="login-box-custom">

    <!-- /.login-logo -->
    <div class="login-box-body">
        <div class="alert alert-info text-center"><h3><?php echo lang('change_password_heading'); ?></h3></div>
        <style type="text/css">
            .alert {
                padding: 0px !important;
            }

            .login-box-custom {
                background-color: #ffffff;
                border: 2px solid #cccccc;
                border-radius: 5px;
                margin: 90px auto 0;
                width: 320px;
            }
        </style>

        <?php echo $message; ?>
        <?php echo form_open("users/update_password"); ?>
        <div class="form-group has-feedback">
            <?php echo form_input($new_password); ?>

        </div>
        <div class="form-group has-feedback">
            <?php echo form_input($new_password_confirm); ?>

        </div>
        <div class="row">
            <div class="col-xs-8">
            </div>
            <!-- /.col -->
            <div class="col-xs-4">
                <?php echo form_submit('submit', 'Update', 'class="btn btn-primary btn-block btn-flat"'); ?>
            </div>
            <!-- /.col -->
        </div>
        <?php echo form_close(); ?>

        <hr>
        <div class="text-center">
            <img width="25" height="25" src="<?php echo base_url() ?>assets/images/coat_of_arms.png">
            <span class="text-aqua">
            <b>NVIP Chanjo</b>
            </span>
            &copy; <?php echo date('Y'); ?>
        </div>

    </div>
    <!-- /.login-box-body -->
</div>
<!-- /.login-box -->
