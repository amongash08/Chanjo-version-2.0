<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Vaccine extends Auth_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('mdl_vaccine', 'vaccines');
        $this->load->model('mdl_vvm', 'vvm');

    }

    public function index()
    {
        $data['title'] = 'View Vaccines';
        $data['page_title'] = 'Vaccine';
        $data['subtitle'] = 'View Vaccines';
        $data['module'] = 'vaccine';
        $data['view_file'] = 'index';

        echo Modules::run('templates', $data);
    }

    function edit($id = null)
    {
        $data['title'] = "Edit Vaccine";
        $data['subtitle'] = 'Edit Vaccine';
        $data['module'] = 'vaccine';
        $data['view_file'] = 'edit';
        $data['id'] = (is_numeric($id)) ? $id : null;

        if (!$this->ion_auth->logged_in() || (!$this->ion_auth->is_admin())) {
            redirect('users', 'refresh');
        }

        $selected_vaccine = $this->vaccines->get($id);

        //validate form input
        $this->form_validation->set_rules('women_population', 'Women Population', 'required');

        if (isset($_POST) && !empty($_POST)) {
            // do we have a valid request?
            if ($id !== $this->input->post('id')) {
                $this->session->set_flashdata('error_message', 'An error has occurred.');
                redirect('vaccine', 'refresh');
            }

            if ($this->form_validation->run() === TRUE) {

                $data = array(
                    'women_population' => $this->input->post('women_population'),
                    'under_one_population' => $this->input->post('under_one_population'),
                    'total_population' => $this->input->post('total_population'),
                );


                //check to see if we are updating the user
                if ($this->vaccines->update($selected_vaccine->id, $data, TRUE)) {

                    //redirect them back to the admin page if admin, or to the base url if non admin
                    $this->session->set_flashdata('success_message', 'Vaccine updated successfully.');
                    if ($this->ion_auth->is_admin()) {
                        redirect('vaccine', 'refresh');
                    } else {
                        redirect('/', 'refresh');
                    }

                } else {
                    //redirect them back to the admin page if admin, or to the base url if non admin
                    $this->session->set_flashdata('error_message', 'An error has occurred.');
                    if ($this->ion_auth->is_admin()) {
                        redirect('vaccine', 'refresh');
                    } else {
                        redirect('vaccine/edit/'.$selected_vaccine->id, 'refresh');
                    }

                }

            }
        }

        //pass the vaccine to the view
        $data['selected_vaccine'] = $selected_vaccine;

        if (!is_null($selected_vaccine)) {
            $data['vaccine_name'] = array(
                'name' => 'vaccine_name',
                'id' => 'vaccine_name',
                'type' => 'text',
                'class' => 'form-control',
                'disabled' => '',
                'value' => $this->form_validation->set_value('vaccine_name', $selected_vaccine->vaccine_name),
            );

        }

        echo Modules::run('templates', $data);
    }

    public function vaccine_dropdown()
    {
        $vaccine_array = $this->vaccines->dropdown('vaccine_name');
        foreach ($vaccine_array as $key => $value){
            if($key == '7' || $key == '11'){
                unset($vaccine_array[$key]);
            }
        }
        asort($vaccine_array);
        return $vaccine_array;
    }

    public function vvm_dropdown()
    {
        $vvm_array = $this->vvm->dropdown('name');
        asort($vvm_array);
        return $vvm_array;
    }


    public function vaccine_data()
    {
        //retrieve vaccine data array
        $vaccines = $this->vaccine->get_all();
        $output['data'] = $vaccines;
        echo json_encode($output);
    }
}