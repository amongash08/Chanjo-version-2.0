<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mdl_vaccine extends MY_Model
{

    public $_table = 'vaccines';

    public function __construct()
    {
        parent::__construct();
    }

    public function get_all()
    {
        $query = $this->db->select('*')->from('vaccines')->where('active =', 1)->get();

        return $query->result();
    }

    public function get_wastage_by_store($store_id)
    {
        $query = $this->db->select('wf.id as wf_id,vaccine_id,store_id,vaccine_name,wastage')->from('wastage_factor wf')
        ->join('vaccines v', 'v.id=wf.vaccine_id')
        ->where('store_id =', $store_id)->get();

        return $query->result();
    }


}
