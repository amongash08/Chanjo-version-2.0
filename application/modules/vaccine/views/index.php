<div class="box">
    <div class="box-header">
        <h3 class="box-data">Below is a list of vaccines</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <table id="vaccines" class="table table-bordered table-striped" cellspacing="0" width="100%">
            <thead>
            <tr>
                <th>Vaccine Name</th>
                <th>Doses Required</th>
                <th>Mode of Administration</th>
                <th align="center">Action</th>
            </tr>
            </thead>
            <tbody>

            </tbody>
            <tfoot>
            <tr>
                <th>Vaccine Name</th>
                <th>Doses Required</th>
                <th>Mode of Administration</th>
                <th align="center">Action</th>
            </tr>
            </tfoot>
        </table>
    </div>
    <!-- /.box-body -->

</div>
<!-- /.box -->
<script type="text/javascript">
    $(document).ready(function () {
        $("#vaccines").DataTable({
            serverSide: false,
            ajax: {
                url: "/vaccine/vaccine_data",
                type: "POST"
            },
            columns: [
                {data: "vaccine_name"},
                {data: "doses_required"},
                {data: "mode_administration"},
                {
                    data: "id",
                    render: function (data, type, full) {
                        return '<a href="<?php echo base_url();?>vaccine/edit/' + data + '" class="btn btn-primary btn-xs">Edit</a>';
                    }

                }
            ]
        });

    });

</script>

<!-- DataTables -->
<script src="<?php echo base_url() ?>assets/plugins/datatables/dataTables.min.js"></script>
<script src="<?php echo base_url() ?>assets/plugins/datatables/Bootstrap-3.3.7/js/bootstrap.min.js"></script>
