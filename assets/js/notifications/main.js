$(function () {

  window.App = {

    //TODO: Find a better way to set these from config.php
    baseUrl : '/',
    maxCharacters: 320,
    maxPostsPerPage : 5,

    init: function () {
      this.setElements();
      this.bindEvents();
      this.setupComponents();

      // Join a room if main page is loaded and user is part of a team
      if($('.fullname').children().length > 0){
        console.log('Join room');
        MY_Socket.joinRoom();
      }
    },

    // Cache all the jQuery selectors for easy reference.
    setElements: function () {
      this.$messageBox = $('#txtNewMessage');
      this.$numChars = $('#spanNumChars');
      this.$postButton = $('#btnPost');
      this.$myMessages = $('#tblMyMessages tbody');
      this.$modalWindow = $('#myModal');
      this.$otherMessages = $('#otherMessages');
     
      this.$totalMessageCount = $('.totalMessageCount');
      this.$messageCount = $('.messageCount');
    },

    // Bind document events and assign event handlers.
    bindEvents: function () {
      this.$messageBox.on('input propertychange', this.updateNumChars);
      this.$postButton.on('click', this.postMessage);
    },

    // Initialize any extra UI components
    setupComponents : function () {
      // Set up the popovers when hovering over another user's avatar.
      // App.$otherPostAvatars.popover({
      //   html:true,
      //   placement:'left',
      //   trigger: 'hover'
      // });
    },

    /* *************************************
     *             Event Handlers
     * ************************************* */

    /**
     * Click handler for the Create button in
     * the New User modal window. It grabs data
     * from the form and submits it to the
     * create_new_user function in the Main controller.
     *
     * @param e event
     */
   


    /* *************************************
     *             AJAX Callbacks
     * ************************************* */


     /**
     * Get the newly posted message back from the server
     * and prepend it to the message list.
     *
     * @param result An HTML <tr> string with the new message
     */
  


    /* *************************************
     *            Real-Time Stuff
     * ************************************* */

     showBroadcastedMessage : function(messageData) {
       $(messageData).hide().prependTo(App.$otherMessages).slideDown('slow');
       //App.$otherMessages.prepend(messageData);
       App.setElements();
       App.setupComponents();
     }
   };

  App.init();

});
