$(function () {

    window.MY_Socket = {

        // Instantiate the Socket.IO client and connect to the server
        socket: io.connect('http://chanjo.dev:8080'),
        // Set up the initial event handlers for the Socket.IO client
        bindEvents: function () {
            this.socket.on('startup', MY_Socket.startupMessage);
        },

        // This just indicates that a Socket.IO connection has begun.
        startupMessage: function (data) {
            console.log(data.message);
        },

        isLoggedIn: function () {
            // get the CodeIgniter sessionID from the cookie
            var sessionId = readCookie('ci_session');

            if (sessionId) {
                // Send the sessionID to the Node server
                MY_Socket.socket.emit('isLoggedIn', sessionId);
            } else {
                // If no sessionID exists, don't try to join a room.
                console.log('No session id found.');
                //forward to logout url?
            }
        }


    } // end window.MY_Socket

    // Start it up!
    MY_Socket.bindEvents();

    // Read a cookie. http://www.quirksmode.org/js/cookies.html#script
    function readCookie(name) {
        var nameEQ = name + "=";
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') c = c.substring(1, c.length);
            if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
        }
        return null;
    }

    /* This will look for the 'Admin' badge on the current window.
     This is a super-hacky method of determining if the user is an admin
     So that messages from user's on the same team as the admin won't get
     doubled up in the message stream. */
    function getFullName() {
        var fullName = $('#fullname').text();
        if (!fullName || fullName.length> 0) {
            return fullName;
        }

    }
});