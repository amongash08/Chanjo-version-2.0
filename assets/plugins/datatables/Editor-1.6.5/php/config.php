<?php if (!defined('DATATABLES')) exit(); // Ensure being used in DataTables env.

include(APPPATH.'/config/database.php');

// Enable error reporting for debugging (remove for production)
// error_reporting(E_ALL);
// ini_set('display_errors', '1');


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Database user / pass
 */
$sql_details = array(
	"type" => "Mysql",   // Database type: "Mysql", "Postgres", "Sqlserver", "Sqlite" or "Oracle"
	"user" => $db['default']['username'],       // Database user name
	"pass" => $db['default']['password'],       // Database password
	"host" => $db['default']['hostname'],       // Database host
	"port" => "",       // Database connection port (can be left empty for default)
	"db"   => $db['default']['database'],       // Database name
	"dsn"  => "",        // PHP DSN extra information. Set as `charset=utf8` if you are using MySQL
	"pdoAttr" => array() // PHP PDO attributes array. See the PHP documentation for all options
);

