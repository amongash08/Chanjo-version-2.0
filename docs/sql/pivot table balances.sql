-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: 192.168.99.100    Database: chanjo
-- ------------------------------------------------------
-- Server version	5.7.18

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Temporary view structure for view `county_balances`
--

DROP TABLE IF EXISTS `county_balances`;
/*!50001 DROP VIEW IF EXISTS `county_balances`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `county_balances` AS SELECT 
 1 AS `County Name`,
 1 AS `ROTA`,
 1 AS `BCG`,
 1 AS `BCG Diluent`,
 1 AS `TT`,
 1 AS `OPV`,
 1 AS `IPV`,
 1 AS `YF`,
 1 AS `YF Diluent`,
 1 AS `PCV`,
 1 AS `DPT`,
 1 AS `MR`,
 1 AS `MR Diluent`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `region_balances`
--

DROP TABLE IF EXISTS `region_balances`;
/*!50001 DROP VIEW IF EXISTS `region_balances`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `region_balances` AS SELECT 
 1 AS `Region Name`,
 1 AS `ROTA`,
 1 AS `BCG`,
 1 AS `BCG Diluent`,
 1 AS `TT`,
 1 AS `OPV`,
 1 AS `IPV`,
 1 AS `YF`,
 1 AS `YF Diluent`,
 1 AS `PCV`,
 1 AS `DPT`,
 1 AS `MR`,
 1 AS `MR Diluent`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `subcounty_balances`
--

DROP TABLE IF EXISTS `subcounty_balances`;
/*!50001 DROP VIEW IF EXISTS `subcounty_balances`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `subcounty_balances` AS SELECT 
 1 AS `Subcounty Name`,
 1 AS `ROTA`,
 1 AS `BCG`,
 1 AS `BCG Diluent`,
 1 AS `TT`,
 1 AS `OPV`,
 1 AS `IPV`,
 1 AS `YF`,
 1 AS `YF Diluent`,
 1 AS `PCV`,
 1 AS `DPT`,
 1 AS `MR`,
 1 AS `MR Diluent`*/;
SET character_set_client = @saved_cs_client;

--
-- Final view structure for view `county_balances`
--

/*!50001 DROP VIEW IF EXISTS `county_balances`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `county_balances` AS select `chanjo`.`counties`.`county_name` AS `County Name`,sum(if((`balances`.`vaccine_id` = 1),`balances`.`balance`,0)) AS `ROTA`,sum(if((`balances`.`vaccine_id` = 2),`balances`.`balance`,0)) AS `BCG`,sum(if((`balances`.`vaccine_id` = 5),`balances`.`balance`,0)) AS `BCG Diluent`,sum(if((`balances`.`vaccine_id` = 3),`balances`.`balance`,0)) AS `TT`,sum(if((`balances`.`vaccine_id` = 4),`balances`.`balance`,0)) AS `OPV`,sum(if((`balances`.`vaccine_id` = 6),`balances`.`balance`,0)) AS `IPV`,sum(if((`balances`.`vaccine_id` = 8),`balances`.`balance`,0)) AS `YF`,sum(if((`balances`.`vaccine_id` = 12),`balances`.`balance`,0)) AS `YF Diluent`,sum(if((`balances`.`vaccine_id` = 9),`balances`.`balance`,0)) AS `PCV`,sum(if((`balances`.`vaccine_id` = 10),`balances`.`balance`,0)) AS `DPT`,sum(if((`balances`.`vaccine_id` = 13),`balances`.`balance`,0)) AS `MR`,sum(if((`balances`.`vaccine_id` = 14),`balances`.`balance`,0)) AS `MR Diluent` from (((`chanjo`.`locations` join `chanjo`.`subcounties` on((`chanjo`.`subcounties`.`id` = json_unquote(json_extract(`chanjo`.`locations`.`json_location`,'$.subcounty'))))) join `chanjo`.`counties` on((`chanjo`.`counties`.`id` = json_unquote(json_extract(`chanjo`.`locations`.`json_location`,'$.county'))))) left join (select distinct `t`.`balance` AS `balance`,`t`.`vaccine_id` AS `vaccine_id`,`t`.`location_id` AS `location_id` from (((select max(`chanjo`.`balances`.`transaction_id`) AS `transaction_id`,`chanjo`.`balances`.`vaccine_id` AS `vaccine_id` from `chanjo`.`balances` group by `chanjo`.`balances`.`vaccine_id`,`chanjo`.`balances`.`location_id`)) `b` join `chanjo`.`balances` `t` on(((`t`.`transaction_id` = `b`.`transaction_id`) and (`t`.`vaccine_id` = `b`.`vaccine_id`))))) `balances` on((`balances`.`location_id` = `chanjo`.`locations`.`id`))) where ((cast(json_extract(`chanjo`.`locations`.`json_location`,'$.subcounty') as unsigned) <> 0) and (cast(json_extract(`chanjo`.`locations`.`json_location`,'$.facility') as unsigned) = 0)) group by `chanjo`.`subcounties`.`county_id` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `region_balances`
--

/*!50001 DROP VIEW IF EXISTS `region_balances`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `region_balances` AS select `chanjo`.`regions`.`region_name` AS `Region Name`,sum(if((`balances`.`vaccine_id` = 1),`balances`.`balance`,0)) AS `ROTA`,sum(if((`balances`.`vaccine_id` = 2),`balances`.`balance`,0)) AS `BCG`,sum(if((`balances`.`vaccine_id` = 5),`balances`.`balance`,0)) AS `BCG Diluent`,sum(if((`balances`.`vaccine_id` = 3),`balances`.`balance`,0)) AS `TT`,sum(if((`balances`.`vaccine_id` = 4),`balances`.`balance`,0)) AS `OPV`,sum(if((`balances`.`vaccine_id` = 6),`balances`.`balance`,0)) AS `IPV`,sum(if((`balances`.`vaccine_id` = 8),`balances`.`balance`,0)) AS `YF`,sum(if((`balances`.`vaccine_id` = 12),`balances`.`balance`,0)) AS `YF Diluent`,sum(if((`balances`.`vaccine_id` = 9),`balances`.`balance`,0)) AS `PCV`,sum(if((`balances`.`vaccine_id` = 10),`balances`.`balance`,0)) AS `DPT`,sum(if((`balances`.`vaccine_id` = 13),`balances`.`balance`,0)) AS `MR`,sum(if((`balances`.`vaccine_id` = 14),`balances`.`balance`,0)) AS `MR Diluent` from ((`chanjo`.`locations` join `chanjo`.`regions` on((`chanjo`.`regions`.`id` = json_unquote(json_extract(`chanjo`.`locations`.`json_location`,'$.region'))))) left join (select distinct `t`.`balance` AS `balance`,`t`.`vaccine_id` AS `vaccine_id`,`t`.`location_id` AS `location_id` from (((select max(`chanjo`.`balances`.`transaction_id`) AS `transaction_id`,`chanjo`.`balances`.`vaccine_id` AS `vaccine_id` from `chanjo`.`balances` group by `chanjo`.`balances`.`vaccine_id`,`chanjo`.`balances`.`location_id`)) `b` join `chanjo`.`balances` `t` on(((`t`.`transaction_id` = `b`.`transaction_id`) and (`t`.`vaccine_id` = `b`.`vaccine_id`))))) `balances` on((`balances`.`location_id` = `chanjo`.`locations`.`id`))) where ((cast(json_extract(`chanjo`.`locations`.`json_location`,'$.region') as unsigned) <> 0) and (cast(json_extract(`chanjo`.`locations`.`json_location`,'$.county') as unsigned) = 0) and (cast(json_extract(`chanjo`.`locations`.`json_location`,'$.subcounty') as unsigned) = 0) and (cast(json_extract(`chanjo`.`locations`.`json_location`,'$.facility') as unsigned) = 0)) group by `chanjo`.`locations`.`id` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `subcounty_balances`
--

/*!50001 DROP VIEW IF EXISTS `subcounty_balances`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `subcounty_balances` AS select `chanjo`.`subcounties`.`subcounty_name` AS `Subcounty Name`,sum(if((`balances`.`vaccine_id` = 1),`balances`.`balance`,0)) AS `ROTA`,sum(if((`balances`.`vaccine_id` = 2),`balances`.`balance`,0)) AS `BCG`,sum(if((`balances`.`vaccine_id` = 5),`balances`.`balance`,0)) AS `BCG Diluent`,sum(if((`balances`.`vaccine_id` = 3),`balances`.`balance`,0)) AS `TT`,sum(if((`balances`.`vaccine_id` = 4),`balances`.`balance`,0)) AS `OPV`,sum(if((`balances`.`vaccine_id` = 6),`balances`.`balance`,0)) AS `IPV`,sum(if((`balances`.`vaccine_id` = 8),`balances`.`balance`,0)) AS `YF`,sum(if((`balances`.`vaccine_id` = 12),`balances`.`balance`,0)) AS `YF Diluent`,sum(if((`balances`.`vaccine_id` = 9),`balances`.`balance`,0)) AS `PCV`,sum(if((`balances`.`vaccine_id` = 10),`balances`.`balance`,0)) AS `DPT`,sum(if((`balances`.`vaccine_id` = 13),`balances`.`balance`,0)) AS `MR`,sum(if((`balances`.`vaccine_id` = 14),`balances`.`balance`,0)) AS `MR Diluent` from ((`chanjo`.`locations` join `chanjo`.`subcounties` on((`chanjo`.`subcounties`.`id` = json_unquote(json_extract(`chanjo`.`locations`.`json_location`,'$.subcounty'))))) left join (select distinct `t`.`balance` AS `balance`,`t`.`vaccine_id` AS `vaccine_id`,`t`.`location_id` AS `location_id` from (((select max(`chanjo`.`balances`.`transaction_id`) AS `transaction_id`,`chanjo`.`balances`.`vaccine_id` AS `vaccine_id` from `chanjo`.`balances` group by `chanjo`.`balances`.`vaccine_id`,`chanjo`.`balances`.`location_id`)) `b` join `chanjo`.`balances` `t` on(((`t`.`transaction_id` = `b`.`transaction_id`) and (`t`.`vaccine_id` = `b`.`vaccine_id`))))) `balances` on((`balances`.`location_id` = `chanjo`.`locations`.`id`))) where ((cast(json_extract(`chanjo`.`locations`.`json_location`,'$.subcounty') as unsigned) <> 0) and (cast(json_extract(`chanjo`.`locations`.`json_location`,'$.facility') as unsigned) = 0)) group by `chanjo`.`locations`.`id` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-10-24 16:22:22
