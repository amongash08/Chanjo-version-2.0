// Start up a Node server with Socket.IO
var io = require('socket.io').listen(8080,{
  'log level':1
});

//Let Node know that you want to use Redis
var redis = require('redis');

// Listen for the client connection event
io.sockets.on('connection', function (socket) {
  // Instantiate a Redis client that can issue Redis commands.
  var rClient = redis.createClient(6379, '192.168.99.100')
  rClient.on('connect', {message: 'Connected to Redis'});
  // Let everyone know it's working
  socket.emit('startup', { message: 'Server is running.' });

  socket.on('isLoggedIn', function (sessionId) {
    console.log(sessionId);
  });

});
