// Setup basic express server
const express = require('express');
var app = express();
var path = require('path');
var fs = require('fs');
var server = require('http').createServer(app);
var io = require('socket.io')(server);
var port = process.env.PORT || 8080;


var publicPath = path.resolve(__dirname);
// Routing
app.use(express.static(publicPath));

server.listen(port, function () {
    console.log('Server listening at port %d', port);
    fs.writeFile(__dirname + '/start.log', 'started on port: ' + port + ' in directory: ' + publicPath);
});

// Chatroom

var numUsers = 0;

io.on('connection', function (socket) {
    var addedUser = false;

    // when the client emits 'new message', this listens and executes
    socket.on('new message', function (data) {
        // we tell the client to execute 'new message'
        socket.broadcast.emit('new message', {
            username: socket.username,
            message: data
        });
    });

    // when the client emits 'add user', this listens and executes
    socket.on('add user', function (username) {
        if (addedUser) return;

        // we store the username in the socket session for this client
        socket.username = username;
        ++numUsers;
        addedUser = true;
        socket.emit('login', {
            numUsers: numUsers
        });
        // echo globally (all clients) that a person has connected
        socket.broadcast.emit('user joined', {
            username: socket.username,
            numUsers: numUsers
        });
    });

    // when the client emits 'typing', we broadcast it to others
    socket.on('typing', function () {
        socket.broadcast.emit('typing', {
            username: socket.username
        });
    });

    // when the client emits 'stop typing', we broadcast it to others
    socket.on('stop typing', function () {
        socket.broadcast.emit('stop typing', {
            username: socket.username
        });
    });

    // when the user disconnects.. perform this
    socket.on('disconnect', function () {
        if (addedUser) {
            --numUsers;
            killConversation(socket);

            // echo globally that this client has left
            socket.broadcast.emit('user left', {
                username: socket.username,
                numUsers: numUsers
            });
        }
    });


    socket.on('joinGame', function (){
        console.log(socket.username + " wants to join a game");

        var alreadyInGame = false;

        for(var i = 0; i < chatCollection.totalChatCount; i++){
            var plyr1Tmp = chatCollection.chatList[i]['gameObject']['userOne'];
            var plyr2Tmp = chatCollection.chatList[i]['gameObject']['userTwo'];
            if (plyr1Tmp == socket.username || plyr2Tmp == socket.username){
                alreadyInGame = true;
                console.log(socket.username + " already has a Game!");

                socket.emit('alreadyJoined', {
                    chatId: chatCollection.chatList[i]['gameObject']['id']
                });

            }

        }
        if (alreadyInGame == false){


            gameSeeker(socket);

        }

    });


    socket.on('leaveGame', function() {


        if (chatCollection.totalChatCount == 0){
            socket.emit('notInGame');

        }

        else {
            killConversation(socket);
        }

    });

});
