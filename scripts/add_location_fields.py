import pymysql
import sys
import json

con = sys.argv[1]

param = json.loads(con)
conn = pymysql.connect(
    host=param['host'],
    user=param['user'],
    passwd=param['password'],
    db=param['database']
)

cursor = conn.cursor(pymysql.cursors.DictCursor)


def main():
    alter_table()
    cursor.execute('SELECT * FROM locations')
    result = cursor.fetchall()
    for data in result:
        locations = data['json_location']
        ids = json.loads(locations)
        try:
            cursor.execute('UPDATE locations SET nation_id ="' + str(ids['national']) + '", region_id ="' + str(ids['region']) +
                           '", county_id = "' + str(ids['county']) + '", subcounty_id = "' + str(ids['subcounty']) +
                           '", facility_id = "' + str(ids['facility']) + '" WHERE id=' + str(data['id']) + '')
            conn.commit()
        except:
            print(cursor._last_executed)
            raise


def alter_table():
    sql = ("""
    ALTER TABLE locations 
    ADD COLUMN nation_id INT(10) NULL AFTER json_location,
    ADD COLUMN region_id INT(10) NULL AFTER nation_id,
    ADD COLUMN county_id INT(10) NULL AFTER region_id,
    ADD COLUMN subcounty_id INT(10) NULL AFTER county_id,
    ADD COLUMN facility_id INT(10) NULL AFTER subcounty_id;
    """)
    try:
        cursor.execute(sql)
        conn.commit()
    except:
        raise


if __name__ == "__main__":
    main()
