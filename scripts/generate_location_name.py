import pymysql
import sys
import json

con = sys.argv[1]

param = json.loads(con)
conn = pymysql.connect(
    host=param['host'],
    user=param['user'],
    passwd=param['password'],
    db=param['database']
)

cursor = conn.cursor(pymysql.cursors.DictCursor)


def main():
    cursor.execute('SELECT id FROM locations')
    result = cursor.fetchall()
    for data in result:
        name = location_name(data['id'])
        try:
            cursor.execute('UPDATE locations SET location_name="' + str(name) + '" WHERE id=' + str(data['id']) + '')
            conn.commit()
        except:
            print(cursor._last_executed)
            raise


def location_name(location_id):
    sql = (
        "SELECT nation.name AS nation, region_name AS region, county_name AS county, subcounty_name AS subcounty, facility_name AS facility "
        "FROM locations "
        "JOIN nation ON nation.id = JSON_UNQUOTE(JSON_EXTRACT(locations.json_location, '$.national')) "
        "LEFT JOIN regions ON regions.id = JSON_UNQUOTE(JSON_EXTRACT(locations.json_location, '$.region')) "
        "LEFT JOIN counties ON counties.id = JSON_UNQUOTE(JSON_EXTRACT(locations.json_location, '$.county')) "
        "LEFT JOIN subcounties ON subcounties.id = JSON_UNQUOTE(JSON_EXTRACT(locations.json_location, '$.subcounty')) "
        "LEFT JOIN facilities ON facilities.id = JSON_UNQUOTE(JSON_EXTRACT(locations.json_location, '$.facility')) "
        "WHERE locations.id = '%s'")

    cursor.execute(sql, location_id)
    result = cursor.fetchone()
    return filter_by_size(result)


def filter_by_size(data):
    for k, v in data.items():
        if v is None:
            del data[k]
    if len(data) == 1:
        return data['nation']
    elif len(data) == 2:
        return data['region']
    elif len(data) == 3:
        return data['county']
    elif len(data) == 4:
        return data['subcounty']
    elif len(data) == 5:
        return data['facility']


if __name__ == "__main__":
    main()
