import pymysql, sys, json


def main():
    con = sys.argv[1]
    location = sys.argv[2]
    location_id = sys.argv[3]

    obj = json.loads(con)

    conn = pymysql.connect(
        host=obj['host'],
        user=obj['user'],
        passwd=obj['password'],
        db=obj['database']
    )
    cursor = conn.cursor()
    cursor.execute("SELECT * FROM chanjo.migrations where location_name=%s and location_id = %s", (location, location_id))
    migrations = cursor.fetchall()
    if len(migrations) == 0:
        cursor.execute("SELECT * FROM nvip.tbl_transaction where station=%s", location)

        data = cursor.fetchall()
        for row in data:
            transaction = {
                "transaction_date": row[1],
                "transaction_voucher": row[2],
                "location_id": row[3],
                "level": row[4],
                "to_from": row[5],
                "user_id": row[6],
                "type": row[7],
                "status": row[8],
                "timestamp": row[10],
            }

            add_transaction = ("INSERT INTO chanjo.transactions "
                               "(transaction_date, transaction_voucher, location_id, level, to_from, user_id, type, status, timestamp) "
                               "VALUES (%(transaction_date)s, %(transaction_voucher)s, %(location_id)s, %(level)s, %(to_from)s, %(user_id)s, %(type)s, %(status)s, %(timestamp)s)")
            cursor.execute(add_transaction, transaction)
            transaction_id = cursor.lastrowid
            cursor.execute("SELECT * FROM nvip.tbl_transaction_items where transaction_id=%s", row[0])
            data_items = cursor.fetchall()

            for items in data_items:
                transaction_items = {
                    "vaccine_id": items[1],
                    "batch": items[2],
                    "expiry_date": items[3],
                    "vvm": items[4],
                    "transaction_quantity": items[5],
                    "max_quantity": items[6],
                    "min_quantity": items[7],
                    "current_quantity": items[8],
                    "comment": items[9],
                    "transaction_id": transaction_id,
                }

                add_items = ("INSERT INTO chanjo.transaction_items "
                             "(vaccine_id, batch, expiry_date, vvm, transaction_quantity, max_quantity, min_quantity, current_quantity, comment, transaction_id) "
                             "VALUES (%(vaccine_id)s, %(batch)s, %(expiry_date)s, %(vvm)s, %(transaction_quantity)s, %(max_quantity)s, %(min_quantity)s, %(current_quantity)s, %(comment)s, %(transaction_id)s)")
                cursor.execute(add_items, transaction_items)

        migration_data = {
            "location_id": location_id,
            "location_name": location
        }
        close_migrations = ("INSERT INTO chanjo.migrations "
                            "(location_id, location_name)"
                            "VALUES (%(location_id)s, %(location_name)s)")
        cursor.execute(close_migrations, migration_data)
        conn.commit()

    cursor.close()
    conn.close()
    return True


if __name__ == '__main__':
    main()
