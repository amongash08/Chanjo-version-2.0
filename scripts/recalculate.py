from warnings import filterwarnings
import pymysql, sys, json

filterwarnings('ignore', category=pymysql.Warning)

con = sys.argv[1]
user_id = sys.argv[2]
location_id = sys.argv[3]
vaccine_id = sys.argv[4]

param = json.loads(con)
conn = pymysql.connect(
    host=param['host'],
    user=param['user'],
    passwd=param['password'],
    db=param['database']
)

cursor = conn.cursor()


def main():
    cursor.execute("DROP TABLE IF EXISTS transactionTable%s" % user_id)
    temp_transactions_table = (
            " CREATE TEMPORARY TABLE transactionTable%s"
            " AS SELECT transaction_date, vaccine_id, batch, expiry_date, transaction_quantity,"
            " vvm, location_id, user_id, type, transactions.id"
            " FROM transactions INNER JOIN"
            " transaction_items ON transactions.id = transaction_items.transaction_id"
            " WHERE location_id = %s AND vaccine_id =%s" % (user_id, location_id, vaccine_id))

    cursor.execute(temp_transactions_table)
    cursor.execute("SELECT * FROM transactionTable%s" % user_id)
    query = cursor.fetchall()

    transactions = []
    for row in query:
        transaction_details = {
            "date": row[0],
            "vaccine_id": row[1],
            "batch": str(row[2]).upper(),
            "expiry_date": row[3],
            "balance": row[4],
            "vvm": row[5],
            "location_id": row[6],
            "user_id": row[7],
            "type": row[8],
            "transaction_id": row[9],
        }
        transactions.append(transaction_details)

    temp_batch_balance()
    temp_balance()
    another_temp_batch_balance()

    for transaction in transactions:
        vaccine_balance = get_temp_batch(transaction)
        balance = _check_new_balances(transaction, vaccine_balance)


        if balance:
            insert_temp_balance(balance)

    batch_result = set_new_batch_balance()

    if batch_result == 0:
        insert_new_batch_balance()

    balance_result = set_new_balance()
    if balance_result == 0:
        insert_new_balance()

    conn.commit()
    cursor.close()
    conn.close()
    return


def temp_batch_balance():
    cursor.execute("DROP TABLE IF EXISTS batchBalanceTable%s" % user_id)
    sql = (
            "CREATE TEMPORARY TABLE batchBalanceTable%s("
            "vaccine_id int(11) NOT NULL,"
            "batch varchar(50) NOT NULL,"
            "expiry_date date NOT NULL,"
            "vvm varchar(10) NOT NULL,"
            "balance int(11) NOT NULL,"
            "user_id varchar(30) NOT NULL,"
            "location_id varchar(50) NOT NULL,"
            "transaction_id int(11) NOT NULL ,"
            "date date NOT NULL,"
            "timestamp TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP  )" % user_id)
    cursor.execute(sql)


def temp_balance():
    cursor.execute("DROP TABLE IF EXISTS balanceTable%s" % user_id)
    sql = (
            "CREATE TEMPORARY TABLE balanceTable%s("
            "vaccine_id int(11) NOT NULL, "
            "balance int(11) NOT NULL,"
            "location_id varchar(50) NOT NULL,"
            "transaction_id int(11) NOT NULL ,"
            "timestamp TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP)" % user_id)
    cursor.execute(sql)


def another_temp_batch_balance():
    cursor.execute("DROP TABLE IF EXISTS batchBalance%s" % user_id)

    sql = (
            "CREATE TEMPORARY TABLE batchBalance%s("
            "vaccine_id int(11) NOT NULL,"
            "batch varchar(50) NOT NULL,"
            "expiry_date date NOT NULL,"
            "vvm varchar(10) NOT NULL,"
            "balance int(11) NOT NULL,"
            "user_id varchar(30) NOT NULL,"
            "location_id varchar(50) NOT NULL,"
            "transaction_id int(11) NOT NULL ,"
            "date date NOT NULL,"
            "timestamp TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP)" % user_id)

    cursor.execute(sql)


def get_temp_batch(transaction_details):
    data = {
        'user_id': int(user_id),
        'vaccine_id': int(transaction_details['vaccine_id']),
        'location_id': int(transaction_details['location_id']),
        'batch': transaction_details['batch']
    }

    sql = (
        " SELECT DISTINCT b.batch AS batch_number, balance, expiry_date FROM"
        " (SELECT MAX(transaction_id) AS transaction_id, vaccine_id, batch FROM"
        " batchBalanceTable%(user_id)s GROUP BY vaccine_id , location_id , batch) AS b"
        " INNER JOIN batchBalance%(user_id)s t ON t.transaction_id = b.transaction_id"
        " AND t.vaccine_id = b.vaccine_id AND t.batch = b.batch"
        " WHERE b.vaccine_id = %(vaccine_id)s AND location_id =%(location_id)s AND b.batch =%(batch)s"
        " GROUP BY b.batch, balance, expiry_date HAVING (balance) > 0 ORDER BY expiry_date ASC"
    )

    cursor.execute(sql, data)
    result = cursor.fetchall()
    return result


def insert_temp_balance(balance):
    insert_into_batch_balance_one(balance)
    insert_into_batch_balance_two(balance)

    totals = get_temp_balance(balance)

    for value in totals:
        if value[0] is not None:
            new_balance = int( value[0])
        else:
            new_balance = 0

        balance['balance'] = new_balance
        del balance['user_id']
        del balance['date']
        del balance['batch']
        del balance['vvm']
        del balance['expiry_date']
    insert_balance(balance)


def insert_new_batch_balance():
    sql = (
            "INSERT INTO batch_balances(vaccine_id, batch, expiry_date, vvm, balance, user_id, location_id, transaction_id, date, timestamp)"
            " SELECT vaccine_id, batch, expiry_date, vvm, balance, user_id, location_id, transaction_id, date, timestamp FROM batchBalanceTable" + user_id)
    cursor.execute(sql)


def set_new_balance():
    sql = ("UPDATE balances dest, balanceTable" + user_id + " src SET"
                                                            " dest.balance = src.balance,"
                                                            " dest.timestamp = src.timestamp"
                                                            " WHERE dest.transaction_id = src.transaction_id AND dest.vaccine_id = src.vaccine_id"
           )
    cursor.execute(sql)
    return cursor.rowcount


def insert_new_balance():
    sql = ("INSERT INTO balances(vaccine_id, balance, location_id, transaction_id, timestamp)"
           "SELECT vaccine_id, balance, location_id, transaction_id, timestamp FROM balanceTable" + user_id)
    cursor.execute(sql)


def set_new_batch_balance():
    sql = ("UPDATE batch_balances dest, batchBalanceTable" + user_id + " src SET"
                                                                       " dest.batch = src.batch,"
                                                                       " dest.balance = src.balance,"
                                                                       " dest.timestamp = src.timestamp"
                                                                       " WHERE dest.transaction_id = src.transaction_id AND dest.vaccine_id = src.vaccine_id AND dest.batch = src.batch"
           )
    cursor.execute(sql)
    return cursor.rowcount


def insert_balance(balance):
    sql = (
            " INSERT INTO balanceTable" + user_id + ""
                                                    " (vaccine_id, balance, location_id, transaction_id)"
                                                    " VALUES (%(vaccine_id)s, %(balance)s, %(location_id)s, %(transaction_id)s)"
    )

    cursor.execute(sql, balance)


def insert_into_batch_balance_one(balance):
    sql = ("INSERT INTO batchBalanceTable""" + user_id + ""
                                                         " (vaccine_id, batch, expiry_date, vvm, balance, user_id, location_id, transaction_id, date) "
                                                         " VALUES(%(vaccine_id)s, %(batch)s, %(expiry_date)s, %(vvm)s, %(balance)s, %(user_id)s, %(location_id)s, %(transaction_id)s, %(date)s)")
    cursor.execute(sql, balance)


def insert_into_batch_balance_two(balance):
    sql = ("""INSERT INTO batchBalance""" + user_id + """
            (vaccine_id, batch, expiry_date, vvm, balance, user_id, location_id, transaction_id, date)
            VALUES(%(vaccine_id)s, %(batch)s, %(expiry_date)s, %(vvm)s, %(balance)s, %(user_id)s, %(location_id)s, %(transaction_id)s, %(date)s)""")
    cursor.execute(sql, balance)


def get_temp_balance(balance):
    data = {
        "user_id": int(user_id),
        "vaccine_id": balance['vaccine_id'],
        "location_id": balance['location_id']
    }
    sql = (
        " SELECT DISTINCT SUM(balance) AS stock_balance"
        " FROM (SELECT MAX(transaction_id) AS transaction_id, vaccine_id, batch"
        " FROM batchBalanceTable%(user_id)s"
        " GROUP BY vaccine_id , location_id , batch) AS b"
        " INNER JOIN batchBalance%(user_id)s t ON t.transaction_id = b.transaction_id"
        " AND t.vaccine_id = b.vaccine_id AND t.batch = b.batch"
        " WHERE b.vaccine_id =%(vaccine_id)s AND location_id=%(location_id)s AND balance >= 0"
    )

    cursor.execute(sql, data)
    return cursor.fetchall()


def _check_new_balances(transaction_details, vaccine_balance):
    if len(vaccine_balance) > 0 and isinstance(vaccine_balance, tuple):
        for batch in vaccine_balance:
            if str(batch[0]) == str(transaction_details['batch']):
                if float(transaction_details['type']) == 1.0:
                    balance = int(batch[1]) + int(transaction_details['balance'])
                    transaction_details['balance'] = balance
                elif float(transaction_details['type']) == 2.0:
                    balance = int(batch[1]) - int(transaction_details['balance'])
                    # balance = (0 if balance <= 0 else balance)
                    transaction_details['balance'] = balance
                elif float(transaction_details['type']) == 3.0:
                    balance = 0 + int(transaction_details['balance'])
                    transaction_details['balance'] = balance
                elif float(transaction_details['type']) == 4.0:
                    balance = int(batch[1]) + int(transaction_details['balance'])
                    transaction_details['balance'] = balance
                elif float(transaction_details['type']) > 4.0:
                    balance = int(batch[1]) - int(transaction_details['balance'])
                    transaction_details['balance'] = balance

            return transaction_details
    else:
        if int(transaction_details['type']) == 1:
            balance = 0 + transaction_details['balance']
            transaction_details['balance'] = int(balance)
            return transaction_details


if __name__ == "__main__":
    main()
