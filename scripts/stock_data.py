import pymysql
import sys
import json

con = sys.argv[1]
location_id = sys.argv[2]
vaccine_id = sys.argv[3]

param = json.loads(con)
conn = pymysql.connect(
    host=param['host'],
    user=param['user'],
    passwd=param['password'],
    db=param['database']
)

cursor = conn.cursor(pymysql.cursors.DictCursor)


def get_new_array():
    where = {
        'location_id': location_id,
        'vaccine_id': vaccine_id
    }
    sql = ("SELECT DISTINCT "
           "transactions.id AS id, "
           "transactions.request_id AS request_id, "
           "transactions.timestamp AS timestamp, "
           "transactions.transaction_date AS date, "
           "transactions.to_from AS to_from, "
           "transactions.location_id AS location_id, "
           "transaction_type.type AS type, "
           "transaction_items.id AS row_id, "
           "transaction_items.vaccine_id AS vaccine_id, "
           "transaction_items.batch AS batch, "
           "transaction_items.expiry_date AS expiry, "
           "transaction_items.transaction_quantity AS quantity "
           "FROM transactions "
           "RIGHT JOIN transaction_items ON transaction_items.transaction_id = transactions.id "
           "INNER JOIN vaccines ON vaccines.id = transaction_items.vaccine_id "
           "INNER JOIN transaction_type ON transaction_type.id = transactions.type "
           "LEFT JOIN balances ON balances.transaction_id = transaction_items.transaction_id "
           "AND balances.vaccine_id = transaction_items.vaccine_id "
           "WHERE transactions.location_id = %(location_id)s AND transaction_items.vaccine_id = %(vaccine_id)s "
           "ORDER BY transactions.transaction_date DESC,transactions.id DESC")
    cursor.execute(sql, where)
    result = cursor.fetchall()
    new_array = []
    total = []
    for data in result:
        where = {
            'vaccine_id': data['vaccine_id'],
            'transaction_id': data['id'],
            'location_id': data['location_id']
        }
        row = {
            'DT_RowId': 'row_' + str(data['id']),
            'id': data['id'],
            'reference_id': str(data['id']) + '_' + str(data['request_id']),
            'date': data['date'].strftime('%d %b %Y') if data['date'] != '0000-00-00' else data['date'],
            'to_from': get_location_name(data['to_from']),
            'station': get_location_name(data['location_id']),
            'type': data['type'],
            'vaccine_id': data['vaccine_id'],
            'batch': data['batch'],
            'expiry': str(data['expiry']),
            'quantity': data['quantity'],
            'balance': balance(where),
            'row_id': data['row_id'],
        }

        new_array.append(row)
    negative_adjustment = get_negative_adjustment()

    running_sum = 0
    for data in reversed(new_array):
        batch = data['batch']
        trans_id = data['id']
        quantity = data['quantity']
        where['transaction_id'] = trans_id
        where['batch'] = batch

        if data['type'] == 'Receive':
            running_sum = quantity + running_sum
        elif data['type'] == 'Issue':

            batch_bal = previous_batch_balance(where)
            if batch_bal is not None:
                result = batch_bal['balance'] - quantity
                if result < 0:
                    quantity = (quantity + result) * -1
                else:
                    quantity = data['quantity'] * -1
            else:
                quantity = data['quantity']
            running_sum = quantity + running_sum
        elif data['type'] == 'Physical count':
            batch_bal = batch_balance(where)
            bal = balance(where)
            previous_bal = batch_bal['balance'] if isinstance(batch_bal, dict) else 0
            current_bal = quantity - previous_bal

            if current_bal > 0:
                running_sum = running_sum - current_bal
            elif current_bal < 0:
                running_sum = running_sum - current_bal
            elif current_bal == 0:
                if isinstance(bal, dict) and bal is not None:
                    running_sum = bal['balance']
        elif data['type'] == 'Positive Adjustment':
            running_sum = quantity + running_sum
        elif data['type'] in [v['type'] for v in negative_adjustment]:
            quantity = data['quantity'] * -1
            running_sum = quantity + running_sum
        else:
            running_sum = quantity + running_sum

        if running_sum < 0:
            running_sum = 0

        total.append(running_sum)

    new_array.reverse()
    for i in range(len(total)):
        if new_array[i]['balance'] != total[i] or total[i] == 0:
            new_array[i]['balance'] = total[i]

    new_array.reverse()
    output = {'data': new_array}
    print json.dumps(output)


def get_location_name(location):
    sql = ("SELECT location_name FROM locations where id = '%s'" % location)
    cursor.execute(sql)
    result = cursor.fetchone()
    if result is not None:
        return result['location_name']


def get_negative_adjustment():
    sql = "SELECT type FROM transaction_type WHERE id > 4"
    cursor.execute(sql)
    return cursor.fetchall()


def balance(data):
    sql = (
        "SELECT vaccine_id, balance FROM balances "
        "WHERE location_id = %(location_id)s AND "
        "vaccine_id = %(vaccine_id)s AND "
        "transaction_id = %(transaction_id)s "
        "ORDER BY transaction_id DESC LIMIT 1"
    )
    cursor.execute(sql, data)
    return cursor.fetchone()


def batch_balance(data):
    sql = (
        "SELECT vaccine_id, batch, balance FROM batch_balances "
        "WHERE location_id = %(location_id)s AND "
        "vaccine_id = %(vaccine_id)s AND "
        "batch = %(batch)s AND "
        "transaction_id = %(transaction_id)s "
        "ORDER BY transaction_id DESC LIMIT 1"
    )
    cursor.execute(sql, data)
    return cursor.fetchone()


def previous_batch_balance(data):
    sql = (
        "SELECT vaccine_id, batch, balance FROM batch_balances "
        "WHERE location_id = %(location_id)s AND "
        "vaccine_id = %(vaccine_id)s AND "
        "batch = %(batch)s AND "
        "transaction_id <  %(transaction_id)s "
        "ORDER BY transaction_id DESC LIMIT 1"
    )
    cursor.execute(sql, data)
    return cursor.fetchone()


def main():
    get_new_array()


if __name__ == "__main__":
    main()
